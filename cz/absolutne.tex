
\chapter{Absolutně spojité náhodné veličiny}

Diskrétní a spojité náhodné veličiny tvoří jen dvě malé disjunktní
třídy pravděpodobnostních distribucí. Jednodušeji se s nimi
pracuje, pokud neznáme teorii míry a Lebesgueův integrál. Musíme
ale poznamenat, že existuje mnoho distribucí, které nejsou ani
spojité ani diskrétní.

\begin{defin}
Řekneme, že náhodná veličina $X$ má absolutně spojité rozdělení
(ASR), jestliže existuje nezáporná reálná funkce $f_X(x)$ taková,
že pro každé $x\in\RR$ platí
$$
F_X(x) = \int_{-\infty}^x f_X(t)\, dt\, .
$$
Funkci $f_X(x)$ nazýváme (pravděpodobnostní) hustota náhodné
veličiny $X$.
\end{defin}

\begin{example}
Mějme náhodnou veličinu $X$ zadanou pomocí své distribuční funkce
$$
F_X(x)=
  \begin{cases}
    0 & \text{pro } x\leq 0 , \\
    \frac{x}{2} & \text{pro } 0<x\leq 2,\\
    1 &\text{pro } x\geq 2.
  \end{cases}
$$
Tato náhodná veličina $X$ má absolutně spojité rozdělení, neboť
když vezmeme funkci
$$
f_X(x) =
  \begin{cases}
    0 & \text{pro } x\leq 0 \,\vee\, x\geq 2, \\
    \frac{1}{2} & \text{pro } 0<x<2,
  \end{cases}
  \qmq{platí}
  F_X(x) = \int_{-\infty}^x f_X(t)\, dt\, .
$$
{\em\large Obr.}

\end{example}
\begin{theorem}
Nechť má náhodná veličina $X$ absolutně spojité rozdělení. Potom
ve všech bodech, kde existuje derivace distribuční funkce $F_X$
platí
$$
f_X(x) = F_X^\prime (x) = \frac{dF_X}{dx}(x)\, .
$$
\end{theorem}
\begin{proof}
Důkaz plyne z vlastností integrálu a derivace.
\end{proof}
\begin{theorem}
Nechť má náhodná veličina $X$ absolutně spojité rozdělení. Potom
pro každou množinu $A\in \B(\RR)$ platí
$$
\P[X\in A] = \int_A f_X(x)\, dx\, .
$$
\end{theorem}
{\em\large Obr.}
\begin{proof}
Tato věta se dokazuje pomocí vlastností $\sigma$-algeber, naznačme
důkaz alespoň pro interval.
$$
\P[a<X\leq b] = F_X(b)-F_X(a) = \int_{-\infty}^b f_X(x)\, dx -
\int_{-\infty}^a f_X(x)\, dx = \int_{a}^b f_X(x)\, dx \, .
$$
\end{proof}
\begin{remark}
Tato vlastnost je analogická vlastnosti diskrétních veličin, pro
které platí
$$
\P[X\in A] = \sum_{j:x_j\in A} \P[X=x_j]\, .
$$
Pravděpodobnostní funkce tedy odpovídá ve spojitém případě hustotě
pravděpodobnosti.
\end{remark}
\begin{remark}
Má-li náhodná veličina $X$ ASR, platí
$$
\P[X=a] = 0, \qquad \forall a \in\RR\, ,
$$
což vyplývá např. z toho, že $\int_a^a f_X(x)\, dx =0$ nebo
$\P[X=a] = F_X(a)-\lim_{x\to a_-} F_X(x) = 0$, protože $F_X$ je
spojitá. To tedy znamená, že
$$
\P[X<a] = \P[X\leq a]
$$
a
$$
\P[a<X<b]= \P[a\leq X<b]=\P[a<X\leq b] = \P[a\leq X \leq b]\, .
$$
\end{remark}

\begin{remark}
Dvě základní vlastnosti každé hustoty pravděpodobnosti jsou
\begin{itemize}
  \item[a)] $$f_X(x) \geq 0 \qmq{pro všechna} x\in\RR$$
  \item[b)] $$ \int_{-\infty}^\infty f_X(x)\, dx =
  \lim_{x\to\infty}F_X(x) = 1\, .$$
\end{itemize}
\end{remark}
\begin{example}
Náhodná veličina $X$ má hustotu
$$
f_X(x) = \frac{a}{1+x^2}\, \quad x\in\RR\, .
$$
Určete konstantu $a$, distribuční funkci $F_X$ a $\P[-1<X<1]$.

Protože
$$
 \int_{-\infty}^\infty \frac{a}{1+x^2}\, dx =
 a[\mbox{arctg}(x)]_{-\infty}^\infty= \pi a\, ,
$$
konstanta $a=1/\pi$. Dále pro všechna $x\in\RR$
$$
F_X(x) =  \int_{-\infty}^x \frac{1}{\pi}\frac{1}{1+t^2}\, dt =
\frac{1}{\pi} [\mbox{arctg}(t)]_{-\infty}^x = \frac{1}{\pi}
\left[\mbox{arctg}(x) +\frac{\pi}{2}\right]
$$
a
$$
\P[-1<X<1] = \int_{-1}^1 \frac{1}{\pi}\frac{1}{1+x^2}\, dx =
F_X(1) -F_X(-1) = \frac{1}{2}\, .
$$
\end{example}


\begin{exercise}
Z bodu $A$ do bodu $B$ přenášíme signál složený z "+1" a "-1".
Signál je ale zašuměn, tj. příjemce dostává signál plus šum. Víme,
že šum je náhodná veličina s hustotou pravděpodobnosti
$$
f_X(x)=
  \begin{cases}
    \frac{1}{2}\, e^{-x} & \text{pro } x>0, \\
    \frac{1}{2}\, e^{x} & \text{pro } x\leq 0.
  \end{cases}
$$
Signál $S$ je v místě $B$ detekován následovně. Pokud $S>d$
detekujeme symbol "+1", je-li $S\leq d$ detekujeme symbol "-1".
Stanovte optimální rozhodovací mez $d$ a pravděpodobnost chyby,
jestliže víme, že průměrně posíláme 60\% "+1" a 40\% "-1".
\end{exercise}
\begin{exercise}
Náhodná veličina $X$ má hustotu
$$
f_X(x) = ax^2e^{-x}\, \quad x\geq 0 .
$$
Určete konstantu $a$, distribuční funkci $F_X$ a $\P[0<X<1]$.
[$a=1/2$, ,$1-5/(2e)$]
\end{exercise}

\begin{exercise}
Distribuční funkce zdaněných ročních příjmů je
$$
F_X(x) =
  \begin{cases}
    1-\left(\fra{c}{x}\right)^\alpha & \text{pro } x\geq c, \\
    0 & \text{jinak},
  \end{cases}
$$
kde $\alpha>0$. Určete hodnotu ročního příjmu, kterou náhodně
zvolený daňový poplatník překročí s pravděpodobností 0.5. [$c\cdot
2^{1/\alpha}$]
\end{exercise}

\section{Absolutně spojité náhodné vektory}

\begin{defin}
Říkáme, že náhodné veličiny $X_1,X_2,\ldots,X_n$ mají sdružené
absolutně spojité rozdělení (SASR), jestliže existuje nezáporná
funkce $f_{X_1,\ldots,X_n}(x_1,\ldots,x_n)$ taková, že
$$
F_{X_1,\ldots,X_n}(x_1,\ldots,x_n)=\int_{-\infty}^{x_1}
\int_{-\infty}^{x_2}\ldots \int_{-\infty}^{x_n}
f_{X_1,\ldots,X_n}(t_1,\ldots,t_n)\, dt_1\ldots dt_n
$$
pro všechny $n$-tice $(x_1,\ldots,x_n)\in\RR^n$. Funkci
$f_{X_1,\ldots,X_n}$ nazýváme sdružená hustota pravděpodobnosti
náhodných veličin $X_1,X_2,\ldots,X_n$.
\end{defin}
\begin{remark}
Podobně jako u jedné náhodné veličiny platí
\begin{description}
  \item[a)] $$f_{X_1,\ldots,X_n}(x_1,\ldots,x_n)=\fra{\partial^n
  F_{X_1,\ldots,X_n}(x_1,\ldots,x_n)}{\partial x_1\ldots \partial
  x_n}\, ,$$ tam kde derivace existuje.
  \item[b)] $$\P[a_1<X_1\leq b_1,a_2<X_2\leq b_2,\ldots , a_n<X_n\leq
  b_n]=$$ $$ \int_{a_1}^{b_1}
\int_{a_2}^{b_2}\ldots \int_{a_n}^{b_n}
f_{X_1,\ldots,X_n}(x_1,\ldots,x_n)\, dx_1\ldots dx_n\, .$$
 \end{description}
\end{remark}
\begin{theorem}
Mají-li náhodné veličiny $X_1,X_2,X_3$ SASR, potom také $X_1,X_3$
mají SASR a platí
$$
f_{X_1,X_3}(x_1,x_3) = \int_{-\infty}^\infty
f_{X_1,X_2,X_3}(x_1,x_2,x_3)\, dx_2\, .
$$
\end{theorem}
\begin{remark}
Tvrzení je opět analogické diskrétnímu případu, kde platí
$$
\P[X_1=x_1]=\sum_{x_2}\P[X_1=x_1,X_2=x_2]\, .
$$
\end{remark}
\begin{proof}
Použitím Fubiniho věty a záměny integrálu a limity ve třetím kroku
(všechny předpoklady jsou v našem případě splněny) dostaneme pro
všechna $(x_1,x_3)\in\RR^2$
\begin{eqnarray*}
F_{X_1,X_3}(x_1,x_3)& = &\lim_{x_2\to\infty}
F_{X_1,X_2,X_3}(x_1,x_2,x_3)\\
&=& \lim_{x_2\to\infty}
\int_{-\infty}^{x_1}\int_{-\infty}^{x_2}\int_{-\infty}^{x_3}f_{X_1,X_2,X_3}(t_1,t_2,t_3)
\, dt_1dt_2dt_3\\
&=& \int_{-\infty}^{x_1}\int_{-\infty}^{x_3} \left(
\lim_{x_2\to\infty}\int_{-\infty}^{x_2}f_{X_1,X_2,X_3}(t_1,t_2,t_3)
\, dt_2\right) dt_1dt_3\\
&=& \int_{-\infty}^{x_1}\int_{-\infty}^{x_3} \left(
\int_{-\infty}^{\infty}f_{X_1,X_2,X_3}(t_1,t_2,t_3) \, dt_2\right)
dt_1dt_3\, ,
\end{eqnarray*}
z čehož pomocí definice SASR plyne tvrzení věty.
\end{proof}
\begin{example}
Náhodné veličiny $X,Y$ jsou zadány pomocí sdružené hustoty
pravděpodobnosti
$$
f_{X,Y}(x,y) =
  \begin{cases}
    e^{-(x+y)} & \text{pro } \min\{x,y\}\geq 0, \\
   0 & \text{jinak}.
  \end{cases}
$$
Určete marginální hustotu $f_Y(y)$ a $\P[X>2Y]$.

Podle předchozí věty
$$
f_Y(y) = \int_{-\infty}^\infty f_{X,Y}(x,y)\, dx =
  \begin{cases}
    \int_0^\infty e^{-(x+y)}\, dx = e^{-y} & \text{pro }y\geq 0, \\
    0 & \text{pro } y<0.
  \end{cases}
$$
Dále
$$ \P[X>2Y]= \int\!\!\!\int_{x>2y} f_{X,Y}(x,y)\, dx dy =
\int_{0}^\infty \left(\int_{2y}^\infty e^{-(x+y)}\, dx\right)dy =
\int_0^\infty e^{-3y}\,dy = \frac{1}{3}\, .
$$
\end{example}

\begin{example}
Náhodné veličiny $X,Y$ jsou zadány pomocí sdružené hustoty
pravděpodobnosti
$$
f_{X,Y}(x,y) =
  \begin{cases}
    \frac{2}{a^2} & \text{pro } 0\leq x<y<a, \\
   0 & \text{jinak}.
  \end{cases}
$$
Určete marginální hustoty $f_X(x)$ a $f_Y(y)$.

$$
f_X(x) = \int_{-\infty}^\infty f_{X,Y}(x,y)\, dy =
  \begin{cases}
    \int_x^a \frac{2}{a^2}\,dy=\frac{2}{a^2}\, (a-x) & \text{pro } 0\leq x< a, \\
    0 & \text{jinak},
  \end{cases}
$$
$$
f_Y(y) = \int_{-\infty}^\infty f_{X,Y}(x,y)\, dx =
  \begin{cases}
    \int_0^y \frac{2}{a^2}\,dx=\frac{2}{a^2}\, y & \text{pro } 0\leq y< a, \\
    0 & \text{jinak}.
  \end{cases}
$$
\end{example}

\begin{theorem}
Nechť náhodné veličiny $X_1,\ldots,X_n$ mají SASR. Potom
$X_1,\ldots,X_n$ jsou nezávislé právě tehdy, když
$$
f_{X_1,\ldots,X_n}(x_1,\ldots, x_n) = \prod_{j=1}^n
f_{X_j}(x_j)\qmq{pro všechny} (x_1,\ldots,x_n)\in\RR^n\, .
$$
\end{theorem}
\begin{proof}
Ukažme, že tvrzení platí pro $n=2$, tedy
$$
X,Y \qmq{jsou nezávislé} \Leftrightarrow \quad f_{X,Y}(x,y) =
f_X(x)f_Y(y),\ \forall (x,y)\in\RR^2\, .
$$
$\Leftarrow:$ Pro všechna $(x,y)\in\RR^2$ platí
\begin{eqnarray*}
F_{X,Y}(x,y) &=& \int_{-\infty}^x\int_{-\infty}^y f_{X,Y}(t,s)\,
dt ds = \int_{-\infty}^x\int_{-\infty}^y f_{X}(t)f_Y(s)\, dt ds\\
&=& \left( \int_{-\infty}^x f_X(t)\,dt\right)\left(
\int_{-\infty}^y f_Y(s)\,ds\right) = F_X(x) F_Y(y),
\end{eqnarray*}
z čehož podle Věty \ref{nez} vyplývá nezávislost náhodných veličin
$X,Y$.\\
$\Rightarrow:$ Opačným postupem dostaneme pro všechna
$(x,y)\in\RR^2$
$$
F_{X,Y}(x,y)= F_X(x) F_Y(y) = \int_{-\infty}^x\int_{-\infty}^y
f_{X}(t)f_Y(s)\, dt ds,
$$
a podle definice SASR tedy
$$
f_{X,Y}(x,y) = f_X(x)f_Y(y),\ \forall (x,y)\in\RR^2\, .
$$
\end{proof}
\begin{example}
Náhodné veličiny $X_1,X_2,X_3$ jsou zadány sdruženou hustotou
$$
f_{X_1,X_2,X_3}(x_1,x_2,x_3) =
  \begin{cases}
    2x_1(x_2+x_3) & \text{pro } 0<x_j<1, j\in\wh 3, \\
    0 & \text{jinak}.
  \end{cases}
$$
Rozhodněte, zda jsou nezávislé.

Spočtěme marginální hustoty pravděpodobnosti jednotlivých veličin.
Pro $0<x_1<1$ platí
$$
f_{X_1}(x_1) = \int_0^1\int_0^1 2x_1(x_2+x_3)\,dx_2dx_3 = 2x_1
\int_0^1 \frac{1}{2}+x_3\, dx_3 = 2x_1\, .
$$
Pro $0<x_2<1$ platí
$$
f_{X_2}(x_2) = \int_0^1\int_0^1 2x_1(x_2+x_3)\,dx_1dx_3 =
\left(\int_0^1 2x_1 \, dx_1\right) \int_0^1(x_2+x_3)\,dx_3 =
x_2+\frac{1}{2}
$$
a symetricky, pro $0<x_3<1$
$$
f_{X_3}(x_3) = x_3+\frac{1}{2}\, .
$$
Celkem tedy
$$
f_{X_1,X_2,X_3}\neq f_{X_1}f_{X_2}f_{X_3}
$$
a veličiny $X_1,X_2,X_3$ nejsou nezávislé.
\end{example}
\begin{defin}
Nechť $X,Y$ jsou náhodné veličiny. Potom podmíněnou distribuční
funkcí náhodné veličiny $X$ při dané hodnotě $Y=y$ definujeme jako
$$
F_{X|Y}(x|y) = \lim_{\ve\to 0_+} \P[X\leq x \,|\, y-\ve<Y\leq
y+\ve]
$$
za předpokladu, že limita existuje. Pokud navíc existuje funkce
$f_{X|Y}(x|y)\geq 0$ taková, že
$$
F_{X|Y}(x|y) = \int_{-\infty}^x f_{X|Y}(t|y) \,dt\qquad \forall
x\in\RR\, ,
$$
nazýváme ji podmíněnou hustotou náhodné veličiny $X$ za podmínky
$Y=y$.
\end{defin}
\begin{remark}
Definice podmíněného rozdělení je pro spojité náhodné veličiny
komplikovanější než pro diskrétní. Vztah
$$
\P[X\leq x|Y=y]= \frac{\P[X\leq x,Y=y]}{\P[Y=y]}
$$
totiž nelze použít, neboť pro spojitou náhodnou veličinu platí
$\P[Y=y]=0$ pro všechna $y\in\RR$.
\end{remark}
\begin{theorem}
Nechť náhodné veličiny $X,Y$ mají SASR. Potom v každém bodě
$(x,y)\in\RR^2$, kde je
\begin{description}
  \item[a)] $f_{X,Y}(x,y)$ spojitá,
  \item[b)] $f_Y(y)$ spojitá a $f_Y(y)>0$,
\end{description}
existuje podmíněná hustota náhodné veličiny $X$ za podmínky $Y=y$
a platí
$$
f_{X|Y}(x|y) = \fra{f_{X,Y}(x,y)}{f_Y(y)}\, .
$$
\end{theorem}
\begin{example}
Náhodné veličiny $X,Y$ mají pro $a>2$ sdruženou hustotu
$$
f_{X,Y}(x,y) =
  \begin{cases}
    c(1+x+y)^{-a} & \text{pro }x>0,y>0, \\
    0 & \text{jinak}.
  \end{cases}
$$
Stanovte konstantu $c$, marginální hustotu $f_X$ a podmíněnou
hustotu $f_{Y|X}$.

\begin{eqnarray*}
1 &=& \int_0^{\infty}\int_0^{\infty} c(1+x+y)^{-a}\, dx dy = c
\int_0^{\infty} \left[\fra{(1+x+y)^{-a+1}}{-a+1} \right]_0^\infty
\, dy\\
&=& \frac{c}{1-a} \int_{0}^\infty (1+y)^{-a+1}\, dy =
\frac{c}{(1-a)(2-a)}
\end{eqnarray*}
a konstanta $c=(1-a)(2-a)$. Pro $x>0$ platí
$$
f_X(x)=\int_0^\infty  c(1+x+y)^{-a}\, dy = \frac{c}{1-a}\left[
(1+x+y)^{-a+1}\right]_0^\infty = (a-2)(1+x)^{-a+1}
$$
a podmíněná hustota má pro $x>0, y>0$ tvar
$$
f_{Y|X}(y|x) = (a-1)(1+x)^{a-1}(1+x+y)^{-a}\, .
$$
\end{example}



\section{Funkce náhodných veličin}
V řadě problémů nás zajímá rozdělení pravděpodobnosti funkce
$Y=h(X)$, pokud známe rozdělení náhodné veličiny $X$.
\begin{example}
$X$ je diskrétní náhodná veličina zadaná tabulkou
\begin{center}
\begin{tabular}{c|ccccc}
    $x_i$ & 0 & $\frac{\pi}{4}$ & $\frac{\pi}{2}$ & $\frac{3\pi}{4}$ & $\pi$ \\ \hline
  $p_i$ & 0.1 & 0.3 & 0.2 & 0.1 & 0.3 \\
\end{tabular}
\end{center}
Určete rozdělení náhodné veličiny $Y=\sin X$.

Pokud do tabulky doplníme hodnoty $\sin x_i$
\begin{center}
\begin{tabular}{c|ccccc}
    $x_i$ & 0 & $\frac{\pi}{4}$ & $\frac{\pi}{2}$ & $\frac{3\pi}{4}$ & $\pi$ \\ \hline
 $\sin x_i$ & 0 & $\frac{\sqrt{2}}{2}$ & 1 & $\frac{\sqrt{2}}{2}$
 & 0 \\
  $p_i$ & 0.1 & 0.3 & 0.2 & 0.1 & 0.3 \\
\end{tabular}
\end{center}
vidíme, že náhodná veličina $Y$ má rozdělení reprezentované
tabulkou
\begin{center}
\begin{tabular}{c|ccc}
    $y_i$ & 0 & $\frac{\sqrt{2}}{2}$ & 1  \\ \hline
   $p_i$ & 0.4 & 0.4 & 0.2  \\
\end{tabular}
\end{center}
neboť například
$$
\P\left[Y=\frac{\sqrt{2}}{2}\right] =
\P\left(\left[X=\frac{\pi}{4}\right]\cup
\left[X=\frac{3\pi}{4}\right] \right) =
\P\left[X=\frac{\pi}{4}\right]+ \P\left[X=\frac{3\pi}{4}\right] =
0.4\, .
$$
\end{example}
\begin{remark}
Po transformaci se může rozdělení náhodné veličiny hodně změnit,
dokonce ze spojité se může stát diskrétní (ne naopak). Protože pro
diskrétní náhodné veličiny se dá nové rozdělení získat přímým
výpočtem, omezíme se dále pouze na spojité náhodné veličiny.
Ukážeme dvě metody:
\begin{itemize}
  \item výpočet distribuční funkce transformované náhodné veličiny
  \item vzorec pro přímý výpočet hustoty transformované náhodné
  veličiny.
\end{itemize}
Pokud je například funkce $h$ ostře rostoucí a existuje tedy
inverzní funkce $h^{-1}$, můžeme psát
\begin{eqnarray*}
F_Y(y) &=&  \P[Y\leq y] = \P[h(X)\leq y ] = \P[X\in\{ t\in \RR\,|\, h(t)\leq y\}]\\
& =&  \P[X\leq h^{-1}(y)]= F_X(h^{-1}(y))\, .
\end{eqnarray*}
Poslední výraz už umíme určit, protože známe rozdělení náhodné
veličiny $X$.
\end{remark}

\begin{example}
Náhodná veličina $X$ je dána hustotou pravděpodobnosti
$$
f_X(x)=
  \begin{cases}
    \frac{1}{5} & \text{pro } x\in\langle -2,3\rangle, \\
    0 & \text{jinak}.
  \end{cases}
$$
Najděte hustotu pravděpodobnosti náhodné veličiny $Y=X^2$.

Vyřešme problém nejdříve pro obecnou náhodnou veličinu $X$.
\begin{eqnarray*}
F_Y(y) &=& \P[Y\leq y] = \P[X^2\leq y] \\
&=&
  \begin{cases}
    0 & \text{pro }y<0, \\
    \P[-\sqrt{y}\leq X\leq \sqrt{y}]= \int_{-\sqrt{y}}^{\sqrt{y}}f_X(x)\, dx = F_X(\sqrt{y})-F_X(-\sqrt{y}) & \text{pro }y\geq 0
  \end{cases}
\end{eqnarray*}
a hustota pravděpodobnosti náhodné veličiny $Y$ má tedy tvar
$$
f_Y(y) = (F_Y(y))^\prime = \frac{1}{2\sqrt{y}}\, f_X(\sqrt{y})
+\frac{1}{2\sqrt{y}}\, f_X(-\sqrt{y}),
$$
pro $y>0$. V našem konkrétním případě vypadá situace následovně
$$
F_Y(y)=\left\{
\begin{array}{lllll}
0 & & y<0 & & 0 \\
\int_{-\sqrt{y}}^{\sqrt{y}}\frac{1}{5}\, dx =
\frac{2}{5}\,\sqrt{y} & & y\in\langle 0,4) & &
\frac{1}{5\sqrt{y}}\\
\int_{-2}^{\sqrt{y}}\frac{1}{5}\, dx = \frac{1}{5}\,(\sqrt{y}+2) &
& y\in\langle 4,9\rangle & &
\frac{1}{10\sqrt{y}}\\
\int_{-2}^3\frac{1}{5}\, dx = 1 & & y>9 & & 0
\end{array}
\right\} = f_{Y}(y)\, .
$$
\end{example}
\begin{theorem}\label{transf}
Nechť $X$ je spojitá náhodná veličina, $h:\RR\mapsto\RR$ je ryze
monotónní funkce na množině $X(\Omega)$ a $h^{-1}$ je
diferencovatelná. Potom náhodná veličina $Y=h(X)$ má hustotu
$$
f_Y(y) = f_X\left(h^{-1}(y)\right)\cdot
\left|\frac{dh^{-1}}{dy}(y)\right|\, .
$$
\end{theorem}
\begin{proof}
Pro rostoucí funkci $h(x)$ dostaneme
$$
F_Y(y) =  \P[Y\leq y] = \P[h(X)\leq y ] = \P[X\leq
h^{-1}(y)]=F_X(h^{-1}(y))
$$
a tedy
$$
f_Y(y)=f_X(h^{-1}(y))\cdot\frac{dh^{-1}}{dy}(y)\, .
$$
Podobně pro klesající
$$
F_Y(y) = \P[h(X)\leq y ] = 1-\P[h(X)\geq y] = 1-\P[X\leq
h^{-1}(y)] = 1 - F_X(h^{-1}(y))
$$
a
$$
f_Y(y) = - f_X(h^{-1}(y))\cdot\frac{dh^{-1}}{dy}(y)\, .
$$
Protože derivace klesající funkce je záporná, můžeme oba obdržené
výrazy zapsat v kýženém tvaru pomocí absolutní hodnoty.
\end{proof}
\begin{example}
Nechť náhodná veličina $X$ má hustotu $f_X(x)\geq 0$ pro $x>0$ a
$f_X(x)= 0$ pro $x\leq 0$. Určete rozdělení náhodně veličiny
$Y=a\cdot \ln X, a\neq 0$.

Ve značení věty máme
$$
h(x)=a\cdot \ln x \qmq{a} h^{-1}(y) = e^{\frac{y}{a}}
$$
a z jejího tvrzení vyplývá
$$
f_Y(y) = f_X(e^{\frac{y}{a}})e^{\frac{y}{a}}\frac{1}{|a|}\, .
$$
Např. pro $c>0$, $f_X(x)=1/c$ pro $x\in(0,c)$ dostáváme, že
náhodná veličina $Y=-\ln X$ má hustotu
$$
f_Y(y)=
  \begin{cases}
    \frac{1}{c}\, e^{-y} & \text{pro }y>-\ln c, \\
    0 & \text{jinak}.
  \end{cases}
$$
\end{example}
\begin{example}
Náhodná veličina $X$ má hustotu $f_X(x)=1/\pi$ pro
$x\in(-\pi/2,\pi/2)$. Určete rozdělení náhodné veličiny
$Y=\mbox{tg}\, X$.

Protože $h^{-1}(y) = \mbox{arctg}(y)$, dostáváme
$$
f_Y(y) = \frac{1}{\pi}\cdot\frac{1}{1+y^2},\qquad
y\in(-\infty,\infty)\, .
$$
\end{example}
\begin{exercise}
Náhodná veličina $X$ má hustotu $f_X(x)=x/8$ pro $0\leq x<4$.
Určete hustotu pravděpodobnosti náhodné veličiny Y=2X+4.
[$(y-4)/32, y\in \langle 4, 12\rangle$]
\end{exercise}

\begin{exercise}
Rychlost částice plynu je náhodná veličina $V$ s hustotu
$$
f_V(v) = av^2e^{-bv}, \qquad v>0,
$$
kde konstanta $b>0$ a její hodnota závisí na teplotě plynu a
hmotnosti částice. Určete konstantu $a$ a pravděpodobnostní
rozdělení kinetické energie částice $W=mV^2/2$.
\end{exercise}

\begin{exercise}
$X_1,X_2$ jsou nezávislé náhodné veličiny s hustotami
$$
f_{X_j}(x_j) = 2e^{-2x_j}, \qquad x_j\geq 0,\ j=1,2\, .
$$
Určete hustotu pravděpodobnosti náhodné veličiny $Y=X_1/X_2$.
[$1/(1+y)^2, y>0$]
\end{exercise}



\section{Funkce více náhodných veličin}

Nechť náhodný vektor $(X_1,\ldots,X_n)$ má sdruženou hustotu
pravděpodobnosti $f_{X_1,\ldots,X_n}(x_1,\ldots,x_n)$ a nechť
$h:\RR^n\mapsto\RR$. Potom distribuční funkce náhodné veličiny
$Y=h(X_1,\ldots,X_n)$ je
$$
F_Y(y) = \P[Y\leq y] = \int\!\! . . .\! \int_{\{(x_1,\ldots,x_n)\in
\RR^n\,|\,h(x_1,\ldots,x_n)\leq y\}}
f_{{X_1,\ldots,X_n}}(x_1,\ldots,x_n)\, dx_1 \ldots dx_n\, .
$$
\begin{example}
Najděte rozdělení součtu $Y=X_1+X_2$, pokud jsou náhodné veličiny
$X_1,X_2$ nezávislé.

Užitím Fubiniovy věty a nezávislosti dostáváme
\begin{eqnarray*}
F_Y(y) &=& \P[X_1+X_2\leq y] = \int\!\!\!\int_{x_1+x_2\leq y}
f_{X_1,X_2}(x_1,x_2)\, dx_1 dx_2\\
& =& \int_{-\infty}^{\infty} \left(\int_{-\infty}^{y-x_1}
f_{X_1,X_2}(x_1,x_2)\,  dx_2\right)
dx_1=\int_{-\infty}^{\infty}f_{X_1}(x_1)
\left(\int_{-\infty}^{y-x_1} f_{X_2}(x_2)\,  dx_2\right) dx_1\\
& =& \int_{-\infty}^{\infty}f_{X_1}(x_1)\cdot F_{X_2}(y-x_1)\,
dx_1
\end{eqnarray*}
a tedy
$$
f_Y(y) = \frac{dF_Y(y)}{dy} =
\int_{-\infty}^{\infty}f_{X_1}(x_1)f_{X_2}(y-x_1)\, dx_1\, .
$$
Symetricky, pokud použijeme opačné řezy ve Fubiniově větě,
dostaneme
$$
f_Y(y) = \int_{-\infty}^{\infty}f_{X_2}(x_2)f_{X_1}(y-x_2)\,
dx_2\, .
$$
\end{example}
\begin{theorem}
Nechť náhodný vektor $(X_1,\ldots,X_n)$ má SASR a
$(Y_1,\ldots,Y_n)=h(X_1,\ldots,X_n)$, kde $h:\RR^n\mapsto\RR^n$ je
spojité bijektivní zobrazení na otevřené množině $G$ takové, že
$\int ...\int_G f_{X_1,\ldots,X_n}\, dx_1\ldots dx_n =1$. Nechť
dále inverzní zobrazení $(x_1,\ldots,x_n)=h^{-1}(y_1,\ldots,y_n)$
je spojité, diferencovatelné a na množině $h(G)$ splňuje podmínku
$$
J=\left| \begin{array}{ccc} \frac{\partial h_1^{-1}}{\partial y_1}
& \ldots & \frac{\partial h_1^{-1}}{\partial y_n} \\
\vdots & \ddots & \vdots \\
\frac{\partial h_n^{-1}}{\partial y_1} & \ldots & \frac{\partial
h_n^{-1}}{\partial y_n}
\end{array}
\right| \neq 0\, .
$$
Potom i náhodný vektor $(Y_1,\ldots,Y_n)$ má SASR a platí
$$
f_{(Y_1,\ldots,Y_n)}(y_1,\ldots,y_n) =
f_{X_1,\ldots,X_n}(h_1^{-1}(y_1,\ldots,y_n),\ldots,
h_n^{-1}(y_1,\ldots,y_n)\cdot |J|\, .
$$
\end{theorem}

\begin{example}
Nechť náhodné veličiny $X_1,X_2$ mají sdruženou hustotu
$$
f_{X_1,X_2}(x_1,x_2) =
  \begin{cases}
    4x_1 x_2 & \text{pro }0<x_1<1, 0<x_2<1, \\
   0 & \text{jinak}.
  \end{cases}
$$
Najděte sdruženou hustotu pravděpodobnosti veličin $X_1^2,X_2^2$.

Ve značení věty máme $(Y_1,Y_2)=h(X_1,X_2) =(X_1^2,X_2^2)$, tj.
$X_1=\sqrt{Y_1}$, $X_2=\sqrt{Y_2}$ a $h^{-1}(y_1,y_2) =
(\sqrt{y_1},\sqrt{y_2})$ a hodnota jakobiánu tedy je
$$
J= \left| \begin{array}{cc} \frac{1}{2\sqrt{y_1}} & 0\\
0 & \frac{1}{2\sqrt{y_2}}
\end{array}
\right| = \frac{1}{4\sqrt{y_1y_2}}\, ,
$$
jež je na dané oblasti nenulová. Podle tvrzení věty tedy
$$
f_{Y_1,Y_2}(y_1,y_2) =
  \begin{cases}
    4\sqrt{y_1}\sqrt{y_2}\cdot \frac{1}{4\sqrt{y_1y_2}}= 1 & \text{pro } 0<y_1<1, 0<y_2<1, \\
    0 & \text{jinak}.
  \end{cases}
$$
\end{example}
\begin{example}
Nechť jsou náhodné veličiny $X_1,X_2$ nezávislé a nechť platí
$f_{X_2}(x_2)=0$ pro všechna $x\leq 0$. Určete hustotu
pravděpodobnosti náhodné veličiny $Y=X_1/X_2$.

Označme $Y_1=X_1/X_2, Y_2=X_2$. Inverzní funkce jsou $X_2=Y_2$,
$X_1=Y_1Y_2$ a pro hodnotu jakobiánu platí
$$
J =\left| \begin{array}{cc} y_2 & y_1\\
0 & 1
\end{array}
\right| = y_2 \neq 0 \qmq{pro} x_2>0\, .
$$
Podle věty má sdružená hustota tvar
$$
f_{Y_1,Y_2}(y_1,y_2) = f_{X_1,X_2}(y_1y_2, y_2) y_2 =
f_{X_1}(y_1y_2) f_{X_2}(y_2) y_2
$$
a marginální hustota je potom
$$
f_{Y_1}(y_1) = \int_0^\infty f_{X_1}(y_1y_2) f_{X_2}(y_2) y_2\,
dy_2\, .
$$
\end{example}
\begin{example}
Nechť náhodné veličiny $X_1,X_2,X_3$ mají sdruženou hustotu
$$
f_{X_1,X_2,X_3}(x_1,x_2,x_3) =
  \begin{cases}
    e^{(-x_1+x_2+x_3)} & \text{pro }x_1>0, x_2>0, x_3>0, \\
   0 & \text{jinak}.
  \end{cases}
$$
Najděte hustotu náhodné veličiny $Y=(X_1+X_2+X_3)/3$.

Označme
$$
Y_1=\frac{X_1+X_2+X_3}{3},\quad Y_2=X_2,\quad Y_3=X_3\, .
$$
Inverzní funkce tedy jsou $X_1=3Y_1-Y_2-Y_3, X_2=Y_2, X_3=Y_3$ a
hodnota jakobiánu transformace je
$$
J = \left| \begin{array}{ccc} 3& -1 & -1\\
0 & 1 & 0\\
0&0& 1
\end{array}
\right| = 3\, .
$$
Sdružená hustota pravděpodobnosti tedy je
$$
f_{Y_1,Y_2,Y_3}(y_1,y_2,y_3) =
  \begin{cases}
    3e^{-3y_1} & \text{pro }3y_1-y_2-y_3>0, y_2>0, y_3>0, \\
   0 & \text{jinak}
  \end{cases}
$$
a postupným vyintegrováním proměnných $y_2,y_3$ dostáváme
$$
f_{Y_1,Y_2}(y_1,y_2) =
  \begin{cases}
    \int_0^{3y_1-y_2}3e^{-3y_1}\, dy_3=3(3y_1-y_2)e^{-3y_1} & \text{pro }3y_1-y_2>0, y_2>0, \\
   0 & \text{jinak},
  \end{cases}
$$
$$
f_{Y_1}(y_1) =
  \begin{cases}
    \int_0^{3y_1}3(3y_1-y_2)e^{-3y_1} dy_2=\frac{27}{2}y_1^2e^{-3y_1} & \text{pro }y_1>0, y_2>0, \\
   0 & \text{jinak}.
  \end{cases}
$$


\end{example}

\section{Příklady spojitých náhodných veličin}

\subsection{Rovnoměrné rozdělení s parametry $a<b,\ a,b\in\RR$}
\begin{defin}Řekneme, že náhodná veličina $X$ má rovnoměrné rozdělení s parametry $a<b,\
a,b\in\RR$, jestliže její hustota má tvar
$$
f_X(x)=
  \begin{cases}
    \fra{1}{b-a} & \text{pro } x\in(a,b), \\
    0 & \text{jinak}.
  \end{cases}
$$
Značíme $X\sim U(a,b)$.
\end{defin}
{\large \em Obr.}

\subsection{Gamma rozdělení s parametry $\alpha,\beta>0$}

\begin{defin}
Řekneme, že náhodná veličina $X$ má Gamma rozdělení s parametry
$\alpha,\beta>0$, jestliže její hustota pravděpodobnosti má tvar
$$
f_X(x) =
  \begin{cases}
    \fra{1}{\Gamma(\alpha)}\cdot\fra{1}{\beta^\alpha}\, x^{\alpha-1} e^{-\frac{x}{\beta}} & \text{pro } x>0, \\
    0 & \text{jinak}.
  \end{cases}
$$
Značíme $X\sim Gamma(\alpha,\beta)$.
\end{defin}

\begin{remark}
Připomeňme definici Gamma funkce
$$
\Gamma(p) = \int_0^\infty x^{p-1} e^{-x}\, dx,\qquad p>0
$$
a její základní vlastnosti
$$
\Gamma(1)=1,\ \Gamma(p+1) = p\Gamma(p)\qmq{a tedy} \Gamma(p+1)=
p!\qmq{a} \Gamma\left(\frac{1}{2}\right) = \sqrt{\pi}\, .
$$
\end{remark}

\subsection{Normální (Gaussovo) rozdělení s parametry $\mu\in\RR, \sigma^2>0$}
\begin{defin}
Řekneme, že náhodná veličina $X$ má normální (Gaussovo) rozdělení
s parametry $\mu\in\RR, \sigma^2>0$, jestliže její hustota
pravděpodobnosti má tvar
$$
f_X(x) = \frac{1}{\sqrt{2\pi\sigma^2}}\cdot e
^{-\frac{(x-\mu)^2}{2\sigma^2}},\qquad x\in\RR\, .
$$ Značíme $X\sim N(\mu,\sigma^2)$.
\end{defin}
 {\large \em Obr.}

 Ověřme, že se jedná o hustotu pravděpodobnosti
 \begin{eqnarray*}
\frac{1}{\sqrt{2\pi\sigma^2}}\int_{-\infty}^\infty e
^{-\frac{(x-\mu)^2}{2\sigma^2}}\, dx &=& \left| \begin{array}{c}
\frac{x-\mu}{\sigma} = t\\
\frac{dx}{\sigma} = dt
\end{array}\right|
= \frac{1}{\sqrt{2\pi}} \int_{-\infty}^\infty e^{-\frac{t^2}{2}}\,
dt = \frac{2}{\sqrt{2\pi}} \int_{0}^\infty e^{-\frac{t^2}{2}}\, dt
\\
&=& \left| \begin{array}{c}
\frac{t^2}{2} = z\\
 t dt = dz
\end{array}\right|
= \frac{1}{\sqrt{\pi}} \int_0^\infty z^{-\frac{1}{2}} e^{-z}\, dz
= \frac{1}{\sqrt{\pi}} \Gamma\left(\frac{1}{2}\right) = 1\, .
 \end{eqnarray*}

Normální rozdělení s parametry $\mu=0$ a $\sigma^2=1$ bude mít
výsadní postavení, budeme ho nazývat standardní normální rozdělení
a zavedeme pro jeho hustotu a distribuční funkci označení
$$
\varphi(x) = \frac{1}{\sqrt{2\pi}} \, e^{-\frac{x^2}{2}},\qquad
\Phi(x) = \frac{1}{\sqrt{2\pi} }\int_{-\infty}^x
e^{-\frac{t^2}{2}}\, dt\, .
$$
{\large\em OBR.}

\begin{cor}
Pro distribuční funkci $\Phi(x)$ standardní normální náhodné
veličiny platí
$$
\Phi(x) = 1- \Phi(-x) \qmq{pro všechna} x\in\RR\, .
$$
\end{cor}
\begin{proof}
\begin{eqnarray*}
\Phi(x) &=& \frac{1}{\sqrt{2\pi}} \int_{-\infty}^x
e^{-\frac{t^2}{2}}\, dt = \left| t=-y \right| =
-\frac{1}{\sqrt{2\pi}} \int_{\infty}^{-x} e^{-\frac{y^2}{2}}\, dy=
\frac{1}{\sqrt{2\pi}} \int_{-x}^\infty e^{-\frac{y^2}{2}}\, dy\\
&=& \frac{1}{\sqrt{2\pi}} \left( \int_{-\infty}^\infty
e^{-\frac{y^2}{2}}\, dy- \int_{-\infty}^{-x} e^{-\frac{y^2}{2}}\,
dy \right)= 1-\Phi(-x)\, .
\end{eqnarray*}
\end{proof}

\begin{theorem}
Jestliže $X\sim N(\mu,\sigma^2)$, potom
$$
F_X(x) = \Phi \left( \frac{x-\mu}{\sigma} \right)\, .
$$
\end{theorem}
\begin{proof}
$$
F_X(x)= \frac{1}{\sqrt{2\pi\sigma^2}}\int_{-\infty}^x e
^{-\frac{(t-\mu)^2}{2\sigma^2}}\, dt = \left| \begin{array}{c}
\frac{t-\mu}{\sigma} = z\\
\frac{dt}{\sigma} = dz
\end{array}\right|
= \frac{1}{\sqrt{2\pi}} \int_{-\infty}^{\frac{x-\mu}{\sigma}}
e^{-\frac{z^2}{2}}\, dz = \Phi \left( \frac{x-\mu}{\sigma}
\right)\, .
$$
\end{proof}

\begin{remark}
Protože integrály potřebné k vyčíslení hodnot distribuční funkce
normálního rozdělení lze počítat pouze numericky, tyto hodnoty
jsou uvedeny ve statistických tabulkách. Z předchozích vět
vyplývá, že stačí tabelovat pouze hodnoty distribuční funkce
standardního normálního rozdělení pro $x>0$.

Uvažujme například náhodnou veličinu $X\sim N(2,16)$ a spočtěme
pravděpodobnost $\P[X\in(1,3)]$.
\begin{eqnarray*}
\P[X\in(1,3)] &=& F_X(3)-F_X(1) = \Phi\left(\frac{3-2}{4}\right) -
\Phi\left(\frac{1-2}{4}\right) \\
& = &\Phi\left(\frac{1}{4}\right)-\Phi\left(\frac{-1}{4}\right) =
2 \Phi\left(\frac{1}{4}\right)-1\doteq 0.197\, .
\end{eqnarray*}
\end{remark}

\begin{theorem}
Nechť $X\sim N(\mu,\sigma^2)$, potom platí
$$
aX+b \, \sim\,  N(a\mu+b,a^2\sigma^2)\, .
$$
\end{theorem}
\begin{proof}
Označme $Y=aX+b$. To znamená $X=(Y-b)/a$ a aplikací Věty
\ref{transf} o transformaci dostaneme
$$
f_Y(y) = \frac{1}{|a|}\cdot f_X\left(\frac{y-b}{a}\right) =
\frac{1}{\sqrt{2\pi\sigma^2}|a|} \,
e^{-\frac{(\frac{y-b}{a}-\mu)^2}{2\sigma^2}} =
\frac{1}{\sqrt{2\pi\sigma^2a^2}} \,
e^{-\frac{(y-(a\mu+b))^2}{2a^2\sigma^2}}\, .
$$
což je hustota pravděpodobnosti náhodné veličiny s rozdělením
$N(a\mu+b,a^2\sigma^2)$.
\end{proof}
\begin{dusl}
Nechť $X\sim N(\mu,\sigma^2)$. Potom
$$
\frac{X-\mu}{\sigma} \sim N(0,1)\, .
$$
\end{dusl}
\begin{proof}
V předchozí větě položíme $a=1/\sigma, b=-\mu/\sigma$ a dostaneme
$$
\frac{X-\mu}{\sigma} \sim N\left(
\frac{\mu}{\sigma}-\frac{\mu}{\sigma}, \frac{1}{\sigma^2}\,
\sigma^2\right)= N(0,1)\, .
$$
\end{proof}
\begin{example}
Nechť $X\sim N(\mu,\sigma^2)$. Určete $\P[|X-\mu|\leq k\sigma]$
pro $k>0$.

Pro všechna $k>0$ platí
$$
\P[|X-\mu|\leq k\sigma] = \P[\mu-k\sigma \leq X\leq \mu +k\sigma]
= F_X(\mu+k\sigma)- F_X(\mu-k\sigma) = \Phi(k)-\Phi(-k)
$$
a pro vybrané hodnoty $k$ tedy dostáváme
$$
\P[|X-\mu|\leq \sigma] = \Phi(1) - \Phi(-1) = 0.6827\, ,
$$
$$
\P[|X-\mu|\leq 2\sigma] = \Phi(2) - \Phi(-2) = 0.9555\, ,
$$
$$
\P[|X-\mu|\leq 3\sigma] = \Phi(3) - \Phi(-3) = 0.9973\, .
$$
Poslednímu výrazu se někdy říká {\em pravidlo $3\sigma$:} hodnoty
náhodné veličiny $X\sim N(\mu,\sigma^2)$ budou téměř jistě (s
pravděpodobností 99.73\%) ležet v intervalu $(\mu-3\sigma, \mu
+3\sigma)$.
\end{example}

\begin{exercise}
Délka skoků sportovce Petra měřená v cm má normální rozdělení
$N(\mu_1,\sigma_1^2)$, kde $\mu_1=690$ a $\sigma_1=10$. Délka
skoků sportovce Pavla měřená v cm má také normální rozdělení
$N(\mu_2,\sigma_2^2)$, kde $\mu_2=705$ a $\sigma_2=15$. Na závody
se kvalifikuje ten, kdo ze dvou skoků alespoň jednou skočí více
než 700 cm.
\begin{itemize}
  \item[a)] S jakou pravděpodobností se oba kvalifikují na závody?
  \item[b)] S jakou pravděpodobností se kvalifikuje Pavel, ale
  Petr ne?
\end{itemize}
\end{exercise}



\subsection{Logaritmicko-normální rozdělení s parametry $\mu\in\RR,\sigma^2>0$}

\begin{defin}
Řekneme, že náhodná veličina $X$ má logaritmicko-normální
 rozdělení s parametry $\mu\in\RR, \sigma^2>0$, jestliže
její hustota pravděpodobnosti má tvar
$$
f_X(x) = \frac{1}{x\sqrt{2\pi\sigma^2}}\cdot e ^{-\frac{(\ln
x-\mu)^2}{2\sigma^2}},\qquad x>0\, .
$$ Značíme $X\sim LN(\mu,\sigma^2)$.
\end{defin}
{\large\em OBR.}

\begin{remark}
Uvažujeme-li náhodnou veličinu $Y=\ln X$, kde $X\sim
LN(\mu,\sigma^2)$, potom $Y\sim N(\mu,\sigma^2)$, z čehož také
pochází název logaritmicko-normálního rozdělení. Že to opravdu
platí, lze jednoduše ukázat pomocí Věty \ref{transf} o
transformaci náhodné veličiny monotónní funkcí. Naopak, pokud
$Y\sim N(\mu,\sigma^2)$, potom $X=e^Y\sim LN(\mu,\sigma^2)$.

Toto rozdělení se používá například při popisu částic sypkých
materiálů nebo v teorii spolehlivosti.
\end{remark}


\subsection{Exponenciální rozdělení s parametry $\mu\in\RR, \theta>0$}
\begin{defin}
Řekneme, že náhodná veličina $X$ má exponenciální rozdělení s
parametry $\mu\in\RR, \theta>0$, jestliže její hustota
pravděpodobnosti má tvar
$$
f_X(x) =
  \begin{cases}
    \frac{1}{\theta}\, e^{-\frac{x-\mu}{\theta}} & \text{pro } x>\mu, \\
    0 & \text{jinak}.
  \end{cases}
$$ Značíme $X\sim Exp(\mu,\theta)$.
\end{defin}
Pro exponenciální rozdělení není problém vyjádřit explicitně
distribuční funkci. Platí totiž pro všechna $x>\mu$
$$
F_X(x) = \int_\mu^x \frac{1}{\theta}\, e^{-\frac{t-\mu}{\theta}}\,
dt =\left[-e^{-\frac{t-\mu}{\theta}}\right]_\mu^x =
1-e^{-\frac{x-\mu}{\theta}}\, .
$$

\begin{remark}
Poznamenejme, že exponenciální rozdělení je speciální případ Gamma
rozdělení, neboť $Exp(0,\theta)=Exp(\theta) = Gamma(1,\theta)$.
\end{remark}
\begin{remark}
Exponenciální rozdělení je také někdy nazýváno rozdělení {\em bez paměti}, pro $X\sim Exp(\theta)$ totiž platí
$$
\P[X>a+x\,|\,X>a] = \P[X>x]\, .
$$
Tento vztah se dá interpretovat například následujícími dvěma
způsoby:
\begin{itemize}
  \item informace o tom, že událost nenastala po dobu $a$ hodin,
  nemění pravděpodobnost výskytu události v dalších hodinách,
  \item nechť $X$ značí dobu do poruchy. Pravděpodobnost, že
  zařízení, které pracovalo bez poruchy $a$ hodin, bude pracovat
  ještě alespoň $x$ hodin, je rovna pravděpodobnosti, že zařízení,
  které nebylo v provozu, bude pracovat alespoň $x$ hodin.
\end{itemize}
Ukažme si, že uvedený vztah skutečně platí. Pro všechny $a>0,x>0$,
\begin{eqnarray*}
\P[X>a+x\,|\,X>a] &=& \frac{\P[X>a+x,X>a]}{\P[X>a]} =
\frac{1-\P[X\leq a+x]}{1-\P[X\leq a]} \\
&=& \fra{e^{-\frac{a+x}{\theta}}}{e^{-\frac{a}{\theta}}} =
e^{-\frac{x}{\theta}} = 1-F_X(x)=\P[X>x]\, .
\end{eqnarray*}
\end{remark}

Exponenciální rozdělení nachází široké uplatnění v teorii
spolehlivosti a životnosti (elektronické součástky), v teorii
hromadné obsluhy atd.

\begin{exercise}
Ve velké počítačové síti se průměrně přihlašuje 25 uživatelů za
hodinu. Určete pravděpodobnost, že
\begin{itemize}
  \item[a)] se nikdo nepřihlásí během intervalu délky 6 minut
  \item[b)] do dalšího přihlášení uběhnou 2-3 minuty
  \item[c)] určete délku časového intervalu, aby pravděpodobnost,
  že se nikdo nepřihlásí, byla 0.9.
\end{itemize}

\end{exercise}



V následujících třech odstavcích se zmíníme o rozděleních
důležitých pro praktické aplikace v matematické statistice.
\subsection{Pearsonovo $\chi^2$ rozdělení s $n$ stupni volnosti, $n\in\NN$}
\begin{defin}
Řekneme, že náhodná veličina $Y$ má Pearsonovo $\chi^2$ rozdělení
s $n$ stupni volnosti, $n\in\NN$, jestliže její hustota
pravděpodobnosti má tvar
$$
f_Y(y) =
  \begin{cases}
    \frac{1}{2\Gamma\left(\frac{n}{2}\right)}\,\left(\frac{y}{2}\right)^{\frac{n}{2}-1} e^{-\frac{y}{2}} & \text{pro } y>0, \\
    0 & \text{jinak}.
  \end{cases}
$$ Značíme $Y\sim \chi^2(n)$.
\end{defin}
\begin{remark}
Platí následující tvrzení. Nechť $X_1,\ldots,X_n$ jsou nezávislé
náhodné veličiny, $X_i\sim N(0,1), \forall i\in\wh n$, a definujme
náhodnou veličinu
$$
Y= X_1^2+X_2^2+\ldots + X_n^2\, .
$$
Potom $Y\sim \chi^2(n)$, tj. $Y$ má $\chi^2$ rozdělení s $n$
stupni volnosti.

$\chi^2$ rozdělení je opět speciálním případem Gamma rozdělení,
$\chi^2(n) = Gamma(n/2,2)$, a ukazuje se jako velmi důležité v
testech dobré shody a testování hypotéz.
\end{remark}

\subsection{Studentovo rozdělení s $n$ stupni volnosti, $n\in\NN$}
\begin{defin}
Nechť $X,Y$ jsou nezávislé náhodné veličiny, $X\sim N(0,1), Y\sim
\chi^2(n)$. Potom říkáme, že náhodná veličina
$$
T =  \frac{X}{\sqrt{\frac{Y}{n}}}
$$
má Studentovo rozdělení s $n$ stupni volnosti. Značíme $T \sim
t(n)$.
\end{defin}

\subsection{Fisherovo rozdělení ($F$-rozdělení)}
\begin{defin}
Nechť $X,Y$ jsou nezávislé náhodné veličiny, $X\sim \chi^2(m),
Y\sim \chi^2(n)$. Potom říkáme, že náhodná veličina
$$
Z =  \frac{\frac{X}{m}}{\frac{Y}{n}}
$$
má Fisherovo rozdělení s $m,n$ stupni volnosti. Značíme $Z \sim
F(m,n)$.
\end{defin}



