
\chapter{Charakteristiky náhodných veličin}

\section{Střední hodnota}


\begin{defin}
Nechť náhodná veličina $X$ má diskrétní rozdělení. Potom střední
hodnotou náhodné veličiny $X$ nazýváme číslo
$$
\E X = \sum_i x_i\P[X=x_i]= \sum_i x_ip_i\, ,
$$
pokud příslušná řada absolutně konverguje.
\end{defin}

\begin{defin}
Nechť má náhodná veličina $X$ absolutně spojité rozdělení s
hustotou $f_X$. Potom její střední hodnotou rozumíme číslo
$$
\E X = \int_{-\infty}^\infty x f_X(x)\, dx\, ,
$$
pokud integrál absolutně konverguje.
\end{defin}

\begin{theorem}
Nechť je dána náhodná veličina $X$, zobrazení $h:\RR\mapsto\RR$ a
nechť $Y=h(X)$ je náhodná veličina. Potom
\begin{itemize}
  \item[a)] má-li $X$ diskrétní rozdělení, platí
  $$
    \E Y = \sum_i h(x_i) p_i,\quad \mbox{pokud $\E Y$ existuje,}
  $$
  \item[b)] má-li $X$ absolutně spojité rozdělení, platí
  $$
  \E Y = \int_{-\infty}^\infty h(x) f_X(x)\, dx,\quad \mbox{pokud $\E Y$ existuje.}
  $$
\end{itemize}

\end{theorem}
\begin{proof}
a) Pro diskrétní rozdělení je důkaz triviální. Pro hodnoty $x_i$,
kterých může nabývat náhodná veličina $X$, platí $h(x_i)=y_i$ a
pro získání pravděpodobnostního rozdělení náhodné veličiny $Y$
stačí sečíst hodnoty $p_i,p_j$ pro taková $i,j$, kde
$h(x_i)=h(x_j)$.\\
b) Dokažme tvrzení pro ostře rostoucí funkci $h$ na $\RR$. S
využitím věty o transformaci a faktu, že mimo interval
$(h(-\infty),h(\infty))$ je hustota $f_Y$ nulová, dostaneme
\begin{eqnarray*}
\int_{-\infty}^\infty h(x) f_X(x)\, dx &=& \left[
\begin{array}{cc}
y=h(x) & x=h^{-1}(y) \\
\multicolumn{2}{c}{dx = \frac{dh^{-1}(y)}{dy} \,dy}
\end{array}
 \right] = \int_{h(-\infty)}^{h(\infty)} y f_X(h^{-1}(y)) \frac{dh^{-1}(y)}{dy}
 \,dy=\\
 &=& \int_{h(-\infty)}^{h(\infty)} y f_Y(y)\, dy = \int_{-\infty}^{\infty} y
 f_Y(y)\,dy = \E Y\, .
 \end{eqnarray*}
\end{proof}
\begin{remark}
\begin{itemize}
  \item[a)] Jsou-li $X,Y$ diskrétní náhodné veličiny a
  $h:\RR^2\mapsto\RR$, potom
  $$
    \E h(X,Y) = \sum_{i,j} h(x_i,y_j) \P[X=x_i,Y=y_j] = \sum_{i,j}
    h(x_i,y_j)p_{ij}\, ,
  $$
  pokud řada absolutně konverguje.
  \item[b)] Jsou-li $X,Y$ absolutně spojité náhodné veličiny a
  $h:\RR^2\mapsto\RR$, potom
  $$
    \E h(X,Y) = \int_{-\infty}^\infty \int_{-\infty}^\infty h(x,y)
    f_{X,Y}(x,y) \, dxdy\, ,
  $$
    pokud integrál absolutně konverguje.
  \item[c)] Je-li $\bm X=(X_1,\ldots,X_n)$ náhodný vektor, potom
  definujeme jeho střední hodnotu po složkách, tj.
  $$
  \E\bm X =\E (X_1,\ldots,X_n) = (\E X_1,\ldots,\E X_n)\, .
  $$
\end{itemize}
\end{remark}

\begin{theorem}
Nechť mají náhodné veličiny $X,Y$ diskrétní nebo absolutně spojité
rozdělení a nechť existují $\E X, \E Y$. Potom existuje i
$\E(X+Y)$ a platí
$$
\E(X+Y) = \E X + \E Y\, .
$$
\end{theorem}
\begin{proof}
Označme $Z=X+Y$, tj. $Z=h(X,Y)$. V diskrétním případě
\begin{eqnarray*}
\E(X+Y) &=& \sum_{i,j} (x_i+y_j) \P[X=x_i,Y=y_j] =  \sum_i x_i
\sum_j \P[X=x_i,Y=y_j]\\
& +& \sum_j y_j \sum_i \P[X=x_i,Y=y_j] = \sum_i x_i \P[X=x_i] +
\sum_j y_j \P[Y=y_j] \\
&=& \E X + \E Y\, .
\end{eqnarray*}
Pro spojitý případ
\begin{eqnarray*}
 \E(X+Y) &=&  \int_{-\infty}^\infty \int_{-\infty}^\infty (x+y)
    f_{X,Y}(x,y) \, dxdy =  \int_{-\infty}^\infty x\left(\int_{-\infty}^\infty
    f_{X,Y}(x,y) \, dy\right) dx \\
    &+&  \int_{-\infty}^\infty y\left( \int_{-\infty}^\infty
    f_{X,Y}(x,y) \, dx \right) dy =  \int_{-\infty}^\infty   x f_{X}(x) \,
    dx+ \int_{-\infty}^\infty   y f_{Y}(y) \, dy\\ &=& \E X + \E Y\, .
\end{eqnarray*}
\end{proof}
\begin{theorem}
Pro diskrétní nebo absolutně spojitou náhodnou veličinu $X$ mající
$\E X$ a $c\in\RR$ platí
$$
\E(cX)=c\, \E X\, .
$$
\end{theorem}
\begin{proof}
Ukažme například pro diskrétní
$$
\E(cX) = \sum_i cx_ip_i = c\sum_i x_i p_i = c\, \E X\, .
$$
\end{proof}

\begin{cor}
Pro libovolné $a\in\RR$ platí $\E a = a$.
\end{cor}
\begin{proof}
Konstantu $a$ si můžeme představit jako náhodnou veličinu $X$, pro
kterou platí $\P[X=a]=1$. Potom $\E X=a\,\P[X=a] = a$.
\end{proof}
\begin{dusl}
Pro diskrétní nebo absolutně spojitou náhodnou veličinu $X$ a
konstanty $a,c\in\RR$ platí
$$
\E(a+cX) = a+c\,\E X\, .
$$
\end{dusl}
\begin{theorem}
Nechť $X,Y$ jsou nezávislé náhodné veličiny mající diskrétní nebo
absolutně spojité rozdělení. Potom
$$
\E(XY) = \E X \cdot \E Y\, ,
$$
pokud všechny střední hodnoty existují.
\end{theorem}
\begin{proof}
Dokažme tvrzení například pro spojité náhodné veličiny. S využitím
nezávislosti a Fubiniovy věty dostaneme
\begin{eqnarray*}
\E(XY) &=& \int_{-\infty}^\infty \int_{-\infty}^\infty xy
    f_{X,Y}(x,y) \, dxdy = \int_{-\infty}^\infty \int_{-\infty}^\infty xy
    f_{X}(x) f_Y(y) \, dxdy\\
    &=& \left( \int_{-\infty}^\infty x f_{X}(x) \, dx\right) \left( \int_{-\infty}^\infty y f_{Y}(y) \,
    dy\right)= \E X \cdot \E Y\, .
\end{eqnarray*}
\end{proof}


\section{Momenty náhodných veličin}

\begin{defin}
Nechť $X$ je náhodná veličina a $k\in\NN$. Potom $k$-tým obecným
momentem náhodné veličiny $X$ nazýváme číslo
$$
\mu_k^\prime(X) = \E(X^k)\, ,
$$
pokud existuje. Dále $k$-tým centrálním momentem náhodné veličiny
$X$ nazýváme číslo
$$
\mu_k(X) = \E(X-\E X)^k\, ,
$$
pokud pravá strana existuje.
\end{defin}
\begin{remark}
Speciálně
$$
\begin{array}{ll}
\mu_0^\prime(X) = 1 & \mu_0(X) = 1\\
\mu_1^\prime(X) = \E X & \mu_1(X) = \E(X-\E X) = 0\\
\mu_2^\prime(X) = \E X^2 & \mu_2(X) = \E(X-\E X)^2 = \mu_2^\prime(X) -(\mu_1^\prime(X))^2 \\
\end{array}
$$
a obecně platí, že každý centrální moment se dá vyjádřit pomocí
obecných.
\end{remark}

\begin{defin}
Druhý centrální moment $\mu_2$ se nazývá rozptyl náhodné veličiny
$X$ a značí se $\Var X$, tj. platí
$$
\Var X = \E(X-\E X)^2\, .
$$
Směrodatnou odchylkou náhodné veličiny $X$ rozumíme číslo
$$
\sd X = \sqrt{\Var X}\, .
$$
\end{defin}

\begin{remark}
$\E X$ udává hodnotu, kolem které náhodná veličina kolísá, $\Var
X$ udává velikost tohoto kolísání.
\end{remark}

\begin{theorem}
Nechť $X$ je náhodná veličina, pro kterou existuje $\E X, \E X^2$.
Potom
$$
\Var X = \E X^2 - (\E X)^2\, .
$$
\end{theorem}
\begin{proof}
Podle pravidel pro počítání se střední hodnotou platí
\begin{eqnarray*}
\Var X &= & \E(X-\E X)^2 = \E (X^2-2X\, \E X +(\E X)^2) = \E X^2 -
\E (2X \E X) + \E(\E X)^2\\
&=& \E X^2 - 2  \E X (\E X) +(\E X)^2 = \E X^2 - (\E X)^2\, .
\end{eqnarray*}
\end{proof}
\begin{theorem}
Nechť $X$ je náhodná veličina, pro kterou ex. $\Var X$, a $a,b\in\RR$. Potom
$$
\Var (aX+b) = a^2 \Var X\, .
$$
\end{theorem}
\begin{proof}
Pomocí stejných pravidel jako v předchozím důkazu dostaneme
\begin{eqnarray*}
\Var (aX+b) &=& \E(aX+b -\E(aX+b))^2 = \E(aX+b-a\E X -b)^2 \\&=&
\E(a^2X^2-2a^2X\E X + a^2 (\E X)^2) = a^2 (\E X^2 -2(\E X)^2 +(\E
X)^2) \\ &=& a^2 \Var X\, .
\end{eqnarray*}
\end{proof}
\begin{theorem}
Nechť $X,Y$ jsou nezávislé náhodné veličiny. Potom platí
$$
\Var (X+Y) = \Var X + \Var Y\, .
$$
\end{theorem}
\begin{proof}
Opět podobným způsobem dostaneme v tomto případě
\begin{eqnarray*}
\Var(X+Y) &=& \E(X+Y-\E(X+Y))^2 = \E(X-\E X +Y-\E Y)^2\\
&=& \E(X-\E X)^2 + \E(Y-\E Y)^2 + \E(2(X-\E X)(Y-\E Y))\\
& =& \Var X + \Var Y\, ,
\end{eqnarray*}
protože pro nezávislé náhodné veličiny platí
\begin{eqnarray*}
\E(2(X-\E X)(Y-\E Y)) &=& 2\E(XY -X\E Y -Y \E X +\E X \E Y)\\& =&
2(\E(XY)-\E X \E Y) = 0\, .
\end{eqnarray*}
\end{proof}
\begin{defin}
Číslo
$$
\alpha_3(X) = \frac{\mu_3(X)}{(\Var X)^\frac{3}{2}}\, ,
$$
pokud existuje, nazýváme koeficient šikmosti náhodné veličiny $X$
a číslo
$$
\alpha_4(X) = \frac{\mu_4(X)}{(\Var X)^2} -3\, ,
$$
pokud existuje, nazýváme koeficient špičatosti náhodné veličiny
$X$.
\end{defin}

\begin{remark}
Koeficient šikmosti měří míru asymetrie, naklonění hustoty náhodné
veličiny na jednu stranu. Pokud
\begin{itemize}
  \item $\alpha_3(X) <0$ hustota náhodné veličiny je vychýlená
  vlevo od $\E X$,
  \item $\alpha_3(X) >0$ hustota náhodné veličiny je vychýlená
  vpravo od $\E X$,
  \item $\alpha_3(X) =0$ hustota náhodné veličiny je symetrická
  vůči $\E X$.
\end{itemize}
Koeficient špičatosti udává, zda je hustota daného rozdělení
"plošší" nebo "špičatější" než hustota normálního rozdělení.

{\em\large Obrázky}

Poznamenejme jenom, že pokud $X\sim N(\mu,\sigma^2)$, potom
$\alpha_3(X)=\alpha_4(X) =0$.
\end{remark}
\begin{example}
Uvažujme náhodnou veličinu $X\sim Bi(n,p)$ a určeme její střední
hodnotu a rozptyl.

Podle definice střední hodnoty pro diskrétní rozdělení platí
\begin{eqnarray*}
\E X &=& \sum_{i=0}^n i \binom{n}{i} p^i(1-p)^{n-i}  =\sum_{i=1}^n
 \frac{n!}{(n-i)!(i-1)!} p^i(1-p)^{n-i} \\
 &=& np \sum_{i=1}^n \frac{(n-1)!}{(n-i)!(i-1)!}
 p^{i-1}(1-p)^{n-i}= np \sum_{j=0}^{n-1}
 \binom{n-1}{j} p^j(1-p)^{n-j-1} \\
 &=& np(p+(1-p))^{n-1}=np\, .
\end{eqnarray*}
Podobně,
$$
\E X^2 = \sum_{i=0}^n i^2 \P[X=i] = \sum_{i=0}^n i(i-1) \P[X=i] +
\sum_{i=0}^n  i \P[X=i] = n(n-1)p^2 + np\, ,
$$
kde se první suma sečte stejným postupem jako předchozí střední
hodnota a druhá suma je přímo již spočítaná střední hodnota
náhodné veličiny $X$. Pro rozptyl tedy platí
$$
\Var X = \E X^2 - (\E X)^2 = n(n-1)p^2 +np -n^2 p^2 = np(1-p)\, .
$$
\end{example}

\begin{example}
Uvažujme náhodnou veličinu $X\sim N(\mu,\sigma^2)$ a určeme její
střední hodnotu a rozptyl.

Podle definice střední hodnoty pro spojité rozdělení platí
\begin{eqnarray*}
\E X &=& \int_{-\infty}^\infty x \frac{1}{\sqrt{2\pi\sigma^2}}
e^{-\frac{(x-\mu)^2}{2\sigma^2}} \, dx \\
&=& \frac{1}{\sqrt{2\pi\sigma^2}} \int_{-\infty}^\infty (x-\mu)
 e^{-\frac{(x-\mu)^2}{2\sigma^2}} \,dx +
 \frac{1}{\sqrt{2\pi\sigma^2}} \int_{-\infty}^\infty \mu
 e^{-\frac{(x-\mu)^2}{2\sigma^2}} \,dx = \mu\, ,
\end{eqnarray*}
neboť druhý integrál (po vytknutí konstanty $\mu$) je integrálem z
hustoty pravděpodobnosti a jeho hodnota je tedy 1 a první integrál
je po zavedení substituce $(x-\mu)/\sigma=t$ roven
$$
\frac{\sigma}{\sqrt{2\pi}} \int_{-\infty}^\infty
te^{-\frac{t^2}{2}}\, dt = \frac{\sigma}{\sqrt{2\pi}}\left[
\int_{-\infty}^0 te^{-\frac{t^2}{2}}\, dt +
 \int_{0}^\infty te^{-\frac{t^2}{2}}\, dt\right] = 0\, ,
$$
protože se jedná o antisymetrický integrál. Dále,
\begin{eqnarray}\nonumber
\E X^2 &=&  \int_{-\infty}^\infty x^2 f_X(x)\,dx =
\int_{-\infty}^\infty (x-\mu+\mu)^2 f_X(x)\, dx \\
&=& \int_{-\infty}^\infty (x-\mu)^2 f_X(x)\, dx +2\mu
\int_{-\infty}^\infty (x-\mu) f_X(x)\, dx +\mu^2
\int_{-\infty}^\infty  f_X(x)\, dx\, . \label{ex2}
\end{eqnarray}
Ze znalosti střední hodnoty $\E X=\mu$ dostaneme přímo, že
prostřední integrál je nulový a z vlastností hustoty vyplyne, že
poslední integrál je roven jedné. Zbývá tedy vyčíslit první
integrál, tj.
\begin{eqnarray*}
&&\frac{\sigma}{\sqrt{2\pi}} \int_{-\infty}^\infty
\frac{(x-\mu)^2}{\sigma^2} \, e^{-\frac{(x-\mu)^2}{2\sigma^2}}\,
dx = \left|\begin{array}{c} \frac{x-\mu}{\sigma}= t \\
\frac{dx}{\sigma} = dt
\end{array}  \right|  = \frac{\sigma^2}{\sqrt{2\pi}} \int_{-\infty}^\infty t^2 e^{-\frac{t^2}{2}}\, dt\\
&&= \left|\begin{array}{c} \frac{t^2}{2}= s \\
t dt = ds \end{array}  \right| =  \frac{2\sigma^2}{\sqrt{2\pi}}
\int_{0}^\infty \sqrt{2s} e^{-s}\, ds =
\frac{2\sigma^2}{\sqrt{\pi}} \Gamma \left(\frac{3}{2}\right)=
\frac{2\sigma^2}{\sqrt{\pi}} \frac{1}{2}\Gamma
\left(\frac{1}{2}\right) = \frac{\sigma^2}{\sqrt{\pi}} \sqrt{\pi} =
\sigma^2 \, .
\end{eqnarray*}
Dosazením do vztahu (\ref{ex2}) dostaneme
$$
\E X^2 = \sigma^2 + \mu^2
$$
a rozptyl má tedy hodnotu
$$
\Var X = \E X^2 - (\E X)^2 = \sigma^2 + \mu^2 -\mu^2 = \sigma^2\,
.
$$
\end{example}




Střední hodnotu náhodného vektoru jsme definovali po složkách,
$\E(X_1,\ldots,X_n) = (\E X_1,\ldots, \E X_n)$. Podobně můžeme
definovat i další charakteristiky jako jsou rozptyl nebo momenty.
Budeme ale popisovat chování jednotlivých složek, což bude
postačovat v případě nezávislých náhodných veličin. Pokud ale
budou složky náhodného vektoru závislé, budou nás zajímat i
charakteristiky popisující jejich interakci. Jednou z nich je
kovariance náhodných veličin. Pro jednoduchost budeme všechny
pojmy definovat pro dvourozměrné vektory.

\begin{defin}
Kovarianci náhodných veličin $X,Y$ definujeme jako
$$
\Cov (X,Y) = \E[(X-\E X)(Y-\E Y)]\, ,
$$
pokud střední hodnota existuje.
\end{defin}
Jako jednoduchý důsledek této definice vidíme, že platí
$$
\Cov(X,X) = \Var X\, .
$$
\begin{theorem}
Nechť $X,Y$ jsou náhodné veličiny. Potom platí
$$
\Cov(X,Y) = \E(XY) - (\E X)(\E Y)\, ,
$$
pokud příslušné střední hodnoty existují.
\end{theorem}
\begin{proof}
Je ponechán studentům.
\end{proof}
\begin{defin}
Nechť $X,Y$ jsou náhodné veličiny. Číslo
$$
\varrho (X,Y) = \frac{\Cov(X,Y)}{\sd X\, \sd Y} = \frac{\Cov
(X,Y)}{\sqrt{\Var X\, \Var Y}}\, ,
$$
pokud existuje, nazýváme koeficient korelace náhodných veličin
$X,Y$.
\end{defin}
\begin{theorem}\label{tcor}
Nechť $X,Y$ jsou náhodné veličiny, pro které existuje $\varrho (X,Y)$ . Potom platí
$$
-1 \leq \varrho(X,Y) \leq 1\, .
$$
Přitom rovnost
$$
\varrho(X,Y) = 1 \mbox{ nastává } \quad \Leftrightarrow \quad
(\exists  \beta>0)(Y-\E Y = \beta(X-\E X))\, ,
$$
$$
\varrho(X,Y) = -1 \mbox{ nastává } \quad \Leftrightarrow \quad
(\exists  \gamma<0)(Y-\E Y = \gamma(X-\E X))\, .
$$
\end{theorem}
Rovnost $\varrho(X,Y) = 1$ tedy vyjadřuje lineární závislost
náhodných veličin $X,Y$ s kladnou směrnicí, rovnost $\varrho(X,Y)
= -1$ lineární závislost se zápornou směrnicí.

\begin{lemma}
(Schwarzova nerovnost) Nechť $X,Y$ jsou náhodné veličiny. Potom
platí
$$
(\E(XY))^2 \leq \E(X^2) \E(Y^2)\, ,
$$
pokud příslušné střední hodnoty existují.
\end{lemma}
\begin{proof}
Definujme náhodnou veličinu $Z=aX-Y$, $a\in\RR$. Potom pro všechna
$a\in\RR$ platí
$$
0\leq \E Z^2 = \E(aX-Y)^2 = \E(a^2X^2-2aXY+Y^2) = a^2 \E X^2 -
2a\E(XY) +\E Y^2\, .
$$
Dostali jsme tedy kvadratickou nerovnici v neznámé $a$, která
platí pro všechna $a\in\RR$. To ale znamená, že její diskriminant
musí být menší nebo roven 0, tj.
$$
4 (\E(XY))^2 - 4 \E X^2 \E Y^2 \leq 0\,
$$
což je ekvivalentní dokazovanému tvrzení.
\end{proof}
\begin{proof}[Důkaz věty \ref{tcor}]
Pokud použijeme Schwarzovu nerovnost pro náhodné veličiny $X-\E X,
Y-\E Y$, dostaneme
$$
\left[ \E(X-\E X)(Y-\E Y)\right]^2 \leq \E(X-\E X)^2 \E(Y-\E
Y)^2\,
$$
což je ekvivalentní zápisu
$$
\Cov^2 (X,Y) \leq \Var X \Var Y
$$
a tedy
$$
\varrho^2(X,Y)\leq 1\, .
$$
Z ekvivalence uvedené v druhé části tvrzení dokážeme pouze jednu
implikaci a to zprava doleva. Předpokládejme tedy, že $Y-\E Y =
\beta(X-\E X)$. To znamená, že $Y=\beta X +\E Y-\beta \E X$ a $Y$
se dá vyjádřit ve tvaru $Y=\beta X + C$, kde $C$ je konstanta.
Potom
\begin{eqnarray*}
\Cov(X,Y) &=& \E(X-\E X) (\beta X +C - \E(\beta X+C))\\
& =& \E(\beta X^2 -2\beta X\E X +\beta (\E X)^2) = \beta \E(X-\E
X)^2 = \beta \Var X \, .
\end{eqnarray*}
Protože dále podle pravidel pro počítání s rozptylem platí $\Var Y
= \beta^2 \Var X$ dostáváme pro koeficient korelace
$$
\varrho(X,Y) = \frac{\beta \Var X}{\sqrt{\beta^2 \Var^2 X}} =
  \begin{cases}
    +1 & \text{pro } \beta>0, \\
    -1 & \text{pro } \beta <0.
  \end{cases}
$$
\end{proof}

\begin{defin}
Nechť $X,Y$ jsou náhodné veličiny. Pokud $\varrho(X,Y)=0$, pak se
náhodné veličiny $X,Y$ nazývají nekorelované.
\end{defin}

\begin{theorem}
Nezávislé náhodné veličiny $X,Y$ jsou nekorelované.
\end{theorem}
\begin{proof}
Z nezávislosti náhodných veličin vyplývá
$$
\Cov (X,Y) = \E(XY) -\E X\E Y = \E X\E Y-\E X\E Y =0
$$
a tedy i $\varrho(X,Y)=0$.
\end{proof}
\begin{example}
Abychom ilustrovali, že předchozí tvrzení neplatí v opačném směru
uvažujme následující příklad. Nechť náhodné veličiny $X,Y$ mají
rovnoměrné rozdělení na kruhu $K=\{(x,y)\in\RR^2 |x^2+y^2\leq
1\}$, tj.
$$
f_{X,Y}(x,y) =
  \begin{cases}
    \frac{1}{\pi} & \text{pro } (x,y)\in K, \\
    0 & \text{jinak}.
  \end{cases}
$$
Úkolem je rozhodnout, zda jsou náhodné veličiny $X,Y$ nezávislé a
nekorelované.

Marginální hustoty mají tvar
$$
f_X(x)= \int_{-\infty }^\infty f_{X,Y}(x,y)\, dy =
\int_{-\sqrt{1-x^2}}^{\sqrt{1-x^2}}\frac{1}{\pi} \, dy =
\frac{2}{\pi} \sqrt{1-x^2},\quad x\in\langle -1,1\rangle
$$
a ze symetrie i
$$
f_Y(y) =\frac{2}{\pi} \sqrt{1-y^2},\quad y\in\langle -1,1\rangle\,
.
$$
Protože tedy platí $f_{X,Y}(x,y) \neq f_X(x)f_Y(y)$, veličiny
$X,Y$ nejsou nezávislé. Spočtěme jejich kovarianci,
$$
\E X =\int_{-1}^1 \frac{2}{\pi}x\sqrt{1-x^2}\, dx =
\frac{2}{\pi}\left[ \int_{-1}^0 x\sqrt{1-x^2}\, dx + \int_0^1
x\sqrt{1-x^2}\, dx\right] =0
$$
a za symetrie i $\E Y=0$. Dále,
$$
\E(XY) = \int\!\!\int_K xy \frac{1}{\pi}\, dxdy =
\frac{1}{\pi}\int_{-1}^1\left( \int_{-\sqrt{1-x^2}}^{\sqrt{1-x^2}}
xy \,dy\right) dx = 0\, ,
$$
kovariance má tudíž hodnotu $\Cov(X,Y) = \E(XY)-\E X\E Y = 0 $ a
veličiny $X,Y$ jsou nekorelované.
\end{example}
\begin{remark}
Koeficient korelace je vlastně kovariance normovaná  na interval
$\langle-1,1\rangle$, což umožňuje lepší srovnání, a vyjadřuje
lineární závislost.

Velká absolutní hodnota $\varrho(X,Y)$ vyjadřuje velkou míru lineární závislosti náhodných veličin
$X,Y$.
 Nízká hodnota $\varrho(X,Y)$ říká, že veličiny $X,Y$ jsou "téměř nekorelované",
což může znamenat, že jsou nezávislé nebo, že jejich závislost je
odlišná od lineární.

Vysoká hodnota $\varrho(X,Y)$ navíc znamená, že se hodnoty obou
veličin vyvíjejí podobně, nemusí ale mezi nimi existovat příčinný
vztah!
\end{remark}

\begin{defin}
Nechť $\bm X = (X_1,\ldots, X_n)$ je náhodný vektor. Matici
$$
\Sigma = (\sigma_{ij})_{i,j=1}^n , \qmq{kde} \sigma_{ij} =
\Cov(X_i,X_j)\, ,
$$
nazveme kovariační maticí náhodného vektoru $\bm X$.
\end{defin}
\begin{remark}
Z definice vyplývá. že je matice $\Sigma$ symetrická, na diagonále
má rozptyly $\sigma_{ii}=\Var X_i$ náhodných veličin
$X_1,\ldots,X_n$ a pokud jsou tyto veličiny nezávislé je $\Sigma$
diagonální matice.
\end{remark}

\begin{exercise}
Nechť má náhodná veličina $X$ rovnoměrné rozdělení na intervalu
$(a,b)$, tj. $X\sim U(a,b)$. Určete střední hodnotu $\E X$ a
rozptyl $\Var X$.
\end{exercise}

\begin{exercise}
Spojitá náhodná veličina má trojúhelníkové rozdělení na intervalu
$\langle -a,a\rangle$ (hustota je rovnoramenný trojúhelník se
základnou $\langle -a,a\rangle$, mimo je nulová). Zapište
matematicky tuto hustotu a spočtěte $\E X, \Var X$.
\end{exercise}

\begin{exercise}
Váha tělesa se může se stejnou pravděpodobností rovnat každému
celému číslu od 1 do 10. Jsou k dispozici tři sady závaží o
hmotnostech
\begin{itemize}
  \item[a)] 1,2,2,5,10
  \item[b)] 1,2,3,4,10
  \item[c)] 1,1,2,5,10\, .
\end{itemize}
Závaží se dává jen na jednu misku dvoumiskových vah a používá se
co nejméně závaží.  Určete při které sadě je průměrný počet závaží
nutných k vyvážení minimální.
\end{exercise}

\begin{exercise}
Firma získá z každého prodaného výrobku 100 Kč. Za výměnu během
záruční doby zaplatí 300 Kč. Životnost výrobku v letech má
normální rozdělení $N(3,1)$. Jakou záruční dobu v měsících má
firma stanovit, aby střední (průměrný) zisk byl alespoň 60 Kč?
\end{exercise}

\begin{exercise}
Určete hodnotu konstanty $c$ a koeficient korelace diskrétních
náhodných veličin $X,Y$ zadaných sdruženou pravděpodobnostní
funkcí
$$
P[X=x,Y=y] = c\,(x+y), \qquad\mbox{pro}\qquad x=1,2,3
\quad\mbox{a}\quad y=1,2,3.
$$
[-1/23]
\end{exercise}

\begin{exercise}
Náhodný vektor $(X,Y)$ má hustotu
$$
f_{X,Y}(x,y) = \left\{
\begin{array}{lcc}
\frac{1}{6}\,\left( \frac{x}{2}+\frac{y}{3}\right) & & x\in (0, 2)
\mbox{ a }  y\in(0, 3)
\\[4mm]
0 & & \mbox{jinak .}
\end{array}\right.
$$
Určete marginální hustoty pravděpodobnosti $f_X, f_Y$ a spočtěte
koeficient korelace $\varrho(X,Y)$ náhodných veličin $X,Y$.
[-1/11]

\end{exercise}


\section{Kvantily náhodných veličin}

\begin{defin}
Nechť $X$ je náhodná veličina s distribuční funkcí $F_X$ a nechť
$\alpha\in(0,1)$. Potom bod $x_\alpha$ nazýváme $\alpha$-kvantilem
náhodné veličiny $X$, právě když platí
$$
x_\alpha = \inf \{x| F_X(x)\geq \alpha\}\, .
$$
Pokud je $F_X$ ostře rostoucí a spojitá, potom je $x_\alpha$
takový bod z $\RR$, že
$$
F_X(x_\alpha) = \alpha,\qmq{tj.} x_\alpha = F_X^{-1} (\alpha)\, .
$$
\end{defin}

 {\large\em Obrázky }

\begin{remark}
Některé kvantily mají vlastní názvy:
\begin{itemize}
  \item[] $x_{0.5}$ - medián (50\% hodnot n.v. leží vlevo od této hodnoty,
  50\% dat vpravo)
  \item[] $x_{0.25}$ - dolní kvartil
  \item[] $x_{0.75}$ - horní kvartil
  \item[] $x_{0.1}$ - dolní decil
  \item[] $(x_{0.75}-x_{0.25})$ - mezikvartilové rozpětí (délka
  intervalu, kde leží 50\% nejčastějších hodnot)\, .
\end{itemize}
\end{remark}

\begin{example}
Uvažujme náhodnou veličinu $X$ s hustotou pravděpodobnosti
$$
f_X(x)= \frac{1}{\pi} \,\frac{\theta}{\theta^2+(x-\mu)^2},\quad
\theta>0, \mu\in\RR, x\in\RR\, .
$$
Jedná se o takzvané Cauchyovo rozdělení. Není těžké ověřit, že
neexistuje střední hodnota náhodné veličiny $X$, neboť integrál
$$
\E X= \frac{1}{\pi} \int_{-\infty}^\infty \frac{\theta
x}{\theta^2+(x-\mu)^2}\, dx
$$
neexistuje. Tuto charakteristiku tedy nemůžeme použít. Distribuční
funkci, ale vyjádřit lze. Pro všechna $x\in\RR$ platí
\begin{eqnarray*}
F_X(x) &=& \frac{1}{\pi} \int_{-\infty}^x \frac{\theta
}{\theta^2+(t-\mu)^2}\, dt = \left|\begin{array}{c} \frac{t-\mu}{\theta}= y \\
\frac{1}{\theta}dt = dy \end{array}  \right| = \frac{1}{\pi}
\int_{-\infty}^{\frac{x-\mu}{\theta}} \frac{1}{1+y^2}\, dy \\
&=& \frac{1}{\pi} \, \mbox{arctg}\frac{x-\mu}{\theta} +
\frac{1}{2}\, .
\end{eqnarray*}
$\alpha$-kvantil tedy určíme z rovnosti
$$
F_X(x_\alpha)=\alpha, \qmq{tj.} \frac{1}{\pi} \,
\mbox{arctg}\frac{x_\alpha-\mu}{\theta} + \frac{1}{2} =\alpha
$$
a tedy
$$
x_\alpha = \theta \, \mbox{tg}
\left(\pi\left(\alpha-\frac{1}{2}\right)\right)+\mu\, .
$$
Speciálně pro medián platí $x_{0.5}= \mu$.
\end{example}

\section{Charakteristická a momentová funkce}



\begin{defin}
{Charakteristickou funkcí} náhodné veličiny $X$ rozumíme funkci $\varphi_X(t):\RR\mapsto \CC$ definovanou pro každé $t\in\RR$ předpisem
$$
\varphi_X(t) = \E e^{itX}.
$$
\end{defin}
\begin{remark}
Charakteristická funkce má následující vlastnosti:
\begin{itemize}
  \item $\varphi_X(t)$ existuje pro lib. n.v. $X$ a všechna $t\in\RR$
  \item $\varphi_X(0) = 1$ a $|\varphi_X(t)| \leq 1$, $\forall t \in \RR$
  \item Má-li $X$ ASR s hustotu $f(x)$, platí
  $$
  \varphi_X(t) = \int_{-\infty}^{\infty} f(x) e^{itx}\, dx\, ,
  $$
  jedná se tedy o Fourierovu transformaci $f(x)$. Tzn. že distribuční funkce $F_X$ je jednoznačně určena charakteristickou funkcí $\varphi_X$.
\end{itemize}
\end{remark}

\begin{defin}
Nechť má náhodná veličina $X$ konečné všechny momenty. Potom definujeme {momentovou vytvářející funkci (MVF)} n.v. $X$ předpisem
$$
{M_X(t) = \E e^{tX}}
$$
ve všech bodech $t$, kde střední hodnota existuje.
\end{defin}
\begin{remark}
Vlastnosti momentové vytvářející funkce:
\begin{itemize}
  \item nemusí existovat pro všechna $t\in\RR$, budeme předpokládat, že existuje na $(-s,s)$, kde $s > 0$,
  \item název od toho, že definuje všechny momenty n.v. $X$,
  \item užití MVF:
  \begin{enumerate}
    \item[a)] výpočet momentů n.v. $X$,
    \item[b)] určení rozdělení součtu nezávislých náhodných veličin.
  \end{enumerate}
\end{itemize}
\end{remark}

\begin{theorem}
Nechť pro náhodnou veličinu $X$ existuje MVF $M_X(t)$ na intervalu $(-s,s)$, $s>0$. Potom pro každé $k\in\NN$ platí
$$
\frac{d^k M_X}{d t^k}\biggr|_{t=0} = \mu_k^\prime (X) = \E X^k.
$$
\end{theorem}
\begin{proof}
Pro spojité náhodné veličiny viz přednáška.
\end{proof}

\begin{example}\label{MVFpoiss}
Určete MVF, střední hodnotu a rozptyl náhodné veličiny $X\sim Po(\lambda)$.\\
\smallskip
Pro pravděpodobnostní funkci náhodné veličiny $X$ platí
$$
\P [X=k] = e^{-\lambda} \frac{\lambda^k}{k!}, \quad k=0,1,2,\ldots.
$$
Pro MVF veličiny $X$ tedy s využitím Taylorova rozvoje funkce $e^z$ dostaneme
$$
M_X(t) = \E\left(e^{tX}\right) = \sum_{k=0}^{\infty} e^{tk} e^{-\lambda} \frac{\lambda^k}{k!} = e^{-\lambda} \sum_{k=0}^{\infty} \frac{(\lambda e^t)^k}{k!} =  e^{-\lambda}  e^{\lambda e^t} =  e^{\lambda (e^t-1)}\, .
$$
Pro první dvě derivace funkce $M_X(t)$ platí
$$
\frac{dM_X(t)}{dt} =  e^{\lambda (e^t-1)} \cdot \lambda e^t \qmq{a} \frac{d^2M_X(t)}{dt^2} =  e^{\lambda (e^t-1)} \left( \lambda^2 e^{2t} + \lambda e^t \right),
$$
dosazením $t=0$ dostaneme
$$
\E X = \left. \frac{dM_X(t)}{dt} \right|_{t=0} = \lambda \qmq{a} \E X^2 =  \left. \frac{d^2M_X(t)}{dt^2} \right|_{t=0} = \lambda^2 + \lambda.
$$
Pro rozptyl pak platí $\Var X = \E X^2 - (\E X)^2 = \lambda$.
\end{example}


\begin{example}
Určete momentovou vytvářející funkci náhodné veličiny $X\sim N(\mu,\sigma^2)$.\\
$$
M_X(t) = \E\left(e^{tX}\right) = \int_{-\infty}^{\infty} e^{tx} \frac{1}{\sqrt{2\pi\sigma^2}} e^{-\frac{(x-\mu)^2}{2\sigma^2}} \, dx = \frac{1}{\sqrt{2\pi\sigma^2}} \int_{-\infty}^{\infty} e^{tx -\frac{(x-\mu)^2}{2\sigma^2} }\, dx\, .
$$
Pomocí substituce $\frac{x-\mu}{\sigma} = y$ dostaneme
$$
M_X(t) = \frac{1}{\sqrt{2\pi}}  \int_{-\infty}^{\infty} e^{t(\sigma y+\mu) - \frac{y^2}{2}} \, dy = \frac{e^{t\mu}}{\sqrt{2\pi}}  \int_{-\infty}^{\infty} e^{t\sigma y- \frac{y^2}{2}} \, dy.
$$
Převedením na čtverce $t\sigma y- \dfrac{y^2}{2} = \dfrac{1}{2} t^2\sigma^2 - \dfrac{1}{2} (y-t\sigma)^2$ získáme
$$
M_X(t) =  e^{t\mu+\frac{1}{2} t^2\sigma^2}  \left(  \frac{1}{\sqrt{2\pi}}  \int_{-\infty}^{\infty} e^{-\frac{(y-t\sigma)^2}{2}} \,dy \right) = e^{t\mu+\frac{1}{2} t^2\sigma^2},
$$
neboť výraz v závorce je integrál z hustoty normálního rozdělení $N(t\sigma,1)$, který má hodnotu 1.
\end{example}

\begin{remark}
Pokud má $X$ všechny momenty a existuje její MVF $M_X$, potom $M_X$ jednoznačně definuje $F_X$ a naopak.
To znamená
\begin{itemize}
  \item  $X$ a $Y$ mají stejné rozdělení $\quad\Rightarrow\quad$ $X$ a $Y$ mají stejnou MVF,
  \item  $X$ a $Y$ mají stejnou MVF $\quad\Rightarrow\quad$ $X$ a $Y$ mají stejné rozdělení.
\end{itemize}
\end{remark}

\begin{theorem}
Nechť $M_X(t)$ je MVF náh. vel. $X$. Potom náhodná veličina {$Y=aX+b$}, $a,b\in\RR$, má MVF
$$
M_Y(t) = e^{bt} M_X(at)\, .
$$
\end{theorem}
\begin{proof}
$$
M_Y(t) = \E\left( e^{tY} \right) = \E\left( e^{t(aX+b)} \right) = \E\left(e^{tb}\cdot e^{(at)X} \right) = e^{bt} M_X(at)\, .
$$
\end{proof}

\begin{theorem}\label{MVFsoucet}
Nechť $X_1,\ldots,X_n$ jsou nezávislé náhodné veličiny mající na intervalu $(-s,s)$, $s>0$, MVF $M_{X_1},\ldots, M_{X_n}$. Potom náhodná veličina {$Y=X_1+X_2+\ldots +X_n$} má MVF
$$
M_Y(t) = M_{X_1}(t) \cdot M_{X_2}(t) \cdots M_{X_n}(t) = \prod_{j=1}^{n} M_{X_j}(t)\, .
$$
\end{theorem}
\begin{proof}
Pro MVF náhodné veličiny $Y$ platí
$$
M_Y(t) = \E\left( e^{t(X_1+X_2+\cdots+X_n)} \right) = \E\left( e^{tX_1} \cdot e^{tX_2} \cdots e^{tX_n} \right) .
$$
Dále nezávislost náhodných veličin $X_1,\ldots,X_n$ implikuje nezávislost veličin $e^{tX_1},\ldots, e^{tX_n}$. S využitím vlastnosti střední hodnoty součinu nezávislých náhodných veličin dostaneme
$$
M_Y(t) =  \E\left( e^{tX_1} \right) \cdot \E\left( e^{tX_2} \right)\cdots \E\left( e^{tX_n} \right) = M_{X_1}(t) \cdot M_{X_2}(t) \cdots M_{X_n}(t)\ .
$$

\end{proof}

\begin{example}
Nechť $X_1,\ldots,X_n$ jsou nezávislé náh. vel. a $X_j\sim Po(\lambda_j)$, $j\in \hat n$. Najděte rozdělení n. vel. $Y=\sum_{j=1}^{n} X_j$.\\
\smallskip
V příkladu \ref{MVFpoiss} jsme ukázali, že
$$
M_{X_j}(t) = e^{\lambda_j (e^t-1)}\, .
$$
Použitím věty \ref{MVFsoucet} dostaneme
$$
M_Y(t) = \prod_{j=1}^{n} e^{\lambda_j (e^t-1)} = e^{(\sum_{j=1}^{n} \lambda_j) \, (e^t-1)},
$$
což je MVF Poissonova rozdělení s parametrem $\sum_{j=1}^{n} \lambda_j$. Protože rozdělení náhodné veličiny $Y$ je jednoznačně určeno její MVF, platí $Y\sim Po(\sum_{j=1}^{n} \lambda_j)$.
\end{example}



\begin{exercise}
Určete MVF, střední hodnotu a rozptyl náhodné veličiny $X\sim Gamma(\alpha,\beta)$.
\end{exercise}

\begin{exercise}
\ \\
1. Nechť $X_1\sim N(\mu_1,\sigma_1^2)$, $X_2\sim N(\mu_2,\sigma_2^2)$ a veličiny $X_1,X_2$ jsou nezávislé. Určete rozdělení náh. vel. $Y=X_1+X_2$. \\
2. Nechť $X_1,\ldots,X_n$ jsou nezávislé a $X_i\sim N(\mu,\sigma^2)$, $i=1,\ldots,n$. Určete rozdělení $Y = \sum_{i=1}^{n} X_i$.
\end{exercise}



\begin{exercise}
\ \\
1. Určete MVF, střední hodnotu a rozptyl náhodné veličiny $X\sim Exp(\theta)$.\\
2. Nechť $X_1,\ldots,X_n$ jsou nezávislé a $X_i\sim Exp(\theta)$, $i=1,\ldots,n$. Určete rozdělení $Y = \sum_{i=1}^{n} X_i$.
\end{exercise}



\begin{exercise}
Určete rozdělení (hustotu) náhodné veličiny $Y=X_1^2+X_2^2+\cdots  + X_n^2$, kde $X_1,\ldots,X_n$ jsou nezávislé a $X_i\sim N(0,1)$, $i=1,\ldots,n$.
\end{exercise}



\begin{exercise}
\ \\
1. Určete MVF náhodné veličiny $X\sim Bi(n,p)$.\\
2. Nechť $X_1,\ldots,X_n$ jsou nezávislé a $X_i\sim Be(p)$, $i=1,\ldots,n$. Určete rozdělení $Y = \sum_{i=1}^{n} X_i$.
\end{exercise}













