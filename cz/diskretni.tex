
\chapter{Diskrétní náhodné veličiny}

\begin{defin}
Říkáme, že náhodná veličina $X$ má diskrétní rozdělení, jestliže
existuje konečná nebo spočetná množina reálných čísel
$H=\{x_1,x_2,\ldots,x_n,\ldots\}$ taková, že pro všechna $x_j\in
H$
$$
\P[X=x_j]> 0 \qmq{a} \sum_{x_j\in H} \P[X=x_j]= 1\, .
$$
\end{defin}
\begin{remark}
Jinými slovy, obor hodnot náhodné veličiny $X$ je nejvýše spočetná
množina.
\end{remark}
\begin{defin}
Funkci $P(x)=\P[X=x]$ definovanou pro všechna $x\in\RR$ nazveme
pravděpodobnostní funkcí náhodné veličiny $X$.
\end{defin}
\begin{remark}
V bodech, kde je $P(x)$ nenulová budeme její hodnoty značit
$$
P(x_i) = \P[X=x_i] \triangleq p_i\, .
$$
\end{remark}
\begin{remark}
Diskrétní náhodné veličiny se obvykle zadávají pravděpodobnostními
funkcemi ve formě tabulky, např.
\begin{center}
\begin{tabular}{c|c|c|c|c}
    $x_i$ & 0 & 1 & 2 & 3 \\ \hline
  $p_i$ & 0.25 & 0.3 & 0.1 & 0.35 \\
\end{tabular}
\end{center}
Distribuční funkce diskrétní náhodné veličiny má tvar
$$
F_X(x)= \sum_{j:x_j\leq x} \P[X=x_j] = \sum_{j:x_j\leq x} p_j
\qmq{pro} x\in\RR\, .
$$
Z toho tedy vyplývá, že distribuční funkce $F_X(x)$ má skoky v
bodech $x_1, x_2, \ldots$ a je konstantní v intervalech $\langle
x_j,x_{j+1})$, $j=1,2,\ldots$. Navíc velikost skoku v bodě $x_j$
je rovna $\P[X=x_j]$. Můžeme tedy psát
$$
\P[X=a] = P(a),\qquad \P[X<a] = F(a)-P(a), \qquad \P[X\leq a] =
F(a),
$$
$$
\P[X>a]=1-F(a),\qquad \P[X\geq a] = 1-F(a)+P(a)\, .
$$
{\em\large Obrázek}
\end{remark}
\begin{remark}
Pokud jsou oborem hodnot náhodné veličiny $X$ nezáporná celá čísla
$\{0,1,2,\ldots \}$, je možno zapsat distribuční funkci ve tvaru
$$
F_X(x) = \sum_{n=0}^{[x]} \P[X=n]\, ,
$$
kde symbol $[x]$ označuje celou část z čísla $x$.
\end{remark}

\begin{exercise}
Automobil má na trase 4 semafory. Ty dávají s pravděpodobností 2/3
zelenou nebo s pravděpodobností 1/3 červenou (pracují nezávisle).
Určete rozdělení pravděpodobnosti náhodné veličiny udávající počet
semaforů, které automobil pojede před prvním zastavením.
\end{exercise}

\begin{exercise}
Terč je rozdělen na tři oblasti očíslované 1,2,3. Za zásah oblasti
1 získává střelec 10 bodů, za oblast 2 získává 5 bodů a za oblast
3 dostává -1 bod. Pravděpodobnosti zásahu střelce jsou po řadě
0.5, 0.3, 0.2. Určete rozdělení pravděpodobnosti náhodného počtu
bodů získaného při dvou výstřelech.
\end{exercise}



\section{Příklady diskrétních rozdělení}
\subsection{Alternativní rozdělení (Bernoulliovo)}
Toto rozdělení slouží pro popis náhodných situací majících dva
možné výsledky (ano $\times$ ne, správně $\times$ špatně).
Například hod mincí, kostkou (když nás zajímá pouze sudost,
lichost). Obvykle se těmto výsledkům přiřazují hodnoty 0
(reprezentující neúspěch s předpokládanou pravděpodobností $1-p$)
a 1 (úspěch s pravděpodobností $p$).
\begin{defin}
Řekneme, že náhodná veličina $X$ má alternativní rozdělení s
parametrem $p\in(0,1)$, jestliže platí
$$
\P[X=1]=p, \qquad \P[X=0]=1-p\, .
$$
\end{defin}
\begin{remark}
Pro vyjádření toho, že náhodná veličina $X$ má alternativní
rozdělení budeme používat označení
$$
X\sim Be(p)\, .
$$
Pravděpodobnostní funkce alternativního rozdělení se dá obecně
zapsat ve tvaru
$$
P(x)=\P[X=x] =
  \begin{cases}
    p^x(1-p)^{1-x} & \text{pro } x\in\{0,1\}, \\
    0 & \text{jinak}.
  \end{cases}
$$
\end{remark}


\subsection{Binomické rozdělení}
Uvažujme náhodnou veličinu $X$ vyjadřující počet výskytů jevu $A$
v $n$ nezávislých pokusech. Jestliže pravděpodobnost jevu $A$ je
pokaždé rovna $p$, pravděpodobnost, že jev $A$ nastane z $n$
pokusů právě $k$-krát je
$$
p_k=\P[X=k] = \binom{n}{k} p^k(1-p)^{n-k}\, .
$$
Protože náhodná veličina $X$ může nabývat hodnot $0,1,\ldots n$ a
podle binomické věty platí
$$
\sum_{k=0}^n \binom{n}{k} p^k(1-p)^{n-k} = (p+1-p)^n =1\, ,
$$
jedná se o rozdělení pravděpodobnosti.
\begin{defin}
Řekneme, že náhodná veličina $X$ má binomické rozdělení s
parametry $n\in\NN$ a $p\in(0,1)$, jestliže
$$
\P[X=k] = \binom{n}{k} p^k(1-p)^{n-k} \qmq{pro} k=0,1,\ldots, n\,
.
$$
Značíme $X\sim Bi(n,p)$.
\end{defin}
\begin{remark}
Platí, jestliže $X_j, j\in\wh n$, jsou nezávislé náhodné veličiny s
alternativním rozdělením s parametrem $p$, potom má náhodná veličina
$X$ definovaná předpisem
$$
X\triangleq X_1+X_2+\ldots +X_n
$$
binomické rozdělení s parametry $n$ a $p$.
\end{remark}
\begin{example}
Uvažujme šachovou partii, ve které jsou dva stejně silní soupeři
(pravděpodobnost výhry i prohry je 1/2). Rozhodněte, zda je
pravděpodobnější
\begin{itemize}
  \item[a)] vyhrát 3 partie ze 4, nebo 5 partií z 8,
  \item[b)] vyhrát alespoň 3 partie ze 4, nebo alespoň 5 partií z
  8.
\end{itemize}
Zaměřme se na jednoho hráče a označme $X$ náhodnou veličinu
vyjadřující počet jeho výher ze 4 partií a $Y$ počet výher z 8 partií. Potom\\
a)
$$
\P[X=3] =
\binom{4}{3}\left(\frac{1}{2}\right)^3\frac{1}{2}=\frac{1}{4}
$$
a
$$
\P[Y=5]
=\binom{8}{5}\left(\frac{1}{2}\right)^5\left(\frac{1}{2}\right)^3=\frac{7}{32}
$$
b)
$$
\P([X=3]\cup[X=4])= \P[X=3]+\P[X=4]=\frac{1}{4} +
\binom{4}{4}\left(\frac{1}{2}\right)^4=\frac{5}{16}
$$
a
$$
\P([Y=5]\cup[Y=6]\cup[Y=7]\cup[Y=8])=\sum_{k=5}^8
\binom{8}{k}\left(\frac{1}{2}\right)^k
\left(\frac{1}{2}\right)^{8-k} = \frac{93}{256}\, .
$$
\end{example}

\subsection{Pascalovo (geometrické) rozdělení}

\begin{defin}
Řekneme, že náhodná veličina $X$ má Pascalovo rozdělení s
parametrem $p\in(0,1)$, jestliže
$$
\P[X=k] = p\cdot (1-p)^k \qmq{pro} k=0,1,2,\ldots\, .
$$
Značíme $X\sim Geom(p)$.
\end{defin}
\begin{remark}
Uvažujme nekonečnou posloupnost Bernoulliovských pokusů (jev $A$
nastává s pravděpodobností $p$, nenastává s pravděpodobností
$1-p$). Náhodná veličina $X\sim Geom(p)$ potom vyjadřuje počet
pokusů před prvním výskytem jevu $A$.

Ukažme, že se skutečně jedná o pravděpodobnostní rozdělení.
$$
\sum_{k=0}^\infty \P[X=k] = \sum_{k=0}^\infty p (1-p)^k =
\frac{p}{1-(1-p)}=1\, .
$$
Pro Pascalovo rozdělení je také možno jednoduše vyjádřit
distribuční funkci
$$
F_X(x)=\sum_{k=0}^{[x]}p(1-p)^k =  p\cdot
\frac{1-(1-p)^{[x]+1}}{1-(1-p)}= 1-(1-p)^{[x]+1}, \quad x\in\RR.
$$
\end{remark}
\begin{example}
Uvažujme začátek hry člověče nezlob se, kdy hráč nasadí figurku do
hry až mu padne šestka. Jaká je pravděpodobnost, že hráč nasadí ve
čtvrtém, resp. nejpozději ve čtvrtém kole?

Označme $X$ počet pokusů, než padne šestka. Potom $X\sim Geom(1/6)$
a platí
$$
\P[X=3] = \frac{1}{6}\cdot \left(1-\frac{1}{6}\right)^3
=\frac{1}{6}\cdot \left(\frac{5}{6}\right)^3\doteq 0.0965
$$
a
$$
\P[X\leq 3] = F_X(3) = 1-\left(\frac{5}{6}\right)^4 \doteq
0.5177\, .
$$
\end{example}

\subsection{Poissonovo rozdělení}
\begin{defin}
Řekneme, že náhodná veličina $X$ má Poissonovo rozdělení s
parametrem $\lambda>0$, jestliže
$$
\P[X=k] =
  \begin{cases}
    e^{-\lambda}\fra{\lambda^k}{k!} & \text{pro } k=0,1,2,\ldots \\
    0 & \text{jinak}.
  \end{cases}
$$
Značení $X\sim Po(\lambda)$.
\end{defin}
\begin{remark}
Ověřme, že se jedná o rozdělení pravděpodobnosti,
$$
\sum_{k=0}^\infty e^{-\lambda}\fra{\lambda^k}{k!} =
e^{-\lambda}\sum_{k=0}^\infty \fra{\lambda^k}{k!} =
e^{-\lambda}\cdot e^{\lambda}=1.
$$
\end{remark}
\begin{remark}
Poissonovo rozdělení bylo odvozeno při popisu radioaktivního
rozpadu. Použití:
\begin{description}
  \item[a)] Při popisu počtu událostí, které nastanou během
  časového intervalu ($\lambda$ - průměrný počet událostí za
  časový interval). Např. Počet signálů v telefonní ústředně.\\
  \noindent Při popisu počtu částic v jednotce plochy nebo objemu ($\lambda$
  - průměrný počet částic v jednotce). Např. počet částic v zorném
  poli mikroskopu.
  \item[b)] Aproximace binomického rozdělení\\
  Uvažujme posloupnost náhodných veličin $X_1,X_2,X_3,\ldots$, kde
  $X_i\sim Bi(n,p_n)$. Nechť $np_n = \lambda$, kde $\lambda>0$ je
  parametr. Předpoklad říká, že relativní počet "příznivých" výsledků mezi
  $n$ pokusy by "se měl pohybovat kolem čísla $\lambda$". Mluví
  pouze o dlouhodobém chování, nikoli o jedné realizaci pokusu.\\
  Není problém spočítat rozdělení $X_i$ pro malá $i$, pro $i$
  velká ($i=10000$) je již výpočet problematický. a právě tady nám
  pomůže Poissonovo rozdělení.
\end{description}
\end{remark}

\begin{theorem}
Nechť $(X_n)_{n=1}^\infty$ je posloupnost náhodných veličin
taková, že $X_n\sim Bi(n,p_n)$. Nechť
$$
\lim_{n\to\infty}p_n=0\qmq{a} \lim_{n\to\infty}n p_n=\lambda,\quad
\lambda\in\RR^+.
$$
Potom platí
$$
\lim_{n\to\infty} \binom{n}{k}p_n^k(1-p_n)^{n-k} =
e^{-\lambda}\fra{\lambda^k}{k!},\quad k=0,1,2,\ldots
$$
(neboli $\lim_{n\to\infty}\P[X_n=k]=\P[X=k]$, kde $X\sim
Po(\lambda$)).
\end{theorem}
\begin{proof}
Rozepišme člen za limitou do tvaru
$$
 \binom{n}{k}p_n^k(1-p_n)^{n-k} = \frac{n(n-1)\ldots
 (n-k+1)}{k!}\,\fra{p_n^k}{(1-p_n)^k}\, (1-p_n)^n\, .
$$
Limitu výrazu na pravé straně budeme zkoumat po částech. Protože
podle předpokladů $np_n\to\lambda$, můžeme psát $p_n=\lambda/n
+z_n$, přičemž $nz_n\to 0$. Potom
$$
(1-p_n)^n = \left( 1-\frac{\lambda+nz_n}{n}\right)^n =
\left[\left(1+\fra{1}{\frac{n}{-(\lambda+nz_n)}}\right)^{\frac{n}{-(\lambda+nz_n)}}\right]^{-(\lambda+nz_n)}\longrightarrow
e^{-\lambda},
$$
pro $n\to\infty$, kde jsme využili faktu, že $(1+1/p_n)^{p_n} \to
e$ pro $|p_n|\to\infty$.

Dále
$$
\left(\frac{p_n}{1-p_n}\right)^k =
\left(\frac{\lambda/n+z_n}{1-\lambda/n-z_n}\right)^k =
\left(\frac{\lambda+nz_n}{n-\lambda-nz_n}\right)^k\, ,
$$
kde
$$
(\lambda+nz_n)^k \to \lambda^k
$$
a všechny zbývající členy mají limitu
$$
\frac{n(n-1)\ldots(n-k+1)}{k!\, (n-\lambda-nz_n)^k}
\longrightarrow \frac{1}{k!},
$$
protože se jedná o limitu typu polynom stupně $k$ lomeno polynom
stupně $k$. Celkem tedy dostáváme tvrzení věty.
\end{proof}
\begin{remark}
Pro malá $p$ a velká $n$ tedy můžeme aproximovat rozdělení
$Bi(n,p)$ rozdělením $Po(np)$. Tato aproximace se často doporučuje
pro $p<0.1$ a $n>30$. Např. hodnoty pravděpodobnosti náhodné
veličiny $Y\sim Bi(1000,0.0015)$ můžeme aproximovat
pravděpodobnostmi náhodné veličiny s rozdělením $Po(1.5)$. Pro
ilustraci zjednodušení výpočtu vyjádřeme
$$
\P[Y=5] = \binom{1000}{5} (0.0015)^5 (0.9985)^{995} \doteq
e^{-1.5}\frac{(1.5)^5}{5!}\, .
$$
\end{remark}
\begin{example}
Pravděpodobnost závady na jeden kilometr vodiče je 0.01. Kabel je
složen ze 100 vodičů, funkční bude pokud je 99 vodičů v pořádku.
Jaká je pravděpodobnost, že kabel délky jeden kilometr bude
fungovat?

Označme $X$ počet chybných vodičů, tedy $X\sim Bi(100,0.01)$ a $A$
jev, že kabel bude fungovat. Potom
$$
\P(A) = \P[X\leq 1] = \P[X=0] + \P[X=1] = \binom{100}{0}
0.01^0\cdot 0.99^{100} + \binom{100}{1} 0.01^1\cdot
0.99^{99}=0.736\, .
$$

Protože je $n$ velké a $p$ malé, můžeme také příslušné
pravděpodobnosti aproximovat pomocí Poissonova rozdělení s
parametrem $\lambda=np=1$,
$$
\P[X=0] + \P[X=1] = e^{-1}\frac{1}{0!} +  e^{-1}\frac{1}{1!}=
2e^{-1} = 0.736\, .
$$
\end{example}
\begin{example}
Pokusy se zjistilo, že radioaktivní látka vyzářila během 7.5
sekundy průměrně 3.87 $\alpha$-částice. Určete pravděpodobnost
toho, že za 1 sekundu vyzáří tato látka alespoň 1
$\alpha$-částici.

Označme $X$ počet částic vyzářených za 1 sekundu. Protože průměrný
počet částic vyzářených za 1 sekundu je $\lambda = 3.87/7.5 =
0.516$, můžeme náhodnou veličinu $X$ popisovat pomocí rozdělení
$Po(0.516)$ a tedy
$$
\P[X\geq 1] = 1-\P[X=0] = 1-e^{-0.516}\frac{0.516^0}{0!}\doteq
0.403\, .
$$
\end{example}
\begin{exercise}
Určete pravděpodobnost, že mezi 200 výrobky jsou alespoň 4 zmetky,
jestliže zmetky tvoří průměrně $1\%$ produkce. [0.143]
\end{exercise}

\begin{exercise}
Během hodiny přijde na telefonní ústřednu průměrně  60 výzev. Jaká
je pravděpodobnost toho, že během půl minuty, na kterou se
manipulantka vzdálila, nebude nikdo volat? [0.61]
\end{exercise}

\begin{exercise}
Korektura 500 stránek obsahuje 1500 tiskových chyb. Určete
pravděpodobnost toho, že na namátkou vybrané stránce jsou alespoň
3 chyby. [0.577]
\end{exercise}




\section{Diskrétní náhodné vektory}
\begin{defin}
Řekneme, že náhodný vektor $\bm{X}=(X_1,X_2,\ldots,X_n)$ je
diskrétní, jestliže existuje konečná nebo spočetná množina $n$-tic
$\{(x_{11}, x_{21},\ldots, x_{n1}), (x_{12}, x_{22},\ldots,
x_{n2}),\ldots \}$ taková, že
$$
P(x_{1i}, x_{2i},\ldots, x_{ni}) = \P[X_1=x_{1i},
X_2=x_{2i},\ldots,X_n=x_{ni}]>0 \qmq{pro všechna } i=1,2,\ldots
$$
a
$$
\sum_i P(x_{1i}, x_{2i},\ldots, x_{ni}) = 1\, .
$$
Funkci $P(x_{1}, x_{2},\ldots, x_{n})$ nazýváme pravděpodobnostní
funkcí náhodného vektoru $\bm X$.
\end{defin}

\subsection{Multinomické rozdělení}
\begin{defin}
Řekneme, že náhodný vektor $\bm{X}=(X_1,X_2,\ldots,X_k)$  má
multinomické rozdělení s parametry $n, p_1,\ldots ,p_k$, kde
$n\in\NN$, $0<p_j<1$, $j\in\wh k$ a $\sum_{j=1}^k p_j =1 $,
jestliže
$$
\P[X_1=x_1,\ldots, X_k=x_k] = \fra{n!}{x_1! x_2!\ldots x_k!}\,
p_1^{x_1} p_2^{x_2}\ldots p_k^{x_k}
$$
pro $x_j=0,1,\ldots n, j\in\wh k$,  a $ \sum_{j=1}^k x_j = n$ a je
nulová jinak.
\end{defin}
\begin{remark}
Název tohoto rozdělení pochází z toho, že $\P[X_1=x_1,\ldots,
X_k=x_k]$ je obecný člen rozvoje $(p_1+p_2+\ldots+p_k)^n$.
Multinomické rozdělení je vlastně zobecněním binomického: uvažujme
$n$ nezávislých pokusů, z nichž v každém nastane právě jeden z $k$
vzájemně disjunktních jevů $A_1,A_2,\ldots, A_k$. Přitom,
předpokládáme, že pravděpodobnost, že v $i$-tém pokusu nastane jev
$A_j$ je $p_j$ a $\sum_{j=1}^k p_j= 1$. Binomické rozdělení je
tedy speciálním případem multinomického pro $k=2$.
\end{remark}
\begin{example}
U výrobku měříme znak $Z$. Výrobek je dobrý, jestliže $T_D\leq
Z\leq T_H$. Uvažujme jevy $A_1: Z<T_D$, $A_2: T_D\leq Z\leq T_H$ a
$A_3: Z>T_H$. Za normálních podmínek je známo, že
$$
\P(A_j) = p_j,\quad j=1,2,3 \qmq{přičemž} \sum_{j=1}^3 p_j =1\, .
$$
Potom pravděpodobnost, že z $n$ výrobků jich bude mít $x_1$
$Z<T_D$, $x_2$ hodnotu $T_D\leq Z\leq T_H$ a $x_3$ hodnotu $Z>T_H$
je rovna
\begin{eqnarray*}
\P[X_1=x_1,X_2=x_2,X_3=x_3] &=& \frac{n!}{x_1! x_2! x_3!}\,
p_1^{x_1} p_2^{x_2}p_3^{x_3}\\
&=&  \frac{n!}{x_1! x_2! (n-x_1-x_2)!}\, p_1^{x_1}
p_2^{x_2}(1-p_1-p_2)^{n-x_1-x_2}\, .
\end{eqnarray*}
Např. pro $p_1=0.02, p_2=0.95, p_3=0.03$ a $n=6$
$$
\P[X_1=1,X_2=4,X_3=1] = \frac{6!}{1!4!1!}\, 0.02\cdot 0.95^4\cdot
0.03 = 0.015\, .
$$
\end{example}


\subsection{Vlastnosti diskrétních náhodných vektorů}
Uvažujme diskrétní náhodný vektor $(X_1,X_2)$. Potom sdružená
distribuční funkce  tohoto vektoru je dána předpisem
$$
F_{X_1,X_2}(x_1,x_2) = \sum_{t_1\leq x_1}\sum_{t_2\leq x_2}
\P[X_1=t_1,X_2=t_2]\, .
$$
\begin{theorem}\label{margdis}
Je-li $\P[X_1=x_1,X_2=x_2]$ sdružená pravděpodobnostní funkce
náhodného vektoru $(X_1,X_2)$, potom pro marginální
pravděpodobnostní funkce náhodných veličin $X_1$ a $X_2$ platí
$$
\P[X_1=x_1] = \sum_{x_2} \P[X_1=x_1,X_2=x_2] \qmq{pro všechna}
x_1\in\RR
$$
a
$$
\P[X_2=x_2] = \sum_{x_1} \P[X_1=x_1,X_2=x_2] \qmq{pro všechna}
x_2\in\RR\, .
$$
\end{theorem}
\begin{proof}
Pro všechna $x_1\in\RR$ platí
\begin{eqnarray*}
F_{X_1}(x_1) &=& \lim_{x_2\to\infty} F_{X_1,X_2}(x_1,x_2) =
\lim_{x_2\to\infty} \sum_{t_1\leq x_1} \sum_{t_2\leq x_2}
\P[X_1=t_1,X_2=t_2]\\
& =& \sum_{t_1\leq x_1} \left( \sum_{t_2}
\P[X_1=t_1,X_2=t_2]\right)\, ,
\end{eqnarray*} ale podle definice
platí také pro všechna $x_1\in\RR$
$$
F_{X_1}(x_1) = \sum_{t_1\leq x_1}\P[X_1=t_1]\, .
$$
Protože obě sumy pro $t_1\leq x_1$ mají stejné hodnoty pro všechna
$x_1 \in\RR$, musí být shodné i jejich argumenty, tedy
$$
\P[X_1=t_1] = \sum_{t_2} \P[X_1=t_1,X_2=t_2] \qmq{pro všechna }
t_1\in\RR\, .
$$
Stejně se ukáže i druhá část tvrzení.
\end{proof}

\begin{theorem}
Dvě diskrétní náhodné veličiny $X,Y$ jsou nezávislé právě tehdy,
když
$$
\P[X=x,Y=y]= \P[X=x]\cdot \P[Y=y] \qmq{pro všechny}
(x,y)\in\RR^2\, .
$$
\end{theorem}
\begin{proof}
Podle Věty \ref{nez} víme, že dvě náhodné veličiny jsou nezávislé
právě tehdy, když
$$
F_{X,Y}(x,y) = F_X(x)\cdot F_Y(y) \quad\forall (x,y)\in\RR^2\, .
$$
Tvrzení této věty dostaneme přímo přechodem ke skokům distribuční
funkce.
\end{proof}
\begin{example}
Mějme dvě diskrétní náhodné veličiny zadané sdruženou
pravděpodobnostní funkcí $\P[X_1=x_1,X_2=x_2]$ ve formě tabulky
\begin{center}
\begin{tabular}{c|ccc}
  $X_1\backslash X_2$ & 0 & 1 & 2 \\ \hline
  0 & 0.42 & 0.12 & 0.06 \\
  1 & 0.28 & 0.08 & 0.04 \\
\end{tabular}
\end{center}
Určete marginální pravděpodobnostní funkce a rozhodněte, zda jsou
náhodné veličiny $X_1,X_2$ nezávislé.

Podle Věty \ref{margdis} platí pro pravděpodobnostní funkci
náhodné veličiny $X_1$
$$
\P[X_1=0] = \sum_{k=0}^2 \P[X_1=0,X_2=k] = 0.42+0.12+0.06 = 0.6
$$
a
$$
\P[X_1=1] = \sum_{k=0}^2 \P[X_1=1,X_2=k] = 0.28+0.08+0.04 = 0.4\,
.
$$
Podobně dostaneme hodnoty pravděpodobnostní funkce náhodné
veličiny $X_2$,
$$
\P[X_2=0]=0.7,\quad \P[X_2=1]=0.2, \quad \P[X_2=2]=0.1\, .
$$
Jak je vidět, hodnoty marginálních pravděpodobnostních funkcí
získáme součtem příslušných řádků nebo sloupců a je výhodné je
zapisovat přímo do tabulky
\begin{center}
\begin{tabular}{c|ccc|c}
  $X_1\backslash X_2$ & 0 & 1 & 2 & $\P_{X_1}$ \\ \hline
  0 & 0.42 & 0.12 & 0.06 & {\bf 0.6}\\
  1 & 0.28 & 0.08 & 0.04 & {\bf 0.4}\\ \hline
  $\P_{X_2}$ & {\bf 0.7} & {\bf 0.2} & {\bf 0.1} & 1
\end{tabular}
\end{center}
Z této tabulky je snadno vidět, že pro všechna $i\in\{0,1\}$ a
$j\in\{0,1,2\}$ platí
$$
\P[X_1=i,X_2=j]= \P[X_1=i]\cdot \P[X_2=j]
$$
a tedy veličiny $X_1,X_2$ jsou nezávislé.
\end{example}



\begin{defin}
Nechť $(X_1, X_2)$ je diskrétní náhodný vektor se sdruženou
pravděpodobnostní funkcí $\P[X_1=x_1,X_2=x_2]$. Jestliže
$\P[X_2=x_2]>0$, definujeme podmíněnou pravděpodobnostní funkci
náhodné veličiny $X_1$ za podmínky, že veličina $X_2$ nabyla
hodnoty $x_2$, výrazem
$$
\P[X_1=x_1|X_2=x_2] =
\fra{\P[X_1=x_1,X_2=x_2]}{\P[X_2=x_2]}=\fra{\P[X_1=x_1,X_2=x_2]}{\sum_{x_1}
\P[X_1=x_1,X_2=x_2]} \, .
$$
\end{defin}
\begin{example}
Házíme dvěma kostkami. Najděte rozdělení náhodné veličiny
označující maximum z počtu ok padlých na obou kostkách, jestliže
víme, že minimum nabylo hodnotu 3.

Označme $X$ maximum a $Y$ minimum počtu ok na dvou kostkách. $Y$
nabývá hodnotu 3, jestliže na první kostce padla hodnota z množiny
$\{4,5,6\}$ a na druhé z množiny $\{3\}$, nebo na první $\{3\}$ a
na druhé $\{4,5,6\}$ nebo na obou kostkách $\{3\}$. Celkem je to 7
možností z 36, takže $\P[Y=3]=7/36$. Pokud $Y=3$, může náhodná
veličina $X$ nabývat hodnot $\{3,4,5,6\}$ a
$$
\P[X=3,Y=3]=\frac{1}{36} \qmq{a} \P[X=i,Y=3]=\frac{2}{36},
\qmq{pro} i=4,5,6\, .
$$
Podle definice podmíněné pravděpodobnosti tedy platí
$$
\P[X=3|Y=3] = \frac{1/36}{7/36} = \frac{1}{7} \qmq{a} \P[X=i|Y=3]
= \frac{2/36}{7/36} = \frac{2}{7}, \qmq{pro} i=4,5,6\, .
$$
\end{example}

