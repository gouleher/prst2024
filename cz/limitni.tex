
\chapter{Limitní věty}
\section{Zákon velkých čísel}

Dosud jsme se zabývali jednotlivými náhodnými veličinami. V této
kapitole budeme studovat posloupnosti náhodných veličin. Příklady
takových posloupností zajímavých prakticky i teoreticky:
\begin{itemize}
  \item Nechť $Z_n$ označuje počet výskytů jistého jevu  $A$ v $n$
  nezávislých pokusech, přičemž pravděpodobnost jevu $A$ v jednotlivých pokusech je $p$.
  Označme $X_n=Z_n/n$, $X_n$ tedy vyjadřuje relativní četnost jevu
  $A$ v $n$ pokusech. Jak se chová $X_n$ pro velká $n$?
  \item Uvažujme $n$ nezávislých měření $X_1,\ldots,X_n$ a jejich
  průměr $\overline X_n = 1/n \sum_{i=1}^n X_i$. Má rozdělení
  náhodné veličiny $\overline X_n$ pro velká $n$ nějaké zajímavé
  vlastnosti? Závisí na rozdělení veličin $X_i$?
\end{itemize}

\begin{defin}
Nechť $(X_n)_{n=1}^\infty$ je posloupnost náhodných veličin a
$c\in\RR$. Říkáme, že posloupnost $(X_n)_{n=1}^\infty$ konverguje
k $c$ podle pravděpodobnosti, jestliže pro každé $\ve>0$ platí
$$
\lim_{n\to\infty} \P [|X_n-c|\geq \ve] = 0\, .
$$
Značíme $X_n\stP c$.
\end{defin}
\begin{example}
Jsou dány náhodné veličiny $X_n\sim N(\mu,\sigma^2/n)$ Dokažte, že
$X_n\stP \mu$.

Podle definice máme ukázat, že pro každé $\ve>0$
$$
 \P [|X_n-\mu|\geq \ve] \to 0\, ,
$$
což je ekvivalentní tvrzení, že pro každé $\ve>0$
$$
\P [|X_n-\mu|< \ve] \to 1\, .
$$
Vyjádřeme tuto pravděpodobnost. Pro každé $\ve>0$ platí
\begin{eqnarray*}
\P [|X_n-\mu|< \ve] &=& \P [\mu-\ve <X_n<\mu+\ve] =
\int_{\mu-\ve}^{\mu+\ve} \frac{1}{\sqrt{2\pi\frac{\sigma^2}{n}}}\,
e^{-\frac{n(x-\mu)^2}{2\sigma^2}}\, dx \\
&=& \left|\begin{array}{c} \frac{x-\mu}{\sigma/\sqrt{n}}= t \\
\frac{1}{\sigma/\sqrt{n}}dx = dt \end{array}  \right| =
\int_{-\frac{\sqrt{n}\ve}{\sigma}}^{\frac{\sqrt{n}\ve}{\sigma}}
\frac{1}{\sqrt{2\pi}}\, e^{-\frac{t^2}{2}}\, dt
\stackrel{n\to\infty}{\longrightarrow} \int_{-\infty}^\infty
\frac{1}{\sqrt{2\pi}}\, e^{-\frac{t^2}{2}}\, dt = 1\, .
\end{eqnarray*}
\end{example}

\begin{theorem}
(Čebyševova nerovnost) Nechť $X$ je náhodná veličina se střední
hodnotou $\E X$ a konečným rozptylem $\Var X$. Potom pro každé
$\ve >0$ platí
$$
\P[|X-\E X|\geq \ve]\leq \frac{\Var X}{\ve^2}\, .
$$
\end{theorem}
\begin{proof}
Proveďme důkaz pro spojité náhodné veličiny. Uvažujme nejprve
náhodnou veličinu $Y$ s konečným druhým momentem $\E Y^2$. Potom
pro každé $\ve>0$ platí
\begin{eqnarray*}
\E (Y^2) &=& \int_{-\infty}^\infty y^2 f_Y(y)\, dy =
\int_{|y|<\ve}
y^2 f_Y(y)\, dy + \int_{|y|\geq \ve}y^2 f_Y(y)\, dy \\
&\geq& \int_{|y|\geq \ve}y^2 f_Y(y)\, dy \geq \ve ^2 \int_{|y|\geq
\ve} f_Y(y)\, dy = \ve^2 \P[|Y|\geq \ve]\, .
\end{eqnarray*}
Aplikací této nerovnosti na náhodnou veličinu $Y=X-\E X$ dostaneme
$$
\P[|X-\E X|\geq \ve] \leq \frac{\E(X-\E X)^2}{\ve^2} = \frac{\Var
X }{\ve^2}\, .
$$
\end{proof}
\begin{theorem}
(Zákon velkých čísel) Nechť $(X_n)_{n=1}^\infty$ je posloupnost
nezávislých, stejně rozdělených náhodných veličin, pro které
existuje $\mu =\E X_i$ a $\sigma^2=\E(X_i-\mu)^2$, $i\in\NN$.
Potom
$$
\overline X_n = \frac{1}{n} \sum_{i=1}^n X_i \stP \mu\, .
$$
\end{theorem}
\begin{proof}
K důkazu použijeme Čebyševovy nerovnosti. Spočtěme nejdříve
střední hodnotu a rozptyl náhodné veličiny $\overline X_n$. Podle
pravidel pro počítání se střední hodnotou
$$
\E \overline X_n = \E\left(\frac{1}{n} \sum_{i=1}^n X_i \right)
=\frac{1}{n} \sum_{i=1}^n \E X_i = \mu
$$
a s využitím nezávislosti dále
$$
\Var \overline X_n = \Var\left(\frac{1}{n} \sum_{i=1}^n X_i
\right) =\frac{1}{n^2} \sum_{i=1}^n \Var X_i =
\frac{\sigma^2}{n}\, .
$$
Dosazením do Čebyševovy nerovnosti dostaneme, že pro všechna
$\ve>0$ platí
$$
0\leq \P[|\overline X_n-\mu|\geq \ve]\leq \frac{\sigma^2}{n\ve^2}
\longrightarrow 0\, ,
$$
což je podle definice konvergence podle pravděpodobnosti
ekvivalentní dokazovanému tvrzení.
\end{proof}
\begin{remark}
Speciálním případem předchozí věty je Bernoulliho věta (1713).
Nechť jsou dány nezávislé náhodné veličiny $X_n\sim Be(p)$ a
označme $S_n = \sum_{i=1}^n X_i$. $S_n$ tedy vyjadřuje počet
úspěchů v $n$ Bernoulliovských pokusech. Potom platí
$$
\frac{S_n}{n} \stP p\, .
$$
Toto tvrzení vyplývá např. přímo z předcházející věty užitím
faktu, že střední hodnota náhodné veličiny $X\sim Be(p)$ je $p$.
\end{remark}
\begin{example}\label{ex82}
S jakou pravděpodobností můžeme tvrdit, že při 1000 hodech mincí
padne orel 400-600 krát?

Nechť veličiny $X_1,\ldots,X_{1000}$ vyjadřují , zda v $i$-tém
hodu padl orel (hodnota 1) nebo panna (hodnota 0). Veličiny $X_i$
jsou nezávislé a platí pro ně
$$
\E X_i = \frac{1}{2}\cdot 1 +  \frac{1}{2}\cdot 0 =
\frac{1}{2},\quad \E X_i^2 = \frac{1}{2}\cdot 1^2 +
\frac{1}{2}\cdot 0^2 =\frac{1}{2} \qmq{a} \Var X_i =
\frac{1}{2}-\left(\frac{1}{2}\right)^2 = \frac{1}{4}\, .
$$
Označme dále $Y=\sum_{i=1}^{1000}X_i$ počet orlů v 1000 hodech
mincí. Pro náhodnou veličinu $Y$ platí
$$
\E Y = \sum_{i=1}^{1000} \E X_i = 500,\quad \Var Y =
\sum_{i=1}^{1000} \Var X_i = 250\, .
$$
Podle Čebyševovy nerovnosti platí pro všechna $\ve>0$ nerovnost
$$
\P[|Y-\E Y|\geq \ve]\leq \frac{\Var Y}{\ve^2}\, .
$$
V našem konkrétním případě dostaneme
$$
\P[|Y-500|\geq 100]\leq \frac{250}{100^2}\, ,
$$
tedy
$$
\P[|Y-500|< 100]\geq 1-0,025 = 0,975\, .
$$
S pravděpodobností vyšší než 0,975 můžeme tedy říci, že v 1000
hodech mincí padne orel 400-600 krát.
\end{example}

\section{Centrální limitní věta}
V různých úlohách matematické statistiky nás zajímá rozdělení
součtu nebo průměru $n$ nezávislých náhodných veličin. Někdy je
však stanovení přesného rozdělení obtížné. Centrální limitní věta
říká, že za jistých podmínek lze toto rozdělení aproximovat
normálním.
\begin{defin}
Nechť $(X_n)_{n=1}^\infty$ je posloupnost náhodných veličin s
distribučními funkcemi $F_{X_n}$ a nechť $X$ je náhodná veličina s
distribuční funkcí $F_X$. Řekneme, že posloupnost
$(X_n)_{n=1}^\infty$ konverguje k náhodné veličině $X$ v
distribuci, jestliže
$$
\lim_{n\to\infty} F_{X_n}(x) = F_X(x)
 $$
 ve všech bodech spojitosti funkce $F_X$. Značíme $X_n \stL X$ (in law).
\end{defin}
\begin{remark}
Řekneme tedy, že $(X_n)_{n=1}^\infty$ konverguje v distribuci k
rozdělení $N(\mu,\sigma^2)$, jestliže
$$
\lim_{n\to\infty} F_{X_n}(x) = \Phi\left(
\frac{x-\mu}{\sigma}\right)
$$
pro všechna $x\in\RR$, budeme značit $X_n\stL N(\mu,\sigma^2)$.
\end{remark}
\begin{theorem}
(CLV - Lindeberg-Lévy) Nechť $(X_n)_{n=1}^\infty$ je posloupnost
nezávislých, stejně rozdělených náhodných veličin se společnou
střední hodnotou $\mu$ a konečným rozptylem $\sigma^2$. Potom
platí
$$
\fra{\sum_{i=1}^n X_i-n\mu}{\sqrt{n}\sigma} \stL N(0,1)\, .
$$
\end{theorem}
\begin{proof}
Náznak důkazu pro veličiny mající momentovou vytvářející funkci viz přednáška.
\end{proof}
\begin{remark}
Pokud označíme $Y_n = \sum_{i=1}^n X_i$, platí $\E Y_n = n\mu$,
$\Var Y_n=n\sigma^2$ a tvrzení lze zapsat v lépe pamatovatelném
tvaru
$$
\frac{Y_n-\E Y_n}{\sqrt{\Var Y_n}} \stL N(0,1)\, .
$$
Další možností je přepsat tvrzení pomocí průměru $\overline X_n$
do tvaru
$$
\sqrt{n}\,\frac{\overline X_n -\mu}{\sigma} \stL N(0,1)\, .
$$
\end{remark}
\begin{theorem}
(Moivre-Laplace 1718) Nechť $(X_n)_{n=1}^\infty$ je posloupnost
nezávislých, stejně rozdělených náhodných veličin, $X_i\sim Be(p),
$ pro všechna $ i\in\NN$. Potom platí
$$
\fra{\sum_{i=1}^n X_i-np}{\sqrt{np(1-p)}} \stL N(0,1)\, .
$$
\end{theorem}
\begin{remark}
Pokud jsou náhodné veličiny $X_i\sim Be(p)$ nezávislé, platí $Y_n
= \sum_{i=1}^nX_i \sim Bi(n,p)$ a podle předchozí věty
$$
\lim_{n\to\infty} \P\left[ \frac{Y_n-np}{\sqrt{np(1-p)}}\leq x
\right] = \Phi (x), \qmq{pro všechna} x\in \RR\, .
$$
Této verze centrální limitní věty se dá výhodně využít pro
aproximaci binomického rozdělení pomocí normálního. Taková
aproximace se doporučuje pro $np(1-p) \geq 9$ .
\end{remark}
\begin{example}
Loď má nosnost 5000 kg. Váha cestujících je náhodná veličina se
střední hodnotou 70 kg a směrodatnou odchylkou 20 kg. Kolik
cestujících můžeme nalodit, aby pravděpodobnost přetížení člunu
byla menší než 0.001?

Označme $X_i$ hmotnost $i$-tého cestujícího a $X=\sum_{i=1}^n X_i$
hmotnost všech cestujících. Potom
$$
\E X  = \sum_{i=1}^n\E X_i = 70n \qmq{a} \Var X = \sum_{i=1}^n
\Var X_i = 400 n\, .
$$
(za předpokladu, že hmotnosti jednotlivých cestujících jsou
nezávislé). Zajímá nás hodnota $n$ tak, aby
$$
\P[X\geq 5000]<0.001\, .
$$
Pokud označíme $U$ náhodnou veličinu s rozdělením $N(0,1)$,
dostaneme pomocí CLV aproximaci
\begin{eqnarray*}
\P[X\geq 5000] &=& \P\left[ \frac{X-70n}{20\sqrt{n}}\geq
\frac{5000-70n}{20\sqrt{n}}\right] \doteq \P\left[ U\geq
\frac{5000-70n}{20\sqrt{n}}\right]\\& =&
1-\Phi\left(\frac{5000-70n}{20\sqrt{n}}\right) <0.001\, ,
\end{eqnarray*}
jinak zapsáno
$$
\Phi\left(\frac{5000-70n}{20\sqrt{n}}\right) >0.999\, .
$$
Po nalezení příslušné hodnoty funkce $\Phi$ v tabulkách dostaneme
nerovnost
$$
\frac{5000-70n}{20\sqrt{n}} > 3.09
$$
neboli
$$
70n+61.8\sqrt{n} -5000<0\, .
$$
Vyřešením této kvadratické nerovnice v proměnné $\sqrt{n}$ najdeme
jeden přípustný kořen $\sqrt{n_1} = 8.02$ a závěr je, že můžeme
nalodit maximálně 64 cestujících.
\end{example}
\begin{example}
S jakou pravděpodobností můžeme tvrdit, že při 1000 hodech mincí
padne orel 450-550 krát?

V označení zavedeném v příkladu \ref{ex82} máme najít
pravděpodobnost $\P[450\leq Y\leq 550]$. V tomto případě je
hodnota $np(1-p) =250$ dostatečně velká, abychom mohli použít
aproximaci pomocí centrální limitní věty a s pomocí náhodné
veličiny $U\sim N(0,1)$ dostáváme
\begin{eqnarray*}
\P[450\leq Y\leq 550] &=& \P\left[ \frac{-50}{\sqrt{250}}\leq
\frac{Y-500}{\sqrt{250}}\leq \frac{50}{\sqrt{250}}\right] \doteq
\P\left[ \frac{-50}{\sqrt{250}}\leq U\leq
\frac{50}{\sqrt{250}}\right]\\& =&
2\Phi\left(\frac{50}{\sqrt{250}}\right)-1 =0.9984\, .
\end{eqnarray*}
Podobně bychom například spočítali
$$
\P[480\leq Y\leq 520] \doteq
2\Phi\left(\frac{20}{\sqrt{250}}\right)-1 =0.7941\, .
$$
Pro ilustraci, přesná hodnota spočtená mnohem náročněji pomocí
binomického rozdělení je v tomto případě 0.7939.
\end{example}

\begin{example}
Házíme symetrickou kostkou. S jakou pravděpodobností padne v 600
hodech více než 110 šestek?

Označme $Y$ počet šestek ze 600 hodů. $Y$ má tedy binomické
rozdělení s parametry $n=600$ a $p=1/6$ a hodnota $np(1-p)=250/3$
nás opravňuje použít aproximaci normálním rozdělením. Již známým
postupem dostaneme
\begin{eqnarray*}
\P[Y\geq 110] &=& \P\left[ \frac{Y-100}{\sqrt{250/3}}\geq
\frac{10}{\sqrt{250/3}}\right] \doteq \P\left[ U\geq
\frac{10}{\sqrt{250/3}}\right]\\& =&
1-\Phi\left(\frac{10}{\sqrt{250/3}}\right) =1-0.863=0.137\, .
\end{eqnarray*}
\end{example}
\begin{example}
Před volbami je v populaci 52\% příznivců koalice. Jaká je
pravděpodobnost, že průzkum veřejného mínění o rozsahu $n=1500$
respondentů ukáže nesprávně převahu opozice?

Označme $X$ počet příznivců koalice ve výběru. Pokud byl výběr
proveden nezávisle, má $X$ binomické rozdělení s parametry
$n=1500$ a $p=0.52$ a $np=780, np(1-p)=374.4$. Použitím aproximace
dostáváme, že hledaná hodnota pravděpodobnosti je přibližně
$$
\P[X\leq 749] =
\P\left[\frac{X-780}{\sqrt{374.4}}\leq\frac{-31}{\sqrt{374.4}}\right]
\doteq \Phi(-1.602) = 1-\Phi(1.602) = 0.055\, .
$$
\end{example}

\begin{example}
Uvažujme nezávislé náhodné veličiny $X_i\sim U(0,1)$ rovnoměrně
rozdělené na intervalu $(0,1)$. Protože pro toto rozdělení platí
$$
\E X_i = \frac{1}{2},\quad \E X_i^2 = \int_0^1 x^2 \,dx =
\frac{1}{3} \qmq{a} \Var X_i = \frac{1}{3}-\frac{1}{4} =
\frac{1}{12}
$$
dostaneme použitím centrální limitní věty
$$
\frac{\sum_{i=1}^n X_i - \frac{n}{2}}{\sqrt{\frac{n}{12}}} \stL
N(0,1)\, .
$$
Tohoto limitního vztahu se dá využít pro sestrojení jednoduchého
generátoru náhodných čísel z rozdělení $N(0,1)$. Například pro
$n=12$ podle předchozího vztahu $U=\sum_{i=1}^{12}X_i - 6$ má
přibližně standardní normální rozdělení. Stačí tedy nagenerovat 12
čísel z rozdělení $U(0,1)$ a jejich sečtením a odečtením 6
dostaneme realizaci náhodného čísla s rozdělením blízkým
standardnímu normálnímu.
\end{example}


\begin{exercise}
Hmotnost výrobků má rozdělení $N(2000,16)$. Jaká je pravděpodobnost, že průměrná hmotnost 8 náhodně vybraných výrobků bude větší než 2002?
\end{exercise}

\begin{exercise}
Zajímá nás podíl $p$ osob s krevní skupinou A v dané velké populaci. U kolika osob musíme zjistit, zda má či nemá skupinu A, abychom s pravděpodobností větší než 90\% odhadli $p$ s chybou nejvýše 0.05?
\end{exercise}



