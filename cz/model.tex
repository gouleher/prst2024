
\chapter{Matematický model pravděpodobnosti}
\setcounter{theorem}{0} \setcounter{defin}{0}
\setcounter{example}{0} \setcounter{lemma}{0}
\setcounter{remark}{0}


\section{Náhodné jevy}

Nyní budeme uvažovat obecný případ, kdy se nepředpokládá, že jednotlivé výsledky pokusu jsou stejně pravděpodobné. Dříve než budeme diskutovat pravděpodobnosti jevů, musíme probrat jevy samotné.

S každým pokusem nebo hrou je spojena množina všech možných výsledků.\\ Označme symbolem $\Omega$ množinu všech možných, navzájem se vylučujících výsledků. Množinu $\Omega$ budeme nazývat {\em základní pravděpodobnostní prostor} (někdy také {\em jistý jev})\\ Libovolný možný výsledek značíme $\omega\in\Omega$, $\omega$ je v jistém smyslu nejjednodušší výsledek, budeme ho nazývat {\em elementární jev}.
\begin{example}
Uvažujme dva hody mincí. Potom je množina
$$
\Omega = \{\omega_1, \omega_2, \omega_3, \omega_4\}
$$
všech možných navzájem se vylučujících výsledků tvořena čtyřmi elementárními jevy
$$
\omega_1=(H,H), \quad \omega_2=(H,O), \quad \omega_3=(O,H), \quad
\omega_4=(O,O),
$$
kde symboly $H$ a $O$ značí, zda při hodu mincí padne hlava nebo orel.
\end{example}

{\em Jevem} budeme prozatím rozumět sjednocení jistých elementárních jevů. Jevy budeme značit velkými písmeny $A, B, C, \ldots$. Libovolný jev $A$ je tedy podmnožinou množiny $\Omega$ ($A\subset\Omega$).
\begin{example}
Uvažujme situaci z předchozího příkladu a nechť $A$ značí jev, že hlava padne v prvním hodu, $B$ jev, že padne alespoň jedna hlava a $C$ jev, že padnou alespoň tři hlavy.

\noindent Jev $A$ je sjednocením dvou elementárních jevů a to
$$
A=\{\omega_1, \omega_2 \}\, .
$$
Jev $B$ se skládá z elementárních jevů
$$
B=\{\omega_1, \omega_2, \omega_3 \}
$$
a jev $C$ nemůže nastat. Takový jev budeme nazývat {\em nemožný jev} a značit $C=\emptyset$.
\end{example}

Jestliže nastane elementární jev $\omega$ a jestliže $\omega\in A$ budeme říkat, že {\em nastal jev } $A$.
\begin{remark}
\begin{itemize}
  \item[a)] jestliže nastane jev $A$ neznamená to, že nemohl nastat také jiný jev. V předcházejícím příkladu, nastane-li $\omega_1=(H,H)$, potom nastávají jevy $A$ i $B$.
  \item[b)] $\Omega$ se nazývá jistý jev, protože ať nastane jakýkoliv elementární jev $\omega$, vždy $\omega\in\Omega$.
\end{itemize}
\end{remark}

\subsection{Operace s náhodnými jevy}
\begin{description}
  \item[Komplementární (opačný) jev] $A^C$ k jevu $A$ se nazývá jev, který nastane právě tehdy, když nenastane jev $A$. Značíme
  $$
  A^C = \Omega \smallsetminus A\, .
  $$
  \item[Sjednocení jevů] $A_1, \ldots,A_n$ se nazývá jev, který nastane právě tehdy, když nastane alespoň jeden z jevů $A_1, \ldots,A_n$. Značíme
  $$
  A_1\cup A_2 \cup \ldots \cup A_n \qmq{nebo} \bigcup_{j=1}^n
A_j\, .  $$
\item[Průnikem jevů] $A_1, \ldots,A_n$ nazýváme jev nastávající právě tehdy, když nastávají všechny jevy $A_1,
  \ldots,A_n$. Značíme
  $$
  A_1\cap A_2 \cap \ldots \cap A_n \qmq{nebo} \bigcap_{j=1}^n
A_j\, .  $$
  Někdy budeme též používat zkrácený zápis $A\cap B = AB$.
  \item[Disjunktní (neslučitelné)] nazýváme takové jevy $A$ a $B$, které nemohou nastat současně, tj. jestliže $A\cap B = \emptyset$.\\
  Pokud jsou jevy $A$ a $B$ disjunktní, budeme značit
  $$
  A\cup B= A+B \qmq{a} \bigcup_{j=1}^n
A_j = \sum_{j=1}^n A_j \, .
  $$
  \item[{ Říkáme, že} jev $A$ implikuje jev $B$] ($A$ má za následek $B$) jestliže jev $B$ nastane vždy, když nastane jev $A$. Značíme
  $$
  A\subset B\, .
  $$
\end{description}

\begin{example}
Označme jev $A$: dožiju se 70 let a jev $B$: dožiju se 40 let.  Potom jev $A$ implikuje jev $B$, $A\subset B$.
\end{example}
\begin{theorem}\label{jevy}
Pro všechny jevy $A,B,C \subset \Omega$ platí:
\begin{enumerate}
  \item $A\subset A$
  \item jestliže $A\subset B$ a $B\subset C$ potom $A\subset C$
  \item $A\cap A = A, \quad A\cup A =A$
  \item $A\cup B = B \cup A, \quad A\cap B = B \cap A$
  \item $A\cap (B \cap C) = (A\cap B) \cap C, \quad A\cup (B \cup C) = (A\cup B) \cup C$
  \item $\emptyset \subset A \subset \Omega$
  \item $ A\cap B \subset A \subset A \cup B$
  \item $ \emptyset \cap A = \emptyset, \quad  \emptyset \cup A = A$
  \item $\Omega \cap A = A, \quad \Omega \cup A = \Omega$
  \item $(A^C)^C=A$
  \item $(A\cup B)^C = A^C\cap B^C \qmq{a} (A\cap B)^C = A^C\cup B^C$ (de Morganovy vztahy)
  \item $A\cup B = A+B\cap A^C$
  \item $B = A\cap B + A^C\cap B = AB + A^C B $\, .
\end{enumerate}
\end{theorem}
\begin{proof}
Dokažme například druhý de Morganův zákon $(A\cap B)^C = A^C\cup B^C$. Máme dokázat rovnost dvou množin, takže ukážeme dvě inkluze. \\
  $\subset:$
  $$
  \omega \in (A\cap B)^C \quad\Rightarrow\quad \omega \notin A\cap B
  \quad\Rightarrow\quad \omega \notin A \vee \omega\notin B \quad\Rightarrow\quad
  $$
  $$
  \quad\Rightarrow\quad \omega \in A^C \vee \omega \in B^C
  \quad\Rightarrow\quad\omega \in A^C\cup B^C\, .
  $$
  $\supset: $ Tato inkluze se ukáže úplně stejně opačným postupem.
\end{proof}

\begin{exercise}
Hodíme třikrát mincí. Vyjmenujte výsledky příznivé následujícím jevům:
\begin{itemize}
  \item[A] - padne právě jeden líc
  \item[B] - padne nejvýš jednou rub
  \item[C] - padne třikrát tatáž strana mince
  \item[D] - padne alespoň dvakrát rub\, .
\end{itemize}
\end{exercise}




\section{Axiomatická definice pravděpodobnosti}
Připomeňme, že symbolem $\Omega$ jsme označili množinu všech možných vzájemně se vylučujících výsledků a její prvky $\omega$ jsme nazývali elementární jevy.
\begin{defin}\label{sigma}
Nechť je dána neprázdná množina $\Omega$ a systém $\A$ podmnožin množiny $\Omega$, pro který platí
\begin{enumerate}
  \item $\Omega\in \A$
  \item je-li $A\in\A$, potom $A^C = \Omega \smallsetminus A \ \in\ \A$
  \item jestliže $A_1, A_2, A_3, \ldots \in \A$, potom i $\bigcup_{i=1}^\infty A_i \in \A\, .$
\end{enumerate}
Potom systém $\A$ nazveme $\sigma$-algebrou a prvky systému $\A$ jevy (náhodné jevy). Dvojici $(\Omega, \A)$ nazýváme pozorovací prostor.
\end{defin}

Zabývejme se teď základními vlastnostmi $\sigma$-algeber.
\begin{cor}
Nemožný jev $\emptyset$ je vždy prvkem $\sigma$-algebry $\A$, tj.  $\emptyset \in\A$.
\end{cor}
\begin{proof}
Protože platí $\emptyset^C = \Omega\in\A$ dostáváme s použitím bodu b) z definice
$$
\emptyset = (\emptyset^C)^C \in \A \, .
$$
\end{proof}

\begin{cor}
Jestliže $A_1, \ldots,A_n$ je konečná posloupnost jevů z $\A$, potom i $$\bigcup_{i=1}^n A_i \in\A\, .$$
\end{cor}
\begin{proof}
Dodefinujme jevy
$$
A_{n+1}= A_{n+2} =\ldots =\emptyset,
$$
podle předchozího tvrzení víme, že $A_i \in\A$ i pro všechna $i\geq n+1$ a vlastnost 3) z~Definice \ref{sigma} implikuje
$$
\bigcup_{i=1}^n A_i = \bigcup_{i=1}^\infty A_i \in\A\, .
$$
\end{proof}
\begin{cor}
Jestliže $A_1, A_2, \ldots$ je posloupnost jevů z $\A$ , potom i $$\bigcap_{i=1}^\infty A_i \in\A\, .$$
\end{cor}
\begin{proof}
Pro všechna $i\in\NN$ platí podle bodu 2) z Definice \ref{sigma}
$$
A_i^C \in\A
$$
a tedy podle bodu 3) téže definice
$$
\bigcup_{i=1}^\infty A_i^C \in\A\, .
$$
Použitím de Morganova vztahu dostaneme
$$
\left( \bigcup_{i=1}^\infty A_i^C\right) = \left(
\bigcap_{i=1}^\infty A_i\right) ^C \in \A
$$
a podle bodu 2) tedy
$$
\bigcap_{i=1}^\infty A_i \in \A \, .
$$
\end{proof}
\begin{remark}
Volba systému $\A$ závisí na konkrétní situaci. Obecně nemusíme brát systém všech podmnožin množiny $\Omega$.
\end{remark}
\begin{example}
Pro hod kostkou je množina všech možných výsledků $\Omega = \{1,2,3,4,5,6\}$ a za systém $\A$ můžeme vzít množinu všech podmnožin množiny $\Omega$, $\A=\mbox{exp }\Omega$. Pokud by nás ale například zajímalo pouze zda padne sudé nebo liché číslo, potom lze systém $\A$ volit jednodušeji
$$
\A=\{\emptyset, \Omega, \left\{1,3,5\}, \{2,4,6\}\right\} \, .
$$
\end{example}

\begin{defin}
Jestliže $\Omega=\RR$, pak nejmenší systém množin, který obsahuje všechny intervaly v $\RR$ nazveme systémem ($\sigma$-algebrou) borelovských množin a značíme $\B(\RR)$ (zkráceně $\B$).
\end{defin}
\begin{defin}\label{prob}
Nechť je dán pozorovací prostor $(\Omega, \A)$. Funkci $\P :\A\rightarrow \RR$ splňující podmínky
\begin{itemize}
  \item[{\rm 1)}] $\P(\Omega)=1$
  \item[{\rm 2)}] $\P(A)\geq 0$ \quad pro všechny \quad $A\in\A$
  \item[{\rm 3)}] $\P\left( \sum_{k=1}^\infty A_k \right) = \sum_{k=1}^\infty\P(A_k)$ \quad pro každou posl. disjunktních mn. $A_1,A_2, \ldots \in\A$
\end{itemize}
nazýváme pravděpodobnostní mírou (pravděpodobností) na $\A$ a trojici $(\Omega,\A,\P)$ pravděpodobnostním prostorem.
\end{defin}
\begin{theorem}
Pravděpodobnost nemožného jevu je nulová,
$$
\P(\emptyset) =0\, .
$$
\end{theorem}
\begin{proof}
Definujme jevy
$$
A_1=A_2=A_3=\ldots = \emptyset\, .
$$
Protože všechny tyto jevy $A_i$ jsou disjunktní, z bodu 3) Definice \ref{prob} plyne
$$
\P(\emptyset) = \P \left( \sum_{k=1}^\infty A_k\right) = \sum_{k=1}^\infty \P(A_k) = \sum_{k=1}^\infty \P(\emptyset)\, ,
$$
což může být splněno pouze tehdy, když $\P(\emptyset)=0$.
\end{proof}
\begin{theorem}\label{v23}
Nechť $A_1, \ldots A_n\in\A$ jsou disjunktní jevy. Potom
$$
\P(A_1+A_2+\ldots +A_n) = \P(A_1)+ \P(A_2) +\ldots + \P(A_n)\, .
$$
\end{theorem}
\begin{proof}
Definujme jevy $A_{n+1} = A_{n+2} =\ldots =\emptyset$. Potom jsou všechny  jevy $A_i, i\in\NN$ disjunktní a platí
$$
\P \left( \sum_{k=1}^n A_k\right)=\P \left( \sum_{k=1}^\infty A_k\right) =  \sum_{k=1}^\infty \P (A_k) = \sum_{k=1}^n \P (A_k)\, ,
$$
neboť podle předchozí věty platí $\P(\emptyset)=0$.
\end{proof}
\begin{theorem}
Pro libovolné jevy $A$ a $B$ platí
$$
\P(A\cup B) = \P(A) +\P(B) - \P(A\cap B)\, .
$$
\end{theorem}
\begin{proof}
Pokud rozložíme jev $A\cup B$ na disjunktní sjednocení dvou jevů
$$
A\cup B = A + A^C \cap B\, , \quad\mbox{(viz. Věta \ref{jevy})}
$$
podle předchozí věty dostaneme
\begin{equation}\label{acb}
\P(A\cup B) = \P(A) +\P(A^C \cap B)\, .
\end{equation}
Podobně rozložme jev $B$ na $B=A\cap B + A^C\cap B$, potom platí
$$
\P(B) =\P(A\cap B)+ \P(A^C\cap B) \qmq{neboli} \P(A^C\cap B)=\P(B)
-\P(A\cap B).
$$
Dosazením do vztahu (\ref{acb}) dostáváme tvrzení věty.
\end{proof}
\begin{dusl}
Pro libovolné jevy $A$ a $B$ platí
$$
\P(A\cup B) \leq \P(A) + \P(B)\, .
$$
\end{dusl}
\begin{theorem}
Jestliže $A\subset B$, potom
$$
\P(A) \leq \P(B)\, .
$$
\end{theorem}
\begin{proof}
Použijeme opět rozklad $B=A\cap B + A^C\cap B$. Z předpokladů věty ovšem vyplývá, že $A\cap B = A$ a tedy
$$
\P(B)=\P(A) + \P(A^C\cap B) \geq \P(A)\, .
$$
\end{proof}
\begin{dusl}
Pro libovolný jev $A$ platí
$$
\P(A) \leq 1\, .
$$
\end{dusl}
\begin{proof}
Protože každý jev splňuje $A\subset \Omega$, dostáváme $\P(A)\leq \P(\Omega) =1$.
\end{proof}
\begin{theorem}
Pro každý jev $A$ platí
$$
\P(A^C) = 1-\P(A)\, .
$$
\end{theorem}
\begin{proof}
Jistý jev můžeme rozložit na disjunktní sjednocení $\Omega = A+A^C$ a podle Věty \ref{v23} tedy platí
$$
1=P(A) +\P(A^C)\, .
$$
\end{proof}
\begin{example}
Tři hráči házejí postupně mincí. Vyhrává ten, komu padne dříve líc. určete pravděpodobnost výhry každého hráče.

Označme symbolem $A_i$ jev, že první hráč vyhraje ve svém $i$-tém tahu a symbolem $A$ jev, že vyhraje první hráč. Potom
\begin{equation}\label{pa}
\P(A) = \P\left( \sum_{i=1}^\infty A_i\right) = \sum_{i=1}^\infty
\P(A_i)\, ,
\end{equation}
 protože jevy $A_i$ jsou disjunktní (nemohou nastat současně). Dále platí
$$
\P(A_1) = \frac{1}{2}, \quad \P(A_2)=
\frac{1}{2}\cdot\frac{1}{2}\cdot\frac{1}{2}\cdot\frac{1}{2}, \quad
\P(A_3) = \left(\frac{1}{2}\right)^7, \ldots
$$
a obecně tedy můžeme psát
$$
\P(A_i) = \left(\frac{1}{2}\right)^{3i-2}\, .
$$
Dosazením do (\ref{pa}) dostáváme
$$
\P(A) = \sum_{i=1}^\infty \left(\frac{1}{2}\right)^{3i-2} = 4
\sum_{i=1}^\infty \frac{1}{2^{3i}}=\frac{4}{7}\, .
$$
Podobně pro jevy $B_i$, že vyhraje druhý hráč ve svém $i$-tém tahu a $B$, že vyhraje druhý hráč, platí
$$
\P(B) = \sum_{i=1}^\infty \P(B_i) = \sum_{i=1}^\infty
\frac{1}{2}\P(A_i)= \frac{2}{7}
$$
a pravděpodobnost výhry třetího hráče tedy je $1/7$.
\end{example}

