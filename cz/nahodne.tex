
\chapter{Náhodné veličiny}

V celé této kapitole budeme uvažovat pravděpodobnostní prostor $(\Omega,\A,\P)$. Výsledkem pokusu ale nemusí být vždy číslo (tramvaj přijela-nepřijela, obrázky na kostce atd.). Abychom výsledky takových experimentů mohli matematicky zpracovávat, přiřazujeme každému výsledku číslo. To je základní princip náhodné veličiny, je to zobrazení $X:\Omega\mapsto\RR$.

\begin{defin}
Náhodná veličina je zobrazení $X:\Omega\mapsto\RR$ takové, že pro každé $x\in\RR$ platí
$$
X^{-1}((-\infty,x \rangle)= \{\omega\in\Omega|X(\omega)\leq x\} \in \A\, .
$$
(tj. vzory všech intervalů $(-\infty,x\rangle$ jsou měřitelné (funkcí $\P$) množiny)
\end{defin}

\begin{example}
Předpokládejme, že házíme najednou dvěma kostkami. Potom množina všech možných výsledků má tvar
$$
\Omega=\{(i,j)| i\in\wh 6, j\in \wh 6 \ \mbox{a} \ i\leq j\}\, .
$$
Za $\sigma$-algebru $\A$ zvolme systém všech podmnožin množiny $\Omega$ a náhodná veličina $X$ nechť vyjadřuje součet výsledků, tj.
$$
X(i,j) = i+j\, .
$$
Potom například
$$
X^{-1}((-\infty,1.2 \rangle)= \{\omega\in\Omega|X(\omega)\leq 1.2\} = \emptyset\, ,
$$
$$
\{\omega\in\Omega|X(\omega)\leq 3.1\} = \{(1,1),(1,2)\}\, ,
$$
protože $X(1,1)=2$ a $X(1,2)=3$ a
$$
\{\omega\in\Omega|X(\omega)\leq 4\} =
\{(1,1),(1,2),(1,3),(2,2)\}\, .
$$
\end{example}
Nadále budeme v tomto textu používat následující označení:
\begin{eqnarray*}
&& \{\omega\in\Omega|X(\omega)\leq x\} \triangleq [X\leq x] \subset \Omega\\
&& \{\omega\in\Omega|X(\omega)< x\} \triangleq [X< x]\\
&&\{\omega\in\Omega|X(\omega)\geq x\} \triangleq [X \geq x]\\
&&\{\omega\in\Omega|X(\omega)> x\} \triangleq [X >x]\\
&&\{\omega\in\Omega|X(\omega)= x\} \triangleq [X=x]\\
&&\{\omega\in\Omega|X(\omega)\in S\} \triangleq [X\in S]
\end{eqnarray*}
kde $S$ je podmnožina $\RR$. Pokud bude $[X\in S]\in\A$ znamená to, že umíme určit $\P[X\in S]$.

\begin{theorem}
Nechť $X,Y$ jsou náhodné veličiny. Potom
\begin{itemize}
  \item[1)] $X+Y$ je také náhodná veličina,
  \item[2)] $kX$ je náhodná veličina pro všechna $k\in\RR$,
  \item[3)] $XY$ je náhodná veličina,
  \item[4)] je-li navíc $[Y=0]=\emptyset$, pak $X/Y$ je náhodná veličina.
  \item[5)] Je-li $g:\RR \mapsto \RR$ spojitá funkce, pak $g(X)$ je náhodná veličina.
\end{itemize}
\end{theorem}

\section{Distribuční funkce náhodných veličin}
Jednou z možností, jak popsat náhodnou veličinu je distribuční funkce.
\begin{defin}
Nechť $X$ je náhodná veličina. Funkci $F_X:\RR\mapsto\RR$ definovanou pro všechna $x \in\RR$ vztahem
\begin{equation}\label{cdf}
    F_X(x) =\P[X\leq x]\, ,
\end{equation}
nazýváme distribuční funkce náhodné veličiny $X$.
\end{defin}

\begin{example}
Uvažujme tři hody mincí. Množina všech možných výsledků je
$$
\Omega=\{(H,H,H),(H,H,O),(H,O,H),(O,H,H),H,O,O),(O,H,O),(O,O,H),(O,O,O)\}\, ,
$$
kde $H$ značí, že padla hlava a $O$, že padl orel. Nechť náhodná veličina $X$ vyjadřuje počet hlav, které padly. Tedy např.  $X(H,H,H)=3, X(H,O,O)=1,\ldots$ Distribuční funkce náhodné veličiny $X$ bude mít tvar
\begin{center}
\renewcommand{\arraystretch}{1.2}
\begin{tabular}{lll}
pro $x<0$ &  $[X\leq x]=\emptyset$ & a  $F_X(x)= 0$ \\
pro $0\leq x<1$ &  $[X\leq x]=\{(O,O,O)\}$ & a $F_X(x)=\frac{1}{8}$ \\
pro $1\leq x<2$ &  $[X\leq x]=\{(O,O,O),(O,O,H),(O,H,O),(H,O,O)\}$ & a $F_X(x)= \frac{1}{2}$ \\
pro $2\leq x<3$ &  $[X\leq x]=\Omega \smallsetminus\{(H,H,H)\}$ & a $F_X(x) =\frac{7}{8}$ \\
pro $x\geq 3$ &  $[X\leq x]=\Omega$ & a $F_X(x)=1$, \\
\end{tabular}
\end{center}
viz. následující obrázek.
\end{example}

\begin{example}
Uvažujme náhodnou veličinu $X$, která popisuje polohu velké ručičky hodin. Tu můžeme udávat pomocí úhlu, o který se posunula od počátku. Pravděpodobnostní model popisující tuto situaci potom bude
$$
\Omega = \langle 0,2\pi),\quad \A=\B(\langle 0,2\pi)),\quad \P(A) = \frac{1}{2\pi}\int_0^{2\pi}I_A(t)\, dt\, ,
$$
kde $I_A(t)$ je charakteristická funkce množiny $A\in\A$, tj.
$$
I_A(t)=
    \begin{cases}
        1 & \text{pro } t\in A, \\
        0 & \text{pro } t\notin A.
    \end{cases}
$$
Distribuční funkce náhodné veličiny $X$ má potom tvar
$$
F_X(x) = \P[X\leq x] = \frac{1}{2\pi}\int_0^{2\pi}I_{(-\infty,x\rangle}(t)\, dt =
  \begin{cases}
    0 & \text{pro } x<0, \\
    \fra{x}{2\pi} & \text{pro } 0\leq x<2\pi, \\
    1 & \text{pro } x\geq 2\pi .
  \end{cases}
$$
\end{example}

Následující věta shrne základní vlastnosti distribuční funkce.

\begin{theorem}
Nechť $X$ je náhodná veličina a $F_X(x)$ její distribuční funkce. Potom
\begin{itemize}
  \item[1)] $0\leq F_X(x) \leq 1$ pro všechna $x\in\RR$
  \item[2)] jestliže $x_1<x_2$, potom $F_X(x_1)\leq F_X(x_2)$ (tj.  $F_X$ je neklesající)
  \item[3)] $\lim_{x\to -\infty} F_X(x) = 0$
  \item[4)] $\lim_{x\to \infty} F_X(x) = 1$
  \item[5)] pro všechna $a\in\RR$ platí $\lim_{x\to a_+} F_X(x) = F_X(a)$  (tj. $F_X$ je zprava spojitá)
  \item[6)] $F_X$ má nejvýše spočetně mnoho bodů nespojitosti.
\end{itemize}
\end{theorem}

\begin{proof}
ad 1)  Pro všechna $x\in\RR$ platí $0\leq \P[X\leq x]\leq 1$ a tvrzení vyplývá z definice distribuční funkce.

ad 2) Náhodný jev $[X\leq x_2]$ se dá zapsat jako disjunktní sjednocení
$$
[X\leq x_2] = [X\leq x_1] + [x_1<X\leq x_2]
$$
a tedy
$$
F_X(x_2) = \P[X\leq x_2] = \P[X\leq x_1] + \P[x_1<X\leq x_2] \geq \P[X\leq x_1 ] = F_X(x_1)\, .
$$

ad 4) Podle definice limity máme dokázat
$$
(\forall \ve >0)(\exists x_\ve\in\RR)(\forall x >x_\ve)(F_X(x)>1-\ve)\, .
$$
Zvolme $\ve>0$ pevně. Protože $X$ je náhodná veličina a $X(\omega)$ je konečná pro všechna $\omega\in\Omega$, lze rozložit $\Omega$ na
$$
\Omega = [X\leq 0] + \sum_{k=1}^\infty [k-1<X\leq k]
$$
a po aplikaci pravděpodobnosti na tuto rovnost dostaneme
$$
1= \P[X\leq 0] + \sum_{k=1}^\infty \P[k-1<X\leq k]\, .
$$
To ale znamená, že řada nezáporných členů na pravé straně je konvergentní a tedy existuje $N_\ve\in\NN$ tak, že pro všechna $n>N_\ve$
$$
\P[X\leq 0] + \sum_{k=1}^n \P[k-1<X\leq k]>1-\ve\, ,
$$
neboli pro všechna $n>N_\ve$
$$
\P[X\leq n]>1-\ve\, .
$$
Zvolme $x_\ve = N_\ve+1$, pro tuto hodnotu tedy platí $\P[X\leq x_\ve]>1-\ve$ a aplikací bodu 2) dostáváme pro všechna $x>x_\ve$
$$
F_X(x) = \P[X\leq x] \geq \P[X\leq x_\ve]>1-\ve\, .
$$

ad 3) V tomto případě máme dokázat, že
$$
(\forall \ve >0)(\exists x_\ve\in\RR)(\forall x <x_\ve)(F_X(x)<\ve)\, .
$$
Důkaz se provede podobně, jako v předchozím bodě. Zvolme $\ve>0$ pevně a rozložme $\Omega$ na
$$
\Omega = [X> 0] + \sum_{k=0}^\infty [-(k+1)<X\leq -k]\, .
$$
Aplikací pravděpodobnosti na tuto rovnost dostaneme
$$
1= \P[X> 0] + \sum_{k=0}^\infty \P[-(k+1)<X\leq -k]\, .
$$
To ale znamená, že existuje $N_\ve\in\NN$ tak, že pro všechna $n>N_\ve$
$$
\P[X> 0] + \sum_{k=0}^n \P[-(k+1)<X\leq -k]>1-\ve\, ,
$$
což je ekvivalentní výrazu
$$
\P[X\leq -(n+1)]<\ve\, .
$$
Zvolme $x_\ve = -( N_\ve+2)$, pro tuto hodnotu tedy platí $\P[X\leq x_\ve]<\ve$ a aplikací bodu 2) dostáváme pro všechna $x<x_\ve$
$$
F_X(x) = \P[X\leq x] \leq \P[X\leq x_\ve]<\ve\, .
$$

ad 5) Pro důkaz tohoto tvrzení věty rozložíme na disjunktní sjednocení jev
$$
[a<X\leq a+1] = \sum_{n=1}^\infty \left[a+\frac{1}{n+1}<X\leq a+\frac{1}{n}\right]\, .
$$
Platí tedy
$$
\P[a<X\leq a+1] = \sum_{n=1}^N \P \left[a+\frac{1}{n+1}<X\leq a+\frac{1}{n}\right]+Z_N \, ,
$$
kde $Z_N$ je zbytek konvergující k nule pro $N\to\infty$. Poslední výraz lze přepsat do tvaru
$$
F_X(a+1)-F_X(a) = \P[a+\frac{1}{N+1}<X\leq a+1] +Z_N = F_X(a+1) - F_X\left(a+\frac{1}{N+1}\right)\, ,
$$
ze kterého vyplývá, že
$$
F_X\left(a+\frac{1}{N+1}\right)=F_X(a) +Z_N \qmq{a tedy} \lim_{N\to\infty} F_X\left(a+\frac{1}{N+1}\right)=F_X(a)\, .
$$
Stejná hodnota limity pro $x\to a_+$ vyplývá z monotonie distribuční funkce $F_X$.

ad 6) Označme $S_n$ množinu bodů, kde má funkce $F_X(x)$ skok velikosti větší nebo rovné $1/n$. Množina všech bodů nespojitosti $S$ funkce $F_X$ se dá potom vyjádřit jako
$$
S=\bigcup_{n=1}^\infty S_n\, .
$$
Pro každé $n$ je množina $S_n$ konečná, jinak by totiž hodnoty funkce $F_X$ přesáhly jedničku, z čehož vyplývá, že množina $S$ je nejvýše spočetná.
\end{proof}

\begin{remark}
Jestliže nějaká reálná funkce splňuje vlastnosti 1),\ldots,5) z předchozí věty, pak je to distribuční funkce nějaké náhodné veličiny.
\end{remark}

\begin{remark}
Někdy se definuje
$$
F_X(x) = \P[X<x]\, .
$$
Taková distribuční funkce je potom spojitá zleva, ostatní vlastnosti zůstávají v platnosti.
\end{remark}

\begin{example}
Přístroj obsahuje 5 tranzistorů. Předpokládáme, že došlo k poruše způsobené jedním tranzistorem. Opravář prohlíží jeden po druhém bez vracení. Určete distribuční funkci náhodné veličiny $X$ označující počet potřebných zkoušek.

Náhodná veličina $X$ může nabývat hodnot z množiny $\{1,2,3,4,5\}$. Pravděpodobnosti jednotlivých hodnot jsou
$$
\P[X=1] = \frac{1}{5}, \quad \P[X=2] = \frac{4}{5}\cdot\frac{1}{4}=\frac{1}{5}, \quad \P[X=3] = \frac{4}{5}\cdot\frac{3}{4}\cdot\frac{1}{3}=\frac{1}{5},
$$

$$  
\P[X=4] = \frac{4}{5}\cdot\frac{3}{4}\cdot\frac{2}{3}\cdot\frac{1}{2}=\frac{1}{5}, \quad  \P[X=5] =\frac{4}{5}\cdot\frac{3}{4}\cdot\frac{2}{3}\cdot\frac{1}{2}\cdot 1 =\frac{1}{5}\, ,
$$
z čehož vyplývají příslušné hodnoty distribuční funkce:
\begin{center}
\renewcommand{\arraystretch}{1.5}
\begin{tabular}{ll}
  $F_X(x) = 0$  & pro $x<1$,  \\
  $F_X(x)=\P[X=1] =\frac{1}{5}$ & pro $1\leq x<2$, \\
  $F_X(x)=\P[X=1]+ \P[X=2] =\frac{2}{5}$ & pro $2\leq x<3$, \\
  $F_X(x)=\P[X=1]+ \P[X=2]+\P[X=3] =\frac{3}{5}$ & pro $3\leq x<4$, \\
  $F_X(x)=\P[X=1]+ \ldots+\P[X=4] =\frac{4}{5}$ & pro $4\leq x<5$, \\
  $F_X(x)=\P[X=1]+ \ldots+\P[X=5] =1$ & pro $ x\geq 5$. \\
\end{tabular}
\end {center}
{\em\large OBR.}
\end{example}

\begin{example}
Náhodná veličina $X$ je zadána distribuční funkcí
$$
F_X(x) =
  \begin{cases}
    0 & \text{pro } x\leq 0 , \\
    1-e^{-2x} & \text{pro} x>0.
  \end{cases}
$$
Najděte $\P[1<X\leq 2], \P[-1<X\leq 2], \P[X=1]$.
$$
\P[1<X\leq 2] = \P[X\leq 2] - \P[X\leq 1]= F_X(2)-F_X(1) = 1-e^{-4}-1+e^{-2}=e^{-2}-e^{-4}\, .
$$

$$
\P[-1<X\leq 2] = F_X(2) - F_X(-1) = 1-e^{-4}-0 = 1-e^{-4}\, .
$$

Všimněte si, že $\P[-1<X\leq 2] = \P[0<X\leq 2]$ z čehož vyplývá $\P[-1<X\leq 0]=0$.
$$
\P[X=1] = \P[X\leq 1]- \P[X<1] = F_X(1)-\lim_{t\to 1_-} F_X(t) = F_X(1)-F_X(1) =0\, .
$$
\end{example}

\begin{remark}
Z příkladu je vidět, že pravděpodobnost určité hodnoty bude nenulová pouze tehdy, když distribuční funkce bude mít v tomto bodě skok.
\end{remark}

\section{Mnohorozměrné distribuční funkce}

Často měříme více veličin najednou, např. teplota, tlak, vlhkost, nebo míry 90-60-90, atd. Každé číslo je náhodná veličina samo o sobě, abychom si ale udělali celkovou představu, musíme znát všechna. Jednotlivé veličiny mohou být nezávislé, mohou ale mít i silnou vazbu, např. úhlopříčka čtverce a jeho plocha, průměrný roční příjem a výše důchodu. Budeme chtít získávat souhrnné informace o obou(všech) proměnných, informace o jedné z veličin, informace o jedné z nich za podmínky na druhou (ostatní). Toto vše budeme schopni popisovat pomocí mnohorozměrné distribuční funkce.

Vektor náhodných veličin $Z=(X_1,X_2,\ldots, X_n)$ budeme nazývat náhodný vektor. Pro jednoduchost budeme většinu pojmů definovat pro náhodné vektory o dvou složkách, případné rozšíření bude zřejmé.

\begin{defin}
Nechť $X, Y$ jsou náhodné veličiny. Pak jejich sdružená distribuční funkce je pro všechny $(x,y)\in\RR^2$ definována předpisem
$$
F_{X,Y}(x,y) = \P([X\leq x][Y\leq y])\, .
$$
\end{defin}

\begin{remark}
Sdružené distribuční funkce mají podobné vlastnosti jako distribuční funkce jedné proměnné. Proto vyslovíme jen některé věty a bez důkazů, které jsou podobné jako v případě jedné proměnné.
\end{remark}

\begin{theorem}
Nechť $F_{X,Y}$ je sdružená distribuční funkce náhodného vektoru $(X,Y)$. Potom pro všechna $x_1\leq x_2$ a $y_1\leq y_2$ platí 
$$ 
F_{X,Y}(x_1,y_1) \leq F_{X,Y}(x_2,y_2)\, .
$$
\end{theorem}

\begin{theorem}
Nechť $F_{X,Y}$ je sdružená distribuční funkce náhodného vektoru $(X,Y)$. Potom
$$
\lim_{x\to -\infty} F_{X,Y}(x,y)=0 \qmq{pro všechna} y\in\RR\, ,
$$
$$
\lim_{y\to -\infty} F_{X,Y}(x,y)=0 \qmq{pro všechna} x\in\RR\, .
$$
\end{theorem}

\begin{theorem}
Nechť $F_{X,Y}$ je sdružená distribuční funkce náhodného vektoru $(X,Y)$. Potom
$$
\lim_{x\to \infty} F_{X,Y}(x,y)=F_Y(y) \qmq{pro všechna} y\in\RR\, ,
$$
$$
\lim_{y\to \infty} F_{X,Y}(x,y)=F_X(x) \qmq{pro všechna} x\in\RR\, .
$$
\end{theorem}

\begin{remark}
Distribuční funkce $F_X,F_Y$ z předchozí věty budeme nazývat {\em marginální distribuční funkce} náhodného vektoru $(X,Y)$.  Obecněji, pokud $Z=(X_1,X_2,\ldots, X_n)$ je náhodný vektor, vektor $Z'=(X_{i_1},X_{i_2},\ldots, X_{i_k})$, pro $k<n$, nazýváme {\em marginální náhodný vektor}, jeho rozdělení marginální rozdělení a speciálně náhodnou veličinu $X_i$ nazýváme {\em marginální náhodná veličina.}
\end{remark}

\begin{defin}
Náhodné veličiny $X$ a $Y$ nazýváme (statisticky) nezávislé, jestliže jsou nezávislé jevy
$$
[a<X\leq b] \qmq{a} [c<Y\leq d]
$$
pro libovolné dvojice $(a,b), (c,d)$, kde $a,b,c,d\in\overline\RR$ a $a\leq b, c\leq d$.
\end{defin}

\begin{theorem}\label{nez}
Náhodné veličiny $X,Y$ jsou nezávislé právě tehdy, když pro každou dvojici $(x,y)\in\RR^2$ platí rovnost
$$
F_{X,Y}(x,y) = F_X(x)\cdot F_Y(y)\, .
$$
(hodnota sdružené distribuční funkce je rovna součinu hodnot marginálních distribučních funkcí.)
\end{theorem}

\begin{proof}
$\Rightarrow:$ Nechť jsou $X,Y$ nezávislé, tj. pro všechna $a,b,c,d\in\overline\RR$, $a\leq b, c\leq d$ platí
$$
\P([a<X\leq b][c<Y\leq d]) = \P[a<X\leq b]\cdot \P[c<Y\leq d]\, .
$$
Položme $a=-\infty, b=x, c=-\infty, d=y$. Potom pro všechna $(x,y)\in\RR^2$ platí
\begin{eqnarray*}
F_{X,Y}(x,y) &=& \P([-\infty<X\leq x][-\infty<Y\leq y]) \\
             &=& \P[-\infty<X\leq x]\cdot \P[-\infty<Y\leq y]=F_X(x)\cdot F_Y(y)\, . 
\end{eqnarray*}

$\Leftarrow:$
\begin{eqnarray*}
&&\P([a<X\leq b][c<Y\leq d]) = F_{X,Y}(b,d)-F_{X,Y}(b,c)-F_{X,Y}(a,d)+F_{X,Y}(a,c)\\
&&= F_X(b)F_Y(d) - F_X(b)F_Y(c)-F_X(a)F_Y(d)+F_X(a)F_Y(c)\\
&&= (F_X(b)-F_X(a))(F_Y(d)-F_Y(c)) = \P[a<X\leq b]\cdot \P[c<Y\leq d]\, .
\end{eqnarray*}
\end{proof}

\begin{example}
Náhodný vektor $(X,Y)$ je zadán pravděpodobnostmi $\P[X=x,Y=y]$ uvedenými v tabulce
\begin{center}
\begin{tabular}{c|ccc}
  $y\setminus x$ & 2 & 5 &8 \\ \hline
 0.4 & 0.15 & 0.30 & 0.35 \\
  0.8 & 0.05 & 0.12 & 0.03\\
\end{tabular}
\end{center}

Určete sdruženou a příslušné marginální distribuční funkce a
rozhodněte, zda jsou náhodné veličiny $X,Y$ nezávislé.

Hodnoty sdružené distribuční funkce jsou vidět na následujícím
Obrázku \ref{cdf1}.

\begin{figure}[h]
\centering
\includegraphics[width=8cm]{cdf1.eps}
\caption{Hodnoty distribuční funkce $F_{X,Y}(x,y)$.} \label{cdf1}
\end{figure}

Marginální distribuční funkce mají tedy tvar
$$
F_X(x) = \lim_{y\to\infty}F_{X,Y}(x,y) =
  \begin{cases}
    0 & \text{pro } x<2, \\
    0.2 & \text{pro } 2\leq x <5,\\
    0.62 & \text{pro } 5\leq x <8,\\
    1 & \text{pro } x \geq 8,\\
  \end{cases}
$$
a
$$
F_Y(y) = \lim_{x\to\infty}F_{X,Y}(x,y) =
  \begin{cases}
    0 & \text{pro } y<0.4, \\
    0.8 & \text{pro } 0.4\leq y<0.8,\\
    1 & \text{pro } y \geq 0.8.\\
  \end{cases}
$$
Protože například pro $x=3$ a $y=0.5$ neplatí $F_{X,Y}(x,y) = F_X(x)\cdot F_Y(y)$, veličiny $X$ a $Y$ nejsou nezávislé.
\end{example}
