
\chapter{Závislé a nezávislé jevy}
\setcounter{theorem}{0} \setcounter{defin}{0}
\setcounter{example}{0} \setcounter{lemma}{0}
\setcounter{remark}{0}

Představme si, že už máme nějakou informaci o pokusu a zajímá nás pravděpodobnost nějakého jevu na základě této informace. Např.
\begin{itemize}
  \item pravděpodobnost, že student dokončí studium, když dokončil první ročník
  \item pravděpodobnost, že student dokončí studium, když obhájil výzkumný úkol.
\end{itemize}
Je zřejmé, že odpovědi na tyto otázky nebudou stejné. Takovéto situace budeme schopni popisovat pomocí podmíněné pravděpodobnosti.
\begin{defin}
Nechť jsou dány jevy $A,B\in\A$ takové, že $\P(B)>0$. Podmíněnou pravděpodobností jevu $A$ za podmínky, že nastal jev $B$, nazveme číslo
\begin{equation}\label{cond}
  \P(A|B) = \frac{\P(A\cap B)}{\P(B)}\, .
\end{equation}
\end{defin}
\begin{theorem}
Je-li $\P(B)>0$, pak $\P(.|B)$ je pravděpodobnost na pozorovacím prostoru $(\Omega, \A)$.
\end{theorem}
\begin{proof}
Na základě předpokladů věty a definice podmíněné pravděpodobnosti máme ukázat tři vlastnosti pravděpodobnostní míry.\\
1)
$$
\P(\Omega|B) = \frac{\P(\Omega\cap B)}{\P(B)}=
\frac{\P(B)}{\P(B)}=1\, .
$$
2) Pro každý jev $A\in\A$ platí
$$
\P(A|B) =\frac{\P(A\cap B)}{\P(B)}\geq 0\, .
$$
3) Pro libovolnou posloupnost $A_1,A_2,\ldots,A_n,\ldots$
disjunktních jevů z $\A$ platí
$$
\P\left(\left.\sum_{i=1}^\infty A_i\right| B\right) =
\frac{\P\left(\left(\sum_{i=1}^\infty A_i\right)\cap
B\right)}{\P(B)} = \frac{\P\left(\sum_{i=1}^\infty(A_i\cap
B)\right)}{\P(B)}=
$$
$$
\frac{\sum_{i=1}^\infty \P(A_i\cap B)}{\P(B)}= \sum_{i=1}^\infty
\P(A_i|B)\, ,
$$
kde jsme v předposledním kroku využili toho, že i jevy $A_i\cap B,\ i\in\NN$ jsou disjunktní.
\end{proof}
\begin{example}
Na základě výzkumu dědičnosti barvy očí v jisté zemi, byla mužská populace rozdělena do čtyř skupin s procentuálním zastoupením uvedeným v tabulce.
\begin{center}
\begin{tabular}{ccc}
  četnost & otec & syn \\ \hline
  5\% & tmavooký  & tmavooký  \\
  7.9\% & tmavooký  & světleoký  \\
  8.9\% & světleoký  & tmavooký  \\
  78.2\% & světleoký  & světleoký
\end{tabular}
\end{center}
Určete pravděpodobnost, že tmavooký otec bude mít tmavookého syna.

Označme jev $A$ - syn bude tmavooký, a jev $B$ - otec je tmavooký. Máme určit $\P(A|B)$, přitom
$$
\P(A)=0.139,\qquad \P(B) = 0.129,\qquad \P(A\cap B) = 0.05
$$
a podle definice podmíněné pravděpodobnosti tedy
$$
\P(A|B)=\frac{\P(A\cap B)}{\P(B)}=\frac{0.05}{0.129}\doteq 0.388\,
,
$$
což koresponduje s naším očekáváním.
\end{example}
\begin{remark}
V modelu $(\Omega,\A,\P)$ vlastně nahrazujeme pravděpodobnostní míru $\P$ jinou mírou $\P(\cdot| B)$, která více odpovídá experimentu (pravděpodobnostních měr na $(\Omega, \A)$ je nekonečně mnoho).
\end{remark}
\begin{theorem}
{\rm (Součinové pravidlo)}  Pro každých $n+1$ jevů $A_0, A_1, \ldots A_n$ z $\A$ takových, že $\P(A_0A_1\ldots A_{n-1})>0$, platí
\begin{equation}\label{soucin}
\P(A_0A_1\ldots A_n) = \P(A_0)\cdot \P(A_1|A_0) \cdot
\P(A_2|A_0A_1)\cdot\ldots\cdot \P(A_n|A_0A_1\ldots A_{n-1})\, .
\end{equation}
\end{theorem}
\begin{proof}
Protože
$$
A_0A_1\ldots A_{n-1} \subset A_0A_1\ldots A_{n-2} \subset \ldots \subset A_0\, ,
$$
platí
$$
0< \P(A_0A_1\ldots A_{n-1})\leq \P(A_0A_1\ldots A_{n-2}) \leq \ldots\leq \P(A_0)
$$
a všechny podmíněné pravděpodobnosti jsou tedy dobře definovány.  Vztah (\ref{soucin}) dokážeme matematickou indukcí.\\
a) Pro $n=1$ máme
$$
\P(A_0A_1) = \P(A_0)\cdot \P(A_1|A_0),
$$
což plyne přímo z definice podmíněné pravděpodobnosti.\\
b) Pro přechod $n\to n+1$ použijeme definici podmíněné pravděpodobnosti v prvním, a indukční předpoklad ve druhém kroku
\begin{eqnarray*}
\P(A_0A_1\ldots A_{n+1}) &=& \P(A_0A_1\ldots A_{n})\cdot \P(A_{n+1}|A_0A_1\ldots A_{n})\\ & =& \P(A_0)\cdot \P(A_1|A_0) \cdot \ldots\cdot \P(A_n|A_0A_1\ldots A_{n-1})\cdot \P(A_{n+1}|A_0\ldots A_n)\, .
\end{eqnarray*}
\end{proof}
\begin{example}
(Polya's urn scheme) V urně je $b$ bílých a $c$ černých koulí.  Náhodně vybereme kouli, vrátíme ji zpět a přidáme dalších $k$ koulí stejné barvy. Určete pravděpodobnost, že ve třech po sobě jdoucích pokusech vytáhneme vždy bílou kouli.

Nechť $B_i$ značí jev, že v $i$-tém tahu vytáhneme bílou kouli.  Hledaná pravděpodobnost potom je
$$
\P(B_1B_2B_3) = \P(B_1)\cdot \P(B_2|B_1)\cdot \P(B_3|B_1B_2) = \frac{b}{b+c}\cdot\frac{b+k}{b+c+k}\cdot\frac{b+2k}{b+c+2k}\, .
$$
\end{example}
\begin{defin}
Konečnou množinu $\{B_1,\ldots,B_n\}$ jevů z $\A$, pro kterou platí
\begin{itemize}
  \item[a)] $B_i\cap B_j =\emptyset$ pro $i\neq j$
  \item[b)] $\P(B_i)>0\ \forall i\in \wh n$
  \item[c)] $\P\left(\sum_{i=1}^n B_i\right) =1$\, ,
\end{itemize}
nazveme úplný soubor jevů.
\end{defin}
\begin{theorem}
{\rm (O úplné pravděpodobnosti)} Nechť $\{B_1,\ldots,B_n\}$ je úplný soubor jevů a nechť $A\in\A$. Potom platí
$$
\P(A) = \sum_{i=1}^n \P(A|B_i)\cdot \P(B_i)
$$
(rozklad nepodmíněné pravděpodobnosti na podmíněné).
\end{theorem}
\begin{proof}
Podle Věty \ref{jevy} platí pro libovolné jevy $A,B$ rozklad $A=AB+AB^C$. Pokud jev $B$ nahradíme jevem $\sum_{i=1}^n B_i$, platí
$$
A= A \left( \sum_{i=1}^nB_i\right) + A\left( \sum_{i=1}^n
B_i\right)^C \, .
$$
Protože ale v našem případě platí
$$
\P\left(\sum_{i=1}^nB_i\right)^C = 0,
$$
dostáváme
$$
\P(A) = \P\left(A\left(\sum_{i=1}^n B_i \right)\right) =
\P\left(\sum_{i=1}^nAB_i \right) = \sum_{i=1}^n\P(AB_i)=
\sum_{i=1}^n \P(A|B_i)\P(B_i)\, ,
$$
kde jsme využili nezávislosti jevů $AB_i$ a definice podmíněné pravděpodobnosti.
\end{proof}
\begin{example}
Mějme krabici s $n_1$ lístky s číslem 1 a $n_2$ lístky s číslem $2$. Vytáhneme náhodně lístek a podle výsledku se přesuneme do prvního nebo druhého osudí, která obsahují $b_1$ bílých a $c_1$ černých, resp. $b_2$ bílých a $c_2$ černých koulí. Jaká je pravděpodobnost, že vytáhneme bílou kouli?

Označme jevy $B_1$ - vytáhnu lístek s číslem 1, $B_2$ - vytáhnu lístek s číslem 2, $A$ - vytáhnu bílou kouli. Protože jevy $B_1, B_2$ tvoří úplný systém jevů, můžeme použít tvrzení předchozí věty a dostáváme
$$
\P(A) = \P(A|B_1)\P(B_1) + \P(A|B_2)\P(B_2) = \frac{b_1}{b_1+c_1}\cdot \frac{n_1}{n_1+n_2} + \frac{b_2}{b_2+c_2}\cdot \frac{n_2}{n_1+n_2}\, .
$$
Poznamenejme jen, že postup přes všechny možné a příznivé výsledky by byl v tomto případě mnohem složitější.
\end{example}
\begin{theorem}
{\rm (Bayesova věta)} Nechť $\{B_1,\ldots,B_n\}$ je úplný soubor jevů a nechť $A\in\A$\, $\P(A)>0$. Potom pro každé $i\in\wh n$ platí
\begin{equation}\label{bayes}
  \P(B_i|A) = \frac{\P(A|B_i)\P(B_i)}{\sum_{i=1}^n \P(A|B_i)\P(B_i)}\, .
\end{equation}
\end{theorem}

\begin{proof}
Použitím definice podmíněné pravděpodobnosti a věty o úplné pravděpodobnosti přímo dostaneme
$$
\P(B_i|A) = \frac{\P(B_iA)}{\P(A)}=\frac{\P(AB_i)}{\P(A)}=\frac{\P(A|B_i)\P(B_i)}{\sum_{i=1}^n \P(A|B_i)\P(B_i)}\, .
$$
\end{proof}
\begin{remark}
Tato věta umožňuje zabývat se "opačným" přístupem. Na konci pozorujeme jev $A$ a ptáme se, jaká je pravděpodobnost, že nastala hypotéza $B_i$.
\end{remark}

\begin{example}
Telegrafní zpráva se skládá ze symbolů "$\cdot$" a "-". Při přenosu se zkreslí průměrně 2/5 teček a 1/3 čárek. Dále víme, že v signálech se symboly "$\cdot$" a "-" vyskytují v poměru 5:3.  Určete pravděpodobnost, že byl přijat vyslaný signál, jestliže a) byla přijata "$\cdot$",  b) byla přijata "-".

Označme $A$ - jev, že byla přijata "$\cdot$", $B$ - jev, že byla přijata "-", $C_1$ - byla vyslána "$\cdot$" a $C_2$ - byla vyslána "-". Jevy $C_1, C_2$ tvoří úplný soubor jevů a platí pro ně
$$
\frac{\P(C_1)}{\P(C_2)}=\frac{5}{3}, \qquad  \P(C_1)+\P(C_2) = 1
$$
a tedy $\P(C_1)=5/8$ a $\P(C_2)=3/8$. Protože dále
$$
\P(A|C_1) = \frac{3}{5}\qmq{a} \P(A|C_2)=\frac{1}{3}
$$
dostáváme s pomocí Bayesovy věty
$$
\P(C_1|A) = \frac{\P(A|C_1)\P(C_1)}{\P(A|C_1)\P(C_1)+\P(A|C_2)\P(C_2)} = \frac{\frac{3}{5}\cdot \frac{5}{8}}{\frac{3}{5}\cdot \frac{5}{8} +\frac{1}{3}\cdot \frac{3}{8}} = \frac{3}{4}\, .
$$
Podobně pro bod b) dostáváme
$$
\P(B|C_1) = \frac{2}{5}\qmq{a} \P(B|C_2)=\frac{2}{3}
$$
a
$$
\P(C_2|B) = \frac{\P(B|C_2)\P(C_2)}{\P(B|C_1)\P(C_1)+\P(B|C_2)\P(C_2)} = \frac{\frac{2}{3}\cdot \frac{3}{8}}{\frac{2}{5}\cdot \frac{5}{8} +\frac{2}{3}\cdot \frac{3}{8}} = \frac{1}{2}\, .
$$
\end{example}

\begin{exercise}
Tři střelci zasáhnou při každém výstřelu terč s pravděpodobnostmi po řadě 4/5, 3/4 a 2/3. Při současném výstřelu všech tří střelců nastaly dva zásahy. S jakou pravděpodobností se minul třetí střelec? [6/13]
\end{exercise}

\begin{exercise}
Společnost 5 mužů a 10 žen se náhodně rozdělí na pět skupin po třech osobách. Určete pravděpodobnost, že v každé skupině bude jeden muž. [0.081]
\end{exercise}

\begin{exercise}
Mějme 5 krabic a v každé z nich $b$ bílých a $c$ černých koulí. Z první krabice se náhodně vytáhne jedna koule a přemístí se do druhé. Pak se z druhé krabice náhodně jedna vybere a přemístí do třetí atd. Určete, jaká je po tomto přemísťování pravděpodobnost toho, že se z poslední krabice vytáhne bílá koule. [b/(b+c)]
\end{exercise}

\begin{exercise}
Mějme tři krabice. V první krabici je 1 bílá, 2 černé a 3 modré koule, ve druhé 2 bílé, 1 černá a 1 modrá a ve třetí 4 bílé, 5 černých a 3 modré. Někdo náhodně vybere krabici, vytáhne z ní kouli a řekne nám, že je bílá. Z jaké krabice ji s největší pravděpodobností vytáhl? [pravděpodobnosti 1/6,1/2,1/3]
\end{exercise}

\begin{exercise}
U elektrického spotřebiče se výrobní vada vyskytuje s pravděpodobností 0.1. U výrobků s touto vadou dochází v záruční době k poruše s pravděpodobností 0.5, výrobky bez vady mají pravděpodobnost poruchy v záruční době 0.01. Určete pravděpodobnost, že výrobek, který se v záruční době porouchal má uvedenou vadu.
\end{exercise}

\begin{exercise}
Zamýšlíte koupit v autobazaru vůz jisté značky. Je ovšem známo, že 30\% takových vozů má vadnou převodovku. Abyste získali více informací, najmete si mechanika, který je po projížďce schopen odhadnout stav vozu a jen s pravděpodobností 0,1 se zmýlí. Jaká je pravděpodobnost, že vůz, který zamýšlíte koupit, má vadnou převodovku
\begin{itemize}
  \item[a)] předtím, než si najmete mechanika
  \item[b)] jestliže mechanik předpoví, že vůz je dobrý?
\end{itemize}
\end{exercise}

\section{Nezávislé jevy}

\begin{defin}
Řekneme, že dva jevy $A$ a $B$ jsou nezávislé, jestliže
$$
\P(A\cap B) = \P(A)\cdot \P(B)\, .
$$
\end{defin}
\begin{remark}
Z definice podmíněné pravděpodobnosti víme, že $\P(A\cap B) = \P(A|B)\P(B)$, jsou-li jevy $A$ a $B$ nezávislé dostáváme
$$
\P(A|B) = \P(A)\, .
$$
Neboli jsou-li jevy $A$ a $B$ nezávislé, znamená to, že výskyt jevu $B$ neovlivní pravděpodobnost jevu $A$ a naopak.
\end{remark}
\begin{example}
Uvažujme hod kostkou a označme jevy $A$ - padne sudé číslo a $B$ - padne číslo nepřevyšující 2. Jsou jevy $A$ a $B$ nezávislé?

Jevy $A$, $B$ a $A\cap B$ jsou reprezentovány výsledky
$$
A=\{2,4,6\}, \quad B=\{1,2\} \qmq{a} A\cap B=\{2\}\, .
$$
To znamená, že
$$
\P(A)=\frac{1}{2},\quad \P(B)=\frac{1}{3}  \qmq{a} \P(A\cap B)=\frac{1}{6}
$$
a tedy $\P(A\cap B) = \P(A)\cdot \P(B)$, z čehož vyplývá, že jevy $A$ a $B$ jsou nezávislé. (poznamenejme jenom, že $\P(A|B)=1/2$ a $\P(A)=1/2$ a výskyt jevu $B$ tedy neovlivnil pravděpodobnost jevu $A$.)
\end{example}

\begin{example}
Pozměňme trochu zadání předchozího příkladu a uvažujme hod kostkou a jevy $A$ - padne sudé číslo a $B$ - padne číslo nepřevyšující 3. Jsou jevy $A$ a $B$ nezávislé?

Jevy $A$, $B$ a $A\cap B$ jsou reprezentovány výsledky
$$
A=\{2,4,6\}, \quad B=\{1,2,3\} \qmq{a} A\cap B=\{2\}\, .
$$
To znamená, že
$$
\P(A)=\frac{1}{2},\quad \P(B)=\frac{1}{2}  \qmq{a} \P(A\cap B)=\frac{1}{6}
$$
a tedy $\P(A\cap B) \neq \P(A)\cdot \P(B)$, z čehož vyplývá, že jevy $A$ a $B$ nejsou nezávislé. (poznamenejme, že $\P(A|B)=1/3$ a $\P(A)=1/2$ a výskyt jevu $B$ tedy ovlivnil pravděpodobnost jevu
$A$.)
\end{example}

\begin{theorem}
Nechť $A,B\in \A,\ \P(B)>0$. Potom jsou jevy $A$ a $B$ nezávislé
právě tehdy, když
$$
\P(A|B) = \P(A)\, .
$$
\end{theorem}

\begin{proof}
$\Rightarrow:$ Z definice podmíněné pravděpodobnosti a nezávislosti jevů $A$ a $B$ dostáváme
$$
\P(A|B) = \frac{\P(A\cap B)}{\P(B)}=\frac{\P(A)\cdot \P(B)}{\P(B)}=\P(A)\, .
$$
$\Leftarrow:$ Z předpokladu $\P(A|B) = \P(A)$ a definice podmíněné pravděpodobnosti vyplývá
$$
\P(A\cap B) = \P(A|B)\P(B) = \P(A)\P(B)
$$
a tedy jevy $A$ a $B$ jsou nezávislé.
\end{proof}

\begin{defin}
Řekneme, že jevy $A_1,A_2,\ldots, A_n$ jsou (sdruženě) nezávislé, jestliže
$$
\P(A_{i_1}\ldots A_{i_k}) = \P(A_{i_1})\ldots \P(A_{i_k})
$$
pro každý konečný výběr $\{i_1,\ldots,i_k\}\in\wh n$.
\end{defin}

\begin{defin}
Jevy $A_1,\ldots,A_n$ se nazývají párově nezávislé, jestliže pro každé $i\neq j$ platí
$$
\P(A_i\cap A_j) = \P(A_i)\cdot \P(A_j)\, .
$$
\end{defin}

\begin{example}
Předpokládejme náhodný pokus s množinou možných elementárních výsledků $\Omega=\{\omega_1, \omega_2,\omega_3,\omega_4\}$, které jsou všechny stejně pravděpodobné (např. hod kostkou ve tvaru čtyřstěnu). Definujme tři jevy
$$
A=\{\omega_1, \omega_2\},\quad B=\{\omega_1,\omega_3\},\quad
C=\{\omega_1,\omega_4\}\, .
$$
Potom $\P(A)=\P(B)=\P(C)=1/2$ a protože $A\cap B=A\cap C = B\cap C = \{\omega_1\}$ dostáváme
$$
\P(AB) = \P(AC) =  \P(BC) = \frac{1}{4}
$$
a tedy jevy $A,B,C$ jsou párově nezávislé. Protože ale
$$
\frac{1}{4} = \P(ABC) \neq \P(A)\P(B)\P(C) =\frac{1}{8}\, ,
$$
jevy $A,B,C$ nejsou sdruženě nezávislé.
\end{example}

\begin{theorem}
Jsou-li jevy $A$ a $B$ nezávislé, pak také jevy $A$ a $B^C$ jsou nezávislé.
\end{theorem}

\begin{proof}
Zapišme jev $A$ jako $A=AB+AB^C$. Potom $\P(A)=\P(AB)+\P(AB^C)$ a tedy
$$
\P(AB^C)=\P(A)-\P(AB)=\P(A)-\P(A)\P(B) = \P(A)(1-\P(B))=\P(A)\P(B^C)\, .
$$
\end{proof}

