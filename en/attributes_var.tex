\chapter{Characteristics of random variables}

\section{Expectation}

\begin{defin}
    The \emph{expectation}, or expected value, of a random variable $X$ is defined as:
    \begin{equation*}
        \E[X] = \int_0^\infty(1-F_X(x))\,dx - \int_{-\infty}^0F_X(x)\,dx,
    \end{equation*}
    provided that both integrals are converge. Otherwise we say that the expectation of $X$ does not exist.
\end{defin}

\begin{figure}\centering
    \includegraphics[scale=.4]{expectation}
    \caption{The definition of expectation of a random variable.}
    \label{fig:expectation}
\end{figure}

In other words the expectation is obtained by subtracting the area under $F_X(x)$ for $-\infty<x<0$ and the area between $F_X(x)$ and 1 for $0<x<\infty$. See \cref{fig:expectation} for an illustration. For discrete or absolutely continuous random variables, the expectation admits alternative descriptions.

\begin{theorem}
    Let $X$ be a discrete random variable and assume that $\P[X=x]=0$ outside of the points $(x_i)_{i=1}^\infty$. Then $X$ has an expectation and 
    \begin{equation*}
        \E[X] = \sum_{i=1}^\infty x_i\P[X=x_i]
    \end{equation*}
    if and only if the series converges absolutely.
\end{theorem}

\begin{theorem}
    Let $X$ be an absolutely continuous random variable. Then $X$ has an expectation and
    \begin{equation*}
        \E[X] = \int_{-\infty}^\infty x f_X(x)\, dx
    \end{equation*}
    if and only if the integral converges absolutely. 
\end{theorem}

We give the proof in the absolutely continuous case. 

\begin{proof}
    The proof relies on integration by parts. First let 
    \begin{equation*}
        I_c = \int_0^c xf_X(x)\,dx\qmq{and} J_c = \int_0^c (1-F_X(x))\,dx
    \end{equation*}
    for $c\geq 0$, and $I_\infty = \lim_{c\to\infty} I_c$, $J_\infty = \lim_{c\to\infty}J_c$. Note that these limits exist, though they might be infinite. Let us show that $I_\infty<\infty$ if and only if $J_c<\infty$, in which case the two limits are equal. Using integration by parts we obtain:
    \begin{align*}
        J_c = \int_0^c (1-F_X(x))\,dx &= [x(1-F_X(x))]_0^c + \int_0^cxf_X(x)\,dx = c(1-F_X(c)) + I_c.
    \end{align*}
    Therefore $I_c\leq J_c$, which shows that $J_\infty\leq I_\infty$. Next observe that
    \begin{equation*}
        c(1-F_X(c)) = c\int_c^\infty f_X(x)\,dx \leq \int_c^\infty xf_X(x)\,dx = I_\infty - I_c,
    \end{equation*}
    where the last equality assumes that $I_\infty<\infty$. Therefore, in that case
    \begin{equation*}
        J_\infty \leq \lim_{c\to\infty}(I_\infty - I_c + I_c) = I_\infty
    \end{equation*}
    This shows that $I_\infty = J_\infty < \infty$ whenever any of the limits is finite.

    Using the same strategy we can show that 
    \begin{equation*}
        -\int_{-\infty}^0xf_X(x)\,dx = \int_{-\infty}^0F_X(x)\,dx
    \end{equation*}
    whenever any of the two integrals is finite. Therefore existence of the expectation is equivalent to absolute convergence of $\int_{-\infty}^\infty xf_X(x)\,dx$, in which case
    \begin{equation*}
        \E[X] = \int_0^\infty (1-F_X(x))\,dx - \int_0^\infty F_X(x)\,dx = \int_0^\infty xf_X(x)\,dx + \int_{-\infty}^0xf_X(x)\,dx = \int_{-\infty}^\infty xf_X(x)\,dx.\qedhere
    \end{equation*}
    
\end{proof}

\begin{theorem}\label{t:expectation-function}
    Let $X$ be a random variable and $h\from\RR\to\RR$ be a function such that $Y=h(X)$ is a random variable.
    \begin{enumerate}[label={\alph{*})}]
        \item If $X$ is discrete then $Y$ has expectation
            \begin{equation*}
                \E[Y] = \sum_i h(x_i) \P[X=x_i],
            \end{equation*}
            provided this series converges absolutely.
        \item If $X$ is absolutely continuous then $Y$ has expectation
            \begin{equation*}
                \E[Y] = \int_{-\infty}^\infty h(x) f_X(x)\, dx,
            \end{equation*}
            provided this integral converges absolutely.
    \end{enumerate}
\end{theorem}

\begin{proof}
    a) $Y$ is a discrete random variable whose values are of the form $y_i = h(x_i)$ where $x_i$ is in the image of $X$. Therefore
    \begin{equation*}
        \E[Y] = \sum_i y_i\P[Y=y_i] = \sum_i h(x_i) \P[X=x_i].
    \end{equation*}

    b) We give the proof for the case where $h$ is a strictly increasing function. By \cref{transf} and the fact that $f_Y$ is zero outside of $(h(-\infty),h(\infty))$, we get
    \begin{align*}
        \int_{-\infty}^\infty h(x) f_X(x)\, dx &= \int_{h(-\infty)}^{h(\infty)} y f_X(h^{-1}(y)) \frac{dh^{-1}(y)}{dy} \,dy & 
            \text{with } y=h(x), dx = \frac{dh^{-1}(y)}{dy} \\
        &= \int_{h(-\infty)}^{h(\infty)} y f_Y(y)\,dy= \int_{-\infty}^{\infty} y f_Y(y)\,dy
    \end{align*}
    where the last integral is $\E[Y]$.
\end{proof}

A complete proof can be found in \cite{book/Tucker1962}.

\begin{remark}
    Both of these theorems also have multivariate forms. If $h\from\rr^n\to\rr$ is continuous and $X_1,\dots,X_n$ are random variables with JACD, then the random variable $h(X_1,\dots,X_n)$ has an expectation and
    \begin{equation*}
        \E[h(X_1,\dots,X_n)] = \int_{-\infty}^\infty\int_{-\infty}^\infty h(x,y)f_{X,Y}(x,y)\,dxdy 
    \end{equation*}
    if and only if the double integral converges absolutely.
\end{remark}

\begin{theorem}
    Let two random variables $X,Y$ be either discrete or absolutely continuous. Assume that $\E[X]$ and $\E[Y]$ exist. Then $\E[X+Y]$ exists and 
    \begin{equation*}
        \E[X+Y] = \E[X] + \E[Y].
    \end{equation*}
\end{theorem}

\begin{proof}
    In the discrete case:
    \begin{align*}
        \E[X+Y] &= \sum_{i=1}^\infty\sum_{j=1}^\infty (x_i+y_j) \P[X=x_i,Y=y_j] \\
                &= \sum_{i=1}^\infty x_i \sum_{j=1}^\infty \P[X=x_i,Y=y_j] + \sum_{j=1}^\infty y_j \sum_{i=1}^\infty \P[X=x_i,Y=y_j] \\
                &= \sum_{i=1}^\infty x_i \P[X=x_i] + \sum_{j=1}^\infty y_j \P[Y=y_j] \\
        &= \E[X] + \E[Y].
    \end{align*}

    In the absolutely continuous case:
    \begin{align*}
        \E[X+Y] &= \int_{-\infty}^\infty \int_{-\infty}^\infty (x+y) f_{X,Y}(x,y) \, dxdy \\
                &= \int_{-\infty}^\infty x\left(\int_{-\infty}^\infty f_{X,Y}(x,y) \, dy\right) dx + \int_{-\infty}^\infty y\left( \int_{-\infty}^\infty f_{X,Y}(x,y) \, dx \right) dy \\
                &= \int_{-\infty}^\infty x f_{X}(x) \, dx+ \int_{-\infty}^\infty y f_{Y}(y) \, dy \\
        &= \E [X] + \E [Y].\qedhere
    \end{align*}
\end{proof}

\begin{theorem}
    Let $X$ be a random variable with expectation $\E[X]$. For every $a\in\RR$
    \begin{equation*}
        \E[aX]=a\E[X].
    \end{equation*}
\end{theorem}

We can think of a constant $a\in\rr$ as a random variable $X$ for which $\P[X=a]=1$. Then we get $\E[a] = a$. 

\begin{cor}
    For every random variable $X$ and every constants $a,c\in\RR$
    \begin{equation*}
        \E[aX+b] = a\E[X]+b.
    \end{equation*}
\end{cor}

\begin{theorem}
    Let $X,Y$ be independent random variables which are either discrete or absolutely continuous. If $X$ and $Y$ have expectations then $XY$ has an expectation and
    \begin{equation*}
        \E[XY] = \E[X] \cdot \E[Y].
    \end{equation*}
\end{theorem}

\begin{proof}
    Using independence and \cref{t:expectation-function}, we get
    \begin{eqnarray*}
        \E[XY] &=& \int_{-\infty}^\infty \int_{-\infty}^\infty xy f_{X,Y}(x,y) \, dxdy = \int_{-\infty}^\infty \int_{-\infty}^\infty xy f_{X}(x) f_Y(y) \, dxdy\\
               &=& \left( \int_{-\infty}^\infty x f_{X}(x) \, dx\right) \left( \int_{-\infty}^\infty y f_{Y}(y) \, dy\right)= \E[X] \cdot \E[Y].
    \end{eqnarray*}
\end{proof}

\section{Moments and variance}

\begin{defin}
    Let $X$ be a random variable and $k\in\NN$. The \emph{$k$-th moment} of $X$ is defined as the quantity
    \begin{equation*}
        \mu_k'(X) = \E[X^k],
    \end{equation*}
    whenever it exists. 

    The \emph{$k$-th central moment} of $X$ is defined as the quantity
    \begin{equation*}
        \mu_k(X) = \E[(X-\E[X])^k],
    \end{equation*}
    whenever it exists.
\end{defin}

\begin{remark}
    For $k=0,1$ we have
    \begin{equation*}
        \mu'_0(X)=\mu_0(X)=1,\quad \mu'_1(X)=\E[X],\quad \mu_1(X) = \E[X-\E[X]] =0.
    \end{equation*}
\end{remark}

\begin{defin}
    The second central moment $\mu_2(X)$ is called the \emph{variance} of the random variable $X$. It is denoted by $\Var(X)$. In other words,
    \begin{equation*}
        \Var(X) = \E[(X-\E X)^2].
    \end{equation*}

    The \emph{standard deviation} of $X$ is the quantity
    \begin{equation*}
        \sd(X) = \sqrt{\Var(X)}.
    \end{equation*}
\end{defin}

\begin{remark}
    The expectation $\E X$ indicates the \emph{center of mass}, the value around which the random variable $X$ fluctuates. The variance $\Var(X)$ indicates the \emph{magnitude} of that fluctuation.
\end{remark}

\begin{properties}
    Let $X,Y$ be random variables whose second moments exist.
    \begin{enumerate}
        \item $\Var(X) = \E[X^2] - \E[X]^2$.
        \item For all $a,b\in \rr$, $\Var(aX+b) = a^2 \Var(X)$.
        \item If $X,Y$ are independent then $\Var(X+Y) = \Var(X) + \Var(Y)$.
        \item $\Var(X)\geq 0$ with equality if and only if $\P[X=\E[X]]=1$.
    \end{enumerate}
\end{properties}

We use the following lemma.
\begin{lemma}\label{l:positive}
    For every random variable $X$,
    \begin{equation*}
        \E[X^2]\geq 0,
    \end{equation*}
    with equality if and only if $\P[X=0]=1$.
\end{lemma}

\begin{proof}
    Since $F_{X^2}(x) =0$ for $x<0$,
    \begin{equation*}
        \E[X^2] = \int_0^\infty(1-F_{X^2}(x))\,dx \geq \int_0^\infty0\,dx = 0.
    \end{equation*}

    For the remaining part of the statement, we prove the contrapositive. Assume first that $\P[X=0] < 1$. Then there exists some $n$ such that
    \begin{equation*}
        \P[X^2>1/n] = \varepsilon > 0.
    \end{equation*}
    Therefore for $x<1/n$, 
    \begin{equation*}
        \E[X^2] = \int_0^\infty(1-F_{X^2}(x))\,dx \geq \int_0^{1/n}(1-F_{X^2}(x))\,dx \geq \int_0^{1/n}\varepsilon\,dx = \varepsilon/n > 0.
    \end{equation*}

    Conversely assume that $\E[X^2]>0$. Then $1-F_{X^2}(x) = {\P[X^2>x]} > 0$ for some $x>0$. But notice that
    \begin{equation*}
        \P[X^2>x] = \P[X>\sqrt{x}] + \P[X<-\sqrt{x}] \leq \P[X\neq 0].
    \end{equation*}
    Therefore $\P[X=0]<1$.
\end{proof}

\begin{proof}
    1. By the linearity of expectation,
    \begin{align*}
        \Var(X) &= \E[X-\E[X]]^2 = \E[X^2-2X\E[X] +\E[X]^2] = \E[X^2] - \E[2X \E[X]] + \E[\E[X]]^2\\
                &= \E[X^2] - 2 \E[X]^2 +\E[X]^2 = \E[X^2] - \E[X]^2.
    \end{align*}

    2. Using similar ideas,
    \begin{align*}
        \Var(aX+b) &= \E[(aX+b -\E[aX+b])^2] = \E[(aX+b-a\E[X]-b)^2] \\
                   &= \E[(aX-a\E[X])^2] = a^2\E[(X-\E[X])^2] = a^2\Var(X). 
    \end{align*}

    3. Once again by linearity,
    \begin{align*}
        \Var(X+Y) &= \E[(X+Y-\E[X+Y])^2] = \E[(X-\E[X]+Y-\E[Y])^2] \\
                  &= \E[(X-\E[X])^2] + \E[(Y-\E[Y])^2] + 2\E[(X-\E[X])(Y-\E[Y])] \\
                  &= \Var X + \Var Y+ 2\E[(X-\E[X])(Y-\E[Y])].
    \end{align*}
    Because the variables are independent,
    \begin{align*}
        \E[(X-\E[X])(Y-\E[Y])]&=\E[XY -X\E[Y] -Y \E[X] +\E[X]\E[Y]]\\
                              &=\E[XY]-\E[X]\E[Y] = 0.\qedhere
    \end{align*}

    4. Apply the previous lemma with to the random variable $X-\E[X]$. 
\end{proof}

The last property tells us that having zero variance is the same as equal to the expectation almost surely (i.e. with probability 1).

\begin{example}
    Let $X\sim \binomial(n,p)$. Let us determine $\E[X]$ and $\Var(X)$.

    According to the formula for the expectation of a discrete distribution,
    \begin{align*}
        \E[X] &= \sum_{i=0}^n i \binom{n}{i} p^i(1-p)^{n-i} =\sum_{i=1}^n \frac{n!}{(n-i)!(i-1)!} p^i(1-p)^{n-i} \\
             &= np \sum_{i=1}^n \frac{(n-1)!}{(n-i)!(i-1)!} p^{i-1}(1-p)^{n-i}= np \sum_{j=0}^{n-1} \binom{n-1}{j} p^j(1-p)^{n-j-1} \\
             &= np(p+(1-p))^{n-1}=np.
    \end{align*}
    Similarly,
    \begin{align*}
        \E[X^2] &= \sum_{i=0}^n i^2 \binom{n}{i} p^i(1-p)^{n-i} \\
                &= \sum_{i=0}^n i(i-1) \binom{n}{i} p^i(1-p)^{n-i} + \sum_{i=0}^n i \binom{n}{i} p^i(1-p)^{n-i} = n(n-1)p^2 + np,
    \end{align*}
    where the first sum is calculated using a similar procedure as the above. The variance is therefore:
    \begin{equation*}
        \Var(X) = \E[X^2] - \E[X]^2 = n(n-1)p^2 +np-n^2 p^2 = np(1-p).
    \end{equation*}
\end{example}

\begin{example}
    Determine its expectation and variance of a random variable $X\sim \normal(\mu,\sigma^2)$.

    According to the formula for the expectation of an absolutely continuous random variable,
    \begin{align*}
        \E[X] = \int_{-\infty}^\infty x \frac{1}{\sqrt{2\pi\sigma^2}} e^{-\frac{(x-\mu)^2}{2\sigma^2}} \, dx = \frac{1}{\sqrt{2\pi\sigma^2}} \int_{-\infty}^\infty (x-\mu) e^{-\frac{(x-\mu)^2}{2\sigma^2}} \,dx + \frac{\mu}{\sqrt{2\pi\sigma^2}} \int_{-\infty}^\infty e^{-\frac{(x-\mu)^2}{2\sigma^2}} \,dx.
    \end{align*}
    Notice that the second integral is equal to $\mu\P[-\infty<X<\infty]=\mu$. With the change of variable $(x-\mu)/\sigma=t$, the first integral is equal to
    \begin{equation*}
        \frac{\sigma}{\sqrt{2\pi}} \int_{-\infty}^\infty te^{-\frac{t^2}{2}}\, dt = \frac{\sigma}{\sqrt{2\pi}}\left( \int_{-\infty}^0 te^{-\frac{t^2}{2}}\, dt + \int_{0}^\infty te^{-\frac{t^2}{2}}\, dt\right) = 0,
    \end{equation*}
    because the function $te^{-\frac{t^2}2}$ is antisymetric. Therefore, $\E[X] = \mu$. 

    For the variance,
    \begin{align*}
        \E[X^2] &= \int_{-\infty}^\infty x^2 f_X(x)\,dx = \int_{-\infty}^\infty (x-\mu+\mu)^2 f_X(x)\,dx \\
                &= \int_{-\infty}^\infty (x-\mu)^2 f_X(x)\, dx +2\mu \int_{-\infty}^\infty (x-\mu) f_X(x)\, dx +\mu^2 \int_{-\infty}^\infty f_X(x)\, dx\\
                &= \frac{1}{\sqrt{2\pi\sigma^2}}\int_{-\infty}^\infty (x-\mu)^2 e^{\frac{(x-\mu)^2}{2\sigma^2}}\,dx + \mu^2.
    \end{align*}
    To compute the remaining integral:
    \begin{align*}
        \int_{0}^\infty (x-\mu)^2e^{-\frac{(x-\mu)^2}{2\sigma^2}}\, dx 
        &= \int_{0}^\infty  2\sigma^2s e^{-s}\, \frac{\sigma}{\sqrt{2s}}ds & 
            \text{with }s = \frac{(x-\mu)^2}{2\sigma^2}, dx=\frac{\sigma}{\sqrt{2s}}ds \\
        &= \sqrt{2}\sigma^3 \int_{0}^\infty \sqrt{s} e^{-s}\, ds &
            \text{with }s=\frac{t^2}{2}= s, ds= t dt \\
        &= \sqrt2\sigma^3 \Gamma(3/2) = \frac{\sqrt{2\pi}\sigma^3}2,
    \end{align*}
    since $\Gamma(3/2) = \frac12\Gamma(1/2) = \frac{\sqrt{\pi}}2$. Substituting this in the above formula for $\E[X^2]$,
    \begin{equation*}
        \E[X^2] = \sigma^2 + \mu^2.
    \end{equation*}
    Therefore
    \begin{equation*}
        \Var(X) = \E[X^2] - \E[X]^2 = \sigma^2 + \mu^2 -\mu^2 = \sigma^2.
    \end{equation*}
\end{example}

\begin{defin}
    The quantity 
    \begin{equation*}
        \mu_3(X/\sigma(X)) = \frac{\mu_3(X)}{\Var(X)^\frac{3}{2}},
    \end{equation*}
    if it exists, is called the \emph{skewness} of the random variable $X$. It is also denoted $\beta_1(X)$ or $\alpha_3(X)$.

    The quantity
    \begin{equation*}
        \mu_4(X/\sigma(X)) = \frac{\mu_4(X)}{\Var(X)^2},
    \end{equation*}
    if it exists, is called the \emph{kurtosis} of $X$, or simply \emph{kurtosis} (from the greek kurtos meaning \emph{curved} or \emph{convex}). It is sometimes denoted $\beta_2(X)$ or $\alpha_4(X)$. The quantity $\mu_4(X/\sigma(X))-3$ is also often used, and is called the \emph{kurtosis excess}.
\end{defin}

\begin{remark}
    The skewness measures the degree of asymmetry of the density function of a random variable.
    \begin{itemize}
        \item When $\mu_3(X/\sigma(X))<0$, the density of the random variable is skewed to the left of $\E[X]$.
        \item When $\mu_3(X/\sigma(X))>0$, the density of the random variable is skewed to the right of $\E[X]$.
        \item When $\mu_3(X/\sigma(X))=0$, the density of the random variable is symmetric with respect to $\E[X]$.
    \end{itemize}

    On the other hand, the kurtosis measures the wait of the tails: a higher curtosis indicates a higher likelihood for outliers. Heavy-tailed distributions tend to (though need not) have sharper peaks, which is why kurtosis is used as a proxy to measure the sharpness of the peaks (for a detailed discussion, see \cite{Westfall2014})

    A random variable $X\sim \normal(\mu,\sigma^2)$ has skewness $\mu_3(X/\sigma(X))=0$ and kurtosis $\mu_4(X/\sigma(X))=3$ (which explains the formula for excess kurtosis). These quantities can therefore be used to be measure \enquote{normality.} 
\end{remark}

\section{Covariance}

\begin{defin}
    The \emph{covariance} of random variables $X,Y$ is the quantity
    \begin{equation*}
        \Cov (X,Y) = \E[(X-\E X)(Y-\E Y)],
    \end{equation*}
    whenever this expectation exists. 

    If $\Cov(X,Y)= 0$ then we say that $X$ and $Y$ are \emph{uncorrelated}.
\end{defin}

\begin{properties}
    Let $X,Y$ be two random variables.
    \begin{enumerate}
        \item $\Cov(X,Y) = \E[XY] - \E[X]\E[Y]$.
        \item $\Cov(X,X) = \Var(X)$.
        \item If $X,Y$ are independent then $\Cov(X,Y)=0$.
    \end{enumerate}
\end{properties}

The next example shows that the converse of property 3 is false.

\begin{example}
    Let $X\sim\normal(0,1)$. It follows from the fact that $f_X(x)=f_X(-x)$ that $\E[X] =\E[X^3]=0$. Therefore 
    \begin{equation*}
        \Cov(X,X^2) = \E[X^3] - \E[X]\E[X^2] = 0.
    \end{equation*}
    In other words, $X$ and $X^2$ are uncorrelated. However, they are not independent since, for instance:
    \begin{equation*}
        \P[X>1]\P[X^2>1] = \P[X>1](\P[X>1]+\P[X<-1]) < \P[X>1] = \P[X>1,X^2>1].
    \end{equation*}
\end{example}

\begin{defin}
    Let $X,Y$ be random variables. The quantity
    \begin{equation*}
        \varrho (X,Y) = \frac{\Cov(X,Y)}{\sd(X)\sd(Y)} = \frac{\Cov(X,Y)}{\sqrt{\Var(X)\Var(Y)}},
    \end{equation*}
    whenever it exists, is called the \emph{correlation coefficient} of the random variables $X,Y$.
\end{defin}

In particular we need $\sd(X)$ and $\sd(Y)$ to be $\neq 0$.

\begin{theorem}\label{tcor}
    If two random variables $X,Y$ have a correlation coefficient then
    \begin{equation*}
        -1 \leq \varrho(X,Y) \leq 1.
    \end{equation*}
    Moreover,
    \begin{equation*}
        \varrho(X,Y) = 1 \iff (\exists \beta>0)(\P[Y-\E Y = \beta(X-\E X)]=1),
    \end{equation*}
    and
    \begin{equation*}
        \varrho(X,Y) = -1 \iff (\exists \beta<0)(Y-\E Y = \beta(X-\E X)).
    \end{equation*}
\end{theorem}

The proof uses the following classical result, named after French mathematician Augustin Louis Cauchy (1789--1857) and German mathematician Hermann Schwarz (1843--1921).

\begin{lemma}[Cauchy--Schwarz inequality] 
    Let $X,Y$ be random variables.
    \begin{equation*}
        \E[XY]^2 \leq \E[X^2] \E[Y^2]
    \end{equation*}
    whenever the corresponding expectations exist. Moreover, equality holds precisely when there exists $\beta\in\rr$ such that
    \begin{equation*}
        \P[Y = \beta X] = 1.
    \end{equation*}
\end{lemma}

\begin{proof}
    For all $a\in\RR$,
    \begin{equation*}
        0\leq \E[(a X-Y)^2] = \E[a^2X^2-2aXY+Y^2] = a^2 \E[X^2] - 2a\E[XY]+\E[Y^2].
    \end{equation*}
    For this inequality to be valid for all $a\in\RR$, the discriminant of the corresponding quadratic equation in $a$ must be less than or equal to 0, which means that
    \begin{equation*}
        4 \E[XY]^2 - 4 \E[X^2] \E[Y^2] \leq 0.
    \end{equation*}
    which proves the first part of the statement. For the second part, observe that the discriminant is equal to 0 precisely when there exists a solution $\beta$ to the above quadratic equation, which is equivalent to $\E[(\beta X-Y)^2]=0$. By \cref{l:positive} this is equivalent to:
    \begin{equation*}
        \P[\beta X=Y] = \P[\beta X-Y=0] = 1.\qedhere
    \end{equation*}
\end{proof}

\begin{proof}[Proof of the theorem \ref{tcor}]
    We use the Cauchy--Schwarz inequality with the random variables $X-\E[X]$ and $Y-\E[Y]$:
    \begin{equation*}
        \Cov(X,Y)^2 = \E[(X-\E[X])(Y-\E[Y])]^2 \leq \E[(X-\E[X])^2]\E[(Y-\E[Y])^2] = \Var(X)\Var(Y).
    \end{equation*}
    Therefore
    \begin{equation*}
        \varrho^2(X,Y) = \frac{\Cov(X,Y)^2}{\Var(X)\Var(Y)}\leq 1.
    \end{equation*}
    Moreover equality holds if and only if there exists $\beta\in\rr$ such that
    \begin{equation*}
        \P[X-\E[X] = \beta(Y-\E[Y])]=1.
    \end{equation*}
    Moreover it must the case that $\beta\neq 0$, otherwise we would have $\Var(X)=0$ and $\varrho(X,Y)$ would be undefined. Since $X=\beta Y+C$ almost surely, where $C =\E[X]-\beta\E[Y]$,
    \begin{equation*}
    \varrho(X,Y) = \frac{\Cov(X,\beta X+C)}{\sqrt{\Var(X)\Var(\beta X+C)}} = \frac{\beta\Var(X)}{\sqrt{\beta^2\Var(X)^2}}
    =\frac{\beta}{|\beta|},
    \end{equation*}
    which is $1$ if $\beta>0$ and $-1$ if $\beta<0$.
\end{proof}

\begin{remark}
    The correlation coefficient expresses the degree of linear dependence. A large absolute value of $\varrho(X,Y)$ means that the random variables $X,Y$ are close to being in a linear relation. 
\end{remark}

\begin{defin}
    Let $Z=(X_1,\ldots, X_n)$ be a random vector. The matrix
    \begin{equation*}
        \Sigma = (\sigma_{i,j})_{i,j=1}^n \qmq{where} \sigma_{ij} = \Cov(X_i,X_j)
    \end{equation*}
    is called the covariance matrix of the random vector $Z$.
\end{defin}

\begin{remark}
    It follows from the definition that $\Sigma$ is symmetric and the coefficients on the diagonal are the variances $\sigma_{ii}=\Var(X_i)$. If the variables are independent, then $\Sigma$ is a diagonal matrix.
\end{remark}

\section{Quantiles}

\begin{defin}
    Let $X$ be a random variable with distribution function $F_X$ and let $\alpha\in(0,1)$. Then \emph{$\alpha$-quantile} of $X$ is the point
    \begin{equation*}
        x_\alpha = \inf\{x \mid F_X(x)\geq \alpha\}.
    \end{equation*}
    If $F_X$ is strictly increasing and continuous, then $x_\alpha = F_X^{-1}(\alpha)$.
\end{defin}

\begin{remark}
    Some quantiles have dedicated names: quartiles of the form $x_{i/4}$ are called \emph{quartiles}, while those of the form $x_{i/10}$ are called \emph{decile}.
    \begin{itemize}
        \item $x_{0.5}$ is the median.
        \item $x_{0.25}$ is the lower quartile.
        \item $x_{0.75}$ is the upper quartile.
        \item $x_{0.1}$ is the lower decile.
        \item $x_{0.9}$ is the upper decile.
        \item $x_{0.75}-x_{0.25}$ is the interquartile range.
    \end{itemize}
\end{remark}

\begin{example}
    Let $X$ be a random variable with density function
    \begin{equation*}
        f_X(x)= \frac{1}{\pi} \,\frac{\theta}{\theta^2+(x-\mu)^2},\quad
        \theta>0, \mu\in\RR, x\in\RR\, .
    \end{equation*}
    This is an example of \emph{Cauchy distribution}, also called \emph{Lorentz distribution} (after Dutch physicist Hendrik Antoon Lorentz, 1853--1928). This random variable has no expectation, because the integral
    \begin{equation*}
        \X= \frac{1}{\pi} \int_{-\infty}^\infty \frac{\theta
        x}{\theta^2+(x-\mu)^2}\, dx
    \end{equation*}
    diverges. Similarly, it does not have a variance, and in fact none of its higher moments exist. 

    Let us compute the density function of $X$. For all $x\in\RR$,
    \begin{align*}
        F_X(x) &= \frac{1}{\pi} \int_{-\infty}^x \frac{\theta}{\theta^2+(t-\mu)^2}\, dt \\
               &= \frac{1}{\pi} \int_{-\infty}^{\frac{x-\mu}{\theta}} \frac{1}{1+y^2}\, dy &
               \text{where } y=\frac{t-\mu}{\theta},
            dt = \theta dy\\
            &= \frac{1}{\pi} \arctan\left(\frac{x-\mu}{\theta}\right)+ \frac{1}{2}.
    \end{align*}

    The $\alpha$-quantile is therefore determined by the equation
    \begin{equation*}
        \frac{1}{\pi}\\arctan\left(\frac{x_\alpha-\mu}{\theta}\right) + \frac{1}{2} =\alpha,
    \end{equation*}
    therefore
    \begin{equation*}
        x_\alpha = \theta\tan\left(\pi\left(\alpha-\frac{1}{2}\right)\right)+\mu.  
    \end{equation*}
    For the median, we obtain $x_{0.5}= \mu$.
\end{example}

\section{Characteristic and moment function}

\begin{defin}
    The \emph{characteristic function} of a random variable $X$ is the map $\varphi_X(t)\from\RR\to\CC$ defined for each $t\in\RR$ by
    \begin{equation*}
        \varphi_X(t) = \E[e^{itX}],
    \end{equation*}
    where by definition $\E[e^{itX}] = \E[\cos(tX)] + i\E[\sin(tX)]$.
\end{defin}

\begin{properties}
    Let $X$ be a random variable.
    \begin{enumerate}
        \item $\varphi_X(t)$ exists for all $t\in\RR$.
        \item $\varphi_X(0) = 1$ and for all $t\in\rr$, $|\varphi_X(t)| \leq 1$.
        \item The characteristic function of $aX+b$ is given by
            \begin{equation*}
                \E[e^{it(aX+b)}] = e^{ibt}\varphi_X(at).
            \end{equation*}
        \item If $X$ is absolutely continuous with density $f_X(x)$, then
        \begin{equation*}
            \varphi_X(t) = \int_{-\infty}^{\infty} f_X(x) e^{itx}\,dx.
        \end{equation*}
    \end{enumerate}
\end{properties}

In the case of an absolutely continuous random variable, $\varphi_X$ is the Fourier transform of $f_X(x)$. In particular, the distribution function $F_X$ is uniquely determined by the characteristic function $\varphi_X$. 

A notable use of characteristic functions is the proof of the \emph{central limit theorem}. A weaker tool, but which avoids the use of complex numbers, is the following.

\begin{defin}
    Let $X$ be a random variable with well defined finite moments. The \emph{moment generating function}, or MGF, of $X$ is defined by
    \begin{equation*}
        M_X(t) = \E[e^{tX}]
    \end{equation*}
    at all points $t$ where the expectation exists.
\end{defin}

\begin{remark}
    The MGF does not necessarily exist for all $t\in\RR$. Often we will assume that it exists on some interval $(-s,s)$ where $s > 0$. Its name comes from the fact that it can be used to calculate the moments of $X$. In addition, the MGF can be used to determine the distribution of a sum of independent random variables.
\end{remark}

\begin{theorem}
    Let $X$ be a random variable whose MGF exists on the interval $(-s,s)$, $s>0$. Then for every $k\in\NN$
    \begin{equation*}
        M^{(k)}_X(0) = \mu_k'(X) = \E[X^k].
    \end{equation*}
\end{theorem}

Moment generating functions are examples of \emph{Laplace transforms}.

\begin{example}\label{MVFpoiss}
    Let us determine the MGF of a random variable $X\sim \poisson(\lambda)$, $\lambda>0$, and then use it to obtain the expectation and variance.

    Recall that $X\sim\poisson(\lambda)$ means that
    \begin{equation*}
        \P [X=k] = e^{-\lambda} \frac{\lambda^k}{k!}, \qmq{for} k=0,1,2,\ldots.
    \end{equation*}
    Thus using the Taylor expansion of the function $e^z$ we get
    \begin{equation*}
        M_X(t) = \E[e^{tX}] = \sum_{k=0}^{\infty} e^{tk} e^{-\lambda} \frac{\lambda^k}{k!} = e^{-\lambda} \sum_{k=0}^{\infty} \frac{(\lambda e^t)^k}{k!} = e^{-\lambda} e^{\lambda e^t} = e^{\lambda (e^t-1)}\, .
    \end{equation*}
    We can then compute the first two derivatives function $M_X(t)$:
    $$
        \frac{dM_X(t)}{dt} = e^{\lambda (e^t-1)} \lambda e^t \qmq{and} \frac{d^2M_X(t)}{dt^2} = e^{\lambda (e^t-1)} \left( \lambda^2 e^{2t} + \lambda e^t \right),
    $$
    We can then substitute $t=0$ to obtain the expectation and second moment:
    $$
        \E[X] = \left. \frac{dM_X(t)}{dt} \right|_{t=0} = \lambda \qmq{a} \E[X^2] = \left. \frac{d^2M_X(t)}{dt^2} \right|_{t=0} = \lambda^2 + \lambda.
    $$
    For the variance, $\Var(X) = \E[X^2] - \E[X]^2 = \lambda$.
\end{example}

\begin{example}
    Let us determine the moment generating function of a random variable $X\sim \normal(\mu,\sigma^2)$. By definition
    \begin{equation*}
        M_X(t) = \E[e^{tX}] = \int_{-\infty}^{\infty} e^{tx} \frac{1}{\sqrt{2\sigma^2}} e^{-\frac{(x-\mu)^2}{2\sigma^2}} \, dx = \frac{1}{\sqrt{2\pi\sigma^2}} \int_{-\infty}^{\infty} e^{tx -\frac{(x-mu)^2}{2\sigma^2} }\, dx.
    \end{equation*}
    Using the change of variable $y = \frac{x-\mu}{\sigma}$, we get
    \begin{equation*}
        M_X(t) = \frac{1}{\sqrt{2\pi}}  \int_{-\infty}^{\infty} e^{t(\sigma y+\mu) - \frac{y^2}{2}} \, dy = \frac{e^{t\mu}}{\sqrt{2\pi}}  \int_{-\infty}^{\infty} e^{t\sigma y- \frac{y^2}{2}} \, dy.
    \end{equation*}
    Using the fact that $t\sigma y- \dfrac{y^2}{2} = \dfrac{1}{2} t^2\sigma^2 - \dfrac{1}{2} (y-t\sigma)^2$ we get
    \begin{equation*}
        M_X(t) = e^{t\mu+\frac{1}{2} t^2\sigma^2} \left( \frac{1}{\sqrt{2\pi}}  \int_{-\infty}^{\infty} e^{-\frac{(y-t\sigma)^2}{2}} \,dy \right) = e^{t\mu+\frac{1}{2} t^2\sigma^2},
    \end{equation*}
    where we used the fact that the expression in parentheses is the integral of the density of a normal distribution $\normal(t\sigma,1)$.
\end{example}

If the MGF of a random variable $X$ exists on some interval $(-s,s)$, $s>0$, then $M_X$ completely determines $F_X$ and vice versa. 
\begin{theorem}
    Let $X,Y$ be two random variables with well-defined MGFs on an interval $(-s,s)$, $s>0$. Then $M_X=M_Y$ if and only if $F_X=F_Y$.
\end{theorem}

\begin{theorem}
    Let $M_X(t)$ be the MGF of a random variable $X$. Then the MGF of the random variable $Y=aX+b$, $a,b\in\rr$, is 
    \begin{equation*}
        M_Y(t) = e^{bt} M_X(at).
    \end{equation*}
\end{theorem}

\begin{proof}
    This is a simple computation.
    \begin{equation*}
        M_Y(t) = \E[e^{tY}] = \E[e^{t(aX+b)}] = \E[e^{tb}\cdot e^{(at)X}] = e^{bt} M_X(at).
    \end{equation*}
\end{proof}

\begin{theorem}\label{MVFsoucet}
    Let $X_1,\ldots,X_n$ be independent random variables having MGFs on some interval $(-s,s)$, $s>0$. Then the random variable $Y=X_1+X_2+\ldots+X_n$ has MGF
    \begin{equation*}
        M_Y(t) = M_{X_1}(t) \cdot M_{X_2}(t) \cdots M_{X_n}(t) = \prod_{j=1}^{n} M_{X_j}(t)\, .
    \end{equation*}
\end{theorem}

\begin{proof}
    The MGF of $Y$ is by definition
    \begin{equation*}
        M_Y(t) = \E[e^{t(X_1+X_2+\dots+X_n)}] = \E[e^{tX_1} e^{tX_2} \dots e^{tX_n}].
    \end{equation*}
    Furthermore, the independence of the random variables $X_1,\ldots,X_n$ implies the independence of $e^{tX_1},\ldots, e^{tX_n}$. Using the property of the formula for expectations of independent random variables, we get
    \begin{equation*}
        M_Y(t) = \E[e^{tX_1}] \cdot \E[e^{tX_2}]\cdots \E[e^{tX_n}] = M_{X_1}(t) \cdot M_{X_2}(t) \cdots M_{X_n}(t).\qedhere
    \end{equation*}
\end{proof}

\begin{example}
    Let $X_1,\ldots,X_n$ be independent random variables where $X_j\sim Po(\lambda_j)$. Let us find the distribution of the sum $Y=\sum_{j=1}^{n} X_j$.

    In \cref{MVFpoiss} we showed that
    \begin{equation*}
        M_{X_j}(t) = e^{\lambda_j (e^t-1)}.
    \end{equation*}
    Therefore
    \begin{equation*}
        M_Y(t) = \prod_{j=1}^{n} e^{\lambda_j (e^t-1)} = e^{(\sum_{j=1}^{n} \lambda_j), (e^t-1)},
    \end{equation*}
    which is the MGF of a Poisson distribution with parameter $\sum_{j=1}^{n} \lambda_j$. Since the distribution of $Y$ is uniquely determined by its MGF we conclude that $Y\sim Po(\sum_{j=1}^{n} \lambda_j)$.
\end{example}

\section{Exercises}

\begin{enumerate}[wide,label={\textbf{\arabic{*}.}},itemsep=1em]

\item Let $X$ be a random variable with $X\sim \uniform(a,b)$. Determine the expectation $\E[X]$ and the variance $\Var(X)$.

\item Assume that a continuous random variable has a triangular distribution on the interval $(-a,a)$, meaning that the density forms an isosceles triangle with base $(-a,a)$. Find the formula for the density and calculate $\E[X], \Var(X)$.

\item A company earns 100 CZK for each product sold, and looses 120 CZK for a product which needs to be replaced before the end of the warranty period. Assume that the lifetime of the products (in years) follows a normal distribution $\normal(3,1)$. How long should the warranty period be so that the expected profit is at least 60 CZK?

\item Determine the value of the constant $c$ and the correlation coefficient of two discrete random variables $X,Y$ given by the joint discrete density
\begin{equation*}
    \P[X=x,Y=y] = c(x+y), \qmq{for} x,y\in\{1,2,3\}.
\end{equation*}
% [-1/23]

\item A random vector $(X,Y)$ has joint density 
\begin{equation*}
    f_{X,Y}(x,y) =  
        \begin{cases}
            \frac16(\frac{x}{2}+\frac{y}{3}) & \text{if } 0<x<2, 0<y<3 \\
            0 & \text{otherwise.}
        \end{cases}
\end{equation*}
Determine the marginal densities $f_X, f_Y$ and compute the correlation coefficient $\varrho(X,Y)$. % [-1/11]

\item Determine the MGF, expectation, and variance of a random variable $X\sim \Gamma(\alpha,\beta)$.

\item Let $X_1,X_2$ be independent random variables where $X_i\sim\normal(\mu_i,\sigma_i^2)$. Determine the distribution of $Y=X_1+X_2$. 

\item Let $X_1,X_2,\dots,X_n$ be independent random variables where $X_i\sim\normal(\mu,\sigma^2)$. Determine the distribution of $Y = \sum_{i=1}^{n} X_i$.

\item Determine the MGF, expectation and variance of a random variable $X\sim \exponential(\theta)$.

\item Let $X_1,\ldots,X_n$ be independent random variables, $X_i\sim \exponential(\theta)$. Determine the distribution of $Y = \sum_{i=1}^{n} X_i$.

\item Determine the distribution and density of a random variable of the form $Y=X_1^2+X_2^2+\cdots + X_n^2$, where $X_1,\ldots,X_n$ are independent and $X_i\sim \normal(0,1)$.

\item Determine the MGF of a random variable $X\sim \binomial(n,p)$.

\item Let $X_1,\ldots,X_n$ be independent random variables such that $X_i\sim \bernoulli(p)$. Determine the distribution of $Y = \sum_{i=1}^{n} X_i$.

\end{enumerate}
