\chapter{Conditional probabilities}

Conditional probabilities allows to take into account additional assumptions about a random experiment, usually in the form of \emph{partial information}. For example we might want to know:
\begin{itemize}
    \item the probability of rolling two 6s in a row with a fair dice \emph{knowing} that the first roll was not a 5,
    \item the probability of someone dying in the next year \emph{knowing} their age (this is called an actuarial or mortality table), or
    \item the probability of a student completing a study program \emph{knowing} that they already completed the first year.
\end{itemize}

Throughout the section, $(\Omega,\mathfrak{A},\P)$ will always denote a probability space.

\begin{definition}
    Let $A$ and $B$ be two events such that $\P(B) > 0$. The \emph{conditional probability} of $A$ knowing $B$ is 
    \begin{equation*}
        \P(A\mid B) = \frac{\P(AB)}{\P(B)}.
    \end{equation*}
\end{definition}

A useful notation for conditional probabilities is $\P_B(A) = \P(A\mid B)$.

\begin{theorem}
    If $\P(B)>0$, then $(\Omega,\mathfrak A,\P_B)$ is a probability space.
\end{theorem}

\begin{proof}
    We have to show that $\P_B$ satisfies the axioms of probability measures.
    \begin{enumerate}[wide]
        \item[\ref{axiom-4}.] $\P_B(\Omega) = \P (\Omega \cap B)/\P(B) = \P(B)/\P(B)= 1$.
        \item[\ref{axiom-5}.] For every event $A$, $\P_B(A) = \P(A B)/\P (B) \geq 0/\P(B) = 0$.
        \item[\ref{axiom-6}.] Let $(A_k)_{k=1}^\infty$ be a sequence of pairwise disjoint events. The events $(A_kB)_{k=1}^\infty$ are also pairwise disjoint and $\sum_{k=1}^\infty A_kB = (\sum_{k=1}^\infty A_k)B$. Therefore by Axiom \ref{axiom-6},
            \begin{equation*}
                \P_B\bigg(\sum_{k=1}^\infty A_k\bigg) =
                \frac{\P(\sum_{k=1}^\infty A_kB)}{\P(B)} =
                \sum_{k=1}^\infty \frac{\P(A_kB)}{\P(B)} =
                \sum_{k=1}^\infty \P_B(A_k).\qedhere
            \end{equation*}
    \end{enumerate}
\end{proof}

\begin{example}
    The geneticist C. C. Hurst collected data on eye colors of 683 male children and their parents from 139 families in Leicestershire, United Kingdom \citep{Hurst1908}. Hurst classified eye colors in two categories: Duplex (D) which corresponds approximately to brown eyes, and Simplex (S) which roughly corresponds to blue eyes. The following table contains a summary of his data.
    \begin{center}
        \begin{tabular}{ccc}
            \toprule
            \multirow{2}{*}{Parents} & \multicolumn{2}{c}{Child} \\ 
            & D & S \\\midrule
            DD & 240 & 18 \\
            DS & 187 & 137 \\
            SS & 0 & 101\\\bottomrule
        \end{tabular}
    \end{center}

    Based on this data, what is the probability of 
    \begin{enumerate}[label={\alph{*})}] 
        \item a child having blue eyes (S) if at least one of his parents has brown eyes (D)?
        \item at least one of the parents having brown eyes (D) knowing the child has blue eyes (S)?
    \end{enumerate}

    Consider the events 
    \begin{align*}
        A&: \text{the child has blue eye},\\
        B&: \text{at least one of the parents has brown eyes}.
    \end{align*}
    We wish to calculate $\P(A\mid B)$ and $\P(B\mid A)$. First we calculate:
    \begin{equation*}
        \P(A) = \frac{256}{683},\quad \P(B) = \frac{582}{683}, \quad \P(AB) = \frac{155}{683}.
    \end{equation*}
    Then by definition of the conditional probability:
    \begin{equation*}
        P (A\mid B) = \bigg(\frac{155}{683}\bigg)/\bigg(\frac{582}{683}\bigg) = \frac{155}{582},
    \end{equation*}
    which is roughly 23\% and
    \begin{equation*}
        P (B\mid A) = \bigg(\frac{155}{683}\bigg)/\bigg(\frac{256}{683}\bigg) = \frac{155}{256},
    \end{equation*}
    which is around 61\%.
\end{example}

\begin{theorem}[Multiplication rule]
    For all events $A_1, A_2, \dots, A_n$ such that $\P(A_1A_2\dots A_{n-1})\!>\!0$.
    \begin{equation}\label{eq:chain-rule}
        \P(A_1 A_2 \dots A_n) = \P(A_1)\P(A_2 \mid A_1)\P(A_3\mid A_1A_2)\dots\P(A_n \mid A_1A_2\dots A_{n-1}).
    \end{equation}
\end{theorem}

\begin{proof}
    First note that
    \begin{equation*}
        A_1 \supseteq A_1A_2\supseteq\dots\supseteq A_1A_2\dots A_n.
    \end{equation*}
    Therefore,
    \begin{equation*}
        \P(A_1) \geq \P(A_1A_2) \geq \dots \geq \P(A_1A_2\dots A_n) > 0.
    \end{equation*}
    This shows that all of the conditional probabilities involved are well defined. The formula \eqref{eq:chain-rule} is proven by induction. For $n = 1$ the statement is simply the trivial equality $\P(A_1) = \P(A_1)$. For the induction step,
    \begin{align*}
        \P(A_1 A_2\dots A_{n+1}) &= \P(A_1 A_2\dots A_n)\P(A_{n+1}\mid A_1 A_2\dots A_n) \\
        &= \P(A_1)\P(A_2 \mid A_1) \dots \P(A_n \mid A_1A_2\dots A_{n-1})\P(A_{n+1}\mid A_1 A_2\dots A_n),
    \end{align*}
    where we used the definition of conditional probability and the induction hypothesis.
\end{proof}

\begin{example}[Polya's urn scheme]
    An urn contains $m$ white and $n-m$ black balls. At each step a ball is picked at random and replaced, and then an additional ball of the same colour is added. Determine the probability of drawing a white ball three consecutive times.

    Let $A_i$ be the event corresponding to drawing a white ball in the $i$-th move. 
    \begin{equation*}
        \P(A_1A_2A_3) = \P(A_1)\P(A_2\mid A_1)\P(A_3\mid A_1A_2) = \frac{m}{n}\cdot\frac{m+1}{n+1}\cdot\frac{m+2}{n+2}.
    \end{equation*}
\end{example}

\begin{theorem}[Law of total probability]
    Let $B_1, \dots, B_n$ be pairwise disjoint events such that $\P(\sum_{k=1}^nB_i) = 1$ and $\P(B_i)>0$ for every $i$. Then for every event $A$
    \begin{equation*}
        \P(A) = \sum_{i=1}^n\P(A\mid B_i)\P(B_i).
    \end{equation*}
\end{theorem}

\begin{proof}
    According to \ref{events-properties}, we can decompose $A$ as $A = AB + AB^\complement$ for any event $B$. If we take $B = \sum_{i=1}^nB_i$ then we get
    \begin{equation*}
        A = A\bigg(\sum_{i=1}^n B_i\bigg) + A\bigg(\sum_{i=1}^nB_i\bigg)^\complement 
    \end{equation*}
    Because $\P(\sum_{k=1}^nB_i) = 1$, we must have $\P((\sum_{i=1}^nB_i)^\complement) = 0$. Passing to the probability we get
    \begin{equation*}
        \P(A) = \P\bigg(A\bigg(\sum_{i=1}^nB_i\bigg)\bigg) = \P\bigg(\sum_{i=1}^nAB_i\bigg) = \sum_{i=1}^n\P(AB_i) = \sum_{i=1}^n\P(A\mid B_i)\P(B_i).\qedhere
    \end{equation*}
\end{proof}

\begin{example}
    Suppose a box contains $n_1$ tickets marked with a 1, and $n_2$ tickets marked with a 2. We pick a ticket at random and, depending on the result, choose a ball from one of two urns. The first urn contains $b_1$ white and $c_1$ black balls, while the second one contains $b_2$ white and $c_2$ black balls. What are the odds of picking a white ball?

    Let $B_1$ be the event of drawing a ticket marked with 1, and $B_2$ the event of drawing a ticket marked with 2. Let $A$ be the event of drawing a white ball. $B_1$ and $B_2$ are disjoint events and $\P(B_1)+\P(B_2)=1$. Therefore we can apply the law of total probability:
    \begin{equation*}
        \P(A) = \P(A\mid B_1)\P(B_1) + \P(A\mid B_2)\P(B_2) = \frac{b_1}{b_1+c_1}\cdot\frac{n_1}{n_1+n_2} + \frac{b_2}{b_2+c_2}\cdot\frac{n_2}{n_1+n_2}.
    \end{equation*}
\end{example}

\begin{theorem}[Bayes' theorem] \label{bayes}
    Let $B_1, \dots, B_n$ be pairwise disjoint events and assume that $\P(\sum_{i=1}^nB_i)=1$ and $\P(B_i)>0$ for every $i$. Let $A\in\mathfrak{A}$ be an event with $\P(A)>0$.
    \begin{equation*}
        \P(B_j\mid A) = \frac{\P(A\mid B_j)\P(B_j)}{\sum_{i=1}^n\P(A\mid B_i)\P(B_i)}
    \end{equation*}
\end{theorem}

\begin{proof}
    Using the fact that $\P(A\cap B_j) = \P(A\mid B_j)\P(B_j)$ and the law of total probability:
    \begin{equation*}
        \P(B_j\mid A) = \frac{\P(A\cap B_j)}{\P(A)}
        = \frac{\P(A\mid B_j)\P(B_j)}{\sum_{i=1}^n\P(A\mid B_i)\P(B_i)}.\qedhere
    \end{equation*}
\end{proof}

Bayes' theorem can be used to \emph{reverse} conditional probabilities. Another common form of Bayes' theorem is the formula:
\begin{equation*}
    \P(A\mid B) = \frac{\P(B\mid A)\P(A)}{\P(B)}. 
\end{equation*}

\begin{example}
    A telegram message consists of a series of signals representing dots ($\bullet$) and dashes (--). On average, the odds of an individual signal being distorted during transmission is 2/5 for dots and 1/3 for dashes. Assume that the ratio of dots to dashes in a given message is $5:3$. Determine the probability that an individual signal was not distorted if it was received as a dot, and then if it was received as a dash.

    Let $A$ be the event that the signal received is a dot, and $B$ the event that it is a dash. Let $C_1$ be the event that a dot was sent and $C_2$ that a dash was sent. The probabilities we are looking for are $\P(C_1\mid A)$ and $\P(C_2\mid B)$. Note that $C_1, C_2$ are disjoint events and
    \begin{equation*}
        \frac{\P(C_1)}{\P(C_2)} = \frac53 \quad\text{and}\quad \P(C_1)+\P(C_2) = 1.
    \end{equation*}
    Solving this system gives $\P(C_1)=5/8$ and $\P(C_2)=3/8$. Moreover we know that
    \begin{equation*}
        \P(A\mid C_1) = \frac35,\quad\P(A\mid C_2) = \frac13,\quad \P(B\mid C_1) = \frac25,\quad\P(B\mid C_2) = \frac23.
    \end{equation*}
    Using Bayes' theorem:
    \begin{equation*}
        \P(C_1\mid A) = \frac{\P(A\mid C_1)\P(C_1)}{\P(A\mid C_1)\P(C_1)+\P(A\mid C_2)\P(C_2)} = \frac{\frac35\cdot\frac58}{\frac35\cdot\frac58 + \frac13\cdot\frac38} = \frac34.
    \end{equation*}
    Similarly:
    \begin{equation*}
        \P(C_2\mid B) = \frac{\P(B\mid C_2)\P(C_2)}{\P(B\mid C_1)\P(C_1)+\P(B\mid C_2)\P(C_2)} = \frac{\frac23\cdot\frac38}{\frac25\cdot\frac58 + \frac23\cdot\frac38} = \frac12.
    \end{equation*}
\end{example}

\section{Independent events}

\begin{definition}
    We say that two events $A$ and $B$ are independent if
    \begin{equation*}
        \P(A \cap B) = \P(A) \P(B).
    \end{equation*}
\end{definition}

\begin{theorem}
    Let $A,B$ be events and $\P(B)>0$. Then $A$ and $B$ are independent if and only if
    \begin{equation*}
        \P(A\mid B) = \P(A) .
    \end{equation*}
\end{theorem}

\begin{proof}
    If $A$ and $B$ are independent then
    \begin{equation*}
        \P(A\mid B) = \P(A)
    \end{equation*}
    since $\P(A\cap B) = \P(A\mid B)\P(B)$. Conversely if $\P(A\mid B) = \P(A)$ then 
    \begin{equation*}
        \P(A\cap B) = \P(A\mid B)\P(B) = \P(A)\P(B).\qedhere
    \end{equation*}
\end{proof}

Note that every event $A$ is independent from $\Omega$ (the certain event) since 
\begin{equation*}
    \P(A\mid\Omega) = \P(A\cap\Omega)/\P(\Omega) = \P(A) 
\end{equation*}
where we used the facts that $A\cap\Omega=A$ and $\P(\Omega)=1$.

\begin{example}
    Consider a roll of a dice and let $A$ be the event that the result is even and $B$ the event that the result does not exceed 2. Are $A$ and $B$ independent events?

    The events $A$, $B$, and $AB$ may be written as
    \begin{equation*}
        A = \{2, 4, 6\},\quad B = \{1, 2\}, \quad AB = \{2\}.
    \end{equation*}
    We obtain
    \begin{equation*}
        \P(A) = \frac12,\quad \P(B) = \frac13,\quad \P(AB) = \frac16
    \end{equation*}
    and therefore $\P(AB) = \P(A)\P(B)$, which confirms that A and B are independent. Alternatively note that $\P(A\mid B) = 1/2 = \P(A)$.
\end{example}

\begin{example}
    Consider the same example but this time with $B$ the event that the result of the dice roll does not exceed 3. Are $A$ and $B$ still independent?

    The relevant events are now represented by
    \begin{equation*}
        A = \{2, 4, 6\},\quad B = {1, 2, 3},\quad AB = \{2\}.
    \end{equation*}
    
    Therefore
    \begin{equation*}
        \P(A) = \P(B) = \frac12,\quad \P(AB) = \frac16.
    \end{equation*}
    The fact that $\P(A)\P(B) = 1/4$ and $\P(AB) = 1/6$ indicates that $A$ and $B$ are not independent. This means that the occurrence of $B$ affects the likelihood of $A$, and indeed we have that $\P(A\mid B) = 1/3$ while $\P(A) = 1/2$.
\end{example}

\begin{definition}
    We will say that the events $(A_i)_{i=1}^\infty$ are mutually independent if for all $i_1<i_2<\dots<i_k$, 
    \begin{equation*}
        \P(A_{i_1}A_{i_2} \dots A_{i_k} ) = \P(A_{i_1})\P(A_{i_2}) \dots \P(A_{i_k})
    \end{equation*}
\end{definition}

\begin{definition}
    A sequence of events $(A_i)_{i=1}^\infty$ is called pairwise independent if for all $i<j$
    \begin{equation*}
        \P(A_iA_j) = \P(A_i)\P(A_j) .
    \end{equation*}
\end{definition}

(Similar definitions are made for finite sequences of events.)

\begin{example}
    Suppose a random experiment has a set of outcomes $\Omega = \{\omega_1 , \omega_2 , \omega_3 , \omega_4\}$, which are all equally likely (e.g. the roll of a fair tetrahedral dice). Consider the three events
    \begin{equation*}
        A = \{\omega_1 , \omega_2\},\quad B = \{\omega_1, \omega_3\},\quad C = \{\omega_1, \omega_4\}.
    \end{equation*}
    Then $\P(A) = \P(B) = \P(C) = 1/2$ and because $A \cap B = A \cap C = B \cap C = \{\omega_1\}$ we get
    \begin{equation*}
        \P(AB) = \P(AC) = \P(BC) = \frac14.
    \end{equation*}
    Therefore $A,B,C$ is a sequence of pairwise independent events. On the other hand
    \begin{equation*}
        \frac14 = \P(ABC) \neq \P(A)\P(B)\P(C) = \frac18,
    \end{equation*}
    so $A,B,C$ are not mutually independent.
\end{example}

\begin{theorem}
    If $A$ and $B$ are independent then so are $A$ and $B^\complement$.
\end{theorem}

\begin{proof}
    First write $A$ as $A = AB + AB^\complement$. Then
    \begin{equation*}
        \P(AB^\complement) = \P(A) - \P(AB) = \P(A) - \P(A)\P (B) = \P(A)(1 - \P (B)) = \P(A)\P(B^\complement).\qedhere
    \end{equation*}
\end{proof}

\section{Exercises}

\begin{enumerate}[wide,label={\textbf{\arabic{*}.}},itemsep=1em]
    \item Three archers have an average accuracy of 4/5, 3/4 and 2/3 respectively. They take turn shooting at a target. Knowing that only two arrows hit the target, what is the probability that it was the third archer who missed? % [6/13]

    \item 10 women and 5 men are randomly divided into five groups of 3. Determine the probability that each group contains a man. % [81/1001]

    \item Five urns each contain $b$ white balls and $c$ black balls. A ball is drawn at random from the first urn and placed in the second. Then a ball is drawn at random from the second and placed in the third, and so on until a ball has been placed in the fifth urn. Finally, one ball is drawn from the fifth urn. What is the probability that this ball is white? % [$b/(b+c)$]

    \item Three urns contain blue, white and red balls where 
    \begin{itemize}
        \item the first contains 1 white, 2 black and 3 blue, 
        \item the second contains 2 white, 1 black and 1 blue,
        \item the third contains, 4 white, 5 black and 3 blue. 
    \end{itemize}
    An urn is chosen at random and then one ball is drawn from it. If that ball is white, what is the probability that the urn chosen was the first one? What about the second and third? % [$1/6$,$1/2$,$1/3$]

    \item A product has a 10\% chance of having a manufacturing defect. Defective products have a 50\% chance of failure during the warranty period, compared to 1\% chance of failure otherwise. Determine the probability that a product that experienced a failure during the warranty period was in fact defective. % [50/59]
    
    \item You want to buy a car of a certain brand, however you know that 30\% of this brand's cars have a faulty transmission. To inspect the car before purchasing it, you hire a mechanic who is able to estimate its condition with 99\% accuracy. What is the probability that the car you want to buy has a faulty transmission if the mechanic predicted that it doesn't?

    \item Prove that if $A$ and $B$ are independent, then so are $A^\complement$ and $B^\complement$.

    \item Can disjoint events be independent?

    \item Let $\Omega = {1, 2, 3, 4, 5, 6, 7, 8}$ be a sample space where each outcome has the same probability. Let $A = \{1, 2, 3, 4\}$, $B = \{1, 2, 3, 5\}$ and $C = \{1, 6, 7, 8\}$. Are these events independent or pairwise independent?

    \item Draw at a random one card from a standard deck of playing cards (52 cards, 4 suits, 13 ranks). Let $A$ be the event that this card is a queen and $B$ that it is a heart.
        \begin{enumerate*}[label={\alph{*}}]
            \item Are $A$ and $B$ independent?
            \item If we add two jokers to the pack, does the answer change (the jokers are considered to have no suit)?
        \end{enumerate*}

    \item (Galton's paradox) When three coins are flipped, then surely \emph{at least} two of them must have the same side facing up (head or tail). Since there is an even chance of the third coin being a head or a tail, one could  argue that the probability that all three coins show the same side is 1/2. Do you agree with this answer? If not, what is the correct one?
\end{enumerate}
