\chapter{Discrete random variables}

\begin{defin}
    A random variable $X$ is called \emph{discrete} if its is finite or countably infinite.  Equivalently, there is a finite or countably infinite set of real numbers $S$ such that $\P[X=s]> 0$ for all $s\in S$ and $\sum_{s\in S} \P[X=s]= 1$.
\end{defin}

\begin{defin}
    Let $X$ be a discrete random variable. Then the function $f_X(x)=\P[X=x]$ is called the \emph{discrete density function} of $X$.
\end{defin}

Discrete random variables are often described by a table giving the non-zero values of their discrete density function, such as:
\begin{center}
\begin{tabular}{ccccccc}
    \toprule
    $x$ & 0 & 1 & 2 & 3 \\ \midrule
    $f_X(x)$ & 0.25 & 0.3 & 0.1 & 0.35  \\ \bottomrule
\end{tabular}
\end{center}

\begin{remark}
    The distribution function of a discrete random variable $X$ has the form
    \begin{equation*}
        F_X(x)= \sum_{s\in S \,:\, s\leq x} \P[X=s] = \sum_{s\in S \,:\, s\leq x} f_X(x).
    \end{equation*}

    In particular the elements of $S$ are points of discontinuity for $F_X$, and $F_X$ is constant over the intervals $[s,t)$ such that $[s,t)\cap S=\{s\}$.  Moreover
    \begin{equation*}
        \P[X<a] = F_X(a)-f_X(a) \qmq{and} \P[X\geq a] = 1-F(a)+f_X(a).
    \end{equation*}
\end{remark}

\section{Discrete distributions}

\subsection{Bernoulli distribution}

This distribution is named after the Swiss mathematician J. Bernoulli (1654--1705). It is used to describe random situations having two possible outcomes (head or tail, true or false, on or off, etc.). Usually these outcomes are assigned the values of 0 and 1.

\begin{defin}
    We say that the random variable $X$ follows a Bernoulli distribution with parameter $p\in(0,1)$ if
    \begin{equation*}
        \P[X=1]=p, \qquad \P[X=0]=1-p.
    \end{equation*}

    We denote this using the notation $X\sim \bernoulli(p)$.
\end{defin}

In other words, $X\sim\bernoulli(p)$ means that the discrete density function of $X$ has the form
\begin{equation*}
    f_X(x) = \P[X=x] =
    \begin{cases}
        1-p & \text{if } x=0, \\
        p & \text{if } x=1, \\
        0 & \text{otherwise}.
    \end{cases}
\end{equation*}

\subsection{Binomial distribution}

Consider a random variable $X$ expressing the number of occurrences of an event $A$ in $n$ independent trials of a given experiment. Assuming that the probability of $A$ is equal to $p$ every time, the probability that $A$ occurs \emph{exactly} $k$ times out of $n$ trials is 
\begin{equation*}
    p_k=\P[X=k] = \binom{n}{k} p^k(1-p)^{n-k}.
\end{equation*}

The random variable $X$ has range $\{0,1,\ldots, n\}$ so it is discrete. It's discrete density function is given by $p_k$ for $k\in\{0,1,\ldots, n\}$ and $0$ otherwise. According to the binomial formula:
\begin{equation*}
    \sum_{k=0}^nf_X(k) = \sum_{k=0}^n \binom{n}{k} p^k(1-p)^{n-k} = (p+1-p)^n =1,
\end{equation*}
as expected from a discrete density function.

\begin{defin}
    We say that a random variable $X$ follows a binomial distribution with parameters $n\in\NN$ and $p\in(0,1)$ if
    \begin{equation*}
        \P[X=k] = \binom{n}{k} p^k(1-p)^{n-k} \qmq{for} k\in\{0,1,\ldots, n\}.
    \end{equation*}
    We denote this by $X\sim\binomial(n,p)$.
\end{defin}

\begin{remark}
    If $X_j$, $0\leq j\leq n$, are independent random variables with $X_j\sim\bernoulli(p)$ then the random variable 
    \begin{equation*}
        X = \sum_{j=1}^nX_j
    \end{equation*}
    follows a binomial distribution with parameters $n$ and $p$.
\end{remark}

\begin{example}
    Two evenly matched opponents play a series of chess games (so the probability of each player winning or losing a game is always $1/2$). Is it more likely that
    \begin{itemize}
        \item a player won 3 games out of 4 or 5 games out of 8?
        \item a player won at least 3 games out of 4, or at least 5 games out of 8?
    \end{itemize}

    Let $X$ be the random variable representing the number of wins of a given player in 4 games, and $Y$ the number of wins in 8 games. Then for a) we have
    \begin{equation*}
        \P[X=3] = \binom{4}{3}\left(\frac{1}{2}\right)^4=\frac{1}{4} \qmq{and}
        \P[Y=5] =\binom{8}{5}\left(\frac{1}{2}\right)^8=\frac{7}{32},
    \end{equation*}
    therefore the first scenario is more likely.  

    On the other hand, for b)
    \begin{equation*}
        \P[X\in\{3,4\}]= \frac{1}{4}+
        \binom{4}{4}\left(\frac{1}{2}\right)^4=\frac{5}{16}
        \qmq{and}
        \P[Y\in\{5,6,7,8\}]=\sum_{k=5}^8
        \binom{8}{k}
        \left(\frac{1}{2}\right)^{8} = \frac{93}{256},
    \end{equation*}
    which means that the second scenario is more likely.
\end{example}

\subsection{Geometric distribution}

\begin{defin}
    We say that a discrete random variable $X$ follows a geometric distribution with parameter $p\in(0,1)$ if
    \begin{equation*}
        \P[X=k] = p\cdot (1-p)^k \qmq{for} k=0,1,2,\ldots.
    \end{equation*}
    We denote by $X\sim \geometric(p)$.
\end{defin}

\begin{remark}
    Consider an infinite sequence of Bernoulli trials for some event $A$ which occurs with probability $p$. Then the random variable $X$ counting the number of trials \emph{before} the first occurrence of $A$ follows a geometric distribution with parameter $p$.
\end{remark}

The fact that this is indeed a probability distribution follows from the formula for geometric series (which also explains the name):
\begin{equation*}
    \sum_{k=0}^\infty \P[X=k] = \sum_{k=0}^\infty p (1-p)^k = \frac{p}{1-(1-p)}=1.
\end{equation*}

If $X\sim\geometric(p)$ then $F_X$ can also be expressed as follows:
\begin{equation*}
    F_X(x)=\sum_{k=0}^{[x]}p(1-p)^k = p\cdot \frac{1-(1-p)^{[x]+1}}{1-(1-p)}= 1-(1-p)^{[x]+1}.
\end{equation*}

\begin{example}
    Consider a game where a player rolls a dice until the result is a 6. What is the probability that the player rolls the dice 
    \begin{enumerate*}[label={\alph{*})}]  
        \item 4 times? or 
        \item \emph{at most} 4 times?
    \end{enumerate*}

    Let $X$ denote the number of attempts before the result is a 6. Then $X\sim \geometric(1/6)$ and
    \begin{equation*}
        \P[X=3] = \frac{1}{6}\cdot \left(1-\frac{1}{6}\right)^3 =\frac{1}{6}\cdot \left(\frac{5}{6}\right)^3\approx 9.65\% \qmq{and}
        \P[X\leq 3] = 1-\left(\frac{5}{6}\right)^4 \approx 51.77\%.
    \end{equation*}
\end{example}

\subsection{Poisson distribution}
\label{poisson}

The Poisson distribution was introduced in 1837 by the french mathematician S. D. Poisson in a treaty titled \emph{Recherches sur la probabilité des jugements en matière criminelle et en matière civile} \cite{book/Poisson1837}. It is used to model the number of occurrences of times a random event occurs in a given amount of time, given the average frequency of that event.

\begin{defin}
    We say that a discrete random variable $X$ follows a Poisson distribution with parameter $\lambda>0$ if
    \begin{equation*}
        \P[X=k] =
        \begin{cases}
            \frac{\lambda^k}{k!}\,e^{-\lambda} & \text{for } k=0,1,2,\ldots\\
            0 & \text{other}.
        \end{cases}
    \end{equation*}
    We denote this by $X\sim \poisson(\lambda)$. $\lambda$ is called the \emph{rate} parameter.
\end{defin}

The fact that this is a valid distribution follows from Taylor expansion of the exponential function:
\begin{equation*}
    \sum_{k=0}^\infty e^{-\lambda}\frac{\lambda^k}{k!} =
    e^{-\lambda}\sum_{k=0}^\infty \frac{\lambda^k}{k!} =
    e^{-\lambda}\cdot e^{\lambda}=1.
\end{equation*}

\begin{remark}
    A notable application of the Poisson distribution is in approximating radioactive decay. More generally, Poisson distributions can be used in a number of ways.
    \begin{enumerate}[label={\alph{*})}]
        \item To describe the number of occurrences of an event over a fixed interval of time, where $\lambda$ gives the average number of occurrences per unit of time.
        \item To describe the number of particles in a given area or volume, where $\lambda$ gives the average number of particles per unit. 
        \item To approximate a binomial distribution. 
    \end{enumerate}
\end{remark}

\begin{theorem}[Poisson limit theorem]
    Let $(X_n)_{n=1}^\infty$ be a sequence of random variables such that $X_n\sim \binomial(n,\lambda/n)$ where $\lambda>0$ is fixed. Then
    \begin{equation*}
        \lim_{n\to\infty} \P[X_n = k] = \frac{\lambda^k}{k!}e^{-\lambda}.
    \end{equation*}
\end{theorem}

In other words $\lim_{n\to\infty}\P[X_n=k]=\P[X=k]$ where $X\sim \poisson(\lambda)$.

\begin{proof}
    First let us decompose the term $\P[X_n =k]$ as follows:
    \begin{equation*}
        \P[X_n=k] = \binom{n}{k}(\lambda/n)^k(1-\lambda/n)^{n-k} = \binom nk \bigg(\frac{\lambda}{n-\lambda}\bigg)^k(1-\lambda/n)^n.
    \end{equation*}
    First we focus on $(1-\lambda/n)^n$. Recall that $e^x = \lim_{n\to\infty}(1+x/n)^n$, so $\lim_{n\to\infty}(1-\lambda/n)^n = e^{-\lambda}$. As for the remaining part we find
    \begin{equation*}
        \lim_{n\to\infty}\binom nk \bigg(\frac{\lambda}{n-\lambda}\bigg)^k = \frac{\lambda^k}{k!}\lim_{n\to\infty}\frac{n(n-1)\dots (n-k)}{(n-\lambda)^k} = \frac{\lambda^k}{k!}.\qedhere
    \end{equation*}
\end{proof}

We can interpret this result by saying that for small $p$ and large $n$, the distribution $\poisson(np)$ is a good approximation for $\binomial(n,p)$. 

\begin{example}
    Let $X\sim \binomial(1000,0.0015)$. Then we can approximate $X$ using a Poisson distribution $\poisson(1.5)$. For instance, we find that 
    \begin{equation*}
        \P[X=5] = \binom{1000}{5} (0.0015)^5 (0.9985)^{995} \approx 1.40687\%
    \end{equation*}
    whereas
    \begin{equation*}
        e^{-1.5}\frac{(1.5)^5}{5!} \approx 1.41200\%.
    \end{equation*}
\end{example}

\begin{example}
    Suppose that a given radioactive emits on average 3.87 $\alpha$-particles every 7.5~s. Determine the probability that during a 1~s interval, this substance emits at least 1 $\alpha$-particle.

    Let $X$ denote the number of particles emitted in 1~s. Since the average number of particles emitted in 1 second is $\lambda = 3.87/7.5 = 0.516$, we can describe the random variable $X$ using a Poisson distribution $\poisson(0.516)$. We find that
    \begin{equation*}
        \P[X\geq 1] = 1-\P[X=0] = 1-e^{-0.516}\approx 40.3\%.
    \end{equation*}
\end{example}

\section{Discrete random vectors}

\begin{definition}
    A random vector $Z = (X_1,X_2,\ldots,X_n)$ is called discrete if all the marginal random variables $X_i$ are discrete. This equivalent to the existence of a finite or countably infinite set of $S$ of vectors $s=(s_1,s_2,\ldots,s_n)$ such that
    \begin{equation*}
        \P[Z=s] > 0 \qmq{and} \sum_{s\in S} \P[Z=s] = 1.
    \end{equation*}

    The function 
    \begin{equation*}
        f_Z(x_1,x_2,\dots x_n) = \P[X_1=x_1,X_2=x_2,\dots, X_n=x_n]
    \end{equation*}
    is called the joint discrete density function of $Z$.
\end{definition}

Consider a discrete random vector $(X_1,X_2)$. Then the associated
distribution function of this vector is given by
\begin{equation*}
    F_{X_1,X_2}(x_1,x_2) = \sum_{t_1\leq x_1}\sum_{t_2\leq x_2}\P[X_1=t_1,X_2=t_2].
\end{equation*}

They also share many properties of discrete random variables.
\begin{theorem}\label{margdis}
    Let $(X_1,X_2)$ be a discrete random vector. Then
    \begin{equation*}
        \P[X_1=x_1] = \sum_{x_2} \P[X_1=x_1,X_2=x_2] \qmq{and} \P[X_2=x_2] = \sum_{x_1} \P[X_1=x_1,X_2=x_2].
    \end{equation*}
\end{theorem}

\begin{theorem}
    Two discrete random variables $X,Y$ are independent if and only if for every $(x,y)\in\rr^2$,
    $$
    \P[X=x,Y=y]= \P[X=x]\cdot \P[Y=y].
    $$
\end{theorem}

In other words the joint discrete density function is the product of the marginal discrete density functions.

\begin{proof}
    ($\Rightarrow$) We use the fact that 
    \begin{equation*}
        \P[X=x,Y=y] = \lim_{a\to x^-}\lim_{b\to y^-}\P[a< X\leq x,b<X\leq y].
    \end{equation*}
    If $X$ and $Y$ are independent then
    \begin{align*}
        \P[X=x,Y=y] &= \lim_{a\to x^-}\lim_{b\to y^-}\P[a< X\leq x,b<X\leq y]  \\
                    &= \lim_{a\to x^-}\lim_{b\to y^-}\P[a< X\leq x]\cdot\P[b<X\leq y] \\
                    &= \bigg(\lim_{a\to x^-}\P[a< X\leq x]\bigg)\cdot\bigg(\lim_{b\to y^-}\P[b<X\leq y]\bigg)  \\
                    &= \P[X=x]\cdot\P[Y=y].
    \end{align*}

    ($\Leftarrow$) Since the variables $X$ and $Y$ are discrete,
    \begin{equation*}
        \P[a\leq X<b, c\leq Y<d] = \sum_{a\leq x<b}\sum_{c\leq y<d}\P[X=x,Y=y].
    \end{equation*}
    If moreover $\P[X=x,Y=y] = \P[X=x]\cdot\P[Y=y]$ then
    \begin{align*}
        \sum_{a\leq x<b}\sum_{c\leq y<d}\P[X=x,Y=y] &= \sum_{a\leq x<b}\sum_{c\leq y<d}\P[X=x]\cdot \P[Y=y] \\
                                                   &= \bigg(\sum_{a\leq x<b}\P[X=x]\bigg)\cdot\bigg(\sum_{c\leq y<d} \P[Y=y]\bigg)\\
                                                   &= \P[a\leq X<b]\cdot\P[c\leq Y<d].\qedhere
    \end{align*}
\end{proof}

\begin{example}
    Let $X_1$ and $X_2$ be the discrete random variables given by the joint discrete density function $\P[X_1=x_1,X_2=x_2]$ described by the following table.
    \begin{center}
    \begin{tabular}{cccc}
        \toprule
        \diagbox{$X_1$}{$X_2$} & 0 & 1 & 2 \\\midrule
        0 & 0.42 & 0.12 & 0.06 \\
        1 & 0.28 & 0.08 & 0.04 \\\bottomrule
    \end{tabular}
    \end{center}

    Calculate the marginal discrete density functions and determine whether $X_1$ and $X_2$ are independent.

    The marginal discrete density for $X_1$ is
    \begin{align*}
        \P[X_1=0] &= \sum_{k=0}^2 \P[X_1=0,X_2=k] = 0.42+0.12+0.06 = 0.6 \\
        \P[X_1=1] &= \sum_{k=0}^2 \P[X_1=1,X_2=k] = 0.28+0.08+0.04 = 0.4.
    \end{align*}
    We can do similar computations for $X_2$:
    \begin{equation*}
        \P[X_2=0]=0.7,\quad \P[X_2=1]=0.2, \quad \P[X_2=2]=0.1\, .
    \end{equation*}

    In other words, the values of the marginal discrete density functions are obtained by summing the corresponding rows and columns. This information can be added to the previous table
    \begin{center}
        \begin{tabular}{ccccc}
            \toprule
            \diagbox{$X_1$}{$X_2$} & 0 & 1 & 2 & $f_{X_1}$ \\\midrule
            0 & 0.42 & 0.12 & 0.06 & {\bf 0.6}\\
            1 & 0.28 & 0.08 & 0.04 & {\bf 0.4}\\ \midrule
            $f_{X_2}$ & {\bf 0.7} & {\bf 0.2} & {\bf 0.1} & 1 \\\bottomrule
        \end{tabular}
    \end{center}
    To check whether the variables are independent we should multiply the rightmost entry of the $i$-th row with the rightmost entry of $j$-th column and compare the result with the $(i,j)$ entry. For instance, we see that 
    \begin{equation*}
        \P[X_1=1]\cdot \P[X_2=1] = 0.2\cdot 0.4 = 0.08 = \P[X_1=1,X_2=2].
    \end{equation*}
    In this case we conclude that $X_1,X_2$ are independent.
\end{example}

\begin{defin}
    Let $(X_1, X_2)$ be a discrete random vector. If $\P[X_2=x_2]>0$, then we define the conditional probability of $X_1$ \emph{knowing} $X_2=x_2$ by
    \begin{equation*}
        \P[X_1=x_1 \mid X_2=x_2] = \frac{\P[X_1=x_1,X_2=x_2]}{\P[X_2=x_2]}= \frac{\P[X_1=x_1,X_2=x_2]}{\sum_{x_1} \P[X_1=x_1,X_2=x_2]}.
    \end{equation*}
\end{defin}

\begin{example}
    Determine the distribution of the random variable $X$ representing the maximum of the result of two dice rolls, knowing that the minimum value is 3.

    Let $X$ denote the maximum of the two dice rolls and $Y$ denote the minimum. $Y=3$ means that the dice can only have values from the set $\{3,4,5,6\}$ and at least one of them is a 3. This amounts to a total of 7 favourable outcomes out of 36, so $\P[Y=3]=7/36$. Moreover knowing $Y=3$, the random variable $X$ has range $\{3,4,5,6\}$ and
    \begin{equation*}
        \P[X=i,Y=3]=
        \begin{cases}
            \frac{1}{36} & \text{if }i=3 \\
            \frac{2}{36} & \text{if }i=4,5,6.
        \end{cases}
    \end{equation*}
    By the definition of conditional probability
    \begin{equation*}
        \P[X=i\mid Y=3] = 
        \begin{cases}
            (\frac1{36})/(\frac7{36}) = \frac{1}{7} & \text{if }i=3\\
            (\frac2{36})/(\frac7{36}) = \frac{2}{7} & \text{if }i=4,5,6.
        \end{cases}
    \end{equation*}
\end{example}

\subsection{Multinomial distribution}

\begin{defin}
    We say that discrete random vector $(X_1,X_2,\ldots,X_k)$ has multinomial distribution with parameters $n, p_1,\ldots , p_k$, where $n\in\NN$, $0<p_j<1$ and $\sum_{j=1}^np_j=1$, if
    \begin{equation*}
        \P[X_1=x_1, X_2=x_2,\ldots,X_k=x_k] = \frac{n!}{x_1! x_2!\ldots x_k!}\, p_1^{x_1} p_2^{x_2}\ldots p_k^{x_k} \qmq{for} x_j\in\{0,1,\ldots n\}, \sum_{j=1}^k x_j = n.
    \end{equation*}
    We denote this $(X_1,X_2,\dots,X_n)\sim\multinomial(n,p_1,p_2,\dots,p_k)$.
\end{defin}

The name of this distribution comes from the fact that expressions for $\P[X_1=x_1,X_2=x_2,\ldots,X_k=x_k]$ are the terms in the expansion of $(p_1+p_2+\ldots+p_k)^n$. The notation 
\begin{equation*}
    \binom{n}{x_1,x_2,\dots,x_k} = \frac{n!}{x_1! x_2!\ldots x_k!}
\end{equation*}
is often used, where it is assumed that $n = \sum_{j=1}^k x_j$. The number $\binom{n}{x_1,x_2,\dots,x_k}$ is called a \emph{multinomial coefficient}.

The multinomial distribution is a generalization of the binomial distribution. Consider $n$ independent trials of a random experiment and $A_1,A_2,\ldots, A_k$ be pairwise disjoint events where $\P(A_j) = p_j$. Assume further that these events exhaust the whole sample space, which is the same as $\sum_{j=1}^k=1$. Let $X_j$ be the random variable representing the number of times $A_j$ occurred during these $n$ trials. Then $(X_1,X_2,\dots, X_k)\sim \multinomial(n,p_1,p_2,\dots,p_k)$. When $k=2$ we recover the multinomial distribution.

\begin{example}
    An urn contains $3$ white balls, $2$ black balls and $5$ red balls. $4$ balls are drawn at random from this urn. Let $X_1$ be the number of white balls drawn, $X_2$ the number of black balls and $X_3$ the number of red balls. Determine the discrete density of the discrete vector $Z= (X_1,X_2,X_3)$ and compute $F_Z(3,3,2)$. 

    First we determine the discrete density function. The random vector $Z = (X_1,X_2,X_3)$ follows a multinomial distribution $\multinomial(4,p_1,p_2,p_3)$ where $p_1 = 3/10$, $p_2 = 1/5$ and $p_3 = 1/2$. For instance, we find that
    \begin{equation*}
        \P[X_1=2,X_1=1,X_3=1] = \frac{4!}{2!\,1!\,1!}(3/10)^2(1/5)(1/2) = 27/250.
    \end{equation*}
    The following table gives the values for $\P[X_1=x_1,X_2=x_2,X_3=4-x_1-x_2]$:
    \begin{center}
        \begin{tabular}{cccccc}
            \toprule
            \diagbox{$X_1$}{$X_2$} & 0 & 1 & 2 & 3 & 4 \\\midrule
                                 0 & 1/16 & 1/10 & 3/50 & 2/125 & 1/625 \\
                                 1 & 3/20 & 9/50 & 9/125 & 6/625 & 0 \\
                                 2 & 27/200 & 27/250 & 27/1250 & 0 & 0 \\
                                 3 & 27/500 & 27/1250 & 0 & 0 & 0 \\
                                 4 & 81/10000 & 0 & 0 & 0 & 0 \\\bottomrule
        \end{tabular}
    \end{center}

    The value of $F_Z(3,3,2)$ is:
    \begin{align*}
        F_Z(3,3,2) &= 1 - f_Z(4,0,0) - f_Z(0,4,0) - f_Z(0,0,4) - f_Z(1,0,3) - f_Z(0,1,3)  \\
                               &= 1 - 81/10000 - 1/625 - 1/16 - 3/20 - 1/10 \\
                               &= 3389/5000
    \end{align*}
\end{example}


\section{Exercises}

\begin{enumerate}[wide,label={\textbf{\arabic{*}.}},itemsep=1em]

\item A car needs to cross 4 traffic lights on his route. Each trafic light has a 2/3 probability of being green. Determine the discrete density function of the random variable which gives the number of traffic lights the car passes before stopping for the first time.

\item According to rule of the World Archery Federation, an archery target is divided in 5 concentric regions, worth 1, 3, 5, 7 and 9 points. Assume that an archer hits these area with the following probabilities: 0.08, 0.31, 0.39, 0.21, and 0.01. What is the distribution of the random variable giving the number of points scored in two attempts? Calculate it's discrete density.

\item An urn contains 4 white and 6 black balls. A trial consists of selecting a ball at random, observing its color, and then replacing it. Let $X$ be the number of white balls selected in 14 trials. Compute $\P[X\leq 7]$.

\item A shipment of 1000 items contains 10 defective ones. An inspector selects items at random, inspects them, and places them back if they are not defective. Let $X$ be the number of inspections made before finding a defective item. Compute $\P[75\leq X\leq 95]$.

\item In a store, about 1\% of the products sold are returned. Determine the probability that 200 sales will result in at least 4 returns. % [0.143]

\item Let $X\sim\geometric(p)$. Show that $\P[X=k+n \mid X\geq k] = \P[X=n]$. 

\item Let $X$ be a random variable whose range is the set $\nn$. Prove that if $0<\P[X=0]<1$ and $\P[X=k+n \mid X\geq k] = \P[X=n]$, then $X\sim\geometric(p)$.

\item In the early age of telephones, calls were directed to centralized switchboards, where they needed to be manually rerouted by telephone operators. Assume that a given switchboard receives on average 60 calls per hour. If a single operator is responsible for that switchboard, what is the probability that they will not miss a call during a 2 minutes break? %

\item After proofreading a 500 pages manuscript, an editor found 1500 typographical errors. Calculate the probability that a randomly selected page has at least 3 errors. % [0.577]

\item A 2019 study conducted in the Czech Republic estimates that the prevalence of rare diseases in newborn is arond 1 in 1043.
    \begin{enumerate}[label={\alph{*})}]
        \item Estimate the probability that in a group of 4 people selected at random among the Czech population, no one has a rare disease.
        \item How many people do you need for the probability of finding at least one person with a rare disease to be greater than 1/2?
    \end{enumerate}

\item A baker blends 600 raisins and 400 chocolate chips into a dough mix which is then used to bake 500 cookies.
    \begin{enumerate}[label={\alph{*})}]
        \item Find the probability that a random cookie has no raisins.
        \item Find the probability that a random cookie has exactly two chocolate chips.
        \item Find the probability that a random cookie has at least two bits (raisins or chips) in it.
    \end{enumerate}

\item An airline estimates that about 4 percent of the passengers that made reservations on a particular flight will not show up. Consequently, their policy is to sell 100 reserved seats on a plane that has only 98 seats. Find the probability that a flight with 100 seats is overbooked (meaning at least one person showed up but could not board for lack of seat).

\end{enumerate}
