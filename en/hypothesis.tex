\chapter{Hypothesis Testing}

In this chapter we will see how to construct tests based on random samples in order to accept or reject some hypothesis about a population. Here are two examples.
\begin{itemize}
    \item A company receives a large shipment of goods and wants to evaluate the overall quality. The decision to accept or reject the shipment is based on the percentage $p$ of defective goods. The delivery is accepted if the percentage $p$ is less than some threshold, say 0.05\%. However, it would be too expensive to check every product in the shipment. The solution is to test a random sample and make a decision based on the results. Here we are testing the hypothesis $H_0: p\leq 0.05$ against the alternative $H_1: p\geq 0.05$.
    \item Suppose that a new method has been designed for treating a material against corrosion. Before it is introduced into production, we want to decide whether it really improves on the existing treatment. To do this we can make measurements on random samples of size $n_1$ and $n_2$ with the old and new treatments, looking at the percentage of the surface affected by corrosion. We want to test whether the average surface area affected by corrosion is smaller with the new method. Thus, we test the hypothesis $H_0: \mu_2<\mu_1$ against the alternative $H_1: \mu_2\geq \mu_1$.
\end{itemize}

\section{Basic terminology}

Assume we have a random sample $X_1,\ldots,X_n$ from a distribution which belongs to a family $\{F_\theta \mid \theta\in \Theta\}$ where $\Theta\subset\RR^d$ is the set of possible values for $\theta$. We consider two competing hypotheses for the parameter $\theta$, represented by a subset $\Theta_0\subseteq\Theta$:
\begin{description}
    \item[Null hypothesis] $H_0:\theta\in\Theta_0$,
    \item[Alternative hypothesis] $H_1:\theta\notin\Theta_0$.
\end{description}

Typically the null hypothesis represents the status quo, or the absence of an effect (for instance: a pharmaceutical treatment does not improve the patients condition). A test for the null hypothesis $H_0$ against the alternative hypothesis $H_1$ is a decision process which uses a random sample $X_1,\ldots,X_n$ to decide whether to {\em reject} or {\em accept} $H_0$. Since the decision is based on partial data (the random sample) the decision will not always be correct. There are four possible cases summarized in the next table.
\begin{center}
    \begin{tabular}{ccc}
        \toprule
        & $H_0$ holds & $H_1$ holds \\ 
        \midrule
        We accept $H_0$ & Correct & Error, 2nd kind  \\
        We reject $H_0$ & Error, 1nd kind & Correct \\
        \bottomrule
    \end{tabular}
\end{center}

The errors are separated in two categories, first and second kind. Errors of the first kind are typically considered worse. Thus if we cannot minimize both error types at once, we usually try to minimize errors of the first kind. 

Consider the space $\X^n$ of all possible values for samples of size $n$ (this is a subset of $\rr^n$). The partition of the parameter space into $\Theta_0$ and $\Theta_1 = \Theta\setminus\Theta_0$ (defining the hypotheses $H_0$ and $H_1$) gives a partition of $\X^n$: one part contains the samples for which we reject $H_0$ and the other part those for which we don't. 

\begin{defin}
    The subset $W\subset\X^n$ which is assigned the decision to reject $H_0$ is called the \emph{critical region}. 
\end{defin}

The choice of the critical region $W$ is governed primarily by the requirement that the probability of an error of the first kind
does not exceed $\alpha$,
\begin{equation*}
    \P_\theta\left[(X_1,\ldots,X_n)\in W\right] \leq \alpha \qmq{for all} \theta\in\Theta_0.
\end{equation*}
The number $\alpha$ is called the significance level. Thus we say that we are testing the null hypothesis $H_0$ against the alternative $H_1$ at the significance level $\alpha$.

\section{Hypothesis tests and interval estimates}

Let $X_1,\ldots,X_n$ be a random sample from a distribution $F(x,\theta)$. Let us test
\begin{equation*}
    H_0: \theta=\theta_0 \qmq{against} H_1: \theta \neq \theta_0.
\end{equation*}
Let $(\underline\theta,\overline\theta)$ be a $100(1-\alpha)\%$ confidence interval for the parameter $\theta$ based on the random sample $X_1,\ldots,X_n$. In other words, 
\begin{equation*}
    \P[\theta\in(\underline\theta,\overline\theta)]\geq 1-\alpha.
\end{equation*}
Then the critical region for testing $H_0$ against $H_1$ at the significance level $\alpha$ is:
\begin{equation*}
W=\{(x_1,\ldots,x_n)\in\RR^n \mid \theta_0 \notin (\underline\theta(x_1,\ldots,x_n),\overline\theta(x_1,\ldots,x_n))\}
\end{equation*}
since under the null hypothesis $\theta=\theta_0$,
\begin{equation*}
    \P[(X_1,\ldots,X_n)\in W] = \P[\theta_0\notin(\underline\theta,\overline\theta) ]\leq \alpha.
\end{equation*}
In other words, we reject the hypothesis $H_0$ if $\theta_0\notin(\underline\theta,\overline\theta)$.

Similarly, we can test $H_0: \theta\neq \theta_0$ against a one-sided alternative $H_1:\theta>\theta_0$ using a lower interval estimate $\underline\theta$. In that case $\P[\theta> \underline\theta]\geq 1-\alpha$ and the critical region at significance level $\alpha$ is the set
\begin{equation*}
    W=\{(x_1,\ldots,x_n)\in\RR^n\,|\, \theta_0 \leq \underline\theta(x_1,\ldots,x_n)\}.
\end{equation*}
Thus, for testing the hypothesis $H_0$ against the one-sided alternative $H_1$ at significance level $\alpha$, we reject $H_0$ if $\theta_0\leq\underline\theta(x_1,\ldots,x_n)$. 

\begin{remark}
    Hypothesis testing with two-sided confidence intervals is called \emph{two-tailed testing}, as opposed to \emph{one-tailed testing}. Notice that in one-tailed tests we are ignoring cases where $\theta<\theta_0$. This can be either because this case cannot happen (for instance testing $H_0: \theta=0$ where $\theta$ is a non-negative parameter), or because we do not care if in fact $\theta<\theta_0$. In the second case we are really testing $H_0: \theta\leq\theta_0$.
\end{remark}

\begin{example}
    A theory predicts that the proportion of iron in a given chemical should be 12.1\%. 9 samples were analyzed and the following percentages of iron were found
    \begin{equation*}
        11.7,\ 12.2,\ 10.9,\ 11.4,\ 11.3,\ 12.0,\ 11.1,\ 10.7,\ 11.6.
    \end{equation*}
    Test the validity of the theoretical prediction at a significance level of 5\%. We assume a normal distribution of the data with parameters $\mu, \sigma^2$ and we want to test
    \begin{equation*}
        H_0: \mu = 12.1 \qmq{against} H_1: \mu\neq 12.1.
    \end{equation*}
    Using the formula for a two-sided confidence interval for the parameter $\mu$ with unknown variance,
    \begin{equation*}
        \left(\overline X_n - t_{1-\alpha/2}(n-1)\,\frac{s_n}{\sqrt{n}},\overline X_n + t_{1-\alpha/2}(n-1)\,\frac{s_n}{\sqrt{n}}\right).
    \end{equation*}
    In our case:
    \begin{equation*}
        \overline X_n \approx 11.43,\quad s_n \approx 0.49,\quad t_{0.975}(8) \approx 2.306.
    \end{equation*}
    We obtain the confidence interval $(11.05,11.81)$. Since $12.1\notin (11.05,11.81)$ we reject the null hypothesis at a significance level of 5\%.
\end{example}

\section{Hypothesis testing for normal distributions}

\subsection{Expectation tests}

Consider a random sample $X_1,\ldots, X_n$ from $N(\mu,\sigma^2)$. Our task is to test
\begin{equation*}
    H_0: \mu=\mu_0 \qmq{against} H_1: \mu\neq \mu_0 \text{ or } H_1^\prime : \mu>\mu_0 \text{ or } H_1^{\prime\prime}: \mu<\mu_0.
\end{equation*}
We give the derivation of simple tests in the next examples. More of them are given in \cref{tab:normal-test-1} at the end of the chapter.

\begin{example}\label{ex99}
    A factory producing steel wire is testing a new procedure with the goal of increasing the wire's strength. The strength of the steel wire is a random variable with distribution $N(\mu_0,\sigma^2)$ where $\mu_0=1250$ and $\sigma=150$. 25 samples were made using the new procedure resulting in an average of $\overline X_{25}= 1312$. Test whether or not the new procedure improved the strength of the steel wire at a level of significance of $\alpha=0.05$.

    We test
    \begin{equation*}
        H_0:\mu=\mu_0 \qmq{against} H_1: \mu>\mu_0
    \end{equation*}
    In other words, $H_1$ is the hypothesis that the procedure makes the wire stronger. Let us assume that the new procedure has the same standard deviation as the previous one, so $\overline X_n \sim N(\mu,\sigma^2/n)$. The larger the difference $\overline X_n-\mu_0$, the better the new procedure will be. Assuming that $H_0$ is valid, i.e. $\mu=\mu_0$, then we get
    \begin{equation*}
        Z_n = \frac{\overline X_n - \mu_0}{\sigma/\sqrt{n}} \sim N(0,1).
    \end{equation*}
    In that case,
    \begin{equation*}
        \P[Z_n \geq u_{0.95}] = 0.05\, .
    \end{equation*}
    If the value of $Z_n$ exceeds $u_{0.95}$, then $Z_n$ probably does not have a distribution $N(0,1)$, which means that $H_0$ does not hold. So we reject the hypothesis $H_0$ if
    \begin{equation*}
        Z_n\geq u_{0.95}.
    \end{equation*}

    Using the data from this example, we get:
    \begin{equation*}
    Z_n = \frac{\overline X_n - \mu_0}{\sigma/\sqrt{n}} =
    \frac{1312-1250}{30} = 2.067.
    \end{equation*}
    On the other hand $u_{0.95}=1.64$. Since $Z_n = 2.067 > u_{0.95}=1.64$, we reject the null hypothesis at the significance level 0.05. Thus it is statistically proven that the new production process gives better results.
\end{example}

\begin{example}
    Measurements were taken to test the calibration of measuring device. The expected value is $\mu_0=15.2$ and the results of the measurements are
    \begin{equation*}
        15.21,\ 15.19,\ 15.16.\ 15.26,\ 15.22,\ 15.23,\ 15.26,\ 15.23,\ 15.29, 15.23.
    \end{equation*}
    At the level of significance 0.05, can the measured deviations be considered random errors, or should we suspect a systematic error? 

    Let us assume that the errors follow a normal distribution. We have to test
    \begin{equation*}
        H_0: \mu=\mu_0 \qmq{against} H_1: \mu\neq \mu_0.
    \end{equation*}
    Looking in \cref{tab:normal-test-1}, we find the test
    \begin{equation*}
        \frac{\sqrt{n}}{s_n}|\overline X_n -\mu_0| \geq t_{1-\alpha/2} (n-1).
    \end{equation*}
    From the given data, we get $\overline X_n = 15.228, s_n = 0.03706$ and
    \begin{equation*}
        \frac{\sqrt{10}}{0.03706}|15.228-15.2| \approx 2.3893 > 2.2622 \approx t_{0.975}(9).
    \end{equation*}
    Thus we reject the null hypothesis at the level of significance 0.05, meaning that a systematic error is likely.
\end{example}

\subsection{Variance tests}

The tests for variance are also given in \cref{tab:normal-test-1}. We will briefly illustrate them in the next example.

\begin{example}
    The variance in length of some parts produced by a factory should not exceed $300~\mu\mathrm{m}^2$. 15 parts were measured and the sampling variance was found to be $s_n^2=580 \mu m^2$. At a level of significance of 0.05, can this be explained only by random errors? Again we assume a normal distribution for the data.

    Here we want to test
    \begin{equation*}
        H_0: \sigma^2\leq 300 \qmq{against} H_1:\sigma^2 >300.
    \end{equation*}
    We claim that we can use the following test from \cref{tab:normal-test-1}:
    \begin{equation}\label{testsigma}
        \frac{(n-1)s_n^2}{\sigma_0^2}\geq \chi_{1-\alpha}^2(n-1).
    \end{equation}
    The reasoning is as follows. Similar to \cref{ex99}, it can be shown that
    \begin{equation*}
        \P\left[\frac{(n-1)s_n^2}{\sigma^2}\geq \chi_{1-\alpha}^2(n-1)\right] = \alpha.
    \end{equation*}
    Now we have a null hypothesis of the form $H_0: \sigma\leq \sigma_0$ and for all $\sigma\leq \sigma_0$
    \begin{equation*}
        \left[\frac{(n-1)s_n^2}{\sigma_0^2}\geq \chi_{1-\alpha}^2(n-1)\right]\subset \left[ \frac{(n-1)s_n^2}{\sigma^2}\geq \chi_{1-\alpha}^2(n-1)\right],
    \end{equation*}
    which then implies for all $\sigma\leq \sigma_0$
    \begin{equation*}
        \P\left[\frac{(n-1)s_n^2}{\sigma_0^2}\geq \chi_{1-\alpha}^2(n-1)\right]\leq \P \left[ \frac{(n-1)s_n^2}{\sigma^2}\geq \chi_{1-\alpha}^2(n-1)\right]=\alpha.
    \end{equation*}

    By substituting the specific values from this example, we get
    \begin{equation*}
        \frac{14\cdot 580}{300} \approx 27.07 \geq 23.7 \approx \chi_{0.95}^2(14).
    \end{equation*}
    Thus we reject the hypothesis $H_0$ at a significance level of 0.05. The machines producing the parts likely need to be readjusted.
\end{example}

\subsection{Equality tests for expectation}

We will now see how to test equality of expectations for normal distributions. Consider two independent random samples $X_1,\ldots, X_{n_1}$ and $Y_1,\ldots,Y_{n_2}$ from two normal distributions $\normal(\mu_1, \sigma^2)$ and $\normal(\mu_2, \sigma^2)$. We want to test
\begin{equation*}
    H_0: \mu_1=\mu_2 \qmq{against} H_1: \mu_1\neq \mu_2.
\end{equation*}
The tests are presented in \cref{tab:normal-test-2} found at the end of the chapter.

\begin{remark}
    In order to use the tests presented in \cref{tab:normal-test-2}, it is important to verify the assumption that both normal distributions have the same variance. If this information is not explicitly given, then we must first apply the test for equality of variances (see \cref{sec:test-eq-var}).
\end{remark}

\begin{example}
    Two tanks contain chlorinated water. Samples were taken from the first tank ($n_1=25$) and from the second tank ($n_2=10$) with the following results
    \begin{equation*}
        \overline X_{n_1} = 34.48,\quad s_{n_1}^2= 1.7482,\quad \overline Y_{n_2} = 35.59,\quad s_{n_2}^2= 1.7121.
\end{equation*}
We assume that these are samples from two normal distributions with the same
variance, say $\normal(\mu_1, \sigma^2)$ and $\normal(\mu_2,\sigma^2)$. Can we say at
level of significance 0.05 that the chlorine content of the two tanks is the same?

In this case we want to test
\begin{equation*}
    H_0: \mu_1=\mu_2 \qmq{against} H_1: \mu_1\neq \mu_2.
\end{equation*}
According to the formula in the table, we should first compute
\begin{equation*}
    (s^\ast)^2 = \frac{1}{25+10-2}\left( (25-1) s_{n_1}^2 + (10-1) s_{n_2}^2\right) \approx 1.7384.
\end{equation*}
Thus $s^\ast = 1.3185$ and the value of the test statistic is
\begin{equation*}
\sqrt{\frac{n_1+ n_2}{n_1+n_2}} \frac{1}{s^\ast} |\overline X_{n_1} - \overline Y{n_2} | \approx 2.25.
\end{equation*}
Since the value of the corresponding quantile is $t_{0.975}(33) \approx 2.035$, we reject the null hypothesis at a significance level of 0.05.
\end{example}

\subsection{Equality tests for variance}
\label{sec:test-eq-var}

Consider two independent random samples $X_1,\ldots, X_{n_1}$ and $Y_1,\ldots,Y_{n_2}$ from two distributions $\normal(\mu_1, \sigma_1^2)$ and $\normal(\mu_2, \sigma_2^2)$. The problem is now to test
\begin{equation*}
H_0: \sigma_1^2=\sigma_2^2 \qmq{against} H_1: \sigma_1^2\neq \sigma_2^2.
\end{equation*}

\begin{example}
    Diastolic blood pressure values were measured in 16 men and 13 women with the following results
\begin{equation*}
    \overline X_{M} = 77.375,\quad s_{M}^2= 69.7167,\quad
    \overline Y_{Z} = 71.0769,\quad s_{Z}^2= 85.0769.
\end{equation*}
    At a level of significance of 0.05, test the hypothesis that men have the same blood pressure as women. We assume normal distributions $\normal(\mu_M,\sigma_M^2)$ and $\normal(\mu_Z,\sigma_Z^2)$)

    First we should test the hypothesis of equality of variances,
    \begin{equation*}
        H_0: \sigma_M^2 = \sigma_Z^2 \qmq{against} H_1: \sigma_M^2 \neq \sigma_Z^2.
    \end{equation*}
    Using the test from \cref{tab:normal-test-2}:
    \begin{equation*}
        \frac{s_M^2}{s_Z^2} = \frac{69.7167}{85.0769} = 0.8195 \in (0.34, 3.17),
    \end{equation*}
    where we used the values $F_{0.025}(15,12) \approx 0.34$ and $F_{0.975}(15,12)\approx 3.17$. Thus we do not reject the hypothesis $H_0$, which means we can assume that the variances of the samples are not statistically different.

    Next we use the test for equality of means,
    \begin{equation*}
        H_0: \mu_M=\mu_Z \qmq{against} H_1:\mu_M\neq \mu_Z\, .
    \end{equation*}
    First we compute the statistic
    \begin{equation*}
        (s^\ast)^2 = \frac{1}{27}(15\cdot 69.1767 + 12\cdot 85.0769) \approx 76.54.
    \end{equation*}
    Therefore $s^\ast \approx 8.75$ and
    \begin{equation*}
        T_n \approx \sqrt{\frac{16\cdot 13}{29}}\frac{1}{8.75} |77.375 - 71.0769| \approx 1.928 < 2.0518 \approx t_{0.975}(27).
    \end{equation*}
    The null hypothesis cannot be rejected at the level of significance 0.05.

    If we were to test instead the hypothesis
    \begin{equation*}
    H_0: \mu_M=\mu_Z \qmq{against} H_1:\mu_M> \mu_Z,
    \end{equation*}
    then the test would be
    \begin{equation*}
        T_n > t_{1-\alpha}(n_1+n_2-2).
    \end{equation*}
    Since $t_{0.95}(27) \approx 1.703 < 1.928 \approx T_n$, we would reject the null hypothesis with a level of significance of 0.05.
\end{example}

\section{Goodness of fit tests}

\subsection{Pearson's chi-squared test, simple hypothesis}

Suppose we have a random sample $X_1,\ldots,X_n$ from an unknown probability distribution $F$ and we are wondering whether this data come from a particular distribution $F_0$. In other words, we would like to test the hypothesis
\begin{equation*}
    H_0: F=F_0 \qmq{against} H_1: F\neq F_0.
\end{equation*}
 First, we will consider the simple hypothesis where the distribution of the corresponding hypothesis $H_0$ is uniquely determined and focus separately on discrete and continuous distributions.

\subsection{Discrete distribution}

Let $X_1,\dots,X_n$ be independent discrete random variables which takes the values $b_1,\dots, b_r$ with probabilities $p_1,\dots,p_r$. We want to test the hypothesis
\begin{equation*}  
    H_0: (\forall 1\leq j\leq r)(p_j=p_j^0) \quad \qmq{against} H_1: (\exists 1\leq j\leq r)(p_j \neq p_j^0)
\end{equation*}
where $p_j^0$ are given constants. Pearson's chi-squared test is based on the Pearson limit theorem.

\begin{theorem}
    Let $X_1,\dots,X_n$ be independent discrete random variables which takes the values $b_1,\dots, b_r$ with probabilities $p_1,\dots,p_r$. Let $n_j$ denote the number of variables $X_1,\ldots,X_n$ taking the value $b_j$. $\P[X_i=b_j]=p_j^0$ then 
    \begin{equation*}
        T_n=\sum_{j=1}^r\frac{(n_j-np_j^0)^2}{np_j^0}\stL {\chi}^2{(r-1)}.
    \end{equation*}
\end{theorem}

In other words if $H_0$ is true then $T_n$ should behave like a $\chi^2(r-1)$, assuming a large sample size. The statistic $T_n$ can then be used to measure the degree of disagreement between the empirical distribution given by the observed data and the distribution function $F_0$. Large values of $T_n$ argue against $H_0$.

\paragraph{Pearson's chi-squared test} We reject the hypothesis $H_0$ with a significance level of $\alpha$ if
\begin{equation*}
    T_n = \sum_{j=1}^r \frac{(n_j-np_j^0)^2}{np_j^0} > \chi_{1-\alpha}^2(r-1).
\end{equation*}

\begin{remark}
    A rule of thumb for a good sample size is $np_j^0 \geq 5$ for all $j$.
\end{remark}

\begin{example}
    With 120 rolls of the dice, we obtained the following results
    \begin{center}
        \begin{tabular}{c|cccccccc}
            result & 1 & 2 & 3 & 4 & 5 & 6 \\ \hline
            frequency $n_i$ & 15 & 16 & 25 & 31 & 15 & 18 \\
        \end{tabular}
    \end{center}
    Can we say that we say that this is a fair dice at the significance level of 5\%?

    We want to test the hypothesis that the results come from the discrete uniform distribution on the set,
    \begin{equation*}
        H_0: F\sim \duniform(\{1,2,3,4,5,6\}\qmq{against} H_1:F\not\sim\duniform\{1,2,3,4,5,6\}.
    \end{equation*}
    In this case, we have $r=6$, $p_j^0=1/6$ and $np^0_j = 120/6 = 20$. Using the formula for the chi-squared statistic:
    \begin{equation*}
        T_n = \sum_{j=1}^6 \frac{(n_j-20)^2}{20} = 10.8.
    \end{equation*}
    Since we have $r=6$ possible values, we need compare the value of $T_n$ with the quantile $\chi^2_{0.95}(5) \approx 11.07$. Based on the results of this experiment, we cannot reject the hypothesis that the dice is fair.
\end{example}

\begin{example} 
    Consider 4096 rolls of 12 identical dices (six-sided). Let $X_i$ be the number of sixes rolled in the $i$th attempt. Thus we have a random sample $X_1,\ldots,X_{4096}$. The observed numbers are summarized in the next table, along with theoretical probabilities for fair dices. Test the hypothesis that the dices are fair at significance level of 5\%.
    \begin{center}
        \begin{tabular}{*{10}c}
            \toprule
            \# of 6s & 0 & 1 & 2 & 3 & 4 & 5 & 6 & 7+ & total \\
            \midrule
            \textbf{$n_j$} & 447 & 1145 & 1181 & 796 & 380 & 115 & 24 & 8 & 4096 \\
            $p_j^0$ & 0.112 & 0.263 & 0.296 & 0.197 & 0.089 & 0.028 & 0.007 & 0.001 & 1 \\
            $np_j^0$ & 459 & 1103 & 1213 & 809 & 364 & 116 & 27 & 5 & 4096 \\ 
            \bottomrule
        \end{tabular}
    \end{center}

    If the dices are fair then $X_i\sim \binomial(12,\frac{1}{6})$ so the probability that $X_i$ takes the value $j$ is
    \begin{equation*}
        p_j^0=\binom{12}{j}\left(\frac{1}{6}\right)^j\left(\frac{5}{6}\right)^{12-j}, \quad j=0,\ldots,12.
    \end{equation*}

    The Pearson statistic has a value of
    \begin{equation*}
        T_n = \sum_{j=0}^{7}\frac{(n_j-np_j^0)^2}{np_j^0}
        =\frac{1}{n}\sum_{j=0}^{7}\frac{n_j^2}{p_j^0}-n\approx 6.5.
    \end{equation*}
    Since we have ${\chi}^2_{0.95}(8-1)\approx14.07$, we do not reject the hypothesis $H_0$ that the dices are fair.
\end{example}

\subsubsection{Continuous distribution}

Consider now a random sample $X_1,\dots,X_n$ from a continuous distribution with distribution function $F$.  The problem is to test the hypothesis
\begin{equation*} 
    H_0: F=F_0 \qmq{against} H_1: F\neq F_0.
\end{equation*}
The idea is to first \emph{discretize} the data and then apply Pearson's chi-squared tests for discrete distributions. 

\paragraph{Practical steps}
\begin{enumerate}
    \item We divide the observation space into $r$  intervals $I_1,\ldots,I_r$.
    \item We compute the theoretical probabilities $p_j^0 = \P(X\in I_j)$ that the random variables $X_i$ fall in the interval $I_j$ under the null hypothesis $H_0$ as in the figure below.
    \begin{center}
        \begin{tikzpicture}
            \node[inner sep=0pt] (pic) at (0,0)
                {\includegraphics[width=7cm]{hustota}};
            \draw[color=black](-2.4,-1.2) node[anchor=north west]{$I_1$};
            \draw [color=black](-1.45,-1.2) node[anchor=north west] {$I_2$};
            \draw [color=black](2.4,-1.2) node[anchor=north west] {$I_r$};
            \draw [color=black](-2.4,-0.2) node[anchor=north west] {$p^0_1$};
            \draw [color=black](-1.55,0.5) node[anchor=north west] {$p^0_2$};
            \draw [color=black](2.4,-0.2) node[anchor=north west] {$p^0_r$};
            \draw [color=black](-0.2,0) node[anchor=north west] {{\dots}};
        \end{tikzpicture}
    \end{center}
    \item We calculate the number $n_j$ of observations $X_1,\dots,X_n$ in the interval $I_j$.
    \item We let $p_j = \P(X_i \in I_j)$ and apply Pearson's chi-squared test to
    \begin{equation*}  
        H_0': (\forall 1\leq j\leq r)(p_j=p_j^0) \quad \qmq{against} H_1': (\exists 1\leq j\leq r)(p_j \neq p_j^0)
    \end{equation*}
\end{enumerate}



\begin{remark}
    Testing for $H_{0}'$ is weaker than testing for $H_0$, since the validity of $H_0$ hypothesis implies that of $H_{0}'$, but the converse is false. 

    The more intervals we use, the closer the continuous and discrete distributions will be. However at the same time, increasing the number of intervals makes the Pearson's chi-squared test weaker. 

    To make sure the chi-squared test is not too weak, we can use the rule of thumb already mentioned and make sure that $np_j^0 \geq 5$. Thus we should try to make the probabilities $p_j^0$ as uniform as possible. In other words, we want to choose the boundaries of the interval so that $p_i^0\approx\frac1r$.
\end{remark}

\begin{example}
    We want to test the quality of a random number generator from a uniform distribution $U(0,1)$. To do this, we generate 1000 random numbers and divide the observation space $(0,1)$ into the 10 intervals $I_j = ((j-1)/10,j/10)$. The observed frequencies of the random numbers in each intervals are given in the table below.
    \begin{center}
        \begin{tabular}{l*{10}c}
            \toprule
            Interval $I_j$ & $I_1$ & $I_2$ & $I_3$ & $I_4$ & $I_5$ & $I_6$ & $I_7$ & $I_8$ & $I_9$ & $I_{10}$ \\ 
            \midrule
            Frequency $n_j$ & 91 & 92 & 97 & 99 & 108 & 103 & 102 & 110 & 99 & 99 \\
            \bottomrule
        \end{tabular}
    \end{center}
    Based on the results and with a level of significance of 0.05, is this a good random number generator?

    We want to test:  
    \begin{equation*}
        H_0:F=U(0,1) \qmq{against} H_1:F\neq U(0,1)
    \end{equation*}
    We perform the Pearson's chi-squared test with
    \begin{equation*}
        n=1000,\quad p_j=0.1,\quad np_j=100.
    \end{equation*}
    Using the data from the table,
    \begin{equation*}
        T_n = \frac{1}{100} \sum_{j=1}^{10} (n_j-100)^2 \approx 3.33 < 16.919 \approx \chi^2_{0.95}(9).
    \end{equation*}
    This can therefore be considered a good quality random number generator at a level of significance of 0.05.
\end{example}

\subsection{Pearson's chi-squared test, composite hypothesis}

Consider a random sample $X_1,\dots,X_n$ from an unknown distribution $F$. This time we will be interested in whether the data come from a certain parametric family of distributions. More precisely we want to test
\begin{equation*}
    H_0: F \in \{F_\theta \mid \theta \in \Theta\} \qmq{against} H_1: F \notin \{F_\theta \mid \theta \in \Theta\}.
\end{equation*}
Again, we focus separately on discrete and continuous distributions. 

\subsubsection{Discrete distribution}

Let $X_1,\dots,X_n$ be a random sample from a discrete distribution taking a finite number of possible values $b_1,\dots,b_r$ with probabilities
\begin{equation*} 
    \P[X_i = b_1]=p_1,\ldots, \P[X_i = b_r]=p_r.
\end{equation*}
We want to test the hypothesis $H_0$ that the data come from some family of discrete distributions $\{F_{\theta}| \theta \in \Theta\}$. If we write $p_j(\theta)=\P[X = b_j]$ where $X\sim P_\theta$, then we can write the hypotheses as
\begin{equation*} 
    H_0: (\exists\theta\in\Theta)(\forall 1\leq j\leq {r})({p_j=p_j(\theta)}) \qmq{against} H_1: (\forall\theta\in\Theta)(\exists 1\leq j\leq {r})({p_j\neq p_j(\theta)}). 
\end{equation*}
We already know how to test $H_0$ for a particular value of $\theta$ using the statistic
\begin{equation*} 
    T_n(\theta)=\sum_{j=1}^r\frac{(n_j-np_j(\theta))^2}{np_j(\theta)}.
\end{equation*}
However we have a lot of candidates for $\theta$. One possible approach is this:
  \begin{enumerate}
	\item Assuming $H_0$ find an estimate of $\theta^\ast$ the parameter $\theta$ based on the data,
    \item Test $H_0$ with the test statistic $T_n(\theta^\ast)$.
\end{enumerate}

The next question is what estimator to choose and how the dependency of $\theta^\ast$ on the sample $X_1,\dots,X_n$ will affect the quality of the chi-squared test. It can be shown that if $\theta^\ast$ is the MLE, i.e. $\theta^\ast$ maximizes the likelihood function
\begin{equation*}
    L(\theta)=\prod_{j=1}^{r}p_j(\theta)^{n_j},
\end{equation*}
then
\begin{equation*} 
    T_n(\theta^\ast) \stL {\chi}^2{(r-s-1)},
\end{equation*}
where $s$ is the number of parameters to be estimated, i.e. $s=\mathrm{dim}(\theta)$.

\paragraph{Formula for the composite chi-squared test.}

Therefore we reject the null hypothesis $H_0$ at a significance level of $\alpha$ if
\begin{equation*}
    T_n(\theta^\ast) = \sum_{j=1}^r \frac{(n_j-np_j(\theta^\ast))^2}{np_j(\theta^\ast)} > \chi_{1-\alpha}^2(r-s-1).
\end{equation*}

\begin{example}
	According to the ABO blood group system, individuals can have one of four blood types: A, B, AB, O. In an individual, the blood type (phenotype) is determined by a pair of traits A, B, O inherited from the parents (genotype), according to the rules described in the following table:
    \begin{center}
        \begin{tabular}{ccccc}
            \toprule
            Genotype & OO & AO, AA & BO, BB & AB \\
            \midrule
            Phenotype & O & A & B & AB\\
            \bottomrule
        \end{tabular}
    \end{center}

    We want to test the validity of this theory based on a random sample from a population. If we assume that $p,q,r=1-p-q$ are the relative frequencies of genotypes A, B and O in the population, then the model predicts that the probabilities for the four blood types in a sample of size $n$ will follow a multinomial distribution
    \begin{equation*}
        \multinomial(n,r^2,p^2+2pr,q^2+2qr,2pq).
    \end{equation*}

    The next table presents data obtained from a random sample of size 353 together with the observed frequencies.
	\begin{center}
		\begin{tabular}{cccc}
			\toprule
            $j$ & blood type & \# observed $n_j$ & rel.\ frequency\\ 
            \midrule
             1  & O  & $121$ & 0.3428 \\
             2  & A  & $120$ & 0.3399 \\
             3  & B  & $79$ & 0.2238 \\ 
             4  & AB & $33$ & 0.0935 \\ 
            \bottomrule
		\end{tabular}
    \end{center}

    In this example we have $r=4$ groups and $s=2$ unknown parameters $p,q$, so the asymptotic distribution of the $T_n$ statistic of the MLE under the null hypothesis will be $\chi^2(1)$. We start by finding the MLE estimates $\hat{p},\hat{q}$. The likelihood function has the form
    \begin{equation*} 
        L(p,q)=C\cdot (r^2)^{n_1}\cdot (p^2+2pr)^{n_2}\cdot (q^2+2qr)^{n_3}\cdot (2pq)^{n_4}
    \end{equation*}
    where $C$ is the appropriate multinomial coefficient. Passing to the logarithm,
	\begin{align*}
        \ln L(p,q) = \ln C &+ {n_1}\ln (r^2) + {n_2}\ln (p^2+2pr) + {n_3}\ln (q^2+2qr) + {n_4}\ln (2pq) \\
                   = \ln C &+ 2n_1\ln (1-p-q) + {n_2}\ln (p^2+2p(1-p-q)) \\
                           &+ {n_3}\ln (q^2+2q(1-p-q))+ {n_4}\ln (2pq).
	\end{align*}
	The system obtained for the MLE is difficult to solve explicitly. Using numerical methods, we obtain the MLE estimates $\wh{p}=0.247$ and $\wh{q}=0.173$. We can then estimate the parameters of the multinomial distribution using the formulas
    \begin{equation*}
        \wh p_1 = (1-\wh p-q)^2,\quad \wh p_2 = \wh p^2 + 2 \wh p (1-\wh p - \wh q),\quad \wh p_3 =  \wh q^2+2\wh q(1-\wh p-\wh q),\quad \wh p_4 = 2\wh p\wh q.
    \end{equation*}
    The next table gives the results of these computations together with the predicted numbers for each blood type.
    \begin{center}
        \begin{tabular}{cccc}
            \toprule
            $j$ & blood type & $n\hat{p_j}$ & $\hat{p_j}$ \\
            \midrule
              1 & {O}  & 118.7492 & 0.3364 \\
              2 & {A}  & 122.6777 & 0.3475 \\
              3 & {B}  & 81.4050 & 0.2306 \\ 
              4 & {AB} & 30.1681 & 0.0855 \\ 
            \bottomrule
        \end{tabular}
    \end{center}
    If we compute the value of Pearson's statistic, we obtain
	\begin{equation*}
        T_n(\wh p,\wh q) = \sum_{j=1}^4\frac{(n_j-n\wh p_j)^2}{n\wh p_j} \approx 0.44 < \chi^2_{0.95}(1) \approx 3.84 
    \end{equation*}
    and thus we do not reject the hypothesis $H_0$ at a signficance level of 5\%. . In fact we can calculate that $\chi^2_{1}(0.44,+\infty)=0.5071$, which can be taken as indication that the data agree very well with the theory.
\end{example}

\subsubsection{Continuous distribution}

Next we consider a random sample $X_1,\dots,X_n$ from a continuous distribution with distribution $F$. We want to test the composite hypothesis
\begin{equation*} 
    H_0: F\in\{F_\theta \mid \theta\in\Theta\} \qmq{against} H_1: F\notin\{F_\theta\mid\theta\in\Theta\}.
\end{equation*}

Again the strategy is to discretize the data and then apply the test for discrete distributions. We sort the data into $r$ intervals $I_1,\dots,I_r$ and let $p^0_j(\theta) = \P[X\in I_j]$ under the assumption that $F = F_\theta$. We then test the weaker hypothesis
\begin{equation*} 
    H_{0}^{\prime}: (\exists\theta\in\Theta)(\forall 1\leq j\leq r)(p_j=p^0_j(\theta)) \qmq{against} H_1' : (\forall\theta\in\Theta)(\exists 1\leq j\leq r)(p_j\neq p^0_j(\theta)) 
\end{equation*}
where $p_j = \P[X\in I_j]$ is the actual probability of the values from the sample being in $I_j$. 

\begin{remark}
	It is important to use the MLE estimate for the discretized hypothetical distribution, and not the continuous one. Take for example the case of a normal distribution $\normal(\mu,{\sigma}^2)$. The classical MLE estimates are
    \begin{equation*}
        \widehat{\mu}=\frac{1}{n}\sum_{i=1}^{n}X_i\quad\text{a}\quad\wh \sigma_n^2=\frac{1}{n}\sum_{i=1}^{n}(X_i-\overline X_n)^2.
    \end{equation*}
    On the other hand the MLE estimates for the discretized problem will involve the likelihood function
	\begin{equation*}
        L(\mu,\sigma^2) = \prod_{j=1}^j p_j(\mu,{\sigma}^2).
    \end{equation*}
    For continuous distributions or distributions with infinitely many values (e.g., normal or Poisson distributions), this can be a difficult numerical problem. If we use the MLE estimates of the continuous distribution, then the Pearson statistic
    \begin{equation*} 
        T_n=\sum_{j=1}^r\frac{(n_j-np_j(\hat{\theta}))^2}{np_j(\hat{\theta})} 
    \end{equation*} 
    does not converge in law to a $\chi^2(r-s-1)$. In fact it can be shown to converge somewhere between $\chi^2(r-s-1)$ and $\chi^2(r-1)$.
\end{remark}

\section{Exercises}

\begin{enumerate}[wide,label={\textbf{\arabic{*}.}},itemsep=1em]

\item During World War II, the Nazis used V2 missiles to target London in retaliation for allied bombing. The southern part of London was divided into squares of 0.25~km$^2$ and the number of missiles which landed in each area was recorded over some period of time. The next table presents the number of areas sorted by number of recorded missile hits (so for instance 93 areas recorded 2 hits). 
\begin{center}
    \begin{tabular}{*{8}c}
        \toprule
        \# missiles & 0 & 1 & 2 & 3 & 4 & 5 or more & total \\ 
        \midrule
        \# areas & 229 & 211 & 93 & 35 & 7 & 1 & 576 \\ 
        \bottomrule
    \end{tabular}
\end{center}
At a significance level of 5\%, test whether this is a Poisson distribution.

\item The number of defective connections on printed circuit boards coming out of a factory is thought to follow a Poisson distribution. 60 circuit boards were examined and the following data was observed.
\begin{center}
    \begin{tabular}{*{5}c}
        \toprule
        \# defective connections & 0 & 1 & 2 & 3 \\ 
        \midrule
        \# circuit boards & 32 & 15 & 9 & 4 \\
        \bottomrule
    \end{tabular}
\end{center}
At a significance level of 5\% test whether this really is a Poisson distribution.

\item 90 people were enrolled in a weight loss program. The following table shows the weight lost by the participants after 3 months of participation in the program. At a significance level of 5\%, test whether the data are normally distributed. The sample mean and sample variance of the original data are $\overline x_n = 3.74$ and $s_n^2 = 4.84$.
\begin{center}
    \begin{tabular}{*{11}c}
        \toprule
        Weight loss (kg) & 0 & 1 & 2 & 3 & 4 & 5 & 6 & 7 & 8 & over 8 \\
        \midrule
        \# of participants & 4 & 5 & 10 & 13 & 18 & 16 & 9 & 9 & 4 & 2 \\
        \bottomrule
    \end{tabular}
\end{center}

\end{enumerate}

\begin{table}[p]\centering
    \renewcommand{\arraystretch}{2}
    \small
    \begin{tabular}{cccc}
        \toprule
        $H_0$ & $H_1$ & critical region $W$ (rejecting $H_0$) & assumptions \\
        \midrule
        $\mu=\mu_0$ & $\mu\neq\mu_0$ & $\displaystyle\frac{\sqrt{n}}{\sigma}|\ol{X}_n-\mu_0| \,\geq\, u_{1-\alpha/2}$ & $\sigma^2$ known \\
        $\mu=\mu_0$ & $\mu>\mu_0$ & $\displaystyle\frac{\sqrt{n}}{\sigma}\,(\ol{X}_n-\mu_0) \,\geq\, u_{1-\alpha}$ & $\sigma^2$ known \\
        $\mu=\mu_0$ & $\mu<\mu_0$ & $\displaystyle\frac{\sqrt{n}}{\sigma}\,(\ol{X}_n-\mu_0) \,\leq\,- u_{1-\alpha}$ & $\sigma^2$ known \\
        $\mu=\mu_0$ & $\mu\neq\mu_0$ & $\displaystyle\frac{\sqrt{n}}{s_n},|\ol{X}_n-\mu_0| \,\geq\, t_{1-\alpha/2}(n-1)$ & $\sigma^2$ unknown \\ 
        $\mu=\mu_0$ & $\mu>\mu_0$ & $\displaystyle\frac{\sqrt{n}}{s_n}\,(\ol{X}_n-\mu_0) \,\geq\, t_{1-\alpha}(n-1)$ & $\sigma^2$ unknown \\
        $\mu=\mu_0$ & $\mu<\mu_0$ & $\displaystyle\frac{\sqrt{n}}{s_n}\,(\ol{X}_n-\mu_0) \,\leq\,- t_{1-\alpha}(n-1)$ & $\sigma^2$ unknown \\
        $\sigma^2 = \sigma_0^2$ & $\sigma^2 \neq \sigma_0^2$ & $\displaystyle\frac{(n-1)s_n^2}{\sigma_0^2} \notin \left( \chi^2_{\alpha/2}(n-1),\,\chi^2_{1-\alpha/2}(n-1)\right)$ & $\mu$ unknown \\
        $\sigma^2 = \sigma_0^2$ & $\sigma^2 < \sigma_0^2$ & $\displaystyle\frac{(n-1)s_n^2}{\sigma_0^2} \leq \chi^2_{\alpha}(n-1)$ & $\mu$ unknown \\
        $\sigma^2 = \sigma_0^2$ & $\sigma^2 > \sigma_0^2$ & $\displaystyle\frac{(n-1)s_n^2}{\sigma_0^2} \geq \chi^2_{1-\alpha}(n-1)$ & $\mu$ unknown \\
        \bottomrule
    \end{tabular}
    \caption{Hypothesis testing at level of significance $\alpha$ for parameters of a normal distribution. Tests are conducted using a sample $X_1,\dots,X_n$ from $\normal(\mu,\sigma^2)$.}
    \label{tab:normal-test-1}
 \end{table}

 \begin{table}[p]\centering\small
\renewcommand{\arraystretch}{2}
    \begin{tabular}{cccc}
        \toprule
        $H_0$ & $H_1$ & critical region $W$ (rejecting $H_0$) & assumptions \\
        \midrule
        $\mu_1=\mu_2$ & $\mu_1\neq\mu_2$ & $\displaystyle\sqrt{\frac{n_1 n_2}{n_1+n_2}}\,\frac{1}{\sigma}\, |\ol{X}_{n_1} - \ol{Y}_{n_2}| \geq u_{1-\alpha/2}$ & $\sigma_1^2 = \sigma_2^2$ known \\
        $\mu_1=\mu_2$ & $\mu_1\neq \mu_2$ & $\displaystyle\sqrt{\frac{n_1 n_2}{n_1+n_2}}\,\frac{1}{s^\ast}\, |\ol{X}_{n_1} - \ol{Y}_{n_2}| \geq t_{1-\alpha/2}(n_1+n_2-2)$ & $\sigma_1^2 = \sigma_2^2$ unknown \\ 
        $\sigma_1^2 = \sigma_2^2$ & $\sigma_1^2 \neq \sigma_2^2$ & $\displaystyle\frac{s_{n_1}^2}{s_{n_2}^2}\, \notin\, \left( F_{\alpha/2}(n_1-1,n_2-1),\, F_{1-\alpha/2}(n_1-1,n_2-1)\right)$ & --- \\
        \bottomrule
    \end{tabular}
\begin{gather*}
    s^{\ast} = \left( \frac{1}{n_1+n_2-2}\left(\sum_{i=1}^{n_1} \left( X_i - \ol{X}_{n_1}\right)^2 + \sum_{i=1}^{n_2} \left( Y_i - \ol{Y}_{n_2}\right)^2\right) \right)^{1/2}
\end{gather*}
\caption{Hypothesis testing at level of significance $\alpha$ for equality of parameters between two normal distributions. The tests are conducted using random samples $X_1,\dots,X_{n_1}$ from $\normal(\mu_1,\sigma_1^2)$ and $Y_1,\dots,Y_{n_2}$ from $\normal(\mu_2,\sigma_2^2)$. In the tests for $\mu_1=\mu_2$ we must have $\sigma_1=\sigma_2=\sigma$.}
\label{tab:normal-test-2}
 \end{table}
