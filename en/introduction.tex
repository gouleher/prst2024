
\chapter{Introduction}

\paragraph{Probability theory} attempts to quantify the likelihood of individual results of a random experiment \emph{in advance}.

\paragraph{Mathematical statistics} addresses, in a certain sense, the opposite problem: it works \emph{backwards} from observations to quantify the likelihood of different explanations.

\section{Classical definition of probability}

In his 1814 book titled \emph{Théorie analytique des probabilités}, P.-S. Laplace proposed one of the first formal definition of probability.
\begin{quote}
    The theory of chance consists in reducing all events of the same kind to a certain number of cases equally likely, that is, such that we are equally undecided regarding their existence; and in determining the number of favorable cases to the events whose probability is sought. The ratio between this number and that of all possible cases is the measure of this probability, which is thus simply a fraction whose numerator is the number of favorable cases, and whose denominator is the number of all possible cases.%
    \footnote{\emph{La théorie des hasards consiste à réduire tous les événements du même genre, à un certain nombre de cas également possibles, c'est-à-dire, tels que nous soyons également indécis sur leur existence; et à déterminer le nombre des cas favorables à l'événement dont on cherche la probabilité. Le rapport de ce nombre à celui de tous les cas possibles, est la mesure de cette probabilité qui n'est ainsi qu'une fraction dont le numérateur est le nombre des cas favorables et dont le dénominateur est le nombre de tous les cas possibles.} \cite{book/Laplace1812}, p. iv.}
\end{quote}

Let us break down this definition.
\begin{itemize}
    \item Consider a random experiment with $n$ possible outcomes (e.g. two rolls of a dice).
    \item We assume that all outcomes are equally likely (uniformity).
    \item If $m$ of these outcomes correspond to a given event $A$ (e.g. rolling two sixes) and the remaining $n-m$ outcomes exclude this event, then we may define the \emph{probability} of this event by
        \begin{equation*}
            \P(A) = \frac{m}{n} = \frac{\text{number of favorable outcomes}}{\text{number of possible outcomes}}
        \end{equation*}
\end{itemize}

This definition has some limitations: 
\begin{enumerate}[label={\alph{*})}]
    \item It cannot deal with non-uniform experiments (e.g. an unfair dice).
    \item It cannot be used for experiments with infinitely many possible outcomes.
\end{enumerate}

Nonetheless it is sufficient for many simple problems.

\begin{example}
    A cube with painted faces is cut evenly into 1000 smaller cubes. Determine the probability that a randomly selected small cube will have 
    \begin{enumerate*}[label={\alph{*})}]
        \item \emph{exactly} 2 painted faces or
        \item \emph{at least} 2 painted faces.
    \end{enumerate*}

    The total number of cubes is $n=1000$. The original cube has 12 edges, and each edge contains 8 small cubes with exactly two painted faces. Therefore the number of favourable outcomes is $m = 12 \cdot 8 = 96$. The probability of having \emph{exactly} two painted faces is
    \begin{equation*}
        \frac{96}{1000} = 0.096.
    \end{equation*}
    In addition, the small cubes located on the 8 corners of the big cube are the ones with 3 painted faces. Therefore there are $12\cdot 8+8 = 104$ cubes with \emph{at least} 2 painted faces and the corresponding probability is
    \begin{equation*}
        \frac{104}{1000} = 0.104
    \end{equation*}
\end{example}

\begin{example}
    Calculate the probability that the sum $s$ of the results of rolling two fair dices is \begin{enumerate*}[label={\alph{*})}]
        \item 9 or
        \item 10.
    \end{enumerate*}

    In this context, $s=9$ can be obtained by rolling $3+6$ or $4+5$. The result $s=10$ can be obtained by rolling either $5+5$ or $4+ 6$. At first glance, it might seem that both events have the same probability. However the number of all possible cases is $n = 36$ while the favourable outcomes are, respectively,
    \begin{center}
        \begin{tabular}{lcccc}
            for $s= 9$:  & 3,6 & 6,3 & 4,5 & 5,4 \\
            for $s=10$:  & 5,5 & 6,4 & 4,6 &
        \end{tabular}
    \end{center}
    and therefore
    \begin{equation*}
        \P(s=9) = \frac{4}{36} = \frac{1}{9}\quad\text{and}\quad 
        \P(s=10) = \frac{3}{36}= \frac{1}{12}.
    \end{equation*}
\end{example}

\subsection{Basic combinatorial formulas}

We next give a few basic combinatorial formulas which are often used for solving simple probability problems. Let $A$ be a set containing $n$ distinct elements $1, 2, \dots, n$. The following table gives the number of ways to sample $k$ elements from $A$ with or without \emph{ordering} and with or without \emph{replacement}.
\begin{center}
    \begin{tabular}{ccc}
        \toprule
        Sampling & without replacement & with replacement \\\midrule\addlinespace
        ordered & $\dfrac{n!}{(n - k)!}$ & $n^k$ \\\addlinespace
        unordered & $\dfrac{n!}{k!(n-k)!}$ & $\dfrac{(n+k-1)!}{k!(n-1)!}$\\ \bottomrule
    \end{tabular}
\end{center}

\begin{itemize}
    \item The number of unordered sampling without replacement, denoted $\binom{n}{k}$, is called a \emph{binomial coefficient}. In mathematical notation:
    \begin{equation*}
        \binom nk = \frac{n!}{k!(n-k)!}.
    \end{equation*}
    \item Observe that the number of unorder sampling \emph{with replacement} is also given by a binomial coefficient:
    \begin{equation*}
        \binom{n+k-1}{k} = \frac{(n+k-1)!}{k!(n-1)!}.
    \end{equation*}
    \item An ordered sampling without repetitions of \emph{all} elements of $A$ ($k=n$) is also called a \emph{permutation} of $A$. According to the above table the number of permutations of $A$ is $n!$.
\end{itemize}

\begin{example}
    Consider an experiment where a fair dice is rolled 6 times.

\begin{enumerate}[wide,label={\alph{*})}]
    \item What is the probability that 6 appears \emph{at least} once?

    We can use sampling with replacement to solve this problem. Let $A$ be the event \enquote{no 6 has been observed.} The number of possible outcomes (all possible 6-tuples $(i_1 , \dots , i_6)$ where $1\leq i_j\leq 6$) is $n = 6^6$. Next the number of favourable outcomes is $m = 5^6$. Therefore
    \begin{equation*}
        \P(A) = \frac{5^6}{6^6} = \left(\frac56\right)^6 \approx 0.3349.
    \end{equation*}
    The probability of a 6 appearing at least once is then
    \begin{equation*}
        1 - \P(A) \approx 0.6651.
    \end{equation*}

    \item What is the probability of a 6 appearing \emph{exactly} once? 

    Let $B$ denote this event. A favourable outcome for $B$ is a tuple $(i_1 , \dots , i_6)$ in which exactly one coordinate is 6. The number of such tuples is $m = 6 \cdot 5^5$ ($5^5$ times the number of possible positions for 6) and therefore
    \begin{equation*}
        \P(B) = \frac{6\cdot 5^5}{6^6} = \left(\frac{5}{6}\right)^5 \approx 0.4019.
    \end{equation*}

    \item What is the probability that 6 appears not more than once?
        
    The probability of this event is simply the sum of the probabilities of $A$ and $B$,
    \begin{equation*}
        \P (A) + \P(B) = \left(\frac{5}{6}\right)^5 +\left(\frac56\right)^6 \approx 0.7368 .
    \end{equation*}

    \item What is the probability that 6 appears strictly more than once?
        
    Using the previous answer the probability is
    \begin{equation*}
        1 - (\P (A) + \P(B)) \approx 0.2632.
    \end{equation*}
\end{enumerate}

\end{example}

\begin{example}
    $2n$ sports teams are evenly divided into two groups. Determine the probability that the two strongest teams will be placed
    \begin{enumerate*}[label={\alph{*})}]%
        \item in different groups or%
        \item in the same group?
    \end{enumerate*}

In both of these problems we only need to select the players in the first group. This can be done using unordered sampling without replacement. For a) we obtain the following result 
\begin{equation*}
    \frac{\binom21\binom{2n-2}{n-1}}{\binom{2n}n} = \frac{n}{2n-1}
\end{equation*}
and for b)
\begin{equation*}
    \frac{\binom21\binom{2n-2}{n-2}+\binom20\binom{2n-2}{n}}{\binom{2n}n} = \frac{n-1}{2n-1}.
\end{equation*}

Note that the sum of their probability is equal to 1 because these two events are complementary.
\end{example}

\section{Geometric probability}

Geometric probability studies random experiments taking place on a geometric object. Contrary to the classical definition, this means dealing with experiments having infinitely many possible outcomes. One of the first definition of geometric probability appeared in 1777 in the work of G.-L. de Buffon. In a two players game of chance taking place on a geometric surface, de Buffon describes the odds in terms of the surfaces favourable to the first and second players: 
\begin{quote}
    The odds of the first player winning will be to that of the second as the first surface is to the second.\footnote{\emph{Le sort du premier joueur est au sort du second, comme cette première superficie est à la seconde.} \cite{book/Buffon1777}, p.96.}
\end{quote}

Let us break down the definition.
\begin{itemize}
    \item Consider an experiment which takes place on a geometric object $S$.
    \item Assume that every point of $S$ is equally likely as an outcome (uniform distribution on the object). 
    \item The probability of an event $B$ is proportional to its size (area, volume, etc.) in $S$ representing favourable outcomes for $B$. More precisely if $\mu$ denotes the geometric size:
    \begin{equation*}
        \P(B) = \frac{\mu(B)}{\mu(S)},
    \end{equation*}
\end{itemize}

\begin{example}
    Two freight trains are to arrive at station to unload their cargo. Both trains can arrive at any time, but if a train is already present the other one has to wait before unloading. What is the probability that one of the train will have to wait if unloading the first train takes 2 hours and unloading the second one takes 3 hours?

    Let:
    \begin{align*}
        t_1 &= \text{time of arrival of the first train};\\
        t_2 &= \text{time of arrival of the second train};\\
        B   &= \text{one of the trains has to wait}.
    \end{align*}

    The arrival times $t_1$ and $t_2$ are independent and evenly distributed over $[0, 24]$. We can represent the outcomes geometrically as points on a $24\times 24$ square, where the $x$-coordinate represents arrival of the first train, and the $y$-coordinate the arrival of the second train.

    The first train has to wait if
    \begin{equation*}
        t_2 < t_1 < t_2 + 3
    \end{equation*}
    and the second train has to wait if
    \begin{equation*}
        t_1 < t_2 < t_1 + 2.
    \end{equation*}

    The areas corresponding to these inequalities are shown in the following picture.
    \begin{center}
        \includegraphics[scale=.5]{example1.5.pdf}
    \end{center}

    The probability of a train waiting is given by the ratio of this area to the total area of the square, that is
    \begin{equation*}
        \P(B) = \frac{24^2 - (22^2/2 +21^2/2)}{24^2} = 1 - \frac{1}{24^2}\left(\frac{22^2}2 +\frac{21^2}2\right) = \frac{227}{1152} \approx 0.197.
    \end{equation*}
\end{example}

\begin{example}
    Determine the probability that the quadratic equation
    \begin{equation*}
        x^2 + 2ax + b = 0
    \end{equation*}
    are real if the coefficients $a, b$ are picked uniformly at randomly with $|a|\leq n$ and $|b|\leq m$. 

    The roots of the quatradic equation will be real if its discriminant $\Delta$ is greater than or equal to zero, which means
    \begin{equation*}
        \Delta = 4a^2 - 4b \geq 0.
    \end{equation*}
    Or more simply $a^2 \geq b$. There are two cases, which are depicted in the figure below.

    \begin{center}
        \begin{tabular}{cc}
            \includegraphics[scale=.4]{example1.6-1} & \includegraphics[scale=.4]{example1.6-2} \\
            $m<n^2$ & $m>n^2$ 
        \end{tabular}
    \end{center}

    In both cases we will compute the complementary probability, i.e. the probability that $a^2 < b$.

    When $\sqrt m < n$ the complementary probability is equal to
    \begin{equation*}
        \frac{1}{4mn}\left(2mn+2(n-\sqrt m)m+2\int_0^{\sqrt m}a^2\,da\right) 
            = \frac{1}{4mn}\left(4mn-\frac43 m^{3/2}\right) = 1 - \frac{\sqrt m}{3n}
    \end{equation*}

    When $\sqrt m \geq n$ it is equal to
    \begin{equation*}
        \frac{1}{4mn}\left(2mn+2\int_0^n a^2\,da\right) = \frac{1}{4mn}\left(2mn + \frac23 n^3\right) = \frac12 + \frac{n^2}{6m}.
    \end{equation*}
\end{example}

\section{Bertrand's paradox}

Bertrand's paradox is a classical example which shows the limitations of these definitions of probability. It was published by J. Bertrand in 1840.

On a circle of radius 1, choose a chord (a line inscribed in the circle) at random. What is the probability that the length of the chord is greater than $\sqrt 3$? Depending on how the problem is approached, the probability obtained is different.

\begin{enumerate}[wide]
    \item We assume that the first point is fixed, and we analyze which locations of the second points yield a line of length $\geq \sqrt{3}$. Consider the equilateral triangle inscribed in the circle and having the first point as a vertex (cf. figure below). Then the line is longer than $\sqrt{3}$ if and only if the second point is below the side of the triangle opposite to the first point. Thus the probability is  the ratio between the length of the arc lying between two vertices of the triangle, over the circumference of the circle, which is $1/3$.
    \item We fix a radius for the circle and we assume that the line is perpendicular to that radius. Then the line is longer than $\sqrt{3}$ exactly when it's intersection with the radius is above the midpoint . Thus the probability is $1/2$. 
    \item We analyze where the middle point of the line lies within the circle. Then the line has length $\geq\sqrt{3}$ exactly when the middle point falls in the circle of radius $1/2$ with the same center as the original circle. Therefore the probability is the ratio of the area of the circle of radius $1/2$ over the circle of radius $1$, which is $1/4$.
\end{enumerate}
\begin{figure}\centering
\begin{tikzpicture}[scale=1.7]
    \fill[color=Gray,fill=Gray,fill opacity=0.1] (0,1) -- (-0.87,-0.5) -- (0.87,-0.5) -- cycle;
    \draw [color=Blue] (0,0) circle (1cm);
    \draw [color=Gray] (0,1)-- (-0.87,-0.5);
    \draw [color=Gray] (-0.87,-0.5)-- (0.87,-0.5);
    \draw [color=Gray] (0.87,-0.5)-- (0,1);
    \draw [color=Blue] (0,1)-- (-1,0);
    \draw [color=Red] (0,1)-- (-0.5,-0.87);
    \draw [color=Red] (0,1)-- (0.5,-0.87);
    \draw [color=Blue] (0,1)-- (1,0);
    \draw [shift={(0,0)},line width=1.2pt,color=Red]  plot[domain=3.67:5.76,variable=\t]({1*1*cos(\t r)+0*1*sin(\t r)},{0*1*cos(\t r)+1*1*sin(\t r)});
    \begin{scriptsize}
    \fill [color=black] (1,0) circle (1pt);
    \fill [color=black] (0,1) circle (1pt);
    \fill [color=black] (-1,0) circle (1pt);
    \fill [color=black] (-0.5,-0.87) circle (1pt);
    \fill [color=black] (0.5,-0.87) circle (1pt);
    \end{scriptsize}
\end{tikzpicture}
\qquad
\begin{tikzpicture}[scale=1.7]
    \draw [line width=1.2pt] (0,0) circle (1cm);
    \draw [line width=1.2pt,color=Blue] (0,1)--(0,0.5);
    \draw [line width=1.2pt,color=Red] (0,0.5)-- (0,0);
    \draw [color=Blue] (-0.6,0.8)-- (0.6,0.8);
    \draw [color=Blue] (-0.8,0.6)-- (0.8,0.6);
    \draw [color=Red] (-0.92,0.4)-- (0.92,0.4);
    \draw [color=Red] (-0.98,0.2)-- (0.98,0.2);
    \begin{scriptsize}
        \fill [color=black] (0,0) circle (1pt);
        \fill [color=black] (0,1) circle (1pt);
        \fill [color=black] (0,0.5) circle (1pt);
    \end{scriptsize}
\end{tikzpicture}
\qquad
\begin{tikzpicture}[scale=1.7]
    \draw [line width=0.4pt,color=Red,fill=red,fill opacity=0.25] (0,0) circle (0.5cm);
    \draw [line width=1.2pt,color=Black] (0,0) circle (1cm);
    \draw [color=Blue] (-0.35,0.94)-- (-0.8,-0.6);
    \draw [color=Red] (0.66,0.75)-- (-0.19,-0.98);
    \draw [color=Blue] (0.25,0.97)-- (0.99,-0.1);
    \draw [color=Red] (0.71,-0.71)-- (-0.21,0.98);
    \begin{scriptsize}
    \fill [color=Blue] (-0.57,0.17) circle (1pt);
    \fill [color=Red] (0.25,0.13) circle (1pt);
    \fill [color=Blue] (0.62,0.43) circle (1pt);
    \fill [color=Red] (0.23,-0.11) circle (1pt);
    \end{scriptsize}
\end{tikzpicture}
\caption{Bertrand's paradox.}
\end{figure}

The problem in this example is that \enquote{choosing a chord at random} is too vague. In other words, different ways of choosing the chord describe different experiments, event though they might seem equivalent. To quote Bertrand himself:
\begin{quote}
    Infinity is not a number; it must not, without explanation, be introduced into reasonnings. The illusory precision of words could give rise to contradictions. Choosing at random, between an infinite number of cases, is not a sufficient indication.\footnote{\emph{L'infini n'est pas un nombre; on ne doit pas, sans explication, l'introduire dans les raisonnements. La précision illusoire des mots pourrait faire naître des contradictions. Choisir au hasard, entre un nombre infini de cas possibles, n'est pas une indication suffisante.} \cite{book/Bertrand1889}, p.4}
\end{quote}

\section{Exercises}

\begin{enumerate}[wide, label={\textbf{\arabic{*}.}},itemsep=1em]
    \item In a shipment of 30 products, 3 are defective. What is the probability that among 5 randomly chosen products, not more than one is defective? % [190/203]
    
    \item A bag contains 10 prizes worth different amounts: 
        \begin{itemize}
            \item 5 worth  10 Kč, 
            \item 3 worth  30 Kč,
            \item 2 worth  50 Kč. 
        \end{itemize}
        If we randomly draw 3 prizes from that bag, what is the probability that the total amount is 70~Kč? % [7/24]
    
    \item 10 books are arranged on a shelf. Determine the probability that 3 books chosen at random are consecutive? % [1/15]

    \item We draw two independent real random numbers $x$ and $y$ from the interval $[0, 1]$. Compute the probability, that
        \begin{enumerate*}[label={\alph{*})}]
            \item $x + y \leq 1$,
            \item $x^2 + y^2 \leq 1$.
        \end{enumerate*}

    \item Two friends agreed to meet between 13h and 14h at a secret location. They each decide to arrive at random times within this period and wait 20 minutes for the other before leaving. What is the probability that the two friends meet?

    \item The game of \emph{franc-carreau} is a two player game of chance where a coin is tossed on a square-tiled floor. In this game, the first player wins if the coin is contained within a single square and otherwise the second player wins. If the sides of the tiles have length $l$ and the coin has radius $r$, what is the ratio $l/r$ which gives both players the same chance of winning? % [$2+\sqrt{2}$]
\end{enumerate}
