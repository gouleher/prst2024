
\chapter{Limit theorems}

\section{Law of large numbers}

So far we have dealt with individual random variables. In this chapter we will touch on the study of \emph{sequences} of random variables. Some examples of such sequences of practical and theoretical interest are:
\begin{itemize}
    \item Let $X_n$ denote the number of occurrences of a certain event $A$ in $n$ independent trials, where $\P(A) = p$ in each trial. Then $X_n/n$ expresses the frequency of $A$ in $n$ trials. How does $X_n$ behave for large $n$?
    \item Consider $n$ independent random variables $X_1,\ldots,X_n$ and their average $\overline X_n = \frac1n \sum_{i=1}^n X_i$. Does the distribution of $\overline X_n$ for large $n$ has any interesting properties? To what extent does it depend on the distributions of the variables $X_i$?
\end{itemize}

\begin{defin}
    Let $(X_n)_{n=1}^\infty$ be a sequence of random variables and $c\in\rr$. We say that $(X_n)_{n=1}^\infty$ \emph{converges in probability} to a random variable $X$ if for every $\ve>0$ 
    \begin{equation*}
        \lim_{n\to\infty} \P [|X_n-X|\geq \ve] = 0.
    \end{equation*}
    We denote this by $X_n\stP X$.
\end{defin}

\begin{example}
    Consider a sequence of random variables $X_n\sim \normal(\mu,\sigma^2/n)$, $n\in\nn$. Let us prove that $X_n\stP\mu$. By definition, we have to show that for every $\ve>0$,
    \begin{equation*}
        \lim_{n\to\infty}\P [|X_n-\mu|\geq \ve] = 0.
    \end{equation*}
    But for every $\ve>0$,
    \begin{align*}
        \P [|X_n-\mu|<\ve] = \frac{1}{\sqrt{2\pi\frac{\sigma^2}{n}}}\int_{\mu-\ve}^{\mu+\ve} e^{-\frac{n(x-mu)^2}{2\sigma^2}}, dx 
            = \frac{1}{\sqrt{2}\pi}\int_{-\frac{\sqrt{n}\ve}{\sigma}}^{\frac{\sqrt{n}\ve}{\sigma}}e^{-\frac{t^2}{2}}\, dt & 
    \end{align*}
    where $t=\frac{x-\mu}{\sigma/\sqrt{n}}, dx = \sigma dt/\sqrt{n}$. Therefore  $\lim_{n\to\infty}\P [|X_n-\mu|<\ve] = \frac{1}{\sqrt{2}\pi}\int_{-\infty}^{\infty}e^{-\frac{t^2}{2}}\, dt = 1$.
\end{example}

\begin{theorem}[Chebyshev inequality] 
    Let $X$ be a random variable with finite second moment. For all $\ve > 0$
    \begin{equation*}
        \P[|X-\E[X]|\geq\ve]\leq \frac{\Var(X)}{\ve^2}.
    \end{equation*}
\end{theorem}

\begin{proof}
    Let $Y = X-\E[X]$ and take $n\geq 1$. By the definition of expectation:
    \begin{equation*}
        \E[Y^2] = \int_0^\infty \P[Y^2>y]\, dy \geq \int_0^{(\ve-\frac1n)^2}\P[Y^2>y]\,dy \geq \int_0^{(\ve-\frac1n)^2}\P[|Y|\geq \ve]\,dy = \left(\ve-\frac1n\right)^2\P[|Y|\geq\ve].
    \end{equation*}
    Then letting $n\to\infty$,
    \begin{equation*}
        \P[|Y|\geq \ve] \leq \frac{\E[Y^2]}{\ve^2} = \frac{\Var (X)}{\ve^2}.\qedhere
    \end{equation*}
\end{proof}

\begin{theorem}
    Let $(X_n)_{n=1}^\infty$ be a sequence of independent and identically distributed random variables with expectation $\mu$ and variance $\sigma^2$. Let $\mu = \E[X_n]$.
    \begin{equation*}
        \overline X_n = \frac{1}{n} \sum_{i=1}^n X_i \stP \mu.
    \end{equation*}
\end{theorem}

\begin{proof}
    We use Chebyshev's inequality. First we calculate the expectation and variance of $\overline X_n$. Using the linearity of expectation:
    \begin{equation*}
        \E[\overline X_n] = \E\left[\frac{1}{n} \sum_{i=1}^n X_i \right]
    =\frac{1}{n} \sum_{i=1}^n \E [X_i] = \mu.
    \end{equation*}
    Since the random variables are independent:
    \begin{equation*}
        \Var(\overline X_n) = \Var\left(\frac{1}{n} \sum_{i=1}^n X_i \right) =\frac{1}{n^2} \sum_{i=1}^n \Var(X_i) = \frac{\sigma^2}{n}.
    \end{equation*}
    By Chebyshev's inequality, we get that for all $\ve>0$
    \begin{equation*}
        \P[|\overline X_n-\mu|\geq \ve]\leq \frac{\sigma^2}{n\ve^2}
    \end{equation*}
    which goes to 0 as $n\to\infty$.
\end{proof}

\begin{remark}
    A special case of the previous theorem is Bernoulli's theorem (1713). Consider a sequence of independent random variables $X_n\sim \bernoulli(p)$ and let $S_n = \sum_{i=1}^n X_i$. Thus $S_n$ expresses the number of successes in $n$ Bernoulli trials. Then
    \begin{equation*}
        \frac{S_n}{n} \stP p.
    \end{equation*}
    This statement follows from the previous theorem since a random variable $X\sim \bernoulli(p)$ has expectation $\E[X]=p$ (and finite variance).
\end{remark}

\begin{example}\label{ex82}
    What is the probability that among 1000 coin flips, tails comes up between 400 and 600 times?

    Let $(X_1)_{n=1}^{1000}$ be the random variables expressing the results of the coin flips (1 for head, 0 for tail). These variables are independent and
    \begin{equation*}
        \E[X_i] = \frac{1}{2}\cdot 1 + \frac{1}{2}\cdot 0 = \frac{1}{2},\qquad \E [X_i^2] = \frac{1}{2}\cdot 1^2 + \frac{1}{2}\cdot 0^2 = \frac{1}{2},\qquad \Var(X_i) = \frac{1}{2}-\left(\frac{1}{2}\right)^2 = \frac{1}{4}\, .
    \end{equation*}
    Let us denote by $Y=\sum_{i=1}^{1000}X_i$, which counts the number of heads in 1000 coin flips.
    \begin{equation*}
        \E[Y] = \sum_{i=1}^{1000} \E[X_i] = 500,\quad \Var(Y) = \sum_{i=1}^{1000} \Var(X_i) = 250.
    \end{equation*}
    According to Chebyshev's inequality, for all $\ve>0$
    \begin{equation*}
        \P[|Y-\E[Y]|\geq \ve]\leq \frac{\Var(Y)}{\ve^2}.
    \end{equation*}
    Therefore
    \begin{equation*}
        \P[|Y-500|\geq 100]\leq \frac{250}{100^2} = 0.025,
    \end{equation*}
    and so
    \begin{equation*}
        \P[|Y-500|< 100]\geq 1-0.025 = 0.975.
    \end{equation*}
    This means we can say that in 1000 coin flips, there will be between 400 and 600 heads with more than 97.5\% probability.
\end{example}

The law of large number also provides theoretical foundations for Monte--Carlo integration.

\begin{theorem}
    Let $(X_i)_{i=1}^\infty$ be a sequence of independent random variables which follow a $\uniform(a,b)$ and $f$ be an integrable function. Then
    \begin{equation*}
        \bar Y_n \stP \int_a^bf(x)\,dx
    \end{equation*}
    where $Y_n = f(X_n)$.
\end{theorem}

This also works for functions of several random variables.
\begin{example}
    Let $f\from \RR^2\to \{0,1\}$ be the function
    \begin{equation*}
        f(x,y) = \begin{cases}
            1 & \text{if } x^2+y^2\leq 1 \\
            0 & \text{otherwise.}
        \end{cases}
    \end{equation*}
    Using a random number generators, $10^6$ pairs of numbers were picked in the interval $(0,1)$ with uniform probability. Out of these pairs, $784570$ were such that $f(x,y)=1$. Then by the law of large numbers
    \begin{equation*}
        \frac{784570}{10^6} \approx \int_0^1\int_0^1 f(x,y)\,dxdy = \pi/4.
    \end{equation*}
    And indeed $\frac{784570}{25000}\approx 3.1383$, which is somewhat close to $\pi$.
\end{example}

\section{Central limit theorem}

In mathematical statistics, we are often interested in the distribution of the sum or average of $n$ independent random variables. However, determining the exact distribution might be difficult. The Central Limit Theorem (CLT) states that, under certain conditions, such distributions can be approximated by a normal distribution. This is a very important result, with many variations and a rich history (entire books have been written on this topic). Here we will present what is now often called the \enquote{classical} CLT.  

\begin{defin}
    Let $(X_n)_{n=1}^\infty$ be a sequence of random variables with distribution functions $F_{X_n}$ and let $X$ be a random variable with distribution function $F_X$. We say that the sequence $(X_n)_{n=1}^\infty$ converges in distribution to the random variable $X$ if
    \begin{equation*}
        \lim_{n\to \infty} F_{X_n}(x) = F_X(x)
    \end{equation*}
    at all points of continuity of $F_X$. We denote this by $X_n \stL X$.
\end{defin}

\begin{remark}
    In particular $(X_n)_{n=1}^\infty$ converges in distribution to $X\sim \normal(\mu,\sigma^2)$ if
    $$
    \lim_{n\to\infty} F_{X_n}(x) = \Phi\left(\frac{x-\mu}{\sigma}\right)
    $$
    for all $x\in\RR$. We will denote this by $X_n\stL \normal(\mu,\sigma^2)$.
\end{remark}

\begin{theorem}[CLT] 
    Let $(X_n)_{n=1}^\infty$ be a sequence of independent and identically distributed random variables, with common expectation $\mu$ and variance $\sigma^2$. 
    \begin{equation*}
        \frac{\sum_{i=1}^n X_i-n\mu}{\sigma\sqrt{n}} \stL \normal(0,1).
    \end{equation*}
\end{theorem}

We provide a sketch of proof that uses moment generating functions, following \cite{book/Rice2007}. Note that the proof is limited to random variables whose MGF exists on some interval $(-s,s)$, $s>0$. A more general proof uses characteristic functions.

\begin{proof}
    Let
    \begin{equation*}
        Z_n = \frac{\sum_{i=1}^nX_i-n\mu}{\sigma\sqrt{n}}. 
    \end{equation*}
    Since the MGF determines the distribution, it suffices to show that $\lim_{n\to\infty} M_{Z_n}(t)$ is the MGF of a $\normal(0,1)$, that is
    \begin{equation*}
        \lim_{n\to\infty} M_{Z_n}(t) = e^{t^2/2}.
    \end{equation*}

    Using the properties of MGFs:
    \begin{equation*}
        M_{Z_n}(t) = \prod_{i=1}^n M_{\frac{X_i-\mu}{\sigma\sqrt n}}(t) = M_{\frac{X_1-\mu}\sigma}(t/\sqrt n)^n.
    \end{equation*}
    Let $Y = \frac{X_1-\mu}\sigma$. Using the Taylor expansion at 0:
    \begin{equation*}
        M_Y(t) = M_Y(0) + M_Y'(0)(t/\sqrt{n}) + \frac12 M''_Y(0)(t/\sqrt n)^2 + \ve_n
    \end{equation*}
    where $\frac{\ve_n}{(t/\sqrt n)^2}\to 0$ as $n\to\infty$. But note that
    \begin{equation*}
        M_Y(0)=1,\quad M'_Y(0) = \mu'_1(Y) = 0,\quad M''_Y(0) = \mu'_2(Y) = 1.
    \end{equation*}
    Therefore
    \begin{equation*}
        M_{Z_n}(t) = (1+\frac{t^2}{2n}+\ve_n)^n
    \end{equation*}
    and it can be shown that if $a_n\to a$ then $(1+\frac{a_n}n)^n\to e^a$.
\end{proof}

\begin{remark}
    If we let $Y_n = \sum_{i=1}^n X_i$, then $\E[Y_n] = n\mu$, $\Var(Y_n)=n\sigma^2$ and the CLT can be expressed in the form
    \begin{equation*}
        \frac{Y_n-\E[Y_n]}{\sqrt{\Var(Y_n)}} \stL \normal(0,1).
    \end{equation*}
\end{remark}

\begin{remark}
    If the random variables $X_i\sim\bernoulli(p)$ are independent, then $Y_n = \sum_{i=1}^nX_i \sim \binomial(n,p)$ and by the previous theorem
    \begin{equation*}
        \lim_{n\to\infty} \P\left[ \frac{Y_n-np}{\sqrt{np(1-p)}}\leq x \right] = \Phi(x), \qmq{for all} x\in \RR.
    \end{equation*}
    This can be used to approximate binomial distributions using the normal distribution. 

    Different sources give different rule of thumb for when this approximation is \enquote{good enough,} but these rules usually of the form $np(1-p)\geq 9$, $\min(np,n(1-p))\geq 10$, etc.
\end{remark}

\begin{example}
    A ship has a carrying capacity of 5000 kg. The weight of the passengers are independent random variables with the same expectation of 70 kg and standard deviation of 20 kg. How many of passengers can be loaded so that the probability of overloading the boat is less than 0.1\%?

    Denote by $X_i$ the mass of the $i$-th passenger and $X=\sum_{i=1}^n X_i$ the mass of all passengers. Then
    \begin{equation*}
        \E[X] = \sum_{i=1}^n \E[X_i] = 70n \qmq{and} \Var(X) = \sum_{i=1}^n \Var(X_i) = 400 n.
    \end{equation*}
    We are interested in the value of $n$ such that
    \begin{equation*}
        \P[X\geq 5000]<0.001\, .
    \end{equation*}
    Taking $U\sim\normal(0,1)$, then the CLT gives the approximation
    \begin{align*}
        \P[X\geq 5000] &= \P\left[\frac{X-70n}{20\sqrt{n}}\geq \frac{5000-70n}{20\sqrt{n}}\right] \\
                       & \approx \P\left[ U\geq \frac{5000-70n}{20\sqrt{n}}\right]\\
                       &= 1-\Phi\left(\frac{5000-70n}{20\sqrt{n}}\right).
    \end{align*}
    Thus we are looking for $n$ such that:
    \begin{equation*}
        \Phi\left(\frac{5000-70n}{20\sqrt{n}}\right) >0.999.
    \end{equation*}
    Following the values in the standard normal table we find:
    \begin{equation*}
        \frac{5000-70n}{20\sqrt{n}} > 3.09
    \end{equation*}
    or
    \begin{equation*}
        70n+61.8\sqrt{n} -5000<0.
    \end{equation*}
    Solving this quadratic inequality in the variable $\sqrt{n}$, we find one root $\sqrt{n_1} = 8.02$ and conclude that we can board a maximum of 64 passengers.
\end{example}

\begin{example}
    With what probability can we say that out of 1000 coin flips, tail will come up between 450 and 550 times?

    In \cref{ex82} we found a lower bound for the probability $\P[450\leq Y\leq 550]$ using Chebyshev's inequality. Let us compare with the approximation obtained using the central limit theorem. Take $U\sim \normal(0,1)$. Then
    \begin{align*}
        \P[450\leq Y\leq 550] &= \P\left[ \frac{-50}{\sqrt{250}}\leq \frac{Y-500}{\sqrt{250}}\leq \frac{50}{\sqrt{250}}\right] \\
                              &\approx \P\left[ \frac{-50}{\sqrt{250}}\leq U\leq \frac{50}{\sqrt{250}}\right]\\
                              &= 2\Phi\left(\frac{50}{\sqrt{250}}\right)-1 \approx 0.9984.
    \end{align*}
    Similarly we could calculate for example
    \begin{equation*}
        \P[480\leq Y\leq 520] \approx 2\Phi\left(\frac{20}{\sqrt{250}}\right)-1 \approx 0.7941.
    \end{equation*}
    To illustrate, the exact value computed using the binomial distribution is $0.7939$ in this case.
\end{example}

\begin{example}
    We consider a fair 6-sided dice. What is the probability of rolling more than 110 sixes in 600 attempts?

    Let $Y$ denote the number of sixes out of 600 throws. So $Y$ has a binomial distribution with parameters $n=600$ and $p=1/6$. By a procedure similar to the previous example:
    \begin{equation*}
        \P[Y\geq 110] = \P\left[ \frac{Y-100}{\sqrt{250/3}}\geq \frac{10}{\sqrt{250/3}}\right] \approx 1-\Phi\left(\frac{10}{\sqrt{250/3}}\right) \approx 0.137.
    \end{equation*}
\end{example}

\begin{example}
    In the 2020 U.S. election, Biden received approximately 51.3\% of the votes. Knowing this, let us estimate the probability that an exit poll with $n=1500$ of respondents would incorrectly predict Trump winning.

    Let $X$ denote the number of Biden voters in the poll. Assuming that the voters sampled are independent, we can say that $X\sim\binomial(1500,0.513)$. Using the CLT and a standard normal table:
    \begin{equation*}
        \P[X\leq 749] = \P\left[\frac{X-769.5}{\sqrt{374.7465}}\leq\frac{-20.5}{\sqrt{374.7465}}\right] \approx \Phi(-1.058) = 1-\Phi(1.06) = 1 - 0.1446,
    \end{equation*}
    or approximately 14.46\%.
\end{example}

\begin{example}
    Consider a sequence $(X_i)_{i=1}^n$ of independent random variables $X_i\sim \uniform(0,1)$, that is, uniformly distributed on the interval $(0,1)$. For this distribution:
    \begin{equation*}
        \E[X_i] = \frac{1}{2},\quad \E[X_i^2] = \int_0^1 x^2,\,dx = \frac{1}{3} \qmq{a} \Var(X_i) = \frac{1}{3}-\frac{1}{4} = \frac{1}{12}.
    \end{equation*}
    Applying the CLT,
    \begin{equation*}
        \frac{\sum_{i=1}^n X_i - n/2}{\sqrt{n/12}} \stL \normal(0,1).
    \end{equation*}
    This can be used to construct a random number generator which approximates a standard normal distribution. For example, the random variables $U=\sum_{i=1}^{12}X_i - 6$ approximately follows a $\normal(0,1)$. Thus, if we generate 12 random numbers using $\uniform(0,1)$, take their sum, and subtract 6, then we generate a random number with distribution close to standard normal. For an even better result, we could generate 48 random numbers using $\uniform(0,1)$ and then take $U = \sum_{i=1}^{48}X_i/2 - 6$.
\end{example}

\section{Exercises}

\begin{enumerate}[wide,label={\textbf{\arabic{*}.}},itemsep=1em]

\item The masses of randomly selected objects follow a $\normal(2000,16)$. What is the probability that the average weight of 8 randomly selected objects is greater than 2002?
    
\item We are interested in the proportion $p$ of people with blood type A in a given population. In order to estimate this proportion, we select $n$ people at random and test their blood type. How many people do we need in order to estimate $p$ within 0.05\% with a probability greater than 90\%?

\end{enumerate}
