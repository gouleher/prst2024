
\chapter{Mathematical model of probability}

This chapter presents the modern axioms of probability, which were laid out by A. N. Kolmogorov in his book \emph{Foundations of the Theory of Probability}~\citep{book/Kolmogorov1950}. 

\section{The notion of event}

Consider an experiment whose outcomes we represent by the elements of a set $\Omega$. We call $\Omega$ the \emph{sample space}. Each element $\omega\in\Omega$, taken on its own, will be called an \emph{elementary event}. Those are the simplest type of events found in a sample space.

By grouping certain elementary events together, we can obtain more complicated events, called \emph{composite events}. In general, events will be \emph{some} subsets $A\subseteq\Omega$. However, exactly which subsets are considered as valid events will vary depending on context. The axioms of probability will clarify this. 

\begin{example}
    Consider an experiment which consists of two flips of a fair coin. Then the set of all possible outcomes may be written as
    \begin{equation*}
        \Omega = \{HH, HT, TH, TT\}
    \end{equation*}
    where the symbols $H$ and $T$ indicate the result of the coin flip, head or tail. For example, the elementary event $\{HH\}$ means that the two flips were heads.

    Consider the following events
    \begin{enumerate}[label={$\Alph{*}$:}]
        \item the first throw is a head, 
        \item at least one head has occurred,
        \item three heads have occurred.
    \end{enumerate}

    The event $A$ is a composite event made up of 2 elementary events. It may be written as 
    \begin{equation*}
        A = \{HH, HT\}.
    \end{equation*}
    
    The event $B$ consists of 3 elementary events:
    \begin{equation*}
        B = \{HH, HT, TH\}.
    \end{equation*}

    Finally, the event $C$ is empty, which indicates that it describes none of the possible outcomes of this experiment.
\end{example}

The symbol $\emptyset$ denotes the empty set. It represents an \emph{impossible event}, also called \emph{null event}. The set $\Omega$ is also an event, which is called the \emph{certain event} or \emph{sure event}. 

If the outcome of the experiment is an element $\omega\in\Omega$ and $A$ is an event which contains $\omega$ (in mathematical notation, $\omega\in A$) then we say that the event $A$ \emph{has occurred}. Following this definition, the certain event always occurs (since it contains all elements) while the null event never does (since it contains none).

\begin{remark}
    Events are not always mutually exclusive, meaning different events can occur simultaneously. In the previous example, the outcome $\omega = HH$ is an occurrence of both $A$ and $B$.
\end{remark}

\subsection{Operations on events}

\paragraph{Complement} 
The complementary event, or negation, of $A$ is the event $A^\complement$ that occurs precisely when $A$ does not. In mathematical notation:
\begin{equation*}
    A^\complement = \Omega \setminus A = \{\omega\in\Omega \mid \omega\notin A\}.
\end{equation*}

\paragraph{Union} 
The union of the events $A_1, \dots , A_n$ is the event which occurs exactly when \emph{at least one} of the events $A_1, \dots, A_n$ occurrs. This is denoted
\begin{equation*}
    A_1\cup A_2\cup\dots\cup A_n \quad\text{or}\quad \bigcup_{j=1}^nA_j.
\end{equation*}

\paragraph{Intersection} 
The intersection of the events $A_1, \dots, A_n$ is the event which occurs exactly when \emph{all of them} occur simultaneously. This is denoted
\begin{equation*}
    A_1 \cap A_2 \cap \dots \cap A_n \quad\text{or}\quad \bigcap_{j=1}^nA_j.
\end{equation*}
We also use the shorthand $A \cap B = AB$.

\paragraph{Disjointness} 
Two events $A, B$ are \emph{disjoint} (sometimes called \emph{exclusive} or \emph{incompatible}) if they cannot occur at the same time. In other words $A\cap B = \emptyset$. 

A sequence of events $(A_k)_{k=1}^\infty$ is called \emph{pairwise disjoint} if all pairs $A_i, A_j$ with $i\neq j$ are disjoint. 

When events are pairwise disjoint, we use a special notations for their union:
\begin{equation*}
    A\cup B = A+B \quad\text{and}\quad \bigcup_{j=1}^nA_j = \sum_{j=1}^nA_j.
\end{equation*}

\paragraph{Implication} 
We say that an event $A$ \emph{implies} an event $B$ if the latter occurs every time that the former does. This is denoted by
\begin{equation*}
    A\subseteq B.
\end{equation*}

\begin{example}
    Take for instance $A = $ \enquote{I will live to be 70 years old} and $B =$ \enquote{I will live to be 40 years old.} Then $A$ implies $B$ ($A\subseteq B$) but $B$ does not imply $A$ ($B\not\subseteq A$).

    The complements of those events are $B^\complement = $ \enquote{I will not live to be 40 years old} and $A^\complement=$ \enquote{I will not live to be 70 years old.} Note that $B^\complement \subseteq A^\complement$. 
\end{example}

\begin{properties}\label{events-properties}
    Let $A$, $B$ and $C$ be events.
    \begin{enumerate}
        \item $A\subseteq A$.
        \item If $A \subseteq B$ and $B \subseteq C$ then $A \subseteq C$.
        \item $A \cap A = A = A\cup A$.
        \item $A\cap B = B\cap A$ and $A \cup B = B\cup A$.
        \item $A \cap (B \cap C) = (A \cap B) \cap C$ and $A\cup (B \cup C) = (A \cup B) \cup C$.
        \item $\emptyset\subseteq A\subseteq \Omega$.
        \item $A \cap B \subseteq A \subseteq A \cup B$.
        \item $A \cap \emptyset = \emptyset$ and $A\cup \emptyset = A$.
        \item $\Omega \cap A = A$ and $\Omega\cup A=\Omega$.
        \item $(A^\complement)^\complement = A$.
        \item $(A \cap B)^\complement = A^\complement \cup B ^\complement$ and $(A \cup B)^\complement = A^\complement \cap B^\complement$ (De Morgan's laws).
        \item $A \cup B = A + B A^\complement$.
        \item $B = AB + A^\complement B$.
    \end{enumerate}
\end{properties}

For example, let us go over the proof of one of de Morgan's laws.

\begin{proof}
    To prove that $(A \cap B)^\complement = A^\complement \cup B^\complement$, we need to show two inclusions:
    \begin{equation*}
        (A \cap B)^\complement \subseteq A^\complement \cup B^\complement \quad\text{and}\quad  A^\complement \cup B^\complement \subseteq (A \cap B)^\complement.
    \end{equation*}
    For the first inclusion, we have:
    \begin{equation*}
        \omega \in (A\cap B)^\complement \implies \omega\notin A\cap B\implies \omega\notin A \lor \omega\notin B \implies \omega\in A^\complement \lor \omega\in B^\complement \implies \omega \in A^\complement \cup B^\complement.
    \end{equation*}
    In the above chains of implications, the reverse implications also hold at every step. This gives a proof of the other inclusion.
\end{proof}

\section{The axioms of probability}

As in the previous section, $\Omega$ will denote a sample space. The next definition presents the first part of Kolmogorov's axioms of probability.

\begin{definition}
    Let $\mathfrak A$ be a set of subsets of $\Omega$ with the following properties
    \begin{enumerate}[label={\Roman{*}.}, ref = {\Roman{*}}, series=axioms]
        \item $\Omega \in \mathfrak A$. \label{axiom-1}
        \item If $A\in\mathfrak A$ then $A^\complement \in \mathfrak A$. \label{axiom-2}
        \item If $(A_i)_{i=1}^\infty$ is a sequence of elements of $\mathfrak{A}$, then $\bigcup_{i=1}^\infty A_i \in\mathfrak A$.  \label{axiom-3}
    \end{enumerate}
    The set $\mathfrak A$ is called a \emph{$\sigma$-algebra}. The elements $A\in\mathfrak A$ are called \emph{events}.
\end{definition}

\begin{properties}
    Let $\mathfrak{A}$ be a $\sigma$-algebra over $\Omega$.
    \begin{enumerate}
        \item The null set $\emptyset$ is in $\mathfrak{A}$.
        \item If $A_1, A_2, \dots, A_n$ are events from $\mathfrak A$ then their union $A_1\cup A_2\cup\dots\cup A_n$ is in $\mathfrak{A}$.
        \item If $(A_i)_{i=1}^\infty$ is a sequence of events from $A$, then $\bigcap_{i=1}^\infty A_i\in\mathfrak{A}$.
        \item If $A_1, A_2, \dots, A_n$ are events from $\mathfrak A$ then their intersection $A_1\cap A_2\cap\dots\cap A_n$ is in $\mathfrak{A}$.
    \end{enumerate}
\end{properties}

We give the proof of the third property.

\begin{proof}
    Using Axiom \ref{axiom-2}, $A_i^\complement\in\mathfrak{A}$ for all $i\in\nn$. Then using Axiom \ref{axiom-3}, $\bigcup_{i=1}^\infty A_i^\complement\in\mathfrak{A}$. Using Axiom \ref{axiom-2} with de Morgan's laws,
    \begin{equation*}
        \bigg(\bigcup_{i=1}^\infty A_i^\complement\bigg)^\complement = \bigcap_{i=1}^\infty (A_i^\complement)^\complement = \bigcap_{i=1}^\infty A_i \in\mathfrak{A}.\qedhere
    \end{equation*}
\end{proof}

\begin{remark}
    The appropriate choice of $\sigma$-algebra depends on the situation. In the case of a finite experiment, we can take the set of all subsets of $\Omega$, denoted $2^\Omega$ (although it may not be the most convenient). However in the infinite case taking $\mathfrak{A}=2^\Omega$ can lead to some contradictions.
\end{remark}

\begin{example}
    For the roll of a dice, the set of all possible outcomes is $\Omega = \{1, 2, 3, 4, 5, 6\}$. We can take for $\sigma$-algebra the set $2^\Omega$ of all subsets of $\Omega$. With this choice, there are $2^6=64$ possible events. If we are interested only, say, in the \emph{parity} of the result (even or odd), then we could consider the smaller $\sigma$-algebra
    \begin{equation*}
        \mathfrak A = \{\Omega, \{1,3,5\}, \{2,4,6\}, \emptyset\}.
    \end{equation*}
\end{example}

\begin{definition}
    Let $\Omega = [a,b]$ where $-\infty\leq a<b\leq\infty$ be a (possibly infinite) interval of the real line. The smallest $\sigma$-algebra containing all the semi-open intervals $(s,t] \subseteq \rr$, $a\leq s<t\leq b$, is called the \emph{Borel $\sigma$-algebra}, denoted $\mathcal B([a,b])$. When $[a,b]=\rr$ we write $\mathcal{B}$ for short.
\end{definition}

The question of whether or not $\mathcal{B}$ contains all subsets of $\rr$ is difficult and historically important. A famous example of subset of $\rr$ which is not in $\mathcal{B}$ is called the \emph{Vitali set}. However it's existence depends on the axiom of choice.

We now state the remaining Axioms of probability.

\begin{definition}
    Let $\Omega$ be a sample space with a $\sigma$-algebra $\mathfrak A$. Let $\P\from\mathfrak A\to\rr$ be a function satisfying
    \begin{enumerate}[resume*=axioms]
        \item $\P(\Omega) = 1$ (finiteness).
            \label{axiom-4}
        \item $\P(A) \geq 0$ for all $A\in\mathfrak A$  (positivity).
            \label{axiom-5}
        \item $\P(\sum_{k=1}^\infty A_k) = \sum_{k=1}^\infty\P(A_k)$ for all sequences $(A_k)_{k=1}^\infty$ of pairwise disjoint events   (countable additivity).
            \label{axiom-6}
    \end{enumerate}
\end{definition}

The function $\P$ is called a \emph{probability measure} while the triple $(\Omega, \mathfrak A, \P)$ is called a \emph{probability space}.

\begin{properties}
    Let $(\Omega,\A,\P)$ be a probability space and let $A,B\in\mathfrak{A}$.
    \begin{enumerate}
        \item If $A \subseteq B$ then $\P(A) \leq \P (B)$.
        \item $\P(A) \leq 1$.
        \item $\P(A^\complement) = 1 - \P(A)$.
        \item $\P(\emptyset) = 0$.
        \item If $A_1, A_2, \dots, A_n$ are pairwise disjoint events in $\mathfrak{A}$ then $\P(\sum_{i=1}^nA_i) = \sum_{i=1}^ n\P(A_i)$.
    \end{enumerate}
\end{properties}

\begin{proof}
    1. Since $A\subseteq B$, we have $B = AB +A^\complement B$. Since $A\subseteq B$, we have that $AB = A$ and then by finite additivity:
    \begin{equation*}
        \P (B) = \P(A) + \P(A^\complement B) \geq \P (A).
    \end{equation*}

    2. Apply the previous property with $A\subseteq\Omega$ and Axiom~\cref{axiom-4}.

    3. The certain event be can be decomposed as the disjoint union $\Omega = A + A^\complement$, so by finite additivity and Axiom \ref{axiom-4},
    \begin{equation*}
        1 = \P(\Omega) = \P(A+A^\complement) = \P (A) + \P(A^\complement).
    \end{equation*}

    4. Consider the sequence of events $(A_k)_{k=1}^\infty$ where every $A_k = \emptyset$ is the null event. Since $\emptyset$ is disjoint with itself, we may apply Axiom \ref{axiom-6} to conclude that
    \begin{equation*}
        \P(\emptyset) = \P\bigg(\sum_{k=1}^\infty\emptyset\bigg) = \sum_{k=1}^\infty\P(\emptyset),
    \end{equation*}
    which can hold only if $\P(\emptyset)=0$.

    5. Let $A_k = \emptyset$ for $k>n$. Then the events in the sequence $(A_k)_{k=1}^\infty$ are pairwise disjoint and $A_1+A_2+\dots A_n = \sum_{k=1}^\infty A_k$. Using Axiom~\ref{axiom-6} and the previous property::
    \begin{equation*}
        \P\bigg(\sum_{k=1}^\infty A_k\bigg) = \sum_{k=1}^\infty \P(A_k) = \P(A_1)+\P(A_2) +\dots +\P(A_n) + \sum_{k=n+1}^\infty\P(\emptyset) = \P(A_1)+\P(A_2) +\dots +\P(A_n).\qedhere
    \end{equation*}
\end{proof}

\begin{theorem}[Inclusion-exclusion principle]
    For any two events $A, B$, the following holds: 
    \begin{equation*}
        \P(A \cup B) = \P(A) + \P(B) - \P(A \cap B).
    \end{equation*}
\end{theorem}

\begin{proof}
    Write $B$ as the disjoint union $B = AB + A^\complement B$. By finite additivity, 
    \begin{equation*}
        \P(B) = \P(AB) + \P(A^\complement B) \qmq{i.e.} \P (A^\complement  B) = \P(B) - \P(AB).
    \end{equation*}
    Likewise we can decompose $A\cup B$ into the disjoint union $A\cup B = A + A^\complement B$. Then by finite additivity,
    \begin{equation*}
        \P (A \cup B) = \P (A) + \P (A^\complement B) = \P(A) + \P(B) - \P(AB).\qedhere
    \end{equation*}
\end{proof}

\begin{corollary}
    For any two events $A, B$ (not necessarily disjoint) 
    \begin{equation*}
        \P (A\cup B) \leq \P(A) + \P(B).
    \end{equation*}
\end{corollary}

\begin{example}\label{infinite-coin}
    Three players take turn flipping a fair coin and the first player who gets a tail wins. Determine the odds of winning for each player.

    We take as sample space the set $\Omega=\{H^nT \mid n\in \nn\}$ where $H^n$ represents $n$ consecutive head flips. Since the coin is fair, the probability measure $\P$ should be defined on elementary events by
    \begin{equation*}
        \P(\{H^nT\}) = (1/2)^{n+1}.
    \end{equation*}
    This can be extended to the $\sigma$-algebra $\mathfrak{A}=2^\Omega$ by
    \begin{equation*}
        \P(A) = \sum_{\omega\in A}\P(\omega) = \sum_{H^nT\in A}(1/2)^{n+1}.
    \end{equation*}
    The probability space $(\Omega,2^\Omega,\P)$ is a mathematical model of our experiment.

    Let $A_i$ denote the event corresponding to the first player winning in his $i$-th turn, and $A$ be the event corresponding to the first player winning at any turn. The events $A_i$ and $A$ can be written as,
    \begin{equation*}
        A_i = \{H^{3i-3}T \},\quad A = \sum_{i=1}^\infty A_i.
    \end{equation*}
    Note that the events $A_i$ are elementary and have probability $\P(A_i) = (1/2)^{3i-2}$. We can compute the probability of the event $A$ using Axiom \ref{axiom-6}:
    \begin{equation*} 
        \P(A) = \P\left(\sum_{i=1}^\infty A_i\right) = \sum_{i=1}^\infty\P(A_i) = \sum_{i=1}^\infty (1/2)^{3i-2}.
    \end{equation*}
    Using the general formula for geometric series:
    \begin{equation*}
        \sum_{i=1}^\infty (1/2)^{3i-2} = 4\sum_{i=1}^\infty\left(\frac18\right)^{i} = 4\cdot\frac1{8-1} = \frac47.
    \end{equation*}
    Therefore the odds of the first player winning are $4/7$, which is around 57\%.

    Similarly, for the events $B_i$ which consist in the second player winning in his $i$-th turn, and $B$ which consists in the second player winning at any turn, we have
    \begin{equation*}
        \P(B) = \sum_{i=1}^\infty\P(B_i) = \sum_{i=1}^\infty \left(\frac12\right)^{3i-1} = \frac27,
    \end{equation*}
    so the odds for the second player are $2/7$, i.e. around 29\%. Finally the odds of the third player winning are $1/7$, or approximately 14\%.
\end{example}

\section{Lebesgue measure}

Although we already introduced the Borel $\sigma$-algebra $\mathcal{B}$, we have yet to define a probability measure on it. Perhaps the most obvious place to start is to try to extend usual measure for intervals, i.e. the length. In other words, we are looking for a measure $\lambda$ on $\mathcal{B}$ such that $\lambda([a,b]) = b-a$. This is exactly what H. Lebesgue did in his PhD thesis, published in 1902.
\begin{theorem}
    There exists a unique probability measure $\lambda$ on $\B$ such that $\lambda[a,b)=b-a$ for every $0\leq a\leq b\leq 1$.
\end{theorem}

The measure $\lambda$ is called the \emph{Lebesgue measure}. The modern proof of its existence is a special case of a more general result known as \emph{Carathéodory's extension theorem} (named after the greek mathematician C. Carathéodory). The Lebesgue measure $\lambda$ is in fact well-defined on a larger $\sigma$-algebra, called the \emph{Lebesgue $\sigma$-algebra}. 

The Lebesgue measure has the property of being \emph{translation invariant}, in the sense that $\lambda(x+A) = \lambda(A)$ for every Borel set $A$. 

A example given by G. Vitali in 1905 shows that $\lambda$ cannot be extended to a probability measure on the $\sigma$-algebra of \emph{all} subsets of $[0,1]$. 

With the Lebesgue measure, the space $([0,1],\mathcal B([0,1]),\lambda)$ is a probability space. Therefore we can speak of the probability that a real number chosen at random in $[0,1]$ has some property \emph{as long as this property describes a Borel set}.

\begin{example}
    Fix a number $x$ with $0<x<1$. What is the probability that a random number $\omega\in [0,1]$ is equal to $x$? 

    First we have to make sure that the question makes sense, or in other words that the set $\{x\}$ is Borel. This comes from the fact that 
    \begin{equation*}
        \{x\} = \bigcap_{n=1}^\infty[x-1/n,x+1/n).
    \end{equation*}
     
    In the probability space $([0,1],\mathcal B([0,1]),\lambda)$, the answer is $\P(\omega=x) = 0$. To see why, let $A_n = [x-1/n,x+1/n)$. Then the event that $\omega=x$ is given by $A = \bigcap_{n=1}^\infty A_n$. But for every $n$, $\P(A) \leq \P(A_n) = \lambda([x-1/n,x+1/n)) = 2/n$, and therefore $\P(A)=0$.
\end{example}

\section{Exercises}

\begin{enumerate}[wide,label={\textbf{\arabic{*}.}},itemsep=1em]
    \item In the experiment consisting of three fair coin flips, we consider the events: 
    \begin{enumerate}[label={$\Alph{*}$:}]
        \item {the first two tosses are tails}, 
        \item {at least one toss is a head}, 
        \item {at most one toss is a head}, 
        \item {all three tosses have the same result},
        \item {no two consecutive tosses have the same result}.
    \end{enumerate}
    \begin{enumerate}[label={\alph{*})}]
        \item Give a model for this experiment in Kolmogorov's axioms.
        \item Write down the list of favourable outcomes for each of the events.
        \item Calculate their union and intersection.
        \item Find all disjointness and implication relations between these events.
    \end{enumerate}

    \item Prove by induction the general form of the inclusion exclusion principle:
    \begin{equation*}
        \P(A_1\cup A_2\dots\cup A_n) = \sum_{k=1}^n(-1)^{k+1}\sum_{1\leq i_1<i_2\dots<i_k\leq n}\P(A_{i_1}\cap A_{i_2}\cap\dots\cap A_{i_j}).
    \end{equation*}

    \item At a wedding dinner, each of the $n$ guests have been assigned their own seat. However during the course of dinner people move about the room, so that by the end of the night the place where each guest is sitting is completely random. Show that the probability that nobody sits in their initially assigned seat by the end of the night is
    \begin{equation*}
        \sum_{i=0}^n\frac{(-1)^i}{i!}.
    \end{equation*}
    Hint: use the inclusion-exclusion principle. What is the limit of this probability as $n\to\infty$?

    \item Using the Axioms of probability (\ref{axiom-1}--\ref{axiom-3}), prove that $\mathcal B(\rr)$ contains
    \begin{enumerate}[label = {\alph{*})}]
        \item all intervals $[a,b]$, $[a,b)$, $(a,b)$ and
        \item all half lines $(-\infty,b)$, $(-\infty,b]$, $(a,\infty)$, $[a,\infty)$.
    \end{enumerate}

    \item Show that the set $\qq\cap[0,1]$ is Borel. What is the probability that a random number $\omega\in[0,1]$ is rational?
\end{enumerate}
