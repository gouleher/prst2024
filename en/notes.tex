\documentclass[11pt,twoside]{report}

%%% PACKAGES %%%
\usepackage[svgnames]{xcolor}
\usepackage{amsmath,amssymb,amsthm,booktabs,csquotes,microtype,graphicx,tikz}
\usepackage{fontspec}
\usepackage[font=small]{caption}
\usepackage{mathspec}
\usepackage{colortbl}
\usepackage{diagbox}
\usepackage[sf]{titlesec}
\usepackage{geometry}
\usepackage[inline]{enumitem}
\usepackage[draft=false,hidelinks]{hyperref}
\usepackage[round]{natbib}
\usepackage{multirow}
\usepackage{cleveref}

\titleformat*{\paragraph}{\bfseries}

\setcounter{secnumdepth}{3}

\graphicspath{{figures/}}

\makeatletter
\renewcommand\@pnumwidth{4ex}
\makeatother

\setkeys{Gin}{draft=false}

\setmainfont[Ligatures = TeX]{Noto Serif}
\setmathsfont(Digits,Latin,Greek)[Ligatures = TeX]{Noto Serif}
\setmathrm[Ligatures = TeX]{Noto Serif}
\setmathsf[Ligatures = TeX]{Noto Sans}
\setsansfont[Ligatures = TeX]{Technika}

\newfontfamily{\logofont}[ItalicFont=*,BoldFont=*,BoldItalicFont=*]{LogoCVUT-Regular}

\usetikzlibrary{arrows,positioning,cd} 


%%% MACROS %%%
\newcommand*{\A}{\mathcal{A}} 
\newcommand*{\B}{\mathcal{B}} 
\newcommand*{\X}{\mathcal{X}} 
\newcommand*{\nn}{\mathbb{N}} 
\newcommand*{\zz}{\mathbb{Z}} 
\newcommand*{\qq}{\mathbb{Q}} 
\newcommand*{\rr}{\mathbb{R}} 
\newcommand*{\RR}{\rr} 
\newcommand*{\CC}{\cc} 
\newcommand*{\NN}{\nn} 
\newcommand*{\cc}{\mathbb{C}} 
\renewcommand*{\complement}{\mathbf{c}}
\renewcommand*{\emptyset}{\varnothing}
\newcommand{\defeq}{\stackrel{\text{def}}{=}}

\newcommand*{\bernoulli}{\mathop{\mathrm{Ber}\kern0pt}\nolimits}
\newcommand*{\binomial}{\mathop{\mathrm{Bin}\kern0pt}\nolimits}
\newcommand*{\multinomial}{\mathop{\mathrm{Mul}\kern0pt}\nolimits}
\newcommand*{\geometric}{\mathop{\mathrm{Geo}\kern0pt}\nolimits}
\newcommand*{\poisson}{\mathop{\mathrm{Poi}\kern0pt}\nolimits}

\newcommand*{\uniform}{\mathop{\mathrm{U}\kern0pt}\nolimits}
\newcommand*{\duniform}{\mathop{\mathrm{DU}\kern0pt}\nolimits}
\newcommand*{\normal}{\mathop{\mathrm{N}\kern0pt}\nolimits}
\newcommand*{\fisher}{\mathop{\mathrm{F}\kern0pt}\nolimits}
\newcommand*{\lognormal}{\mathop{\mathrm{LN}\kern0pt}\nolimits}
\newcommand*{\exponential}{\mathop{\mathrm{Exp}\kern0pt}\nolimits}

\newcommand*{\stL}{\stackrel{L}{\to}}
\newcommand*{\stP}{\overset{P}{\to}}
\newcommand*{\stas}{\stackrel{a.s.}{\to}}
\newcommand*{\stUL}{\stackrel{UL}{\to}}
\newcommand*{\wh}{\widehat}
\newcommand*{\wt}{\widetilde}
%
\newcommand*{\isom}{\cong} 
\newcommand*{\from}{\colon} 
\newcommand*{\id}{\mathrm{id}}  
\newcommand*{\ve}{\varepsilon}
\newcommand{\qmq}[1]{\quad\mbox{#1}\quad}
\newcommand{\bm}[1]{\ensuremath{\mathbf{#1}}}

\newcommand{\ol}{\overline}

%%% OPERATORS %%%
\newcommand*{\argmax}{\operatorname{argmax}}
\newcommand*{\E}{\mathop{\mathsf{E}{\kern0pt}}}
\newcommand*{\Var}{\mathop{\mathsf{Var}{\kern0pt}}}
\newcommand*{\Cov}{\mathop{\mathsf{Cov}{\kern0pt}}}
\newcommand*{\sd}{\mathop{\sigma{\kern0pt}}}
\renewcommand*{\P}{\mathop{\mathsf{P}\kern0pt}\nolimits}

%%% THEOREMS %%%
\theoremstyle{plain}
\newtheorem{theorem}{Theorem}[chapter]
\newtheorem{proposition}[theorem]{Proposition}
\newtheorem{claim}[theorem]{Claim}
\newtheorem{property}[theorem]{Property}
\newtheorem{properties}[theorem]{Properties}
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{corollary}[theorem]{Corollary}

\theoremstyle{definition}
\newtheorem{definition}[theorem]{Definition}
\newtheorem{remark}[theorem]{Remark}
\newtheorem{example}[theorem]{Example}
\newtheorem{exercise}[theorem]{Exercise}

\newenvironment{defin}{\begin{definition}}{\end{definition}}
\newenvironment{cor}{\begin{corollary}}{\end{corollary}}

\newcommand{\TODO}{\text{\upshape\color{Red}TODO}}

\frenchspacing
\flushbottom

% \includeonly{introduction,math_model,conditional,random_var,discrete_var,continuous_var,attributes_var,limit_theorems,statistics}

%%% BODY %%%
\begin{document}

\author{Herman Goulet-Ouellet}
\title{Probability and \\ Mathematical Statistics}
\date{\today}

\makeatletter
\begin{titlepage}
    \sffamily\centering

    \begin{Large}
        \noindent Lecture notes\par
    \end{Large}
    \vspace{3em}

    \begin{Huge}
        \noindent\MakeUppercase{\@title}\par
    \end{Huge}

    \vfill

    \begin{LARGE}
        \noindent\@date\par
    \end{LARGE}

    \vfill

    \includegraphics[height=3em]{loga_nuclear_sciences_and_physical_engineering_cb}
\end{titlepage}

\setcounter{page}{2}

\newpage
\thispagestyle{empty}

\mbox{}
\vspace{12em}
\small

\begin{center}
    {\logofont\Huge C}

    \bigskip

    Lecture notes prepared by Herman Goulet-Ouellet for a course taught during the winter semester of 2024 at the Faculty of Nuclear Sciences and Physical Engineering of the Czech Technical University in Prague.

    \bigskip

    Based on a translation of the 2024 edition of Tomáš Hobza's lecture notes (in Czech). The translation was done with the help of two neural machine translation softwares, LibreTranslate and DeepL Translator. LibreTranslate is a free and open source software.

    \bigskip

    Typeset in Noto Serif and Technika using XeTeX. 
\end{center}

\newpage

\makeatother

\tableofcontents

\include{introduction}
\include{math_model}
\include{conditional}
\include{random_var}
\include{discrete_var}
\include{continuous_var}
\include{attributes_var}
\include{limit_theorems}
\include{statistics}
\include{hypothesis}

\bibliographystyle{plainnat}
\bibliography{/home/herman/Documents/uni/ref/biblio.bib}

\end{document}
