
\chapter{Random variables}

Throughout this chapter, $(\Omega,\mathfrak A,\P)$ will denote a probability space. 

A random variable is, simply put, a function which assigns numerical values to the outcomes of a random experiment \emph{in a measurable way}. This can be used to perform mathematical operations on outcomes. More precisely, random variables are defined as follows.

\begin{definition}
    A random variable is a function $X \from \Omega \to \rr$ which satisfies for all $x\in\rr$
    \begin{equation*}
        X^{-1}(-\infty, x] = \{\omega\in\Omega \mid X(\omega) \leq x\} \in \mathfrak{A}.
    \end{equation*}
\end{definition}

In other words, we ask for the preimages of all intervals $(-\infty, x]$ to be measurable sets.

\begin{example}
    Let us consider the experiment which consists of throwing two fair dices. We can model the set of all possible outcomes by
    \begin{equation*}
        \Omega = \{(i,j) \mid 1\leq i\leq j\leq 6\}.
    \end{equation*}
    For our $\sigma$-algebra $\A$ let us take the set of all subsets of $\Omega$. Let $X$ be the random variable which expresses the sum of the results, $X(i, j) = i + j$.

    Then for example
    \begin{align*}
        X^{-1} ((-\infty, 1.2]) &= \{\omega \in \Omega \mid X(\omega) \leq 1.2\} = \varnothing,\\ 
        X^{-1} ((-\infty, 3.1]) &= \{\omega\in\Omega \mid X(\omega) \leq 3.1\} = \{(1,1), (1,2)\},\\
        X^{-1}((-\infty,4]) &= \{\omega\in\Omega \mid X(\omega) \leq 4\} = \{ (1,1), (1,2), (2,2), (1,3)\}.
    \end{align*}
\end{example}

We will use from now on the following notation:
\begin{align*}
    &[X\leq x] \defeq \{\omega\in\Omega \mid X(\omega)\leq x\},
    &[X< x]    \defeq \{\omega\in\Omega \mid X(\omega)< x\},   \\
    &[X\geq x] \defeq \{\omega\in\Omega \mid X(\omega)\geq x\},
    &[X> x]    \defeq \{\omega\in\Omega \mid X(\omega)> x\},   \\
    &[X= x]    \defeq \{\omega\in\Omega \mid X(\omega)= x\}, 
    &[X\in S]  \defeq \{\omega\in\Omega \mid X(\omega)\in S\}. 
\end{align*}
where $x\in \rr$ and $S\subseteq \rr$. 

It follows from the axioms of $\sigma$-algebras that if $X$ is a random variable, then the sets
\begin{equation*}
    [X\leq x], [X< x], [X\geq x], [X> x], [X= x]
\end{equation*}
are events (i.e. part of the $\sigma$-algebra $\mathfrak A$). Therefore, they have well-defined probabilities. For an arbitrary subset $S\subseteq\rr$, we first need to make sure that $[X\in S]\in\mathfrak A$. 

\begin{properties}
    Let $X,Y$ be two random variables. Then
    \begin{enumerate}
        \item $X + Y$ is also a random variable.
        \item $X\cdot Y$ is a random variable.
        \item If $[Y = 0] = \varnothing$, then $X/Y$ is a random variable.
        \item $kX$ is a random variable for all $k\in\rr$.
        \item $g(X)$ is a random variable for every continuous function $g\from \rr\to\rr$.
    \end{enumerate}
\end{properties}

\section{Distribution functions}

A convenient way to describe random variables is with distribution functions.
\begin{definition}
    Let $X$ be a random variable. The function $F_X\from \rr \to \rr$ defined by
    \begin{equation*}
        F_X (x) = \P[X \leq x]
    \end{equation*}
    is called the \emph{distribution function} of $X$.
\end{definition}

\begin{example}
    Consider three consecutive coin flips. The set of all possible outcomes is
    \begin{align*}
        \Omega = \{ HHH, HHT, HTH, THH, HTT, THT, TTH, TTT \}
    \end{align*}
    where $H$ stands for head and $T$ for tail. Assume that $\P$ is the uniform probability measure on $\Omega$. Let $X$ be the random variable counting the number of heads. Thus, for example, $X(HHH) = 3, X(HTT) = 1,$ and so on. The distribution function $F_X$ is given by:
    \begin{itemize}
        \item for $x < 0$         : $[X\leq x]=\varnothing$             and $F_X(x)=0$; 
        \item for $0 \leq x < 1$  : $[X\leq x]=\{TTT\}$                 and $F_X(x)=1/8$; 
        \item for $1 \leq x < 2$  : $[X\leq x]=\{TTT, TTH, THT, HTT\}$  and $F_X(x)=1/2$;
        \item for $2 \leq x < 3$  : $[X\leq x]=\Omega\setminus\{HHH\}$  and $F_X(x)=7/8$;
        \item for $3\leq x$       : $[X\leq x]=\Omega$                  and $F_X(x)=1$.
    \end{itemize}
    It is depicted in \cref{fig:distrib}.
    \begin{figure}
        \centering
        \includegraphics[width=.5\linewidth]{example4.2.png}
        \caption{A distribution function}
        \label{fig:distrib}
    \end{figure}
\end{example}

\begin{example}
    Consider the random variable $X$ that describes the position of the big hand of the clock, represented by the angle by it forms with the 12 (clockwise). The probability space used for describing this situation will be
    \begin{equation*}
        \Omega = [0,2\pi), \quad \mathfrak A = \B([0,2\pi)),\quad \P(A) = \frac{1}{2\pi}\int_0^{2\pi}I_A(t)\,dt,
    \end{equation*}
    where $I_A(t)$ is the characteristic function of the set $A$, i.e.
    \begin{equation*}
        I_A(t)=
        \begin{cases}
            1 & t\in A\\
            0 & t\notin A.
        \end{cases}
    \end{equation*}
    In fact $\P$ is the Lebesgue measure on $\mathcal B([0,2\pi))$ scaled by a factor $1/2\pi$.

    The distribution function of the random variable $X$ takes the form:
    \begin{equation*}
        F_X(x) = \P[X\leq x] = \frac1{2\pi}\int_0^{2\pi}I_{[0,x]}(t)\,dt = 
        \begin{cases}
            0 & x<0,\\
            x/2\pi & 0\leq x\leq 2\pi,\\
            1 & x\geq 2\pi.
        \end{cases}
    \end{equation*}
\end{example}

\begin{properties}
    Let $X$ be a random variable and $F_X$ be its distribution function.
    \begin{enumerate}
        \item $0 \leq F_X(x) \leq 1$ for all $x \in \rr$.
        \item If $x_1 < x_2$, then $F_X(x_1) \leq F_X(x_2)$ (non-decreasing).
        \item $\lim_{x\to\infty} F_X (x) = 1$.
        \item $\lim_{x\to -\infty} F_X (x) = 0$.
        \item For all $a\in \rr$, $\lim_{x\to a^+}F_X(x) = F_X(a)$ (right continuous).
        \item $F_X$ has at most countably many points of discontinuity.
    \end{enumerate}
\end{properties}

\begin{proof}
    1. Since $0 \leq \P[X \leq x] \leq 1$ holds for all $x\in\rr$, the result follows from the definition of distribution functions.

    2. We can write the event $[X \leq x_2]$ as a disjoint union
    \begin{equation*}
        [X \leq x_2] = [X \leq x_1] + [x_1 < X \leq x_2]
    \end{equation*}
    and thus
    \begin{equation*}
        F_X(x_2) = \P[X\leq x_2] = \P[X\leq x_1] + \P[x_1<X\leq x_2] \geq \P[X\leq x_1 ] = F_X(x_1).
    \end{equation*}
    
    3. According to the definition of limits, we have to prove
    \begin{equation*}
        (\forall \ve >0)(\exists x_\ve\in\RR)(\forall x >x_\ve)(F_X(x)>1-\ve).
    \end{equation*}
    Let us fix $\ve>0$. Since $X$ is a random variable and $X(\omega)$ is finite for all $\omega\in\Omega$, we can decompose $\Omega$ into
    \begin{equation*}
        \Omega = [X\leq 0] + \sum_{k=1}^\infty [k-1<X\leq k].
    \end{equation*}
    After applying the probability measure to both sides of this equation, we get
    \begin{equation*}
        1= \P[X\leq 0] + \sum_{k=1}^\infty \P[k-1<X\leq k].
    \end{equation*}
    But this means that the series of nonnegative terms on the right-hand side is convergent, and so there exists $N_\ve\in\NN$ such that for all $n>N_\ve$,
    \begin{equation*}
        \P[X\leq 0] + \sum_{k=1}^n \P[k-1<X\leq k]>1-\ve.
    \end{equation*}
    In other words, for all $n>N_\ve$,
    \begin{equation*}
        \P[X\leq n]>1-\ve.
    \end{equation*}
    Consider $x_\ve = N_\ve+1$. For this value, $\P[X\leq x_\ve]>1-\ve$, and since $F_X$ is non-decreasing we get for all $x>x_\ve$
    \begin{equation*}
        F_X(x) = \P[X\leq x]\geq \P[X\leq x_\ve]>1-\ve.
    \end{equation*}

    4. In this case we have to prove that
    \begin{equation*}
        (\forall \ve >0)(\exists x_\ve\in\RR)(\forall x <x_\ve)(F_X(x)<\ve).
    \end{equation*}
    The proof is done similarly to the previous point. Let us fix $\ve>0$ and decompose $\Omega$ into
    \begin{equation*}
        \Omega = [X>0] + \sum_{k=0}^\infty [-(k+1)<X\leq -k] .
    \end{equation*}
    Applying $\P$ to both sides of this equation, we get
    \begin{equation*}
        1= \P[X> 0] + \sum_{k=0}^\infty \P[-(k+1)<X\leq -k].
    \end{equation*}
    But this means that there are $N_\ve\in\NN$ such that for all $n>N_\ve$,
    \begin{equation*}
        \P[X>0] + \sum_{k=0}^n \P[-(k+1)<X\leq -k]>1-\ve ,
    \end{equation*}
    which is equivalent to the expression
    \begin{equation*}
        \P[X\leq -(n+1)]<\ve .
    \end{equation*}
    Let $x_\ve = -( N_\ve+1)$. For this value we have $\P[X\leq x_\ve]<\ve$. Using again the non-decreasing property of $F_X$ we get for all $x<x_\ve$
    \begin{equation*}
        F_X(x) = \P[X\leq x]\leq \P[X\leq x_\ve]<\ve.
    \end{equation*}

    5. To prove this statement, we decompose the event $[a<X\leq a+1]$ into the disjoint union
    \begin{equation*}
        [a<X\leq a+1] = \sum_{n=1}^\infty \left[a+\frac{1}{n+1}<X\leq a+\frac{1}{n}\right].
    \end{equation*}
    Then we get
    \begin{equation*}
        \P[a<X\leq a+1] = \sum_{n=1}^N \P \left[a+\frac{1}{n+1}<X\leq a+\frac{1}{n}\right]+Z_N ,
    \end{equation*}
    where $Z_N$ converges to 0 for $N\to\infty$. The last expression can be rewritten in the form
    \begin{equation*}
        F_X(a+1)-F_X(a) = \P[a+\frac{1}{N+1}<X\leq a+1]+Z_N = F_X(a+1) - F_X\left(a+\frac{1}{N+1}\right),
    \end{equation*}
    which implies that
    \begin{equation*}
        F_X\left(a+\frac{1}{N+1}\right)=F_X(a) +Z_N \qmq{and therefore} \lim_{N\to\infty} F_X\left(a+\frac{1}{N+1}\right)=F_X(a).
    \end{equation*}
    Using the fact that $F_X$ is non-decreasing we obtain the same limit for $F_X(x)$ as $x\to a+$.

    6. Let $S_n$ denote the set of points where the function $F_X(x)$ has a discontinuity of size greater than or equal to $1/n$. More precisely,
    \begin{equation*}
        S_n = \{ a\in\rr \mid \lim_{x\to a^-}F_X(x)\leq F_X(a)-1/n\}.
    \end{equation*}
    The set of all discontinuity points $S$ of the function $F_X$ can then be expressed as the union $S=\bigcup_{n=1}^\infty S_n$. We claim that each $S_n$ is finite. Indeed, if $a<a'\in S_n$, then $F_X(a)-F_X(a')\geq 1/n$ because $F_X$ is non-decreasing. If $S_n$ would be infinite then $F_X$ would be arbitrarily large, contradicting the fact that $F_X(x)\to 1$ as $x\to \infty$. This means that $S$ is a countable union of finite sets, and is therefore countable.
\end{proof}

\begin{remark}
    If a real function satisfies the properties 1, \ldots, 5 above, then it is the distribution function of some random variable.
\end{remark}

\begin{remark}
    Sometimes the distribution function is instead defined as
    \begin{equation*}
        F_X(x) = \P[X<x].
    \end{equation*}
    In that case the distribution function is continuous from the left, rather from the right, and all of the other properties above remain valid.
\end{remark}

\begin{example}
    A circuit contains 5 identical transistors, one of which is defective. In order to solve the problem, all five transistors are removed and tested one by one. Determine the distribution function of the random variable $X$ indicating the number of tests required to find the defective transistor.

    The random variable $X$ can take values in the set $\{1,2,3,4,5\}$. Since we assume that each of the transistors are identical, $\P[X=i] = \frac15$ for each $i=1,\ldots, 5$. The distribution function is given by 
    \begin{align*}
        F_X(x) &= 0                                     & \text{for } x<1, \\
        F_X(x) &=\P[X=1]=\frac{1}{5}                    & \text{for } 1\leq x<2, \\
        F_X(x) &=\P[X=1]+ \P[X=2]=\frac{2}{5}           & \text{for } 2\leq x<3, \\
        F_X(x) &=\P[X=1]+\P[X=2]+\P[X=3] =\frac{3}{5}   & \text{for } 3\leq x<4, \\
        F_X(x) &=\P[X=1]+\ldots+\P[X=4]=\frac{4}{5}     & \text{for } 4\leq x<5, \\
        F_X(x) &=\P[X=1]+ \ldots+\P[X=5]=1              & \text{for } x\geq 5.
    \end{align*}
\end{example}

\begin{example}
    Consider the random variable $X$ given by the distribution function
    \begin{equation*}
        F_X(x) =
        \begin{cases}
            0 & \text{for } x\leq 0\\
            1-e^{-2x} & \text{for } x>0.
        \end{cases}
    \end{equation*}
    Find $\P[1<X\leq 2], \P[-1<X\leq 2]$ and $\P[X=1]$. 

    This is done with the following computations:
    \begin{gather*}
        \P[1<X\leq 2] = \P[X\leq 2] - \P[X\leq 1]= F_X(2)-F_X(1) = 1-e^{-4}-1+e^{-2}=e^{-2}-e^{-4},\\
        \P[-1<X\leq 2] = F_X(2) - F_X(-1) = 1-e^{-4}-0 = 1-e^{-4},\\
        \P[X=1] = \P[X\leq 1]- \P[X<1] = F_X(1)-\lim_{t\to 1^-} F_X(t) = F_X(1)-F_X(1) =0.
    \end{gather*}
\end{example}

The last part of the example hints that the probability at a specific point will be non-zero precisely that point is a point of discontinuity for the distribution function.

\begin{theorem}\label{t:almost-surely}
    Let $X$ and $Y$ be two random variables. If $\P[X=Y]=1$ then $\P[X\in A] = \P[Y\in A]$ for every $A\in\B(\rr)$;
    in particular, $F_X=F_Y$.
\end{theorem}

\begin{proof}
    Since $\P[X\neq Y]=0$,
    \begin{equation*}
        \P[X\in A] = \P[X\in A,X=Y] + \P[X\in A,X\neq Y] \leq \P[Y\in A] + \P[X\neq Y] = \P[Y\in A]
    \end{equation*}
    and by the same token $\P[Y\in A]\leq \P[X\in A]$.
\end{proof}

In that case we say that $X=Y$ \emph{almost surely}.

\section{Joint distribution functions}

Many experiments involve more than one variables (for instance temperature, pressure and humidity). While each of these variables may be examined separately, often much can be gained by studying their relationship (the temperature-pressure-humidity is a good example). This motivates the notion of \emph{joint distribution functions}, also called \emph{multivariate distribution functions}.

A vector $Z=(X_1,X_2,\ldots, X_n)$ composed of random variables will be called a \emph{random vector}. For simplicity, we will usually work with random vectors that have two components.

\begin{defin}
    Let $X, Y$ be two random variables. Then their \emph{joint distribution function} is defined by
    \begin{equation*}
        F_{X,Y}(x,y) = \P([X\leq x][Y\leq y]).
    \end{equation*}
\end{defin}

We usually write $[X\leq x,Y\leq y]$ instead of $[X\leq x][Y\leq y]$.

Joint distribution functions are closely related to their univariate counterparts, and often have similar properties. 

\begin{properties}
    Let $F_{X,Y}$ be the joint distribution function of a random vector $(X,Y)$. \begin{enumerate}
        \item For all $x_1\leq x_2$ and $y_1\leq y_2$
        \begin{equation*}
            F_{X,Y}(x_1,y_1)\leq F_{X,Y}(x_2,y_2).
        \end{equation*}
        \item For all $x, y\in\rr$,
        \begin{equation*}
            \lim_{x\to-\infty} F_{X,Y}(x,y)=0 \qmq{and} \lim_{y\to-\infty} F_{X,Y}(x,y)=0.
        \end{equation*}
        \item For all $x,y\in\rr$,
        \begin{equation*}
            \lim_{x\to\infty}F_{X,Y}(x,y)=F_Y(y) \qmq{and} \lim_{y\to\infty} F_{X,Y}(x,y)=F_X(x).
        \end{equation*}
    \end{enumerate}
\end{properties}

\begin{remark}
    In the context of an experiment with several variables, the univariate distribution functions are called the \emph{marginal distribution} functions. More generally, if $Z'=(X_1,X_2,\ldots, X_n)$ is a random vector, then for a choice of $k$ indexes $1\leq i_1<i_2<\ldots<i_k\leq n$ where $k<n$, the vector $Z'=(X_{i_1},X_{i_2},\ldots, X_{i_k})$ may be called a marginal random vector, and its distribution function $F_{Z'}$ a marginal distribution function for $Z$.
\end{remark}

\begin{defin}
    Two random variables $X$ and $Y$ are called (statistically) independent if the events
    \begin{equation*}
        [a<X\leq b] \qmq{and} [c<Y\leq d]
    \end{equation*}
    are independent for all pairs $(a,b), (c,d)$ where $a,b,c,d\in\RR\cup\{\pm\infty\}$ and $a\leq b, c\leq d$.
\end{defin}

\begin{theorem}\label{nez}
    Two random variables $X,Y$ are independent if and only if for each $(x,y)\in\RR^2$,
    \begin{equation*}
        F_{X,Y}(x,y) = F_X(x)\cdot F_Y(y).
    \end{equation*}
\end{theorem}

In other words the joint distribution function is equal to the product of the the marginal distribution functions.

\begin{proof}
    ($\Rightarrow$) Assume that $X,Y$ are independent, which means that for all $a,b,c,d\in\RR\cup\{\pm\infty\}$ with $a\leq b, c\leq d$,
    \begin{equation*}
        \P([a<X\leq b][c<Y\leq d]) = \P[a<X\leq b]\cdot \P[c<Y\leq d].
    \end{equation*}
    Take $a=-\infty, b=x, c=-\infty, d=y$. Then for all $(x,y)\in\RR^2$,
    \begin{align*}
    F_{X,Y}(x,y) &= \P([-\infty<X\leq x][-\infty<Y\leq y]) \\
                &= \P[-\infty<X\leq x]\cdot \P[-\infty<Y\leq y]=F_X(x)\cdot F_Y(y).
    \end{align*}

    ($\Leftarrow$) Assume that $F_{X,Y}(x,y) = F_X(x)F_Y(y)$. 
    \begin{align*}
        \P([a<X\leq b][c<Y\leq d]) &= F_{X,Y}(b,d)-F_{X,Y}(b,c)-F_{X,Y}(a,d)+F_{X,Y}(a,c)\\
        &= F_X(b)F_Y(d) - F_X(b)F_Y(c)-F_X(a)F_Y(d)+F_X(a)F_Y(c)\\
        &= (F_X(b)-F_X(a))(F_Y(d)-F_Y(c)) = \P[a<X\leq b] \P[c<Y\leq d].\qedhere
    \end{align*}
\end{proof}

\begin{example}
    Consider the random vector $(X,Y)$ given by the probabilities $\P[X=x,Y=y]$ given in the table below.
    \begin{center}
        \begin{tabular}{c|ccc}
            \toprule
            \diagbox[]{$y$}{$x$} & 2 & 5 & 8 \\ \hline\rule{0pt}{2.5ex}%
                0.4 & 0.15 & 0.30 & 0.35 \\
                0.8 & 0.05 & 0.12 & 0.03\\
                \bottomrule
        \end{tabular}
    \end{center}

    Compute the joint and marginal distribution functions and determine whether the random variables $X,Y$ are independent.

    The joint distribution function is depicted in Figure \ref{cdf1}.

    \begin{figure}[h]
    \centering
    \begin{tikzpicture}[yscale=3,xscale=.5]
        \node[anchor=east] at (0,0.4) {0.4} ;
        \node[anchor=east] at (0,0.8) {0.8} ;
        \node[anchor=north] at (2,0) {2} ;
        \node[anchor=north] at (5,0) {5} ;
        \node[anchor=north] at (8,0) {8} ;
        \node at (1,.2) {0} ;
        \node at (1,.6) {0} ;
        \node at (1,1) {0} ;
        \node at (3.5,.2) {0} ;
        \node at (6.5,.2) {0} ;
        \node at (9.5,.2) {0} ;
        %
        \node at (3.5,.6) {0.15} ;
        \node at (6.5,.6) {0.45} ;
        \node at (9.5,.6) {0.8} ;
        \node at (3.5,1) {0.2} ;
        \node at (6.5,1) {0.62} ;
        \node at (9.5,1) {1} ;
        %
        \draw[->] (0,0) to (0,1.2) node [above left] {$y$};
        \draw[->] (0,0) to (11,0) node [below right] {$x$};
        \draw[-,dashed] (2,0) to (2,1.2) ;
        \draw[-,dashed] (5,0) to (5,1.2) ;
        \draw[-,dashed] (8,0) to (8,1.2) ;
        \draw[-,dashed] (0,0.4) to (11,.4) ;
        \draw[-,dashed] (0,0.8) to (11,.8) ;
    \end{tikzpicture}
    \caption{Values of distribution function $F_{X,Y}(x,y)$.} \label{cdf1}
    \end{figure}

    The marginal distribution functions are
    \begin{equation*}
        F_X(x) = \lim_{y\to\infty}F_{X,Y}(x,y) =
        \begin{cases}
            0 & \text{for } x<2, \\
            0.2 & \text{for } 2\leq x<5,\\
            0.62 & \text{for } 5\leq x <8,\\
            1 & \text{for } x \geq 8,
        \end{cases}
    \end{equation*}
    and
    \begin{equation*}
        F_Y(y) = \lim_{x\to\infty}F_{X,Y}(x,y) =
        \begin{cases}
            0 & \text{for } y<0.4, \\
            0.8 & \text{for } 0.4\leq y<0.8,\\
            1 & \text{for } y \geq 0.8.
        \end{cases}
    \end{equation*}

    The variables are not independent since, for instance, 
    \begin{equation*}
        F_{X,Y}(3,0.5) = 0.15 \neq 0.16 = 0.2\cdot 0.8 = F_X(3)\cdot F_Y(0.5).
    \end{equation*}
    \end{example}

\section{Exercises}

\begin{enumerate}[wide,label={\textbf{\arabic{*}.}},itemsep=1em]

\item Let $X$ be the random variable denoting  the largest number of \emph{consecutive} heads in three coin flips.
\begin{enumerate}[label={\alph{*})}]
    \item Determine the distribution function of this random variable if the coin is fair.
    \item Do the same under assumption that the probability of a head is given by some parameter $0\leq p\leq 1$.
\end{enumerate}

\item In the experiment which consists in rolling a fair dice until the result is a 6, let $X$ be the number of dice rolls needed.
\begin{enumerate}[label={\alph{*})}]
    \item Describe the distribution function $F_X$.
    \item Find $\P[X > 3]$.
    \item Find $\P[X > 7 \mid X > 3]$.
\end{enumerate}

\item Two basketball players take turns shooting at the basket until one of them makes scores. The first player scores with probability $p_1$ and the second with probability $p_2$. Determine the probability distribution of the number of shots made by each player. 

\item A point is randomly selected from a sphere of radius $r$. Find the distribution function for its distance from the origin.

\item Let $X$ be a random variable with distribution function $F_X$. Prove that:
\begin{enumerate}[label={\alph{*})}]
    \item $\P[X > x] = 1 - F_X (x)$,
    \item $\P[x < X \leq y] = F_X (y) - F_X (x)$.
\end{enumerate}

\item Prove that $\lim_{x\to a^-}F_X(x) = \P[x<a]$. Deduce that $a$ is a point of discontinuity of $F_X$ if and only if $\P[X=a]\neq 0$.

\item Let $X$ be a random variable with distribution function $F_X$ and $Y = aX + b$ where $a \neq 0$. Express $F_Y$ in terms of $F_X$. Pay close attention to what happens when $a<0$.

\item Two players place bets on the result of a dice roll by choosing three numbers (out of six). A winning bet earns the player 100 CZK, while a loosing bet costs 100 CZK. The first player chooses to bet on even numbers (2, 4, 6) while the second one bets on large numbers (4, 5, 6). Let $X$ be the random variable which gives the outcome for the first player, and $Y$ the outcome for the second (i.e., the amount gained or lost).  
\begin{enumerate}[label={\alph{*})}]
    \item Determine the joint distribution $F_{X,Y}$.
    \item Determine the marginal distributions $F_X$ and $F_Y$.
    \item Are $X$ and $Y$ independent?
    \item Find the distribution of $X$ given $Y = 100$ and given $Y = -100$.
\end{enumerate}

\end{enumerate}
