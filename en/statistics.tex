\chapter{First steps in statistics}

\section{Introduction}

The goal of mathematical statistics is to find a good model for a real world situation and, based on experimental data, estimate the values of unknown parameters in these models, test hypotheses about the parameters, and verify the \emph{goodness of fit} of the model. Typically it involves the following steps.

\begin{enumerate}
    \item Estimate the shape of the distribution (choice of model), based on intuition or experience. 
    \item Estimate the parameters.
    \begin{enumerate}[label={\alph{*})}]
        \item Point estimate: consider a model $X\sim F_X(x,\theta)$ with a free parameter $\theta$. The problem is to find a function $\wh\theta(\bm X)$ of the observation vector $\bm X$ which estimates the most likely value of $\theta$.
        \item Interval estimate: with the same model, we look instead for an interval where the value of the parameter $\theta$ lies with some probability. That is, we are looking for functions $\underline\theta(\bm X),\overline\theta(\bm X)$ such that $\P[\underline\theta(\bm{X}) \leq \theta\leq \overline\theta(\bm X)]\geq 1-\alpha$, where $\alpha\in(0,1)$ is represents the desired \emph{level of confidence}.
    \end{enumerate}
    \item Validate the model.
    \begin{enumerate}[label={\alph{*})}]
        \item Hypothesis tests: consider a parametric model and some hypothesis about the parameter $\theta$. Based on the data, decide whether or not to reject the hypothesis. For example, the hypothesis could be $H_0\from\theta=5$, which means \enquote{the correct value of the parameter is $\theta=5$.} From the data, we examine whether $\P[H_0 \mbox{ holds}]\geq 1-\alpha$. Depending on the result, we accept or reject the hypothesis.
        \item Goodness-of-fit tests: in this case, we test the shape of the distribution. For example, does the data follows a Poisson distribution?
    \end{enumerate}
\end{enumerate}

Hypothesis and goodness-of-fit testing will be covered in the next chapter.

\begin{defin}
    A tuple of independent and identically distributed random variables $(X_i)_{i=1}^n$ is called a \emph{random sample}.
\end{defin}

For example, we could perform a physical measurement or experiment $n$ times, independently and with the same methodology.

\begin{remark}
    Suppose we are given a single random variable $X\from\Omega\to\rr$ defined on some probability space $(\Omega,\mathfrak A,\P)$. How does one describe, mathematically, what it makes to \enquote{repeat the experiment} $n$ times?

    The answer is that we need to enlarge the probability space to create \emph{independent copies} of $X$. More precisely, we want a probability space
    \begin{equation*}
        (\Omega^n,\mathfrak A^{(n)}, \P^{(n)})
    \end{equation*}
    where 
    \begin{enumerate}
        \item $\mathfrak{A}^{(n)}$ contains all events of the form $A_1\times\dots\times A_n$ with $A_i\in\mathfrak A$, and
        \item $\P^{(n)}(A_1\times\dots\times A_n) = \prod_{i=1}^n\P(A_i)$.
    \end{enumerate}
    In fact this is always possible, and there is a unique way to do this which makes $\mathfrak A$ as small as possible. We can then define the random variables $X_i\from\Omega^n\to\rr$ by 
    \begin{equation*}
        X_i(\omega_1,\dots,\omega_n) = X(\omega_i).
    \end{equation*}
\end{remark}

In general, the statistical model we find will depend on the size of the sample (the number $n$). For a finite population, we could give an exact model by simply testing all elements, but this is often unpractical or even impossible (for instance in the case of destructive tests). In addition to sample size, another difficulty is to ensure independence (for example in opinion polls).

\begin{remark}
    Once the experiment is complete, the random sample gives a vector with fixed numerical values (measurements). This is usually denoted using lowercase letters. For instance, $x_1,\ldots,x_n$ would denote a particular realization of the random variables $X_1,\ldots, X_n$.
\end{remark}

Let $X_1,\ldots,X_n$ be a random sample. The term \emph{statistic} refer to any function which can be computed directly from the sample. 

\begin{definition}
    We give the definition of some commonly used statistics.
    \begin{enumerate}
        \item The \emph{sample mean} 
        \begin{equation*}
            \overline X_n = \frac{1}{n} \sum_{i=1}^n X_i.
        \end{equation*}
        This is the empirical counterpart of the expectation.  

        \item The \emph{sample variance}
        \begin{equation*}
            s_n^2 = \frac{1}{n-1} \sum_{i=1}^n( X_i-\overline X_n)^2.
        \end{equation*}
        It is the counterpart of the variance. The quantity $\sqrt{s_n^2}$ is the sample standard deviation.

        \item The \emph{biased sample variance}
        \begin{equation*}
            \sigma_n^2 = \frac{1}{n} \sum_{i=1}^n( X_i-\overline X_n)^2.
        \end{equation*}
        It is alternative form of the sample variance with less desirable properties.

        \item The $r$-th \emph{sample moment}
        \begin{equation*}
            m_r= \frac{1}{n} \sum_{i=1}^n X_i^r.
        \end{equation*}
    \end{enumerate}
\end{definition}

\section{Parameter estimates}

\subsection{Point estimates}

In this section we consider a random sample with distribution belonging to a parametric family $\{F(x,\theta)\mid\theta\in\Theta\}$, where $\Theta\subset\RR^d$ is the set of possible values for the parameter $\theta$. Our goal is to estimate the actual value of $\theta$ for a given random sample.

\begin{defin}
    A \emph{point estimator}, or simply \emph{estimator}, for $\theta$ is a sequence of random variables $(\wh\theta_n(X_1,\dots,X_n)\from\Omega\to\rr)_{n=1}^\infty$ which depend on the random sample.  

    The estimator $\wh\theta_n$ is called \emph{unbiased} if 
    \begin{equation*}
        \E[\wh\theta_n] = \theta \qmq{for all} \theta\in\Theta.
    \end{equation*}

    The estimator $\wh\theta_n$ is called \emph{consistent} if for $n\to\infty$
    \begin{equation*}
        \wh\theta_n \stP \theta \qmq{for all} \theta\in\Theta.
    \end{equation*}
\end{defin}

\begin{remark}
    The following conditions are \emph{sufficient} for the consistency of an estimator $\wh\theta_n$:
    \begin{equation*}
        \lim_{n\to\infty}\E[\wh\theta_n] = \theta \qmq{and} \lim_{n\to\infty}\Var\wh\theta_n = 0.
    \end{equation*}

    In particular an unbiased estimator whose variance converges to $0$ as $n\to\infty$ is consistent. However in general unbiased estimators can be inconsistent and consistent estimators can be biased.
\end{remark}

\begin{theorem}
    Let $X_1,\ldots,X_n$ be a random sample from a distribution $F(x,\mu,\sigma^2)$ where $\mu = \E[X_1]$ and $\sigma^2 = \Var(X_1)$. 
    \begin{enumerate}
        \item The sample mean $\overline X_n$ is an unbiased and consistent estimator of $\mu$. 
        \item The sample variance $s_n^2$ is an unbiased estimator of $\sigma^2$. If the 4th moment exists then it is also consistent.
    \end{enumerate}
\end{theorem}

\begin{proof}
    Let us start with the sample mean. By linearity of expectation:
    \begin{equation*}
        \E[\overline X_n] = \frac{1}{n} \sum_{i=1}^n \E[X_i] = \mu.
    \end{equation*}
    Therefore $\overline X_n$ is an unbiased estimator of $\mu$. The law of large numbers states that $\overline X_n\stP\mu$, which is exactly the statement that $\overline X_n$ is a consistent estimator of $\mu$.

    Next we move on to the sample variance.
    \begin{align*}
        \E[s_n^2] &= \frac{1}{n-1}\E\left[\sum_{i=1}^n(X_i-\overline X_n)^2\right]\\ 
                  &= \frac{1}{n-1}\E\left[\sum_{i=1}^n (X_i-\mu)^2-2(X_i-\mu) + (\overline X_n-\mu)^2\right]\\ 
                  &=\frac{1}{n-1}\left(\sum_{i=1}^n \E[(X_i-\mu)^2] - 2\E\left[ \sum_{i=1}^n (X_i-\mu)(\overline X_n - \mu)\right] +n\E[(\overline X_n-\mu)^2]\right)\\
                  &= \frac{1}{n-1} \left( n\E[(X_1-\mu)^2] -2n\E[(\overline X_n-\mu)^2] +n\E[(\overline X_n-\mu)^2]\right)\\
                &= \frac{1}{n-1} \left( n\sigma^2 -n\frac{\sigma^2}{n} \right) = \sigma^2,
    \end{align*}
    where we took advantage of the fact that $\Var(\overline X_n) = \sigma^2/n$. Therefore $s_n^2$ is an unbiased estimator for $\sigma^2$. 

    To prove consistency, we can show, under the assumption that the 4th moment exists,  that $\Var(s_n^2)\to 0$ as $n\to\infty$. We omit the details.
\end{proof}

\begin{remark}
    In contrast with the sample variance, the biased sample variance is (as the name indicates) biased. This is because
    \begin{equation*}
        \E[\sigma_n^2] = \frac{n}{n-1}\sigma^2.
    \end{equation*}
    Nonetheless it is consistent.

    If we already know the value of $\mu$ then
    \begin{equation*}
        \frac{1}{n} \sum_{i=1}^n (X_i -\mu)^2
    \end{equation*}
    is an unbiased and consistent estimator for $\sigma^2$, while
    \begin{equation*}
        \frac{1}{n-1} \sum_{i=1}^n (X_i -\mu)^2
    \end{equation*}
    is consistent but biased. 
\end{remark}

\begin{remark}
    There can be multiple unbiased estimates for the same parameter. For example, for a random sample from the normal distribution $\normal(\mu,\sigma^2)$, both the sample mean and the sample median are unbiased estimators of the parameter $\mu$. 
\end{remark}

\begin{defin}
    An estimator $\wh\theta_n$ for $\theta$ is the \emph{best unbiased estimator} if it is unbiased and for every other unbiased estimator $\wt\theta_n$,
    \begin{equation*}
        \Var(\wt\theta_n) \geq \Var(\wh\theta_n) \qmq{for all} \theta\in\Theta.
    \end{equation*}
\end{defin}

\begin{remark}
    For binomial, Poisson, exponential or normal distributions, the sample mean $\overline X_n$ is the best unbiased estimator of $\E[X]$. Moreover for a normal distribution the sampling variance $s_n^2$ is the best unbiased estimator of the variance.
\end{remark}

\subsection{Method of moments}

This method is simple and gives consistent estimators, but they are often not very good. It can be used to obtain first estimates which are then passed into more sophisticated methods (maximum likelihood estimation, iterative equation solving).

The idea of the method is as follows. Let $X_1,\ldots,X_n$ be random sample from a distribution $F(x,\theta_1,\ldots,\theta_k)$ and let $X\sim F(x,\theta_1,\ldots,\theta_k)$. First, express the $r$-th moment
\begin{equation*}
    \mu_r^\prime = \E[X^r].
\end{equation*}
as a function of the parameters $\theta_1,\ldots,\theta_k$. By the large of large numbers, the $r$-th sample moment
\begin{equation*}
    m_r=\frac{1}{n} \sum_{i=1}^{n} X_i^r
\end{equation*}
satisfies $m_r\stP \mu_r^\prime$. We thus use the approximation $\mu'_r\approx m_r$ to create a system of equations:
\begin{equation*}
    \mu_j^\prime = m_j \qmq{pro} 1\leq j\leq k.
\end{equation*}
Solutions to this system of equations provides an estimator for the parameters $\theta_1$, \dots, $\theta_k$.

\paragraph{Practical steps:}

\begin{enumerate}
    \item Express the moments in terms of the parameters:
    \begin{equation*}
        \mu_j^\prime = \mu_j^\prime(\theta_1,\ldots,\theta_k) = \E[X^j],\quad 1\leq j\leq k.
    \end{equation*}
    \item Calculate the sample moments $m_j, 1\leq j\leq k$.
    \item Solve the system of equations
    \begin{equation*}
        \mu_j^\prime(\theta_1,\ldots,\theta_k) = m_j, \quad 1\leq j\leq k.
    \end{equation*}
\end{enumerate}

\begin{example}
    Let $X_1,\ldots,X_n$ be a random sample from the distribution as a random variable $X$, where
    \begin{equation*}
        f_X(x,\theta_1,\theta_2) =
        \begin{cases}
            \frac{1}{\theta_2} & \text{for } \theta_1\leq x\leq \theta_1+\theta_2, \\
            0 & \text{otherwise}.
        \end{cases}
    \end{equation*}
    Estimate the parameters $\theta_1,\theta_2$ using the method of moments. First we find the first two moments of $X$,
    \begin{equation*}
        \mu_1'(X) = \int_{\theta_1}^{\theta_1+\theta_2} \frac{x}{\theta_2}\,dx = \theta_1+\frac{\theta_2}{2} \qmq{and}
        \mu_2'(X) = \int_{\theta_1}^{\theta_1+\theta_2} \frac{x^2}{\theta_2}dx = \theta_1^2 +\theta_1\theta_2 +\frac{\theta_2^2}{3}.
    \end{equation*}
    We obtain the system of equations
    \begin{align*}
        \theta_1 +\frac{\theta_2}{2} &= m_1 \\
        \theta_1^2 +\theta_1\theta_2 +\frac{\theta_2^2}{3} &= m_2.
    \end{align*}
    Therefore
    \begin{equation*}
        \theta_1 = m_1 -\frac{\theta_2}{2} \qmq{and}  \theta_2 = \sqrt{12(m_2-m_1^2)}.
    \end{equation*}
    We obtain the estimators
    \begin{equation*}
        \wh\theta_2 = \sqrt{\frac{12}{n}\sum_{i=1}^nX_i^2 - \overline X_n^2}\qmq{and} \wh\theta_1 = \overline X_n - \frac{\wh\theta_2}{2}.
    \end{equation*}
\end{example}

\begin{example}
    Find an estimator for the parameters of a binomial distribution $\binomial(n,p)$ based on a random sample $X_1,\ldots, X_n$.

    We know that $\E[X]=np$, $\Var X=np(1-p)$ and $\E[X^2] = np-np^2+n^2p^2$. This gives the system of equations
    \begin{align*}
        np &= m_1 \\
        np-np^2+n^2p^2 &= m_2.
    \end{align*}
    From the first equation, we get $n=m_1/p$ and substituting into the second equation $p=1+m_1-m_2/m_1$. The estimators given by the method of moments are of the form
    \begin{equation*}
        \wh p = 1+m_1-\frac{m_2}{m_1}\qmq{and} \wh n = \frac{m_1^2}{m_1+m_1^2-m_2}\, .
    \end{equation*}
\end{example}

\subsection{Maximum likelihood estimation}

\begin{example}
    Let $X_1,X_2,X_3,X_4$ be a random sample from a Bernoulli distribution with parameter $p$. We will try to give an estimate for $p$ given the observation data $0,0,1,0$.

    The joint probability of this outcome is a function of $p$
    \begin{equation*}
        L(p) = \P[X_1=0,X_2=0,X_3=1,X_4=0]=p(1-p)^3.
    \end{equation*}
    The idea behind maximum likelihood estimation is to use the value maximizing the probability as estimator.  

    Let us find the maximum of the function $L(p)$,
    \begin{equation*}
        \frac{dL}{dp}(p)= (1-p)^3 -3p(1-p)^2 = (1-p)^2(1-4p) =0.
    \end{equation*}
    Thus the critical points are $q_1=1, q_2=1/4$, with the maximum being at $q_2=1/4$. Thus we estimate $\wh p =1/4$.
\end{example}

We now generalize the procedure shown.
\begin{defin}
    The joint probability density of random sample $X_1,\ldots,X_n$ considered as a function of the parameter $\theta$ at a given realization $(X_1,\ldots,X_n)=(x_1,\ldots,x_n)$ is called the likelihood function. We denote it by $L(\theta)$. 

    For an absolutely continuous distribution
    \begin{equation*}
        L(\theta) = \prod_{i=1}^n f_{X_i}(x_i).
    \end{equation*}
    For a discrete distribution
    \begin{equation*}
        L(\theta) = \prod_{i=1}^n \P[X_i=x_i].
    \end{equation*}
\end{defin}

\begin{defin}
    Let $X_1,\ldots,X_n$ be a random sample from a distribution with distribution $F(x,\theta), \theta\in\Theta$. The maximum likelihood estimator (MLE) of the parameter $\theta$ is defined by
    \begin{equation*}
        \wh\theta = \argmax_{\theta\in\Theta} L(\theta).
    \end{equation*}
\end{defin}

\paragraph{Practical steps}
\begin{enumerate}
    \item Write down the likelihood function $L(\theta_1,\ldots,\theta_k)$.
    \item Maximize $L(\theta_1,\ldots,\theta_k)$ over the possible values of the parameters $\theta_1,\ldots,\theta_k$. 
    \item It is sometimes convenient to maximize instead the function $\ln L(\theta_1,\ldots,\theta_k)$.
    \item The MLE is obtained as the solution of a system of $k$-equations
    \begin{equation*}
        \frac{\partial \ln L(\theta_1,\ldots,\theta_k)}{\partial \theta_j}
        =0,\qquad j=1,\ldots,k
    \end{equation*}
  called credibility equations.
\end{enumerate}

\begin{remark}
    Here are some properties of maximum likelihood estimators.
    \begin{itemize}
        \item They can be computationally intensive if the system is solved numerically.
        \item They are consistent (though not always unbiased).
        \item They are robust under application of functions. This is usually called \emph{equivariance}.
        \item Under certain conditions, the asymptotic distribution of the MLE (as the sample size goes to $\infty$) can be determined.
    \end{itemize}
\end{remark}

\begin{example}
    A common type of biological experiment consists in counting microorganisms in the cells of a square grid dividing the field of view of a microscope. 

    The first three rows of the following table give the number $k$ of microorganisms in the array; the number $n_k$ of squares where $k$ microorganisms were observed; and the relative abundances $n_k/n$. The total number of squares is $n=118$.
    \begin{center}
        \begin{tabular}{*{11}c}
            \toprule
            $k$ & 0 & 1 & 2 & 3 & 4 & 5 & 6 & 7 \\ \midrule
            $n_k$ & 5 & 19 & 26 & 26 & 21 & 13 & 5 & 3 \\
            $n_k/n$ & 0.042 & 0.161 & 0.220 & 0.220 & 0.178 & 0.110 & 0.042 & 0.025\\
            \bottomrule
        \end{tabular}
    \end{center}
    We want to estimate the value of the parameter $\lambda$ of the Poisson distribution that would best model the situation.

    The likelihood function has the form
    \begin{equation*}
        L(\lambda) = \prod_{i=1}^n e^{-\lambda}\frac{\lambda^{x_i}}{x_i!}.
    \end{equation*}
    Passing to the logarithm:
    \begin{equation*}
        \ln L(\lambda) = \sum_{i=1}^n \ln\left( e^{-\lambda}\frac{\lambda^{x_i}}{x_i!}\right) =\sum_{i=1}^n \left( -\lambda+x_i\ln\lambda -\ln(x_i!)\right) = -n\lambda +\ln\lambda \sum_{i=1}^n x_i - \sum_{i=1}^n \ln(x_i!).
    \end{equation*}
    Taking the derivative with respect to $\lambda$ gives the equation
    \begin{equation*}
        \frac{\partial \ln L(\lambda)}{\partial \lambda}= -n
        +\frac{1}{\lambda}\sum_{i=1}^n x_i =0
    \end{equation*}
    Thus the MLE is:
    \begin{equation*}
        \wh\lambda = \frac{1}{n}\sum_{i=1}^n x_i = \overline x_n\, .
    \end{equation*}
    In our particular case
    \begin{equation*}
        \sum_{i=1}^{118} x_i = 19 + 2\cdot 26 + 3\cdot 26 + 4\cdot 21 + 5\cdot 13 + 6\cdot 5 + 7\cdot 3 = 349
    \end{equation*}
    so the MLE is
    \begin{equation*}
        \wh\lambda = \frac{349}{118}=2.96.
    \end{equation*}
    The theoretical probabilities calculated with the estimator are compared with the relative abundances in the next table.
    \begin{center}
        \begin{tabular}{*{11}c}
            \toprule
            $k$ & 0 & 1 & 2 & 3 & 4 & 5 & 6 & 7 \\ 
            \midrule
            $n_k/n$ & 0.042 & 0.161 & 0.220 & 0.220 & 0.178 & 0.110 & 0.042 & 0.025\\
            $\wh p_k$ & 0.052 & 0.153 & 0.227 & 0.224 & 0.166 & 0.098 & 0.048 & 0.020\\
            \bottomrule
        \end{tabular}
    \end{center}
\end{example}

\begin{example}
    Let $x_1,\ldots,x_n$ be data observed for a random sample from a normal distribution $\normal(\mu,\sigma^2)$. Find the MLE of the parameters $\mu, \sigma^2$.

    First we determine the likelihood function:
    \begin{equation*}
            L(\mu,\sigma^2) = \prod_{i=1}^n \frac{1}{\sqrt{2\pi\sigma^2}}\, e^{-\frac{(x_i-\mu)^2}{2\sigma^2}} = \left( \frac{1}{\sqrt{2\pi\sigma^2}}\right)^n \, e^{-\frac{1}{2\sigma^2}\sum_{i=1}^n(x_i-\mu)^2}.
    \end{equation*}
    Then we pass to the logarithm:
    \begin{equation*}
    \ln L(\mu,\sigma^2) = -\frac{n}{2}\ln 2\pi - \frac{n}{2}
    \ln\sigma^2 - \frac{1}{2\sigma^2}\sum_{i=1}^n (x_i-\mu)^2\, .
    \end{equation*}
    Next we compute the partial derivatives with respect to the parameters $\mu,\sigma^2$ and we set those equal to 0:
    \begin{align*}
        \frac{\partial \ln L(\mu,\sigma^2)}{\partial \mu} = \frac{1}{\sigma^2}\sum_{i=1}^n (x_i-\mu) = \frac{1}{\sigma^2}\left( \sum_{i=1}^n x_i-n\mu\right) &= 0 \\
        \frac{\partial \ln L(\mu,\sigma^2)}{\partial \sigma^2} = -\frac{n}{2}\frac{1}{\sigma^2} + \frac{1}{2(\sigma^2)^2}\sum_{i=1}^n (x_i-\mu)^2 &= 0.
    \end{align*}
    Since $\sigma^2>0$ we get from the first equation
    \begin{equation*}
        \wh\mu=\frac{1}{n}\sum_{i=1}^nx_i = \overline x_n.
    \end{equation*}
    Substituting into the second equation,
    \begin{equation*}
        \wh\sigma^2 = \frac{1}{n}\sum_{i=1}^n(x_i - \overline x_n)^2.
    \end{equation*}
\end{example}

\subsection{Interval estimates}

Instead of a specific point estimate, we are sometimes interested in finding an interval in which the parameter lies with a some (high) probability $1-\alpha$.

Concretely, we are looking for two statistics $\underline\theta(X_1,\ldots,X_n)$ and $\overline\theta (X_1,\ldots,X_n)$ such that 
\begin{equation*}
    \P[\underline\theta (X_1,\ldots,X_n)\leq\theta\leq \overline\theta (X_1,\ldots,X_n)]\geq 1-\alpha.
\end{equation*}

\begin{defin}
    A pair of statistics $(\underline\theta,\overline\theta)$ satisfying the condition above is called a $100\cdot(1-\alpha)\%$ \emph{confidence interval}. $\underline\theta (X_1,\ldots,X_n)$ is called the lower bound and $\overline\theta (X_1,\ldots,X_n)$ the upper bound. The number $1-\alpha$ is called the \emph{confidence coefficient}.
\end{defin}

\begin{remark}
    The most commonly used values are $\alpha=0.05$ and $\alpha=0.01$.

A 99\% confidence interval means that the actual value
of the parameter is 99\% likely to lie within $(\underline\theta,\overline\theta)$. If we have a particular realization $(x_1,\ldots,x_n)$ of the random sample, then we can also say that in 100 repetitions of the experiment, we expect 99 cases where the value of the parameter $\theta$ will lie within the calculated confidence interval.
\end{remark}

\begin{remark}
    Sometimes we are only interested in only one of the two bounds (upper or lower), which means we want to solve for
\begin{equation*}
\P[\theta \geq \underline\theta (X_1,\ldots,X_n)]\geq 1-\alpha
\qmq{or} \P[\theta\leq \overline\theta (X_1,\ldots,X_n)]\geq
1-\alpha.
\end{equation*}
We call this a one-sided confidence interval.
\end{remark}

In general, the construction of interval estimators is difficult. We focus on the normal distribution.

\begin{theorem}
    Let $X_1,\ldots,X_n$ be a random sample from the distribution $N(\mu,\sigma^2)$ and we know the value of $\sigma^2$. Then
    \begin{equation*}
        \P\left[ \overline X_n - u_{1-\alpha/2}\,\frac{\sigma}{\sqrt{n}} <\mu<\overline X_n + u_{1-\alpha/2}\,\frac{\sigma}{\sqrt{n}}\right] = 1-\alpha,
    \end{equation*}
    where $u_\beta$ is the $\beta$-quantile of the standard normal distribution $\normal(0,1)$.
\end{theorem}

In other words
\begin{equation*}
    \left(\overline X_n - u_{1-{\alpha/2}}\frac{\sigma}{\sqrt{n}}, \overline X_n + u_{1-\alpha/2}\,\frac{\sigma}{\sqrt{n}}\right)
\end{equation*}
is the $100\cdot(1-\alpha)\%$ confidence interval for the parameter $\mu$.

\begin{proof}
    Using the MGF it can be shown that the sample mean $\overline X_n\sim \normal(\mu,\sigma^2/n)$. In particular $\E[\overline X_n] = \mu$, $\Var(\overline X_n) = \sigma^2/n$ and
    \begin{equation*}
        Z=\frac{\overline X_n - \mu}{\sigma/\sqrt{n}} \sim \normal(0,1).
    \end{equation*}
    Therefore
    \begin{equation*}
    \P[u_{\alpha/2}<Z<u_{1-\alpha/2}] = \P[Z<u_{1-\alpha/2}] -\P[Z<u_{\alpha/2}] = 1-\frac{\alpha}{2} - \frac{\alpha}{2} = 1-\alpha
    \end{equation*}
    Using the fact that $\normal(0,1)$ satisfies $u_{\alpha/2}=-u_{1-\alpha/2}$,
\begin{align*}
    1-\alpha &= \P\left[-u_{1-\alpha/2}<\frac{\overline X_n - \mu}{\sigma/\sqrt{n}}<u_{1-\alpha/2}\right] \\
             &= \P\left[-u_{1-\alpha/2}\frac{\sigma}{\sqrt{n}}<\overline X_n - \mu<u_{1-\alpha/2}\frac{\sigma}{\sqrt{n}}\right]\\
             &= \P\left[\overline X_n-u_{1-\alpha/2}\frac{\sigma}{\sqrt{n}}<\mu<\overline X_n+u_{1-\alpha/2}\frac{\sigma}{\sqrt{n}}\right].\qedhere
\end{align*}
\end{proof}

Other confidence intervals for the normal distribution are listed in the table found at the end of the chapter (\cref{tab:confidence}). We will not derive them.

\begin{example}
    A control test conducted on the lifetime of 16 light bulbs gave a sample mean of $\overline x_{16} = 3000$ hours and a biased sample variance of $\sigma_{16}^2=400$ hours. Assuming that the lifetimes follow a random distribution $\normal(\mu,\sigma^2)$, calculate the 90\% confidence interval for the parameters $\mu$ and $\sigma$.

    First we need to calculate the sample standard deviation $s_{16}$. Recall $s_n^2 = n/(n-1) \sigma_n^2$, so $s_{16}=80/\sqrt{15}$. Then we can look at the table for the confidence interval. In this case
    \begin{equation*}
        \left(\overline X_n - t_{1-\alpha/2}(n-1)\,\frac{s_n}{\sqrt{n}},\overline X_n + t_{1-\alpha/2}(n-1)\,\frac{s_n}{\sqrt{n}}\right).
    \end{equation*}
After inputing the values $\alpha= 0.1$, $n=16$ and $t_{0.95}(15)=1.753$ we obtain
\begin{equation*}
\left(
3000-1.753\frac{20}{\sqrt{15}},3000+1.753\frac{20}{\sqrt{15}}\right)
= ( 2990.9, 3009.1).
\end{equation*}
Therefore $\mu$ lies with probability $90\%$ in the interval
$( 2990.9, 3009.1)$. For the lower confidence interval we get
\begin{equation*}
    \overline x_{16} - t_{0.9}(15) \frac{s_{16}}{\sqrt{16}} = 3000 - 1.3406 \frac{20}{\sqrt{15}} = 2993.3.
\end{equation*}
So $\mu > 2993.3$ with 90\% probability.

For the parameter $\sigma^2$ the confidence interval has the form
\begin{equation*}
    \left(\frac{(n-1)s_n^2}{\chi_{1-\alpha/2}^2(n-1)}, \frac{(n-1)s_n^2}{\chi_{\alpha/2}^2(n-1)}\right).
\end{equation*}
With the values $\chi_{0.95}(15) = 24.996, \chi_{0.05}(15) =
4.601$ we obtain the interval $(256, 1391)$. Therefore the parameter
$\sigma$ lies with probability 90\% in the interval
$(16,37.3)$. Similarly, for the upper confidence interval
\begin{equation*}
    \frac{15 s_{16}^2}{\chi_{0.1}^2(15)} = \frac{16\cdot 400}{5.2293}= 1223.8,
\end{equation*}
which means that $\sigma<34.9$ with 90\% probability.
\end{example}

\begin{example}
    The depth of the sea floor is measured with an instrument whose systematic error is zero, and the random errors follow a normal distribution with standard deviation of $\sigma=20$~m and expectation $\mu=0$. How many independent measurements are needed to determine the depth up to $10~m$ with confidence level of 95\%?

    Let us denote the results of the measurements by $Y_j, j=1,\ldots, n$. We know that $Y_j = D + X_j$ where $D$ is the actual depth of the sea and $X_j\sim \normal(0,\sigma^2)$ is the random error in the measurement. By averaging the measurements we get
    \begin{equation*}
        \overline Y_n = \frac{1}{n} \sum_{i=1}^n Y_j = \frac{1}{n} \sum_{i=1}^n (h+X_j) = h + \frac{1}{n} \sum_{i=1}^n X_j = \overline X_n,
    \end{equation*}
    The confidence interval for the parameter $\mu$ is
    \begin{equation*}
        \P\left[ \overline X_n - u_{1-\alpha/2}\,\frac{\sigma}{\sqrt{n}} <\mu<\overline X_n + u_{1-\alpha/2}\,\frac{\sigma}{\sqrt{n}}\right] = 1-\alpha,
    \end{equation*}
    and since $\mu=0$ this can be rewritten as
    \begin{equation*}
        \P\left[| \overline X_n|< u_{1-\alpha/2}\,\frac{\sigma}{\sqrt{n}} \right] = 1-\alpha.
    \end{equation*}
    We want to find the number $n$ of measurements so that the error that is less than 10~m with probability 95\%, i.e.
    \begin{equation*}
        \P[|\overline X_n|<10]= 0.95.
    \end{equation*}
    Using the formula for the confidence interval, we want 
    \begin{equation*}
        u_{0.975}\frac{20}{\sqrt{n}} \leq 10 
    \end{equation*}
    Solving for $n$ we find that we need
    \begin{equation*}
        n \geq 4 u_{0.975}^2 \approx 15.37.
    \end{equation*}
    Therefore we need to make at least 16 measurements.
\end{example}

\section{Exercises}

\begin{enumerate}[wide,label={\textbf{\arabic{*}.}},itemsep=1em]

    \item Let $X_1,\ldots,X_n$ be a random sample from a distribution with density
    \begin{equation*}
        f_{X_i}(x_i) =
        \begin{cases}
            \frac{1}{\theta} e^{-\frac{x_i-\mu}{\theta}} & \text{for } x_i>\mu, \\
            0 & \text{otherwise},
        \end{cases}
    \end{equation*}
    where $\theta>0,\mu\in\RR$. Use the method of moments to find estimates of of the parameters $\theta,\mu$.

    \item Let $X_1,\ldots,X_n$ be a random sample from a distribution with density
    \begin{equation*}
        f_{X_i}(x_i) = 
        \begin{cases}
            (\theta+1)x_i^\theta & \text{for } 0\leq x_i\leq 1\\
            0 & \text{otherwise}.
        \end{cases}
    \end{equation*}
    Find the maximum likelihood estimate of $\theta$.

    \item Consider a random sample $X_1,\ldots,X_n$ from a distribution with density
    \begin{equation*}
        f(x,\theta) = 
        \begin{cases}
            3\theta^3 \frac{1}{x^4} & \text{for } x>\theta\\
            0 & \text{otherwise}
        \end{cases}
    \end{equation*}
    where $\theta>0$.  
    \begin{enumerate}[label={\alph{*})}]
        \item Find the maximum likelihood estimate $\widehat\theta$ of the parameter $\theta$ and decide whether it is unbiased.
        \item Find an estimate $\theta^\ast$ of the parameter $\theta$ by the method of moments and decide whether it is unbiased.
        \item Calculate the variances of the estimates of $\widehat\theta$ and $\theta^\ast$ and compare their rate of convergence to 0 as $n\to\infty$.
    \end{enumerate}

    \item In a bottling plant for liquid soap, a random sample of 20 bottles gave a sample variance in content of $s_n^2 = 0.0153$. A variance exceeding 0.01 is considered to be unacceptable. At a level of significance of 0.05, can we say that the manufacturer has a bottling problem?

    \item A pharmaceutical company claims that one of its drugs is effective in 75\% of cases. A hospital has tested the drug on 200 patients and reported a success in 136 of them. Is there a statistically significant difference between the manufacturer's claims and the observed success rate of the drug?
\end{enumerate}

\begin{table}\centering
    \renewcommand*{\arraystretch}{2}
    \small
    \begin{tabular}{cccc}
        \toprule
        parameter & type & confidence interval & assumptions \\ \midrule
        $\mu$ & 2 sides  & $\left(\,\ol{X}_n - u_{1-\alpha/2}\,
        \displaystyle\frac{\sigma}{\sqrt{n}},\ \ol{X}_n +u_{1-\alpha/2}\,
        \frac{\sigma}{\sqrt{n}}\, \right)$ & $\sigma^2$ known \\
        $\mu$ & lower & $\ol{X}_n - u_{1-\alpha},\displaystyle\frac{\sigma}{\sqrt{n}}$ & $\sigma^2$ known \\ 
        $\mu$ & upper & $\ol{X}_n + u_{1-\alpha},\displaystyle\frac{\sigma}{\sqrt{n}}$ & $\sigma^2$ known \\ 
        $\mu$ & 2 sides & $\left(\,\ol{X}_n - t_{1-\alpha/2}(n-1)\, \displaystyle\frac{s_n}{\sqrt{n}},\ \ol{X}_n +t_{1-\alpha/2}(n-1)\, \frac{s_n}{\sqrt{n}}\, \right)$ & $\sigma^2$ unknown \\ 
        $\mu$ & lower & $\ol{X}_n - t_{1-\alpha}(n-1)\, \displaystyle\frac{s_n}{\sqrt{n}} $ & $\sigma^2$ unknown \\ 
        $\mu$ & upper & $\ol{X}_n+t_{1-\alpha}(n-1)\, \displaystyle\frac{s_n}{\sqrt{n}} $ & $\sigma^2$ unknown \\ 
        $\sigma^2$ & 2 sides & $\left(\displaystyle\frac{(n-1)s_n^2}{\chi^2_{1-\alpha/2}(n-1)},\ \frac{(n-1)s_n^2}{\chi^2_{\alpha/2}(n-1)} \right)$ & $\mu$ unknown \\  
        $\sigma^2$ & lower & $\displaystyle\frac{(n-1)s_n^2}{\chi^2_{1-\alpha}(n-1)}$ & $\mu$ unknown \\ 
        $\sigma^2$ & upper & $\displaystyle\frac{(n-1)s_n^2}{\chi^2_{\alpha}(n-1)}$ & $\mu$ unknown \\ \bottomrule
    \end{tabular}
    \par\medskip
    \renewcommand*{\arraystretch}{1}
    \begin{tabular}{ll}
        $u_\beta$ & $\beta$-quantile of $\normal(0,1)$ \\
        $t_\beta(n)$ & $\beta$-quantile of a $t$-distribution $t(n)$ \\
        $\chi^2_\beta(n)$ & $\beta$-quantile of a chi-squared distribution $\chi^2(n)$ \\
        $F_\beta(n,m)$ & $\beta$-quantile of an $F$-distribution $F(n,m)$
    \end{tabular}
    \caption{Confidence intervals for a random sample $X_1,\dots,X_n$ from a normal distribution $N(\mu,\sigma^2)$ with sample mean $\overline X_n$ and sample variance $s_n^2$.}
    \label{tab:confidence}
\end{table}

