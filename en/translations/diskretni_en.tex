\chapter{Discrete random variables}

\begin{defin}
We say that a random variable $X$ has a discrete distribution if
there is a finite or countable set of real numbers
$H={x_1,x_2,\ldots,x_n,\ldots}$ such that for all $x_j\in
H$
$$
\P[X=x_j]> 0 \qmq{a} \sum_{x_j\in H} \P[X=x_j]= 1\, .
$$
\end{defin}
\begin{remark}
In other words, the range of values of the random variable $X$ is at most countable
set.
\end{remark}
\begin{defin}
We call the function $P(x)=\P[X=x]$ defined for all $x\in\RR$
the probability function of the random variable $X$.
\end{defin}
\begin{remark}
At points where $P(x)$ is non-zero, we will denote its values by
$$
P(x_i) = \P[X=x_i] \triangleq p_i\, .
$$
\end{remark}
\begin{remark}
Discrete random variables are usually given by probabilistic
functions in the form of a table, e.g.
\begin{center}
\begin{tabular}{c|c|c|c|c|c|c}
    $x_i$ & 0 & 1 & 2 & 3 \\\ \hline
  $p_i$ & 0.25 & 0.3 & 0.1 & 0.35 \
\end{tabular}
\end{center}
The distribution function of a discrete random variable has the form
$$
F_X(x)= \sum_{j:x_j\leq x} \P[X=x_j] = \sum_{j:x_j\leq x} p_j
\qmq{pro} x\in\RR\, .
$$
It follows that the distribution function $F_X(x)$ has jumps in
points $x_1, x_2, \ldots$ and is constant over the intervals $\langle
x_j,x_{j+1})$, $j=1,2,\ldots$. Moreover, the size of the jump at point $x_j$
is equal to $\P[X=x_j]$. So we can write
$$
\P[X=a] = P(a),\qquad \P[X<a] = F(a)-P(a),\qquad \P[X\leq a] =
F(a),
$$
$$
\P[X>a]=1-F(a),\qquad \P[X\geq a] = 1-F(a)+P(a)\, .
$$
{\em\large Image}
\end{remark}
\begin{remark}
If the range of values of the random variable $X$ are non-negative integers
$³{0,1,2,\ldots\}$, the distribution function can be written as
$$
F_X(x) = \sum_{n=0}^{[x]} \P[X=n]\, ,
$$
where the symbol $[x]$ denotes the integer part of $x$.
\end{remark}

\begin{exercise}
The car has 4 traffic lights on the route. These give a 2/3 probability
green or red with probability 1/3 (they work independently).
Determine the probability distribution of a random variable giving the number of
of traffic lights the car will run before the first stop.
\end{exercise}

\begin{exercise}
The target is divided into three areas numbered 1,2,3. For hitting the area
1, the shooter receives 10 points, for area 2 he receives 5 points and for area
3 receives -1 point. The probabilities of hitting the shooter are in order
0.5, 0.3, 0.2. Determine the probability distribution of a random number
of points scored in two shots.
\end{exercise}



\section{Examples of discrete distributions}
\subsection{Alternative distribution (Bernoulli)}
This distribution is used to describe random situations having two
possible outcomes (yes $\times$ no, right $\times$ wrong).
For example, a coin toss, a die roll (when we are only interested in evenness,
oddness). Usually these outcomes are assigned values of 0
(representing failure with an assumed probability of $1-p$)
and 1 (success with probability $p$).
\begin{defin}
We say that the random variable $X$ has an alternative distribution with
parameter $p\in(0,1)$ if
$$
\P[X=1]=p, \qquad \P[X=0]=1-p\, .
$$
\end{defin}
\begin{remark}
To express the fact that the random variable $X$ has an alternative
distribution, we will use the notation
$$
X\sim Be(p)\, .
$$
The probability function of the alternative distribution can be generally
can be written in the form
$$
P(x)=\P[X=x] =
  \begin{cases}
    p^x(1-p)^{1-x} & \text{for } x\in\{0,1\}, \
    0 & \text{other}.
  \end{cases}
$$
\end{remark}


\subsection{Binomial distribution}
Consider a random variable $X$ expressing the number of occurrences of the phenomenon $A$
in $n$ independent experiments. If the probability of the phenomenon $A$ is
is equal to $p$ each time, the probability that phenomenon $A$ occurs out of $n$
trials exactly $k$ times is
$$
p_k=\P[X=k] = \binom{n}{k} p^k(1-p)^{n-k}\, .
$$
Since the random variable $X$ can take values $0,1,\ldots n$ and
according to the binomial theorem
$$
\sum_{k=0}^n \binom_{n}{k} p^k(1-p)^{n-k} = (p+1-p)^n =1\, ,
$$
this is a probability distribution.
\begin{defin}
We say that the random variable $X$ has a binomial distribution with
parameters $n\in\NN$ and $p\in(0,1)$ if
$$
\P[X=k] = \binom{n}{k} p^k(1-p)^{n-k} \qmq{pro} k=0,1,\ldots, n\,
.
$$
We denote by $X\sim Bi(n,p)$.
\end{defin}
\begin{remark}
Holds if $X_j, j\in\wh n$, are independent random variables with
alternative distribution with parameter $p$, then the random variable has
$X$ defined by the prescription
$$
X\triangleq X_1+X_2+\ldots +X_n
$$
has a binomial distribution with parameters $n$ and $p$.
\end{remark}
\begin{example}
Consider a chess game in which there are two equally strong opponents
(the probability of winning and losing is 1/2). Decide whether
more likely
\begin{itemize}
  \to win 3 games out of 4, or 5 games out of 8,
  \item[b] to win at least 3 games out of 4, or at least 5 games out of 8.
  8.
\end{itemize}
Let's focus on one player and label $X$ a random variable
representing his number of wins in 4 games and $Y$ the number of wins in 8 games. Then
a)
$$
\P[X=3] =
\binom{4}{3}\left(\frac{1}{2}\right)^3\frac{1}{2}=\frac{1}{4}
$$
a
$$
\P[Y=5]
=\binom{8}{5}\left(\frac{1}{2}\right)^5\left(\frac{1}{2}\right)^3=\frac{7}{32}
$$
b)
$$
\P([X=3]\cup[X=4])= \P[X=3]+\P[X=4]=\frac{1}{4}+
\binom{4}{4}\left(\frac{1}{2}\right)^4=\frac{5}{16}
$$
a
$$
\P([Y=5]\cup[Y=6]\cup[Y=7]\cup[Y=8])=\sum_{k=5}^8
\binom{8}{k}\left(\frac{1}{2}\right)^k
\left(\frac{1}{2}\right)^{8-k} = \frac{93}{256}\, .
$$
\end{example}

\subsection{Pascal (geometric) distribution}

\begin{defin}
We say that the random variable $X$ has a Pascal distribution with
parameter $p\in(0,1)$ if
$$
\P[X=k] = p\cdot (1-p)^k \qmq{pro} k=0,1,2,\ldots\, .
$$
We denote by $X\sim Geom(p)$.
\end{defin}
\begin{remark}
Consider an infinite sequence of Bernoulli trials (phenomenon $A$
occurs with probability $p$, does not occur with probability
$1-p$). The random variable $X\sim Geom(p)$ then expresses the number of
trials before the first occurrence of $A$.

Let us show that this is indeed a probability distribution.
$$
\sum_{k=0}^\infty \P[X=k] = \sum_{k=0}^\infty p (1-p)^k =
\frac{p}{1-(1-p)}=1\, .
$$
For Pascal's distribution, it is also possible to simply express
distribution function
$$
F_X(x)=\sum_{k=0}^{[x]}p(1-p)^k = p\cdot
\frac{1-(1-p)^{[x]+1}}{1-(1-p)}= 1-(1-p)^{[x]+1}, \quad x\in\RR.
$$
\end{remark}
\begin{example}
Consider the beginning of the game of man's anger, when the player puts a piece into
the game until he gets a six. What is the probability that the player puts in
the fourth, or at the latest the fourth round?

Let's denote $X$ the number of tries before a six is rolled. Then $X\sim Geom(1/6)$
and we have
$$
\P[X=3] = \frac{1}{6}\cdot \left(1-\frac{1}{6}\right)^3
=\frac{1}{6}\cdot \left(\frac{5}{6}\right)^3\doteq 0.0965
$$
a
$$
\P[X\leq 3] = F_X(3) = 1-\left(\frac{5}{6}\right)^4 \doteq
0.5177\, .
$$
\end{example}

\subsection{Poisson distribution}
\begin{defin}
We say that the random variable $X$ has a Poisson distribution with
with parameter $$lambda>0$ if
$$
\P[X=k] =
  \begin{cases}
    e^{-\lambda}\fra{\lambda^k}{k!} & \text{for } k=0,1,2,\ldots\
    0 & \text{other}.
  \end{cases}
$$
Marking $X\sim Po(\lambda)$.
\end{defin}
\begin{remark}
Let's verify that this is a probability distribution,
$$
\sum_{k=0}^\infty e^{-\lambda}\fra{\lambda^k}{k!} =
e^{-\lambda}\sum_{k=0}^\infty \fra{\lambda^k}{k!} =
e^{-\lambda}\cdot e^{\lambda}=1.
$$
\end{remark}
\begin{remark}
The Poisson distribution was derived to describe the radioactive
decay. Usage:
\begin{description}
  \When describing the number of events that occur during
  a time interval ($\lambda$ - the average number of events per
  time interval). E.g. Number of signals in a telephone exchange.\\\
  \noindent When describing the number of particles per unit area or volume ($\lambda$)
  - average number of particles per unit). E.g., the number of particles in a line of sight
  field of a microscope.
  \item[b)] Approximation of a binomial distribution\\
  Consider a sequence of random variables $X_1,X_2,X_3,\ldots$, where
  $X_i\sim Bi(n,p_n)$. Let $np_n = \lambda$, where $\lambda>0$ is
  parameter. The assumption says that the relative number of "favorable" outcomes between
  $n$ trials "should be around $\lambda$". Speaking
  only the long-term behavior, not a single trial realization.
  It is not a problem to calculate the distribution of $X_i$ for small $i$, for $i$
  large ($i=10000$) the calculation is already problematic. And this is where
  the Poisson distribution will help.
\end{description}
\end{remark}

\begin{theorem}
Let $(X_n)_{n=1}^\infty$ be a sequence of random variables
such that $X_n\sim Bi(n,p_n)$. Let
$$
\lim_{n\to\infty}p_n=0\qmq{a} \lim_{n\to\infty}n p_n=\lambda,\quad
\lambda\in\RR^+.
$$
Then
$$
\lim_{n\to\infty} \binom_{n}{k}p_n^k(1-p_n)^{n-k} =
e^{-\lambda}\fra{\lambda^k}{k!},\quad k=0,1,2,\ldots
$$
(or $\lim_{n\infty}\P[X_n=k]=\P[X=k]$, where $X\sim
Po(\lambda$)).
\end{theorem}
\begin{proof}
Let's decompose the term after the limit into the form
$$
 \binom{n}{k}p_n^k(1-p_n)^{n-k} = \frac{n(n-1)\ldots
 (n-k+1)}{k!},\fra{p_n^k}{(1-p_n)^k}\, (1-p_n)^n\, .
$$
We will examine the limit of the expression on the right-hand side in parts. Since
under the assumptions of $np_n\to\lambda$, we can write $p_n=\lambda/n
+z_n$, with $nz_n\to 0$. Then
$$
(1-p_n)^n = \left( 1-\frac{\lambda+nz_n}{n}\right)^n =
\left[\left(1+\fra{1}{\frac{n}{-(\lambda+nz_n)}}\right)^{\frac{n}{-(\lambda+nz_n)}}\right]^{-(\lambda+nz_n)}\longrightarrow
e^{-\lambda},
$$
for $n\to\infty$, where we took advantage of the fact that $(1+1/p_n)^{p_n}
e$ for $|p_n|\to\infty$.

Further
$$
\left(\frac{p_n}{1-p_n}\right)^k =
\left(\frac{\lambda/n+z_n}{1-\lambda/n-z_n}\right)^k =
\left(\frac{\lambda+nz_n}{n-\lambda-nz_n}\right)^k\, ,
$$
where
$$
(\lambda+nz_n)^k \to \lambda^k
$$
and all remaining terms have a limit
$$
\frac{n(n-1)\ldots(n-k+1)}{k!\, (n-\lambda-nz_n)^k}
\longrightarrow \frac{1}{k!},
$$
since it is a limit polynomial of degree $k$ slash polynomial
of degree $k$. Thus, in total, we get the assertion of the theorem.
\end{proof}
\begin{remark}
Thus, for small $p$ and large $n$ we can approximate the distribution
$Bi(n,p)$ by the distribution of $Po(np)$. This approximation is often recommended
for $p<0.1$ and $n>30$. For example, the probability values of a random
$Y\sim Bi(1000,0.0015)$ can be approximated by
by the probabilities of a random variable with distribution $Po(1.5)$. For
illustrate the simplicity of the calculation, we express
$$
\P[Y=5] = \binom{1000}{5} (0.0015)^5 (0.9985)^{995} \doteq
e^{-1.5}\frac{(1.5)^5}{5!}\, .
$$
\end{remark}
\begin{example}
The probability of a fault per kilometre of wire is 0.01. The cable is
consists of 100 conductors, it will work if 99 conductors are good.
What is the probability that a cable one kilometer long will
will work?

Let $X$ denote the number of faulty wires, i.e. $X\sim Bi(100,0.01)$ and $A$
the phenomenon that the cable will work. Then
$$
\P(A) = \P[X\leq 1] = \P[X=0] + \P[X=1] = \binom{100}{0}
0.01^0\cdot 0.99^{100} + \binom{100}{1} 0.01^1\cdot
0.99^{99}=0.736\, .
$$

Since $n$ is large and $p$ is small, we can also
probabilities can be approximated by a Poisson distribution with
with the parameter $\lambda=np=1$,
$$
\P[X=0] + \P[X=1] = e^{-1}\frac{1}{0!} + e^{-1}\frac{1}{1!}=
2e^{-1} = 0.736\, .
$$
\³{example}
\begin{example}
Experiments have shown that the radioactive substance was emitted within 7.5
seconds, an average of 3.87 $\alpha$-particles. Determine the probability
that in 1 second this substance emits at least 1
$\alpha$-particle.

Let $X$ denote the number of particles emitted in 1 second. Since the average
number of particles emitted in 1 second is $\lambda = 3.87/7.5 =
0.516$, we can describe the random variable $X$ by a distribution
$Po(0.516)$ and thus
$$
\P[X\geq 1] = 1-\P[X=0] = 1-e^{-0.516}\frac{0.516^0}{0!}\doteq
0.403\, .
$$
\end{example}
\begin{exercise}
Determine the probability that there are at least 4 rejects among 200 products,
if rejects account for an average of $1%$ of production. [0.143]
\end{exercise}

\begin{exercise}
On average, 60 calls come into the switchboard within an hour. What
is the probability that in the half-minute it takes
the manipulator has moved away, no one will call? [0.61]
\end{exercise}

\begin{exercise}
Proofreading of 500 pages contains 1500 typographical errors. Identify
the probability that a randomly selected page has at least
3 errors. [0.577]
\end{exercise}




\section{Discrete random vectors}
\begin{defin}
We say that the random vector $\bm{X}=(X_1,X_2,\ldots,X_n)$ is
discrete if there is a finite or countable set of $n$-ticks
${(x_{11}, x_{21},\ldots, x_{n1}), (x_{12}, x_{22},\ldots,
x_{n2}),\ldots \}$ such that
$$
P(x_{1i}, x_{2i},\ldots, x_{ni}) = \P[X_1=x_{1i},
X_2=x_{2i},\ldots,X_n=x_{ni}]>0 \qmq{for all } i=1,2,\ldots
$$
a
$$
\sum_i P(x_{1i}, x_{2i},\ldots, x_{ni}) = 1\, .
$$
The function $P(x_{1}, x_{2},\ldots, x_{n})$ is called the probability function
function of the random vector $\bm X$.
\end{defin}

\subsection{Multinomial distribution}
\begin{defin}
We say that the random vector $\bm{X}=(X_1,X_2,\ldots,X_k)$ has
multinomial distribution with parameters $n, p_1,\ldots , p_k$, where
$n\in\NN$, $0<p_j<1$, $j\in\wh k$ and $sum_{j=1}^k p_j=1$,
if
$$
\P[X_1=x_1,\ldots, X_k=x_k] = \fra{n!}{x_1! x_2!\ldots x_k!}\,
p_1^{x_1} p_2^{x_2}\ldots p_k^{x_k}
$$
for $x_j=0,1,\ldots n, j\in\wh k$, and $sum_{j=1}^k x_j = n$ and is
zero otherwise.
\end{defin}
\begin{remark}
The name of this distribution comes from the fact that $\P[X_1=x_1,\ldots,
X_k=x_k]$ is a general expansion term of $(p_1+p_2+\ldots+p_k)^n$.
The multinomial distribution is actually a generalization of the binomial distribution: consider
$n$ independent trials, in each of which exactly one of $k$ occurs
mutually disjoint phenomena $A_1,A_2,\ldots, A_k$. Herewith,
we assume that the probability that the $i$th trial has the phenomenon
$A_j$ is $p_j$ and $$sum_{j=1}^k p_j=1$. The binomial distribution is
a special case of multinomial for $k=2$.
\end{remark}
\begin{example}
For a product, we measure the sign of $Z$. The product is good if $T_D\leq
Z\leq T_H$. Consider the phenomena $A_1: Z<T_D$, $A_2: T_D\leq Z\leq T_H$ and
$A_3: Z>T_H$. Under normal conditions, it is known that
$$
\P(A_j) = p_j,\quad j=1,2,3 \qmq{while} \sum_{j=1}^3 p_j =1\, .
$$
Then the probability of having $x_1$ of the $n$ products
$Z<T_D$, $x_2$ is $T_D\leq Z\leq T_H$ and $x_3$ is $Z>T_H$
is equal to
\begin{eqnarray*}
\P[X_1=x_1,X_2=x_2,X_3=x_3] &=& \frac{n!}{x_1! x_2! x_3!}\,
p_1^{x_1}p_2^{x_2}p_3^{x_3}
&=& \frac{n!}{x_1! x_2! (n-x_1-x_2)!}\, p_1^{x_1}
p_2^{x_2}(1-p_1-p_2)^{n-x_1-x_2}\, .
\³{eqnarray*}
E.g. for $p_1=0.02, p_2=0.95, p_3=0.03$ and $n=6$
$$
\P[X_1=1,X_2=4,X_3=1] = \frac{6!}{1!4!1!}\, 0.02\cdot 0.95^4\cdot
0.03 = 0.015\, .
$$
\end{example}


\subsection{Properties of discrete random vectors}
Consider a discrete random vector $(X_1,X_2)$. Then the associated
distribution function of this vector is given by
$$
F_{X_1,X_2}(x_1,x_2) = ³{sum_{t_1}\sum_{t_2}\sum_{t_2\leq x_2}
\P[X_1=t_1,X_2=t_2]\, .
$$
\begin{theorem}\label{margdis}
If $\P[X_1=x_1,X_2=x_2]$ is a joint probability function
of the random vector $(X_1,X_2)$, then for a marginal
probability functions for the marginal random variables $X_1$ and $X_2$ are
$$
\P[X_1=x_1] = \sum_{x_2} \P[X_1=x_1,X_2=x_2] \qmq{for all}
x_1\in\RR
$$
a
$$
\P[X_2=x_2] = \sum_{x_1} \P[X_1=x_1,X_2=x_2] \qmq{for all}
x_2\in\RR\, .
$$
\end{theorem}
\begin{proof}
For all $x_1\in\RR$
\begin{eqnarray*}
F_{X_1}(x_1) &=& \lim_{x_2\to\infty} F_{X_1,X_2}(x_1,x_2) =
\lim_{x_2\to\infty} \sum_{t_1\leq x_1} \sum_{t_2\leq x_2}
\P[X_1=t_1,X_2=t_2]\\
& =& \sum_{t_1\leq x_1} \left( \sum_{t_2}
\P[X_1=t_1,X_2=t_2]\right)\, ,
\end{eqnarray*} but by definition
also applies to all $x_1\in\RR$
$$
F_{X_1}(x_1) = \sum_{t_1\leq x_1}\P[X_1=t_1]\, .
$$
Since both sums for $t_1\leq x_1$ have the same values for all
$x_1 \in\RR$, their arguments must also be the same, i.e.
$$
\P[X_1=t_1] = \sum_{t_2} \P[X_1=t_1,X_2=t_2] \qmq{for all }
t_1\in\RR\, .
$$
The second part of the statement turns out the same way.
\end{proof}

\begin{theorem}
Two discrete random variables $X,Y$ are independent just then,
when
$$
\P[X=x,Y=y]= \P[X=x]\cdot \P[Y=y] \qmq{for all}
(x,y)\in\RR^2\, .
$$
\end{theorem}
\begin{proof}
By the \ref{nez} theorem, we know that two random variables are independent
if and only if
$$
F_{X,Y}(x,y) = F_X(x)\cdot F_Y(y) \quad\forall (x,y)\in\RR^2\, .
$$
We get the statement of this theorem directly by going to the jumps of the distribution
function.
\end{proof}
\begin{example}
Let's have two discrete random variables given by a pooled
probability function $\P[X_1=x_1,X_2=x_2]$ in the form of a table
\begin{center}
\begin{tabular}{c|ccc}
  $X_1\backslash X_2$ & 0 & 1 & 2 \\\hline
  0 & 0.42 & 0.12 & 0.06 \\
  1 & 0.28 & 0.08 & 0.04 \\
\end{tabular}
\end{center}
Determine the marginal likelihood functions and decide whether they are
the random variables $X_1,X_2$ are independent.

According to Theorem \ref{margdis}, the probability function
of the random variable $X_1$
$$
\P[X_1=0] = \sum_{k=0}^2 \P[X_1=0,X_2=k] = 0.42+0.12+0.06 = 0.6
$$
a
$$
\P[X_1=1] = \sum_{k=0}^2 \P[X_1=1,X_2=k] = 0.28+0.08+0.04 = 0.4\,
.
$$
Similarly, we get the values of the probability function of a random
of the probability function $X_2$,
$$
\P[X_2=0]=0.7,\quad \P[X_2=1]=0.2, \quad \P[X_2=2]=0.1\, .
$$
As can be seen, the values of the marginal likelihood functions
are obtained by summing the corresponding rows or columns and it is convenient to
to write directly into the table
\begin{center}
\begin{tabular}{c|ccc|c}
  $X_1\backslash X_2$ & 0 & 1 & 2 & $\P_{X_1}$ \\hline
  0 & 0.42 & 0.12 & 0.06 & {\bf 0.6}\
  1 & 0.28 & 0.08 & 0.04 & {\bf 0.4}\\\hline
  $\P_{X_2}$ & {\bf 0.7} & {\bf 0.2} & {\bf 0.1} & 1
\end{tabular}
\end{center}
From this table it is easy to see that for all $i\in{0.1}$ and
$j\in\{0,1,2}$
$$
\P[X_1=i,X_2=j]= \P[X_1=i]\cdot \P[X_2=j]
$$
and thus the quantities $X_1,X_2$ are independent.
\end{example}



\begin{defin}
Let $(X_1, X_2)$ be a discrete random vector with associated
probability function $\P[X_1=x_1,X_2=x_2]$. If
$\P[X_2=x_2]>0$, we define a conditional probability function
of the random variable $X_1$ under the condition that the variable $X_2$ has acquired
the value of $x_2$, by the expression
$$
\P[X_1=x_1|X_2=x_2] =
\fra{\P[X_1=x_1,X_2=x_2]}{\P[X_2=x_2]}=\fra{\P[X_1=x_1,X_2=x_2]}{\sum_{x_1}
\P[X_1=x_1,X_2=x_2]} \, .
$$
\end{defin}
\begin{example}
We roll two dice. Find the distribution of the random variable
denoting the maximum of the number of stitches rolled on both dice if
we know that the minimum has taken the value 3.

Let $X$ denote the maximum and $Y$ the minimum of the number of stitches on the two dice. $Y$
takes the value 3 if the first die has a value from the set
$\{4,5,6}$ and the second dice from the set $\{3}$, or the first $\{3}$ and
on the second ${4,5,6}$ or on both dice ${3}$. That's a total of 7
out of 36, so $\P[Y=3]=7/36$. If $Y=3$, a random
variable $X$ can take the values ${3,4,5,6}$ and
$$
\P[X=3,Y=3]=\frac{1}{36} \qmq{a} \P[X=i,Y=3]=\frac{2}{36},
\qmq{pro} i=4,5,6\, .
$$
Thus, by the definition of conditional probability
$$
\P[X=3|Y=3] = \frac{1/36}{7/36} = \frac{1}{7} \qmq{a} \P[X=i|Y=3]
= \frac{2/36}{7/36} = \frac{2}{7}, \qmq{pro} i=4,5,6\, .
$$
\end{example}

