\chapter{Limit sentences}
\Section{Law of large numbers}

So far we have dealt with individual random variables. In this
chapter we will study sequences of random variables. Examples
of such sequences of practical and theoretical interest:
\begin{itemize}
  \item Let $Z_n$ denote the number of occurrences of a certain phenomenon $A$ in $n$
  independent trials, where the probability of the phenomenon $A$ in each trial is $p$.
  Denote by $X_n=Z_n/n$, $X_n$ thus expresses the relative frequency of the phenomenon
  $A$ in $n$ trials. How does $X_n$ behave for large $n$?
  \Consider $n$ independent measurements $X_1,\ldots,X_n$ and their
  average of $\overline X_n = 1/n \sum_{i=1}^n X_i$. Does the distribution
  of the random variable $\overline X_n$ for large $n$ has any interesting
  properties? Does it depend on the distribution of $X_i$?
\end{itemize}

\begin{defin}
Let $(X_n)_{n=1}^\infty$ be a sequence of random variables and
$c\in\RR$. We say that the sequence $(X_n)_{n=1}^{infty$ converges
to $c$ by probability if for every $³{ve>0$
$$
\lim_{n\to\infty} \P [|X_n-c|\geq \ve] = 0\, .
$$
We denote by $X_n\stP c$.
\end{defin}
\begin{example}
Given random variables $X_n\sim N(\mu,\sigma^2/n)$ Prove that
$X_n\stP \mu$.

By definition, we have to show that for every $\ve>0$
$$
 \P [|X_n-\mu|\geq \ve] \to 0\, ,
$$
which is equivalent to saying that for every $\ve>0$
$$
\P [|X_n-\mu|< \ve] \to 1\, .
$$
Express this probability. For every $\ve>0$, the following holds
\begin{eqnarray*}
\P [|X_n-\mu|< \ve] &=& \P [\mu-\ve <X_n<\mu+\ve] =
\int_{\mu-\ve}^{\mu+\ve} \frac{1}{\sqrt{2\pi\frac{\sigma^2}{n}}}\,
e^{-\frac{n(x-mu)^2}{2\sigma^2}}, dx
&=& \left|\begin{array}{c} \frac{x-\mu}{\sigma/\sqrt{n}}= t \
\frac{1}{\sigma/\sqrt{n}}dx = dt \end{array} \right| =
\int_{-\frac{\sqrt{n}\ve}{\sigma}}^{\frac{\sqrt{n}\ve}{\sigma}}
\frac{1}{\sqrt{2}pi}\, e^{-\frac{t^2}{2}}\, dt
\stackrel{n\to\infty}{\longrightarrow} \int_{-\infty}^\infty
\frac{1}{\sqrt{2\pi}}\, e^{-\frac{t^2}{2}}\, dt = 1\, .
\end{eqnarray*}
\end{example}

\begin{theorem}
(Chebyshev inequality) Let $X$ be a random variable with mean
value $\E X$ and a finite variance $\Var X$. Then for each
$\ve > 0$ the following holds
$$
\P[|X-\E X|\geq \ve]\leq \frac{\Var X}{\ve^2}\, .
$$
\end{theorem}
\begin{proof}
Let's do the proof for continuous random variables. Consider first
a random variable $Y$ with a finite second moment $\E Y^2$. Then
for every $\ve>0$
\begin{eqnarray*}
\E (Y^2) &=& \int_{-\infty}^\infty y^2 f_Y(y)\, dy =
\int_{|y|<\ve}
y^2 f_Y(y)\, dy + \int_{|y|\geq \ve}y^2 f_Y(y)\, dy \
&\geq& \int_{|y|\geq \ve}y^2 f_Y(y)\, dy \geq \ve ^2 \int_{|y|\geq
\ve} f_Y(y)\, dy = \ve^2 \P[|Y|\geq \ve]\, .
\end{eqnarray*}
Applying this inequality to the random variable $Y=X-\E X$, we get
$$
\P[|X-\E X|\geq \ve] \leq \frac{\E(X-\E X)^2}{\ve^2} = \frac{\Var
X }{\ve^2}\, .
$$
\end{proof}
\begin{theorem}
(Let $(X_n)_{n=1}^{infty$ be a sequence
of independent, equally distributed random variables for which
there exists $\mu =\E X_i$ and $\sigma^2=\E(X_i-\mu)^2$, $i\in\n$.
Then
$$
\X_n = \frac{1}{n} \sum_{i=1}^n X_i \stP \mu\, .
$$
\end{theorem}
\begin{proof}
We use Chebyshev's inequalities for the proof. Let's first calculate
the mean and variance of the random variable $\overline X_n$. According to
the rules for calculating the mean
$$
\E \overline X_n = \E\left(\frac{1}{n} \sum_{i=1}^n X_i \right)
=\frac{1}{n} \sum_{i=1}^n \E X_i = \mu
$$
and using independence further
$$
\Var \overline X_n = \Var\left(\frac{1}{n} \sum_{i=1}^n X_i
\right) =\frac{1}{n^2} \sum_{i=1}^n \Var X_i =
\frac{\sigma^2}{n}\, .
$$
Substituting in Chebyshev's inequality, we get that for all
$\ve>0$ are
$$
0\leq \P[|\overline X_n-\mu|\geq \ve]\leq \frac{\sigma^2}{n\ve^2}
\longrightarrow 0\, ,
$$
which, by definition, is convergence in probability
equivalent to the statement being proven.
\end{proof}
\begin{remark}
A special case of the previous theorem is Bernoulli's theorem (1713).
Given independent random variables $X_n\sim Be(p)$ and
denote by $S_n = $sum_{i=1}^n X_i$. Thus $S_n$ expresses the number of
of successes in $n$ Bernoulli trials. Then
$$
\frac{S_n}{n} \stP p\, .
$$
This statement follows, for example, directly from the previous theorem by using
the fact that the mean value of the random variable $X\sim Be(p)$ is $p$.
\end{remark}
\begin{example}\label{ex82}
With what probability can we say that with 1000 coin flips
will come up tails 400-600 times?

Let the quantities $X_1,\ldots,X_{1000}$ express whether in the $i$-th
roll was an eagle (value 1) or a maiden (value 0). The quantities $X_i$
are independent and are
$$
\X_i = \frac{1}{2}\cdot 1 + \frac{1}{2}\cdot 0 =
\frac{1}{2},\quad \E X_i^2 = \frac{1}{2}\cdot 1^2 +
\frac{1}{2}\cdot 0^2 = \frac{1}{2} \qmq{a} \Var X_i =
\frac{1}{2}-\left(\frac{1}{2}\right)^2 = \frac{1}{4}\, .
$$
Let us denote by $Y=\sum_{i=1}^{1000}X_i$ the number of eagles in 1000 hours
of coins. For the random variable $Y$
$$
\Y = \sum_{i=1}^{1000} \E X_i = 500,\quad \Var Y =
\sum_{i=1}^{1000} \Var X_i = 250, .
$$
According to Chebyshev's inequality, for all $\ve>0$ the inequality holds
$$
\P[|Y-\E Y|\geq \ve]\leq \frac{\Var Y}{\ve^2}\, .
$$
In our particular case, we get
$$
\P[|Y-500|\geq 100]\leq \frac{250}{100^2}\, ,
$$
thus
$$
\P[|Y-500|< 100]\geq 1-0.025 = 0.975\, .
$$
With probability greater than 0.975, we can say that in 1000
coin flips will land tails 400-600 times.
\end{example}

\section{Central limit theorem}
In various problems in mathematical statistics, we are interested in the distribution
of the sum or average of $n$ independent random variables. Sometimes the
however, determining the exact distribution is difficult. The Central Limit Theorem
says that under certain conditions this distribution can be approximated
normal.
\begin{defin}
Let $(X_n)_{n=1}^\infty$ be a sequence of random variables with
distribution functions $F_{X_n}$ and let $X$ be a random variable with
distribution function $F_X$. We say that the sequence
$(X_n)_{n=1}^\infty$ converges to the random variable $X$ in
distribution if
$$
\³_lim_{n^infty} F_{X_n}(x) = F_X(x)
 $$
 at all points of the continuity of the function $F_X$. We denote by $X_n \stL X$ (in law).
\end{defin}
\begin{remark}
So we say that $(X_n)_{n=1}^{infty$ converges in the distribution to
distribution $N(\mu,\sigma^2)$ if
$$
\lim_{n\to\infty} F_{X_n}(x) = \Phi\left(
\frac{x-\mu}{\sigma}\right)
$$
For all $x\in\RR$, we will denote by $X_n\stL N(\mu,\sigma^2)$.
\end{remark}
\begin{theorem}
(CLV - Lindeberg-Levy) Let $(X_n)_{n=1}^\infty$ be a sequence
of independent, identically distributed random variables with a common
mean $\mu$ and finite variance $\sigma^2$. Then
is
$$
\fra{\sum_{i=1}^n X_i-n\mu}{\sqrt{n}\sigma} \stL N(0,1)\, .
$$
\end{theorem}
\begin{proof}
For a hint of the proof for quantities having a moment generating function, see the lecture.
\end{proof}
\begin{remark}
If we denote $Y_n = \sum_{i=1}^n X_i$, then $³ Y_n = n\mu$,
$\Var Y_n=n\sigma^2$ and the statement can be written in a more memorable
form
$$
\frac{Y_n-\E Y_n}{\sqrt{\Var Y_n}} \stL N(0,1)\, .
$$
Another possibility is to rewrite the statement using the average of $\overline X_n$
into the form
$$
\sqrt{n}\,\frac{\overline X_n -\mu}{\sigma} \stL N(0,1)\, .
$$
\end{remark}
\begin{theorem}
(Moivre-Laplace 1718) Let $(X_n)_{n=1}^\infty$ be a sequence
of independent, equally distributed random variables, $X_i\sim Be(p),
$ for all $i\in\n$. Then
$$
\fra{\sum_{i=1}^n X_i-np}{\sqrt{np(1-p)}} \stL N(0,1)\, .
$$
\end{theorem}
\begin{remark}
If the random variables $X_i^sim Be(p)$ are independent, $Y_n
= \sum_{i=1}^nX_i \sim Bi(n,p)$ and by the previous theorem
$$
\lim_{n} \P\left[ \frac{Y_n-np}{\sqrt{np(1-p)}}\leq x
\right] = \Phi (x), \qmq{for all} x\and \RR\, .
$$
This version of the central limit theorem can be conveniently used for
to approximate the binomial distribution using the normal distribution. Such
approximation is recommended for $np(1-p) \geq 9$ .
\end{remark}
\begin{example}
The ship has a carrying capacity of 5000 kg. The weight of the passengers is a random variable with
with a mean of 70 kg and a standard deviation of 20 kg. How many
of passengers can be loaded so that the probability of overloading the boat
to be less than 0.001?

Denote by $X_i$ the mass of the $i$-th passenger and $X={sum_{i=1}^n X_i$
the mass of all passengers. Then
$$
\E X = \sum_{i=1}^n \E X_i = 70n \qmq{a} \Var X = \sum_{i=1}^n
\Var X_i = 400 n\, .
$$
(assuming that the masses of each passenger are
independent). We are interested in the value of $n$ such that
$$
\P[X\geq 5000]<0.001\, .
$$
If we denote $U$ by a random variable with distribution $N(0,1)$,
we get the CLV approximation
\begin{eqnarray*}
\P[X\geq 5000] &=& \P\left[ \frac{X-70n}{20\sqrt{n}}\geq
\frac{5000-70n}{20\sqrt{n}}\right] \doteq \P\left[ U\geq
\frac{5000-70n}{20\sqrt{n}}\right]\\& =&
1-\Phi\left(\frac{5000-70n}{20\sqrt{n}}\right) <0.001\, ,
\end{eqnarray*}
otherwise written
$$
\Phi\left(\frac{5000-70n}{20\sqrt{n}}\right) >0.999\, .
$$
After finding the appropriate value of the function $\Phi$ in the tables, we get
inequality
$$
\frac{5000-70n}{20\sqrt{n}} > 3.09
$$
or
$$
70n+61.8\sqrt{n} -5000<0\, .
$$
Solving this quadratic inequality in the variable $\sqrt{n}$, we find
one admissible root $\sqrt{n_1} = 8.02$ and conclude that we can
board a maximum of 64 passengers.
\end{example}
\begin{example}
With what probability can we say that with 1000 coin flips
will come up tails 450-550 times?

In the label introduced in the \ref{ex82} example we find
probability $\P[450\leq Y\leq 550]$. In this case, the
the value $np(1-p) =250$ is large enough to use
approximation using the central limit theorem and using a random
variable $U\sim N(0,1)$ we get
\begin{eqnarray*}
\P[450\leq Y\leq 550] &=& \P\left[ \frac{-50}{\sqrt{250}}\leq
\frac{Y-500}{\sqrt{250}}\leq \frac{50}{\sqrt{250}}\right] \doteq
\P\left[ \frac{-50}{\sqrt{250}}\leq U\leq
\frac{50}{\sqrt{250}}\right]\\& =&
2\Phi\left(\frac{50}{\sqrt{250}}\right)-1 =0.9984\, .
\end{eqnarray*}
Similarly, for example, we would calculate
$$
\P[480\leq Y\leq 520] \doteq
2\Phi\left(\frac{20}{\sqrt{250}}\right)-1 =0.7941\, .
$$
To illustrate, the exact value computed more laboriously using
binomial distribution is 0.7939 in this case.
\end{example}

\begin{example}
We roll a symmetrical die. What is the probability of rolling a 600
more than 110 sixes in 600 rolls?

Let $Y$ denote the number of sixes out of 600 throws. So $Y$ has a binomial
distribution with parameters $n=600$ and $p=1/6$ and the value $np(1-p)=250/3$
entitles us to use the normal distribution approximation. By the already known
procedure we obtain
\begin{eqnarray*}
\P[Y\geq 110] &=& \P\left[ \frac{Y-100}{\sqrt{250/3}}\geq
\frac{10}{\sqrt{250/3}}\right] \doteq \P\left[ U\geq
\frac{10}{\sqrt{250/3}}\right]\\& =&
1-\Phi\left(\frac{10}{\sqrt{250/3}}\right) =1-0.863=0.137\, .
\end{eqnarray*}
\end{example}
\begin{example}
Before the election, there are 52% coalition supporters in the population. What is the
the probability that an opinion poll of size $n=1500$
of respondents will incorrectly show a preponderance of opposition?

Let $X$ denote the number of coalition supporters in the sample. If the selection was
was made independently, $X$ has a binomial distribution with parameters
$n=1500$ and $p=0.52$ and $np=780, np(1-p)=374.4$. Using the approximation
we get that the probability value we are looking for is approximately
$$
\P[X\leq 749] =
\P\left[\frac{X-780}{\sqrt{374.4}}\leq\frac{-31}{\sqrt{374.4}}\right]
\doteq \Phi(-1.602) = 1-\Phi(1.602) = 0.055\, .
$$
\end{example}

\begin{example}
Consider independent random variables $X_i\sim U(0,1)$ uniformly
uniformly distributed on the interval $(0,1)$. Since for this distribution
$$
\E X_i = \frac{1}{2},\quad \E X_i^2 = \int_0^1 x^2,\,dx =
\frac{1}{3} \qmq{a} \Var X_i = \frac{1}{3}-\frac{1}{4} =
\frac{1}{12}
$$
is obtained by applying the central limit theorem
$$
\frac{\sum_{i=1}^n X_i - \frac{n}{2}}{\sqrt{\frac{n}{12}}} \stL
N(0,1)\, .
$$
This limit relation can be used to construct a simple
random number generator from the $N(0,1)$ distribution. For example, for
$n=12$ according to the previous relation $U={sum_{i=1}^{12}X_i - 6$ has
approximately a standard normal distribution. Thus, it is sufficient to generate 12
numbers from the distribution $U(0,1)$ and add and subtract 6
to get a random number realization with a distribution close to
standard normal.
\end{example}


\begin{exercise}
The mass of the products has distribution $N(2000,16)$. What is the probability that the average weight of 8 randomly selected products is greater than 2002?
\end{exercise}

\begin{exercise}
We are interested in the proportion $p$ of people with blood type A in a given large population. For how many people do we need to find out whether or not they have type A in order to estimate $p$ with a probability greater than 90\% with an error of at most 0.05?
\end{exercise}



