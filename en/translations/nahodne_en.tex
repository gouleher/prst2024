\chapter{Relevant variables}

Throughout this chapter we will consider the probability space $(\Omega,\A,\P)$. However, the result of an experiment does not always have to be a number (the tram arrived-not arrived, the pictures on the cube, etc.). To process the results of such experiments mathematically, we assign a number to each result. This is the basic principle of random variables, it is the representation of $X:\Omega\map\RR$.

\begin{defin}
A random variable is a representation of $X:\Omega\map\RR$ such that for every $x\in\RR$
$$
X^{-1}((-\infty,x \rangle)= \{\omega\and\Omega|X(\omega)\leq x\} \and \A\, .
$$
(i.e., the patterns of all intervals $(-\infty,x\rangle$ are measurable (functions of $\P$) sets)
\end{defin}

\begin{example}
Suppose we roll two dice at the same time. Then the set of all possible outcomes has the form
$$
\Omega=\{(i,j)| i\in\wh 6, j\in \wh 6\mbox{a}\i\leq j\}\, .
$$
Let $\sigma$-algebra $\A$ be the system of all subsets of the set $\Omega$ and let the random variable $X$ express the sum of the results, i.e.
$$
X(i,j) = i+j\, .
$$
Then, for example
$$
X^{-1}((-\infty,1.2 \rangle)= \{\omega\and\Omega|X(\omega)\leq 1.2\} = \emptyset\, ,
$$
$$
\{\omega\and\Omega|X(\omega)\leq 3.1\} = \{(1,1),(1,2)\}\, ,
$$
because $X(1,1)=2$ and $X(1,2)=3$ and
$$
\³{³{omega\in\Omega|X(\omega)\leq 4\} =
\{(1,1),(1,2),(1,3),(2,2)\}\, .
$$
\end{example}
From now on, we will use the following notation in this text:
\begin{eqnarray*}
&& \{\omega\in\Omega|X(\omega)\leq x\} \triangleq [X\leq x] \subset \Omega\
&& \{\omega\and\Omega|X(\omega)< x\} \triangleq [X<x]\
&&\\{\omega\and\Omega|X(\omega)\geq x\} \triangleq [X\geq x]\
&&&{\{\omega\and\Omega|X(\omega)> x\} \triangleq [X >x]\
&&\{\{\omega\and\Omega|X(\omega)=x\} \triangleq [X=x]\
&&\{\{\omega\and\Omega|X(\omega)\and S\} \triangleq [X\and S]
\end{eqnarray*}
where $S$ is a subset of $\RR$. If $[X\in S]\in\A$ means that we can determine $\P[X\in S]$.

\begin{theorem}
Let $X,Y$ be random variables. Then
\begin{itemize}
  \item[1)] $X+Y$ is also a random variable,
  \item[2)] $kX$ is a random variable for all $k\in\RR$,
  \item[3)] $XY$ is a random variable,
  \item[4)] if in addition $[Y=0]=\emptyset$, then $X/Y$ is a random variable.
  \item[5)] If $g:\RR \map \RR$ is a continuous function, then $g(X)$ is a random variable.
\end{itemize}
\end{theorem}

\section{Distribution function of random variables}
One way to describe a random variable is a distribution function.
\begin{defin}
Let $X$ be a random variable. The function $F_X:\RR\map\RR$ defined for all $x \in\RR$ by
\begin{equation}\label{cdf}
    F_X(x) =\P[X\leq x]\, ,
\end{equation}
is called the distribution function of the random variable $X$.
\end{defin}

\begin{example}
Consider three coin flips. The set of all possible outcomes is
$$
\Omega={(H,H,H),(H,H,O),(H,O,H),(O,H,H),H,O,O),(O,H,O),(O,O,H),(O,O,O,O)\}, ,
$$
where $H$ means that the head fell and $O$ means that the eagle fell. Let the random variable $X$ denote the number of heads that fell. So, for example, $X(H,H,H)=3, X(H,O,O)=1,\ldots$ The distribution function of the random variable $X$ will be
\begin{center}
\renewcommand{\arraystretch}{1.2}
\begin{tabular}{lll}
for $x<0$ & $[X\leq x]=\emptyset$ & and $F_X(x)=0$
for $0\leq x<1$ & $[X\leq x]={(O,O,O)}$ & and $F_X(x)=\frac{1}{8}$
for $1\leq x<2$ & $[X\leq x]=\{(O,O,O),(O,O,H),(O,H,O),(H,O,O)\}$ & and $F_X(x)=\frac{1}{2}$
for $2\leq x<3$ & $[X\leq x]=\Omega\smallsetminus\{(H,H,H,H)\}$ & and $F_X(x) =\frac{7}{8}$
for $x\geq 3$ & $[X\leq x]=\Omega$ & and $F_X(x)=1$, \
\end{tabular}
\end{center}
see the following figure.
\end{example}

\begin{example}
Consider a random variable $X$ that describes the position of the large hand of a clock. This can be given by the angle by which it has moved from the origin. A probabilistic model describing this situation would then be
$$
\Omega = \langle 0.2\pi),\quad \A=\B(\langle 0.2\pi)),\quad \P(A) = \frac{1}{2\pi}\int_0^{2\pi}I_A(t)\, dt\, ,
$$
where $I_A(t)$ is the characteristic function of the set $A\and\A$, i.e.
$$
I_A(t)=
    \begin{cases}
        1 & \text{for } t\in A, \
        0 & \text{for } t\notin A.
    \end{cases}
$$
The distribution function of the random variable $X$ then takes the form
$$
F_X(x) = \P[X\leq x] = \frac{1}{2\pi}\int_0^{2\pi}I_{(-\infty,x\rangle}(t)\, dt =
  \begin{cases}
    0 & \text{for } x<0, \
    \fra{x}{2\pi} & \text{for } 0\leq x<2\pi, \
    1 & \text{for } x\geq 2\pi .
  \end{cases}
$$
\end{example}

The following theorem summarizes the basic properties of the distribution function.

\begin{theorem}
Let $X$ be a random variable and $F_X(x)$ its distribution function. Then
\begin{itemize}
  \item[1)] $0\leq F_X(x) \leq 1$ for all $x\in\RR$
  \item[2)] if $x_1<x_2$, then $F_X(x_1)\leq F_X(x_2)$ (i.e. $F_X$ is non-decreasing)
  \item[3)] $\lim_{x\to -\infty} F_X(x) = 0$
  \item[4)] $\lim_{x\to \infty} F_X(x) = 1$
  \item[5)] for all $a\in\RR$, $\lim_{x\to a_+} F_X(x) = F_X(a)$ (i.e. $F_X$ is right continuous)
  \item[6)] $F_X$ has at most countably many discontinuity points.
\end{itemize}
\end{theorem}

\begin{proof}
ad 1) For all $x\in\RR$, $0\leq \P[X\leq x]\leq 1$ holds and the statement follows from the definition of the distribution function.

ad 2) The random phenomenon $[X\leq x_2]$ can be written as a disjunctive union
$$
[X\leq x_2] = [X\leq x_1] + [x_1<X\leq x_2]
$$
and thus
$$
F_X(x_2) = \P[X\leq x_2] = \P[X\leq x_1] + \P[x_1<X\leq x_2] \geq \P[X\leq x_1 ] = F_X(x_1)\, .
$$

ad 4) According to the definition of limits we have to prove
$$
(\forall \ve >0)(\exists x_\ve\in\RR)(\forall x >x_\ve)(F_X(x)>1-\ve)\, .
$$
Let's choose $\ve>0$ fixed. Since $X$ is a random variable and $X(\omega)$ is finite for all $\omega\and\Omega$, we can decompose $\Omega$ into
$$
\Omega = [X\leq 0] + \sum_{k=1}^\infty [k-1<X\leq k]
$$
and after applying probability to this equation, we get
$$
1= \P[X\leq 0] + \sum_{k=1}^\infty \P[k-1<X\leq k]\, .
$$
But this means that the series of nonnegative terms on the right-hand side is convergent, and thus there exists $N_\ve\in\NN$ such that for all $n>N_\ve$
$$
\P[X\leq 0] + \sum_{k=1}^n \P[k-1<X\leq k]>1-\ve\, ,
$$
or for all $n>N_\ve$
$$
\P[X\leq n]>1-\ve\, .
$$
Let's choose $x_\ve = N_\ve+1$, so for this value $\P[X\leq x_\ve]>1-\ve$ and applying 2) we get for all $x>x_\ve$
$$
F_X(x) = \P[X\leq x]\geq \P[X\leq x_\ve]>1-\ve$, .
$$

ad 3) In this case we have to prove that
$$
(\forall \ve >0)(\exists x_\ve\in\RR)(\forall x <x_\ve)(F_X(x)<\ve)\, .
$$
The proof is done similarly to the previous point. Let's choose $\ve>0$ fixed and decompose $\Omega$ into
$$
\Omega = [X>0] + \sum_{k=0}^\infty [-(k+1)<X\leq -k]\, .
$$
Applying probability to this equation, we get
$$
1= \P[X> 0] + \sum_{k=0}^\infty \P[-(k+1)<X\leq -k]\, .
$$
But this means that there are $N_\ve\in\NN$ such that for all $n>N_\ve$
$$
\P[X>0] + \sum_{k=0}^n \P[-(k+1)<X\leq -k]>1-\ve\, ,
$$
which is equivalent to the expression
$$
\P[X\leq -(n+1)]<\ve\, .
$$
Let us choose $x_\ve = -( N_\ve+2)$, so for this value $\P[X\leq x_\ve]<\ve$ holds, and applying (2) we get for all $x<x_\ve$
$$
F_X(x) = \P[X\leq x]\leq \P[X\leq x_\ve]<\ve$, .
$$

ad 5) To prove this statement, we decompose the theorems into disjunctive unification phenomena
$$
[a<X\leq a+1] = \sum_{n=1}^\infty \left[a+\frac{1}{n+1}<X\leq a+\frac{1}{n}\right]\, .
$$
It is therefore true
$$
\P[a<X\leq a+1] = \sum_{n=1}^N \P \left[a+\frac{1}{n+1}<X\leq a+\frac{1}{n}\right]+Z_N \, ,
$$
where $Z_N$ is the residue converging to zero for $N\to\infty$. The last expression can be rewritten in the form
$$
F_X(a+1)-F_X(a) = \P[a+\frac{1}{N+1}<X\leq a+1]+Z_N = F_X(a+1) - F_X\left(a+\frac{1}{N+1}\right)$, ,
$$
which implies that
$$
F_X\left(a+\frac{1}{N+1}\right)=F_X(a) +Z_N \qmq{a therefore} \lim_{N\to\infty} F_X\left(a+\frac{1}{N+1}\right)=F_X(a)\, .
$$
The same limit for $x\to a_+$ follows from the monotonicity of the distribution function $F_X$.

ad 6) Let $S_n$ denote the set of points where the function $F_X(x)$ has a jump of size greater than or equal to $1/n$. The set of all discontinuity points $S$ of the function $F_X$ can then be expressed as
$$
S=\bigcup_{n=1}^\infty S_n\, .
$$
For every $n$, the set $S_n$ is finite, otherwise the values of the function $F_X$ would exceed one, which implies that the set $S$ is at most countable.
\³{proof}

\begin{remark}
If a real function satisfies properties 1),\ldots,5) of the previous theorem, then it is a distribution function of some random variable.
\end{remark}

\begin{remark}
Sometimes defined
$$
F_X(x) = \P[X<x]\, .
$$
Such a distribution function is then continuous from the left, the other properties remain valid.
\end{remark}

\begin{example}
The device contains 5 transistors. We assume that there is a failure caused by one transistor. The repairman looks at them one by one without returning them. Determine the distribution function of the random variable $X$ indicating the number of tests required.

The random variable $X$ can take values from the set ${1,2,3,4,5}$. The probabilities of each value are
$$
\P[X=1] = \frac{1}{5}, \quad \P[X=2] = \frac{4}{5}\cdot\frac{1}{4}=\frac{1}{5}, \quad \P[X=3] = \frac{4}{5}\cdot\frac{3}{4}\cdot\frac{1}{3}=\frac{1}{5},
$$

$$
\P[X=4] = \frac{4}{5}\cdot\frac{3}{4}\cdot\frac{2}{3}\cdot\frac{1}{2}=\frac{1}{5}, \quad \P[X=5] = \frac{4}{5}\cdot\frac{3}{4}\cdot\frac{2}{3}\cdot\frac{1}{2}\cdot 1 =\frac{1}{5}\, ,
$$
resulting in the appropriate values of the distribution function:
\begin{center}
\renewcommand{\arraystretch}{1.5}
\begin{tabular}{ll}
  $F_X(x) = 0$ & for $x<1$, \
  $F_X(x)=\P[X=1]=\frac{1}{5}$ & for $1\leq x<2$, \
  $F_X(x)=\P[X=1]+ \P[X=2]=\frac{2}{5}$ & for $2\leq x<3$, \
  $F_X(x)=\P[X=1]+\P[X=2]+\P[X=3] =\frac{3}{5}$ & for $3\leq x<4$, \
  $F_X(x)=\P[X=1]+\ldots+\P[X=4]=\frac{4}{5}$ & for $4\leq x<5$, \
  $F_X(x)=\P[X=1]+ \ldots+\P[X=5]=1$ & for $x\geq 5$. \\
\end{tabular}
\end {center}
{\em\large OBR.}
\end{example}

\begin{example}
The random variable $X$ is given by the distribution function
$$
F_X(x) =
  \begin{cases}
    0 & \text{for } x\leq 0 , \
    1-e^{-2x} & \text{for} x>0.
  \end{cases}
$$
Find $\P[1<X\leq 2], \P[-1<X\leq 2], \P[X=1]$.
$$
\P[1<X\leq 2] = \P[X\leq 2] - \P[X\leq 1]= F_X(2)-F_X(1) = 1-e^{-4}-1+e^{-2}=e^{-2}-e^{-4}\, .
$$

$$
\P[-1<X\leq 2] = F_X(2) - F_X(-1) = 1-e^{-4}-0 = 1-e^{-4}\, .
$$

Note that $\P[-1<X\leq 2] = \P[0<X\leq 2]$ which implies $\P[-1<X\leq 0]=0$.
$$
\P[X=1] = \P[X\leq 1]- \P[X<1] = F_X(1)-\lim_{this 1_-} F_X(t) = F_X(1)-F_X(1) =0\, .
$$
\end{example}

\begin{remark}
The example shows that the probability of a certain value will be non-zero only if the distribution function has a jump at that point.
\end{remark}

\section{Multivariate distribution functions}

We often measure several variables at once, e.g. temperature, pressure, humidity, or 90-60-90 measurements, etc. Each number is a random variable in itself, but to get a general idea we need to know all of them. Individual quantities can be independent, but they can also have a strong relationship, e.g. the diagonal of a square and its area, the average annual income and the amount of income. We will want to obtain aggregate information about both(all) variables, information about one of the variables conditional on the other (the other). We will be able to describe all this using a multivariate distribution function.

The vector of random variables $Z=(X_1,X_2,\ldots, X_n)$ will be called a random vector. For simplicity, we will define most of the terms for random vectors with two components; any extensions will be obvious.

\begin{defin}
Let $X, Y$ be random variables. Then their joint distribution function for all $(x,y)\in\RR^2$ is defined by
$$
F_{X,Y}(x,y) = \P([X\leq x][Y\leq y])\, .
$$
\end{defin}

\begin{remark}
Pooled distribution functions have similar properties to single variable distribution functions. Therefore, we only state some theorems and without proofs that are similar to the single variable case.
\end{remark}

\begin{theorem}
Let $F_{X,Y}$ be the joint distribution function of a random vector $(X,Y)$. Then for all $x_1\leq x_2$ and $y_1\leq y_2$
$$
F_{X,Y}(x_1,y_1)\leq F_{X,Y}(x_2,y_2)$, .
$$
\end{theorem}

\begin{theorem}
Let $F_{X,Y}$ be the joint distribution function of a random vector $(X,Y)$. Then
$$
\F_{X,Y}(x,y)=0 \qmq{for all} y\in\RR\, ,
$$
$$
\lim_{y\to -\infty} F_{X,Y}(x,y)=0 \qmq{for all} x\in\RR\, .
$$
\end{theorem}

\begin{theorem}
Let $F_{X,Y}$ be the joint distribution function of a random vector $(X,Y)$. Then
$$
\lim_{x}F_{X,Y}(x,y)=F_Y(y) \qmq{for all} y\in\RR\, ,
$$
$$
\lim_{y\to \infty} F_{X,Y}(x,y)=F_X(x) \qmq{for all} x\in\RR\, .
$$
\end{theorem}

\begin{remark}
The distribution functions $F_X,F_Y$ from the previous theorem will be called {{our marginal distribution function} of the random vector $(X,Y)$.  More generally, if $Z'=(X_1,X_2,\ldots, X_n)$ is a random vector, we call the vector $Z'=(X_{i_1},X_{i_2},\ldots, X_{i_k})$, for $k<n$, {{only marginal random vector}, its distribution a marginal distribution, and the special random variable $X_i$ a {{only marginal random variable}.}
\end{remark}

\begin{defin}
Random variables $X$ and $Y$ are called (statistically) independent if they are independent phenomena
$$
[a<X\leq b] \qmq{a} [c<Y\leq d]
$$
for any pair $(a,b), (c,d)$, where $a,b,c,d\in\overline\RR$ and $a\leq b, c\leq d$.
\end{defin}

\begin{theorem}\label{nez}
The random variables $X,Y$ are independent if and only if for each pair $(x,y)\in\RR^2$ the equality
$$
F_{X,Y}(x,y) = F_X(x)\cdot F_Y(y)\, .
$$
(The value of the joint distribution function is equal to the product of the values of the marginal distribution functions.)
\end{theorem}

\begin{proof}
$\Rightarrow:$ Let $X,Y$ be independent, i.e. for all $a,b,c,d\in\overline\RR$, $a\leq b, c\leq d$
$$
\P([a<X\leq b][c<Y\leq d]) = \P[a<X\leq b]\cdot \P[c<Y\leq d]\, .
$$
Let's put $a=-\infty, b=x, c=-\infty, d=y$. Then, for all $(x,y)\in\RR^2$
\begin{eqnarray*}
F_{X,Y}(x,y) &=& \P([-\infty<X\leq x][-\infty<Y\leq y]) \\
             &=& \P[-\infty<X\leq x]\cdot \P[-\infty<Y\leq y]=F_X(x)\cdot F_Y(y)\, .
\end{eqnarray*}

$\Leftarrow:$
\begin{eqnarray*}
&&\P([a<X\leq b][c<Y\leq d]) = F_{X,Y}(b,d)-F_{X,Y}(b,c)-F_{X,Y}(a,d)+F_{X,Y}(a,c)\
&&= F_X(b)F_Y(d) - F_X(b)F_Y(c)-F_X(a)F_Y(d)+F_X(a)F_Y(c)\
&&= (F_X(b)-F_X(a))(F_Y(d)-F_Y(c)) = \P[a<X\leq b]\cdot \P[c<Y\leq d]\, .
\end{eqnarray*}
\end{proof}

\begin{example}
The random vector $(X,Y)$ is given by the probabilities $\P[X=x,Y=y]$ given in the table
\begin{center}
\begin{tabular}{c|ccc}
  $y\setminus x$ & 2 & 5 & 8 \\ \hline
 0.4 & 0.15 & 0.30 & 0.35 \\
  0.8 & 0.05 & 0.12 & 0.03\\
\end{tabular}
\end{center}

Determine the joint and corresponding marginal distribution functions and
Decide whether the random variables $X,Y$ are independent.

The values of the pooled distribution function can be seen in the following
Figure \ref{cdf1}.

\begin{figure}[h]
\centering
\includegraphics[width=8cm]{cdf1.eps}
\caption{Values of distribution function $F_{X,Y}(x,y)$.} \label{cdf1}
\end{figure}

The marginal distribution functions are therefore of the form
$$
F_X(x) = \lim_{y\to\infty}F_{X,Y}(x,y) =
  \begin{cases}
    0 & \text{for } x<2, \
    0.2 & \text{for } 2\leq x<5,\
    0.62 & \text{for } 5\leq x <8,\
    1 & \text{for } x \geq 8,\
  \end{cases}
$$
a
$$
F_Y(y) = \lim_{x\to\infty}F_{X,Y}(x,y) =
  \begin{cases}
    0 & \text{for } y<0.4, \
    0.8 & \text{for } 0.4\leq y<0.8,\
    1 & \text{for } y \geq 0.8.\
  \end{cases}
$$
For example, since for $x=3$ and $y=0.5$ $F_{X,Y}(x,y) = F_X(x)\cdot F_Y(y)$, the quantities $X$ and $Y$ are not independent.
\end{example}
