$pdflatex = 'xelatex -file-line-error %O %S';
$clean_ext = 'synctex.gz synctex.gz(busy) run.xml fdb_latexmk';
$aux_dir = '.cache';
$emulate_aux = 1;
