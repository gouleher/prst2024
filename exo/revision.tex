\documentclass[11pt,oneside]{amsart}

%%% PACKAGES %%%
\usepackage[svgnames]{xcolor}
\usepackage{amsmath,amssymb,amsthm,booktabs,csquotes,microtype,graphicx,tikz,fancyhdr}
\usepackage{fontspec}
\usepackage{mathspec}
\usepackage{diagbox}
\usepackage[sf]{titlesec}
\usepackage{geometry}
\usepackage[inline]{enumitem}
\usepackage[draft=false,hidelinks]{hyperref}
\usepackage[round]{natbib}
\usepackage{multirow}
\usepackage{cleveref}

\renewcommand*{\bar}{\overline}

\graphicspath{{figures/}}

\makeatletter
\renewcommand\@pnumwidth{4ex}
\makeatother

\setmainfont[Ligatures = TeX]{Noto Serif}
\setmathsfont(Digits,Latin,Greek)[Ligatures = TeX]{Noto Serif}
\setmathrm[Ligatures = TeX]{Noto Serif}
\setmathsf[Ligatures = TeX]{Noto Sans}
\setsansfont[Ligatures = TeX]{Technika}

\newfontfamily{\logofont}[ItalicFont=*,BoldFont=*,BoldItalicFont=*]{LogoCVUT-Regular}

\usetikzlibrary{arrows,positioning,cd} 

%%% MACROS %%%
\newcommand*{\A}{\mathcal{A}} 
\newcommand*{\B}{\mathcal{B}} 
\newcommand*{\X}{\mathcal{X}} 
\newcommand*{\nn}{\mathbb{N}} 
\newcommand*{\zz}{\mathbb{Z}} 
\newcommand*{\qq}{\mathbb{Q}} 
\newcommand*{\rr}{\mathbb{R}} 
\newcommand*{\RR}{\rr} 
\newcommand*{\CC}{\cc} 
\newcommand*{\NN}{\nn} 
\newcommand*{\cc}{\mathbb{C}} 
\renewcommand*{\complement}{\mathbf{c}}
\renewcommand*{\emptyset}{\varnothing}
\newcommand{\defeq}{\stackrel{\text{def}}{=}}

\newcommand*{\stL}{\stackrel{L}{\to}}
\newcommand*{\stP}{\stackrel{P}{\to}}
\newcommand*{\stas}{\stackrel{a.s.}{\to}}
\newcommand*{\stUL}{\stackrel{UL}{\to}}
\newcommand*{\wh}{\widehat}
%
\newcommand*{\isom}{\cong} 
\newcommand*{\from}{\colon} 
\newcommand*{\id}{\mathrm{id}}  
\newcommand*{\ve}{\varepsilon}
\newcommand{\qmq}[1]{\quad\mbox{#1}\quad}
\newcommand{\bm}[1]{\ensuremath{\mathbf{#1}}}

\newcommand{\ol}{\overline}

%%% OPERATORS %%%
\newcommand*{\argmax}{\operatorname{argmax}}
\newcommand*{\E}{\mathop{\mathsf{E}{\kern0pt}}}
\newcommand*{\geometric}{\mathop{\mathrm{Geo}\kern0pt}\nolimits}
\newcommand*{\Var}{\mathop{\mathsf{Var}{\kern0pt}}}
\newcommand*{\Cov}{\mathop{\mathsf{Cov}{\kern0pt}}}
\newcommand*{\sd}{\mathop{\sigma{\kern0pt}}}
\renewcommand*{\P}{\mathop{\mathsf{P}\kern0pt}\nolimits}
\newcommand*{\uniform}{\mathop{\mathrm{U}\kern0pt}\nolimits}
\newcommand*{\normal}{\mathop{\mathrm{N}\kern0pt}\nolimits}

%%% THEOREMS %%%

\frenchspacing
\raggedbottom

%%% BODY %%%
\begin{document}

\begin{center}
    \sffamily 

    \vspace{1em}

    Probability and Mathematical statistics\par
    \bigskip

    \LARGE
    Exercises -- revision for final test

    \vspace{2em}

\end{center}

\begin{enumerate}[wide,label={\textbf{\arabic{*}.}},itemsep=1em]
    \item Suppose that a random variable Y is normally distributed. We want to estimate its expected value using a 95\% confidence interval of width equal to 1. From previous measurements we observed that $\sigma^2 = 4$ and take this value as known and fixed.
    \begin{enumerate}[label={\alph*)}]
        \item Estimate how large a sample do we need.
        \item How does the needed number of observations change if we need the width of the interval to be 0.1?
        \item Do we need a larger sample size if we change the level of confidence to 99\%?
    \end{enumerate}

    \item Suppose we observe a random sample of $n = 16$ values from a normal distribution. The sample mean and sample variance are $\ol X_{16} = 10.3$ and $s^2_{16} = 1.2$.
    \begin{enumerate}[label={\alph*)}]
        \item Find the two-sided interval estimate of the expected value with confidence level of 90\%.
        \item Find the two-sided interval estimate of the variance with confidence level of 90\%.
        \item Compare these with the one-sided interval estimates for the expected value and the variance calculated with a confidence level of $95\%$. What do you notice?
    \end{enumerate}

    \item Suppose we observe a random sample of $n = 16$ values from a normal distribution. The sample mean and sum of second powers are
        \begin{equation*}
            \overline X_{16} = 13 \qmq{and} \sum_{i=1}^{16} = 2708.
        \end{equation*}
        \begin{enumerate}[label={\alph*)}]
            \item Test the hypothesis $H_0 : \mu = 15$ against $H_1 : \mu \neq 15$ at levels of significance of 10\%, 5\%, and 1\%.
            \item Test the hypothesis $H_0 : \sigma^2 = 0.5$ against $H_1 : \sigma^2 \neq 0.5$ at levels of significance of 10\%, 5\%, and 1\%.
    \end{enumerate}

    \item A car manufacturer claims that the average fuel consumption of a new car model is 6 liters per l00 kilometers.
    After testing $n=20$ cars, the average consumption was found to be $\ol X_n = 6.8$ liters per 100 kilometers with
    a sample variance of $s^2_n = 2.56$.
    \begin{enumerate}[label = {\alph*)}]
        \item At a level of significance $\alpha = 5\%$, test whether the manufacturer’s statement is true or whether the actual consumption is higher.
        \item Repeat for $\alpha = 1\%$.
    \end{enumerate}

    \item A computer was used to generate four random numbers from a normal distribution with some fixed expectation $\mu_1$ and variance $\sigma^2$,
        \begin{equation*}
            1.1650, .6268, .0751, .3516.
        \end{equation*}
        Five more random normal numbers with the same variance $\sigma^2$ but perhaps a different expectation $\mu_2$ were then generated (the mean may or may not actually be different),
    \begin{equation*}
        .3035, 2.6961, 1.0591, 2.7971, 1.2641.
    \end{equation*}
    \begin{enumerate}[label={\alph*)}]
        \item Assuming $\sigma^2=1$, do you think the two random number generators had the same expectation?
        \item Answer the question again without assuming that $\sigma^2=1$. Does your answer change?
\end{enumerate}

    \item In an experiment, two different methods were used to determine the latent heat of fusion of ice. The investigators wanted to find out whether or not the methods differed significantly. The following table gives the change in total heat from ice at -$0.72^\circ$C to water at $0^\circ$C.
        \begin{center}
            \medskip
            \begin{tabular}{cc}
                \toprule
                method 1 & method 2 \\
                \midrule
                79.98 & 80.02 \\
                80.04 & 79.94 \\
                80.02 & 79.98 \\
                80.04 & 79.97 \\
                80.03 & 79.97 \\
                80.03 & 80.03 \\
                80.04 & 79.95 \\
                79.97 & 79.97 \\
                80.05 &       \\
                80.03 &       \\
                80.02 &       \\
                80.00 &       \\
                80.02 &       \\
                \bottomrule
            \end{tabular}
            \medskip
        \end{center}
        Test whether the two methods are different at a level of significance of 0.05.
\end{enumerate}

\end{document}
