\documentclass[11pt,oneside]{amsart}

%%% PACKAGES %%%
\usepackage[svgnames]{xcolor}
\usepackage{amsmath,amssymb,amsthm,booktabs,csquotes,microtype,graphicx,tikz,fancyhdr}
\usepackage{fontspec}
\usepackage{mathspec}
\usepackage{diagbox}
\usepackage[sf]{titlesec}
\usepackage{geometry}
\usepackage[inline]{enumitem}
\usepackage[draft=false,hidelinks]{hyperref}
\usepackage[round]{natbib}
\usepackage{multirow}
\usepackage{cleveref}

\renewcommand*{\bar}{\overline}

\graphicspath{{figures/}}

\makeatletter
\renewcommand\@pnumwidth{4ex}
\makeatother

\setmainfont[Ligatures = TeX]{Noto Serif}
\setmathsfont(Digits,Latin,Greek)[Ligatures = TeX]{Noto Serif}
\setmathrm[Ligatures = TeX]{Noto Serif}
\setmathsf[Ligatures = TeX]{Noto Sans}
\setsansfont[Ligatures = TeX]{Technika}

\newfontfamily{\logofont}[ItalicFont=*,BoldFont=*,BoldItalicFont=*]{LogoCVUT-Regular}

\usetikzlibrary{arrows,positioning,cd} 

%%% MACROS %%%
\newcommand*{\A}{\mathcal{A}} 
\newcommand*{\B}{\mathcal{B}} 
\newcommand*{\X}{\mathcal{X}} 
\newcommand*{\nn}{\mathbb{N}} 
\newcommand*{\zz}{\mathbb{Z}} 
\newcommand*{\qq}{\mathbb{Q}} 
\newcommand*{\rr}{\mathbb{R}} 
\newcommand*{\RR}{\rr} 
\newcommand*{\CC}{\cc} 
\newcommand*{\NN}{\nn} 
\newcommand*{\cc}{\mathbb{C}} 
\renewcommand*{\complement}{\mathbf{c}}
\renewcommand*{\emptyset}{\varnothing}
\newcommand{\defeq}{\stackrel{\text{def}}{=}}

\newcommand*{\stL}{\stackrel{L}{\to}}
\newcommand*{\stP}{\stackrel{P}{\to}}
\newcommand*{\stas}{\stackrel{a.s.}{\to}}
\newcommand*{\stUL}{\stackrel{UL}{\to}}
\newcommand*{\wh}{\widehat}
%
\newcommand*{\isom}{\cong} 
\newcommand*{\from}{\colon} 
\newcommand*{\id}{\mathrm{id}}  
\newcommand*{\ve}{\varepsilon}
\newcommand{\qmq}[1]{\quad\mbox{#1}\quad}
\newcommand{\bm}[1]{\ensuremath{\mathbf{#1}}}

\newcommand{\ol}{\overline}

%%% OPERATORS %%%
\newcommand*{\argmax}{\operatorname{argmax}}
\newcommand*{\E}{\mathop{\mathsf{E}{\kern0pt}}}
\newcommand*{\geometric}{\mathop{\mathrm{Geo}\kern0pt}\nolimits}
\newcommand*{\Var}{\mathop{\mathsf{Var}{\kern0pt}}}
\newcommand*{\Cov}{\mathop{\mathsf{Cov}{\kern0pt}}}
\newcommand*{\sd}{\mathop{\sigma{\kern0pt}}}
\renewcommand*{\P}{\mathop{\mathsf{P}\kern0pt}\nolimits}
\newcommand*{\uniform}{\mathop{\mathrm{U}\kern0pt}\nolimits}
\newcommand*{\normal}{\mathop{\mathrm{N}\kern0pt}\nolimits}

%%% THEOREMS %%%

\frenchspacing
\raggedbottom

%%% BODY %%%
\begin{document}

\begin{center}
    \sffamily 

    \vspace{1em}

    Probability and Mathematical statistics\par
    \bigskip

    \LARGE
    Exercises -- week 11

    \vspace{2em}

\end{center}

\begin{enumerate}[wide,label={\textbf{\arabic{*}.}},itemsep=1em]

\item Suppose that $X_1,\dots,X_{64}$ are independent and identically distributed random variables with $\E[X_i] = 6$ and $\Var(X_i) = 4$.
    \begin{enumerate}[label={\alph*)}]
        \item Estimate the probabilities $\P[\bar X_{64} \leq 6.5]$ and  $\P[5.2 < \bar X_{64} < 6.9]$.
        \item Find a constant $c$ such that $\P[\bar X_{64}<c] \approx 0.36$ and $\P[6-a<\bar X_{64}<6+a]=0.73$.
    \end{enumerate}

\item The masses of randomly selected objects follow a $\normal(2000,16)$. What is the probability that the average weight of 8 randomly selected objects is greater than 2002?
    
\item We are interested in the proportion $p$ of people with blood type A in a given population. In order to estimate this proportion, we select $n$ people at random and test their blood type. How many people do we need in order to estimate $p$ within 0.05\% with a probability greater than 90\%?

\item Suppose we observe a random sample of 10 pairs $(X_i,Y_i)$ with the following results:
    \begin{equation*}
        \sum_{i=1}^{10} X_i = 10,\quad \sum_{i=1}^{10} X_i^2=15,\quad 
        \sum_{i=1}^{10} Y_i = 4,\quad \sum_{i=1}^{10} Y_i^2=7,\quad 
        \sum_{i=1}^{10} X_iY_i = 6.
    \end{equation*}

    \begin{enumerate}[label={\alph*)}]
        \item Find the sample means and variances for $(X_i)_{i=1}^n$ and $(Y_i)_{i=1}^n$.
        \item Calculate the sample covariance,
            \begin{equation*}
                s_{X,Y}^2 = \frac{1}{n-1}\sum_{i=1}^n(X_i-\bar X_i)(Y_i-\bar Y_i).
            \end{equation*}
        \item Estimate the expectation of $Z = X+Y^2$.
    \end{enumerate}

    \item Let $X_1,\ldots,X_n$ be a random sample from a distribution with density
    \begin{equation*}
        f_{X_i}(x_i) =
        \begin{cases}
            \frac{1}{\theta} e^{-\frac{x_i-\mu}{\theta}} & \text{for } x_i>\mu, \\
            0 & \text{otherwise},
        \end{cases}
    \end{equation*}
    where $\theta>0,\mu\in\RR$. Use the method of moments to estimate the parameters $\theta,\mu$.

    \item Let $X_1,\ldots,X_n$ be a random sample from a distribution with density
    \begin{equation*}
        f_{X_i}(x_i) = 
        \begin{cases}
            (\theta+1)x_i^\theta & \text{for } 0\leq x_i\leq 1\\
            0 & \text{otherwise}.
        \end{cases}
    \end{equation*}
    Find the maximum likelihood estimate of $\theta$.

    \item Consider a random sample $X_1,\ldots,X_n$ from a distribution with density
    \begin{equation*}
        f(x,\theta) = 
        \begin{cases}
            3\theta^3 x^{-4} & \text{for } x>\theta\\
            0 & \text{otherwise}
        \end{cases}
    \end{equation*}
    where $\theta>0$.  
    \begin{enumerate}[label={\alph{*})}]
        \item Find the maximum likelihood estimate $\widehat\theta$ of the parameter $\theta$ and decide whether it is unbiased.
        \item Find an estimate $\theta^\ast$ of the parameter $\theta$ by the method of moments and decide whether it is unbiased.
        \item Calculate the variances of the estimates of $\widehat\theta$ and $\theta^\ast$ and compare their rate of convergence to 0 as $n\to\infty$.
    \end{enumerate}
\end{enumerate}

\end{document}
