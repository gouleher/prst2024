\documentclass[11pt,oneside]{amsart}

%%% PACKAGES %%%
\usepackage[svgnames]{xcolor}
\usepackage{amsmath,amssymb,amsthm,booktabs,csquotes,microtype,graphicx,tikz,fancyhdr}
\usepackage{fontspec}
\usepackage{mathspec}
\usepackage{diagbox}
\usepackage[sf]{titlesec}
\usepackage{geometry}
\usepackage[inline]{enumitem}
\usepackage[draft=false,hidelinks]{hyperref}
\usepackage[round]{natbib}
\usepackage{multirow}
\usepackage{cleveref}

\renewcommand*{\bar}{\overline}

\graphicspath{{figures/}}

\makeatletter
\renewcommand\@pnumwidth{4ex}
\makeatother

\setmainfont[Ligatures = TeX]{Noto Serif}
\setmathsfont(Digits,Latin,Greek)[Ligatures = TeX]{Noto Serif}
\setmathrm[Ligatures = TeX]{Noto Serif}
\setmathsf[Ligatures = TeX]{Noto Sans}
\setsansfont[Ligatures = TeX]{Technika}

\newfontfamily{\logofont}[ItalicFont=*,BoldFont=*,BoldItalicFont=*]{LogoCVUT-Regular}

\usetikzlibrary{arrows,positioning,cd} 

%%% MACROS %%%
\newcommand*{\A}{\mathcal{A}} 
\newcommand*{\B}{\mathcal{B}} 
\newcommand*{\X}{\mathcal{X}} 
\newcommand*{\nn}{\mathbb{N}} 
\newcommand*{\zz}{\mathbb{Z}} 
\newcommand*{\qq}{\mathbb{Q}} 
\newcommand*{\rr}{\mathbb{R}} 
\newcommand*{\RR}{\rr} 
\newcommand*{\CC}{\cc} 
\newcommand*{\NN}{\nn} 
\newcommand*{\cc}{\mathbb{C}} 
\renewcommand*{\complement}{\mathbf{c}}
\renewcommand*{\emptyset}{\varnothing}
\newcommand{\defeq}{\stackrel{\text{def}}{=}}

\newcommand*{\stL}{\stackrel{L}{\to}}
\newcommand*{\stP}{\stackrel{P}{\to}}
\newcommand*{\stas}{\stackrel{a.s.}{\to}}
\newcommand*{\stUL}{\stackrel{UL}{\to}}
\newcommand*{\wh}{\widehat}
%
\newcommand*{\isom}{\cong} 
\newcommand*{\from}{\colon} 
\newcommand*{\id}{\mathrm{id}}  
\newcommand*{\ve}{\varepsilon}
\newcommand{\qmq}[1]{\quad\mbox{#1}\quad}
\newcommand{\bm}[1]{\ensuremath{\mathbf{#1}}}

\newcommand{\ol}{\overline}

%%% OPERATORS %%%
\newcommand*{\argmax}{\operatorname{argmax}}
\newcommand*{\E}{\mathop{\mathsf{E}{\kern0pt}}}
\newcommand*{\geometric}{\mathop{\mathrm{Geo}\kern0pt}\nolimits}
\newcommand*{\Var}{\mathop{\mathsf{Var}{\kern0pt}}}
\newcommand*{\Cov}{\mathop{\mathsf{Cov}{\kern0pt}}}
\newcommand*{\sd}{\mathop{\sigma{\kern0pt}}}
\renewcommand*{\P}{\mathop{\mathsf{P}\kern0pt}\nolimits}
\newcommand*{\uniform}{\mathop{\mathrm{U}\kern0pt}\nolimits}
\newcommand*{\normal}{\mathop{\mathrm{N}\kern0pt}\nolimits}

%%% THEOREMS %%%

\frenchspacing
\raggedbottom

%%% BODY %%%
\begin{document}

\begin{center}
    \sffamily 

    \vspace{1em}

    Probability and Mathematical statistics\par
    \bigskip

    \LARGE
    Exercises -- week 12

    \vspace{2em}

\end{center}

\begin{enumerate}[wide,label={\textbf{\arabic{*}.}},itemsep=1em]

    \item Let $X_1,\ldots,X_n$ be a random sample from a distribution with density
    \begin{equation*}
        f_{X_i}(x_i) =
        \begin{cases}
            \frac{1}{\theta} e^{-\frac{x_i-\mu}{\theta}} & \text{for } x_i>\mu, \\
            0 & \text{otherwise},
        \end{cases}
    \end{equation*}
    where $\theta>0,\mu\in\RR$. Use the method of moments to estimate the parameters $\theta,\mu$.

    \item Let $X_1,\ldots,X_n$ be a random sample from a distribution with density
    \begin{equation*}
        f_{X_i}(x_i) = 
        \begin{cases}
            (\theta+1)x_i^\theta & \text{for } 0\leq x_i\leq 1\\
            0 & \text{otherwise}.
        \end{cases}
    \end{equation*}
    Find the maximum likelihood estimate of $\theta$.

    \item Consider a random sample $X_1,\ldots,X_n$ from a distribution with density
    \begin{equation*}
        f(x,\theta) = 
        \begin{cases}
            3\theta^3 x^{-4} & \text{for } x>\theta\\
            0 & \text{otherwise}
        \end{cases}
    \end{equation*}
    where $\theta>0$.  
    \begin{enumerate}[label={\alph{*})}]
        \item Find the maximum likelihood estimate $\widehat\theta$ of the parameter $\theta$ and decide whether it is unbiased.
        \item Find an estimate $\theta^\ast$ of the parameter $\theta$ by the method of moments and decide whether it is unbiased.
        \item Calculate the variances of the estimates of $\widehat\theta$ and $\theta^\ast$ and compare their rate of convergence to 0 as $n\to\infty$.
    \end{enumerate}

    \item During World War II, the Nazis used V2 missiles to target London in retaliation for allied bombing. The southern part of London was divided into squares of 0.25~km$^2$ and the number of missiles which landed in each area was recorded over some period of time. The next table presents the number of areas sorted by number of recorded missile hits (so for instance 93 areas recorded 2 hits). 
    \begin{center}
        \begin{tabular}{*{8}c}
            \toprule
            \# missiles & 0 & 1 & 2 & 3 & 4 & 5 or more & total \\ 
            \midrule
            \# areas & 229 & 211 & 93 & 35 & 7 & 1 & 576 \\ 
            \bottomrule
        \end{tabular}
    \end{center}
    At a significance level of 5\%, test whether this is a Poisson distribution.

    \item The number of defective connections on printed circuit boards coming out of a factory is thought to follow a Poisson distribution. 60 circuit boards were examined and the following data was observed.
    \begin{center}
        \begin{tabular}{*{5}c}
            \toprule
            \# defective connections & 0 & 1 & 2 & 3 \\ 
            \midrule
            \# circuit boards & 32 & 15 & 9 & 4 \\
            \bottomrule
        \end{tabular}
    \end{center}
    At a significance level of 5\% test whether this really is a Poisson distribution.

    \item 90 people were enrolled in a weight loss program. The following table shows the weight lost by the participants after 3 months of participation in the program. At a significance level of 5\%, test whether the data are normally distributed. The sample mean and sample variance of the original data are $\overline x_n = 3.74$ and $s_n^2 = 4.84$.
    \begin{center}
        \begin{tabular}{*{11}c}
            \toprule
            Weight loss (kg) & 0 & 1 & 2 & 3 & 4 & 5 & 6 & 7 & 8 & over 8 \\
            \midrule
            \# of participants & 4 & 5 & 10 & 13 & 18 & 16 & 9 & 9 & 4 & 2 \\
            \bottomrule
        \end{tabular}
    \end{center}
\end{enumerate}

\end{document}
