\documentclass[11pt]{amsart}

%%% PACKAGES %%%
\usepackage[svgnames]{xcolor}
\usepackage{amsmath,amssymb,amsthm,booktabs,csquotes,microtype,graphicx,tikz,fancyhdr}
\usepackage{fontspec}
\usepackage{mathspec}
\usepackage{diagbox}
\usepackage[sf]{titlesec}
\usepackage{geometry}
\usepackage[inline]{enumitem}
\usepackage[draft=false,hidelinks]{hyperref}
\usepackage[round]{natbib}
\usepackage{multirow}
\usepackage{cleveref}

\graphicspath{{figures/}}

\makeatletter
\renewcommand\@pnumwidth{4ex}
\makeatother

\setmainfont[Ligatures = TeX]{Noto Serif}
\setmathsfont(Digits,Latin,Greek)[Ligatures = TeX]{Noto Serif}
\setmathsf[Ligatures = TeX]{Noto Sans}
\setsansfont[Ligatures = TeX]{Technika}

\newfontfamily{\logofont}[ItalicFont=*,BoldFont=*,BoldItalicFont=*]{LogoCVUT-Regular}

\usetikzlibrary{arrows,positioning,cd} 

%%% MACROS %%%
\newcommand*{\A}{\mathcal{A}} 
\newcommand*{\B}{\mathcal{B}} 
\newcommand*{\X}{\mathcal{X}} 
\newcommand*{\nn}{\mathbb{N}} 
\newcommand*{\zz}{\mathbb{Z}} 
\newcommand*{\qq}{\mathbb{Q}} 
\newcommand*{\rr}{\mathbb{R}} 
\newcommand*{\RR}{\rr} 
\newcommand*{\CC}{\cc} 
\newcommand*{\NN}{\nn} 
\newcommand*{\cc}{\mathbb{C}} 
\renewcommand*{\complement}{\mathbf{c}}
\renewcommand*{\emptyset}{\varnothing}
\newcommand{\defeq}{\stackrel{\text{def}}{=}}

\newcommand*{\stL}{\stackrel{L}{\to}}
\newcommand*{\stP}{\stackrel{P}{\to}}
\newcommand*{\stas}{\stackrel{a.s.}{\to}}
\newcommand*{\stUL}{\stackrel{UL}{\to}}
\newcommand*{\wh}{\widehat}
%
\newcommand*{\isom}{\cong} 
\newcommand*{\from}{\colon} 
\newcommand*{\id}{\mathrm{id}}  
\newcommand*{\ve}{\varepsilon}
\newcommand{\qmq}[1]{\quad\mbox{#1}\quad}
\newcommand{\bm}[1]{\ensuremath{\mathbf{#1}}}

\newcommand{\ol}{\overline}

%%% OPERATORS %%%
\newcommand*{\argmax}{\operatorname{argmax}}
\newcommand*{\E}{\mathop{\mathsf{E}{\kern0pt}}}
\newcommand*{\Var}{\mathop{\mathsf{Var}{\kern0pt}}}
\newcommand*{\Cov}{\mathop{\mathsf{Cov}{\kern0pt}}}
\newcommand*{\sd}{\mathop{\sigma{\kern0pt}}}
\renewcommand*{\P}{\mathop{\mathsf{P}\kern0pt}\nolimits}

%%% THEOREMS %%%

\frenchspacing
\raggedbottom


%%% BODY %%%
\begin{document}

\begin{center}
    \sffamily 

    \vspace{1em}

    Probability and Mathematical statistics\par
    \bigskip

    \Large
    Exercises -- week 2

    \vspace{2em}

\end{center}

\subsection*{Mathematical model of probability}

\begin{enumerate}[wide,label={\textbf{\arabic{*}.}},itemsep=1em]
    \item In the experiment consisting of three fair coin flips, we consider the events: 
    \begin{enumerate}[label={$\Alph{*}$:}]
        \item {the first two tosses are tails}, 
        \item {at least one toss is a head}, 
        \item {at most one toss is a head}, 
        \item {all three tosses have the same result},
        \item {no two consecutive tosses have the same result}.
    \end{enumerate}
    \bigskip

    \begin{enumerate}[label={\alph{*})}]
        \item Give a model for this experiment in Kolmogorov's axioms.
        \item Write down the list of favourable outcomes for each of the events.
        \item Calculate their union and intersection.
        \item Find all disjointness and implication relations between these events.
    \end{enumerate}

    \item Prove by induction the general form of the inclusion exclusion principle:
    \begin{equation*}
        \P(A_1\cup A_2\dots\cup A_n) = \sum_{k=1}^n(-1)^{k+1}\sum_{1\leq i_1<i_2\dots<i_k\leq n}\P(A_{i_1}\cap A_{i_2}\cap\dots\cap A_{i_j}).
    \end{equation*}

    \item At a wedding dinner, each of the $n$ guests have been assigned their own seat. However during the course of dinner people move about the room, so that by the end of the night the place where each guest is sitting is completely random. Show that the probability that nobody sits in their initially assigned seat by the end of the night is
    \begin{equation*}
        \sum_{i=0}^n\frac{(-1)^i}{i!}.
    \end{equation*}
    Hint: use the inclusion-exclusion principle. What is the limit of this probability as $n\to\infty$?

    \item Using the Axioms of probability prove that $\mathcal B(\rr)$ contains
    \begin{enumerate}[label = {\alph{*})}]
        \item all intervals $[a,b]$, $[a,b)$, $(a,b)$ and
        \item all half lines $(-\infty,b)$, $(-\infty,b]$, $(a,\infty)$, $[a,\infty)$.
    \end{enumerate}

    \item Show that the set $\qq\cap[0,1]$ is Borel. What is the probability that a random number $\omega\in[0,1]$ is rational?

\end{enumerate}

\pagebreak

\subsection*{Conditional probability}

\begin{enumerate}[wide,label={\textbf{\arabic{*}.}},itemsep=1em]

    \item Three archers have an average accuracy of 4/5, 3/4 and 2/3 respectively. They take turn shooting at a target. Knowing that only two arrows hit the target, what is the probability that it was the third archer who missed? % [6/13]

    \item 10 women and 5 men are randomly divided into five groups of 3. Determine the probability that each group contains a man. % [81/1001]

    \item Five urns each contain $b$ white balls and $c$ black balls. A ball is drawn at random from the first urn and placed in the second. Then a ball is drawn at random from the second and placed in the third, and so on until a ball has been placed in the fifth urn. Finally, one ball is drawn from the fifth urn. What is the probability that this ball is white? % [$b/(b+c)$]

    \item Three urns contain blue, white and red balls where 
    \begin{itemize}
        \item the first contains 1 white, 2 black and 3 blue, 
        \item the second contains 2 white, 1 black and 1 blue,
        \item the third contains, 4 white, 5 black and 3 blue. 
    \end{itemize}
    An urn is chosen at random and then one ball is drawn from it. If that ball is white, what is the probability that the urn chosen was the first one? What about the second and third? % [$1/6$,$1/2$,$1/3$]

    \item A product has a 10\% chance of having a manufacturing defect. Defective products have a 50\% chance of failure during the warranty period, compared to 1\% chance of failure otherwise. Determine the probability that a product that experienced a failure during the warranty period was in fact defective. % [50/59]
    
    \item You want to buy a car of a certain brand, however you know that 30\% of this brand's cars have a faulty transmission. To inspect the car before purchasing it, you hire a mechanic who is able to estimate its condition with 99\% accuracy. What is the probability that the car you want to buy has a faulty transmission if the mechanic predicted that it doesn't?
\end{enumerate}

\end{document}
