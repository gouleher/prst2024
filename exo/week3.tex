\documentclass[11pt,oneside]{amsart}

%%% PACKAGES %%%
\usepackage[svgnames]{xcolor}
\usepackage{amsmath,amssymb,amsthm,booktabs,csquotes,microtype,graphicx,tikz,fancyhdr}
\usepackage{fontspec}
\usepackage{mathspec}
\usepackage{diagbox}
\usepackage[sf]{titlesec}
\usepackage{geometry}
\usepackage[inline]{enumitem}
\usepackage[draft=false,hidelinks]{hyperref}
\usepackage[round]{natbib}
\usepackage{multirow}
\usepackage{cleveref}

\graphicspath{{figures/}}

\makeatletter
\renewcommand\@pnumwidth{4ex}
\makeatother

\setmainfont[Ligatures = TeX]{Noto Serif}
\setmathsfont(Digits,Latin,Greek)[Ligatures = TeX]{Noto Serif}
\setmathsf[Ligatures = TeX]{Noto Sans}
\setsansfont[Ligatures = TeX]{Technika}

\newfontfamily{\logofont}[ItalicFont=*,BoldFont=*,BoldItalicFont=*]{LogoCVUT-Regular}

\usetikzlibrary{arrows,positioning,cd} 

%%% MACROS %%%
\newcommand*{\A}{\mathcal{A}} 
\newcommand*{\B}{\mathcal{B}} 
\newcommand*{\X}{\mathcal{X}} 
\newcommand*{\nn}{\mathbb{N}} 
\newcommand*{\zz}{\mathbb{Z}} 
\newcommand*{\qq}{\mathbb{Q}} 
\newcommand*{\rr}{\mathbb{R}} 
\newcommand*{\RR}{\rr} 
\newcommand*{\CC}{\cc} 
\newcommand*{\NN}{\nn} 
\newcommand*{\cc}{\mathbb{C}} 
\renewcommand*{\complement}{\mathbf{c}}
\renewcommand*{\emptyset}{\varnothing}
\newcommand{\defeq}{\stackrel{\text{def}}{=}}

\newcommand*{\stL}{\stackrel{L}{\to}}
\newcommand*{\stP}{\stackrel{P}{\to}}
\newcommand*{\stas}{\stackrel{a.s.}{\to}}
\newcommand*{\stUL}{\stackrel{UL}{\to}}
\newcommand*{\wh}{\widehat}
%
\newcommand*{\isom}{\cong} 
\newcommand*{\from}{\colon} 
\newcommand*{\id}{\mathrm{id}}  
\newcommand*{\ve}{\varepsilon}
\newcommand{\qmq}[1]{\quad\mbox{#1}\quad}
\newcommand{\bm}[1]{\ensuremath{\mathbf{#1}}}

\newcommand{\ol}{\overline}

%%% OPERATORS %%%
\newcommand*{\argmax}{\operatorname{argmax}}
\newcommand*{\E}{\mathop{\mathsf{E}{\kern0pt}}}
\newcommand*{\Var}{\mathop{\mathsf{Var}{\kern0pt}}}
\newcommand*{\Cov}{\mathop{\mathsf{Cov}{\kern0pt}}}
\newcommand*{\sd}{\mathop{\sigma{\kern0pt}}}
\renewcommand*{\P}{\mathop{\mathsf{P}\kern0pt}\nolimits}

%%% THEOREMS %%%

\frenchspacing
\raggedbottom


%%% BODY %%%
\begin{document}

\begin{center}
    \sffamily 

    \vspace{1em}

    Probability and Mathematical statistics\par
    \bigskip

    \LARGE
    Exercises -- week 3

    \vspace{2em}

\end{center}

The (+) symbols indicates optional exercises. If you run out of time for solving the exercises, they should be the ones you skip.

\section*{Conditional probability}

\begin{enumerate}[wide,label={\textbf{\arabic{*}.}},itemsep=1em]
    \item Three archers have an average accuracy of 4/5, 3/4 and 2/3 respectively. They take turn shooting at a target. Knowing that only two arrows hit the target, what is the probability that it was the third archer who missed? % [6/13]

    \item 10 women and 5 men are randomly divided into five groups of 3. Determine the probability that each group contains a man. % [81/1001]

    \item Five urns each contain $b$ white balls and $c$ black balls. A ball is drawn at random from the first urn and placed in the second. Then a ball is drawn at random from the second and placed in the third, and so on until a ball has been placed in the fifth urn. Finally, one ball is drawn from the fifth urn. What is the probability that this ball is white? % [$b/(b+c)$]

    \item Three urns contain blue, white and red balls where 
    \begin{itemize}
        \item the first contains 1 white, 2 black and 3 blue, 
        \item the second contains 2 white, 1 black and 1 blue,
        \item the third contains, 4 white, 5 black and 3 blue. 
    \end{itemize}
    An urn is chosen at random and then one ball is drawn from it. If that ball is white, what is the probability that the urn chosen was the first one? What about the second and third? % [$1/6$,$1/2$,$1/3$]

    \item A product has a 10\% chance of having a manufacturing defect. Defective products have a 50\% chance of failure during the warranty period, compared to 1\% chance of failure otherwise. Determine the probability that a product that experienced a failure during the warranty period was in fact defective. % [50/59]
    
    \item You want to buy a car of a certain brand, however you know that 30\% of this brand's cars have a faulty transmission. To inspect the car before purchasing it, you hire a mechanic who is able to estimate its condition with 99\% accuracy. What is the probability that the car you want to buy has a faulty transmission if the mechanic predicted that it doesn't?

    \item (+) Prove that if $A$ and $B$ are independent, then so are $A^\complement$ and $B^\complement$.

    \item Can disjoint events be independent?

    \item Let $\Omega = {1, 2, 3, 4, 5, 6, 7, 8}$ be a sample space where each outcome has the same probability. Let $A = \{1, 2, 3, 4\}$, $B = \{1, 2, 3, 5\}$ and $C = \{1, 6, 7, 8\}$. Are these events independent or pairwise independent?

    \item Draw at random one card from a standard deck of playing cards (52 cards, 4 suits, 13 ranks). Let $A$ be the event that this card is a queen and $B$ that it is a heart.
        \begin{enumerate*}[label={\alph{*}}]
            \item Are $A$ and $B$ independent?
            \item If we add two jokers to the pack, does the answer change (jokers are considered to have no suit)?
        \end{enumerate*}

    \item (+) When three coins are flipped, then surely \emph{at least} two of them must have the same side facing up (head or tail). Since there is an even chance of the third coin being a head or a tail, one could  argue that the probability that all three coins show the same side is 1/2. Do you agree with this answer? If not, what is the correct one? (This is known as Galton's paradox.)
\end{enumerate}

\section*{Random variables}

\begin{enumerate}[wide,label={\textbf{\arabic{*}.}},itemsep=1em]

\item Let $X$ be the random variable denoting  the largest number of \emph{consecutive} heads in three coin flips.
\begin{enumerate}[label={\alph{*})}]
    \item Determine the distribution function of this random variable if the coin is fair.
    \item Do the same under assumption that the probability of a head is given by some parameter $0\leq p\leq 1$.
\end{enumerate}

\item In the experiment which consists in rolling a fair dice until the result is a 6, let $X$ be the number of dice rolls needed.
\begin{enumerate}[label={\alph{*})}]
    \item Describe the distribution function $F_X$.
    \item Find $\P[X > 3]$.
    \item Find $\P[X > 7 \mid X > 3]$.
\end{enumerate}

\item (+) Let $X$ be a random variable with distribution function $F_X$. Prove that:
\begin{enumerate}[label={\alph{*})}]
    \item $\P[X > x] = 1 - F_X (x)$,
    \item $\P[x < X \leq y] = F_X (y) - F_X (x)$.
\end{enumerate}

\item (+) Prove that $\lim_{x\to a^-}F_X(x) = \P[x<a]$. Deduce that $a$ is a point of discontinuity of $F_X$ if and only if $\P[X=a]\neq 0$.

\item (+) Let $X$ be a random variable with distribution function $F_X$ and $Y = aX + b$ where $a \neq 0$. Express $F_Y$ in terms of $F_X$. Pay close attention to what happens when $a<0$.

\item Two players place bets on the result of a dice roll by choosing three numbers (out of six). A winning bet earns the player 100 CZK, while a loosing bet costs 100 CZK. The first player chooses to bet on even numbers (2, 4, 6) while the second one bets on large numbers (4, 5, 6). Let $X$ be the random variable which gives the outcome for the first player, and $Y$ the outcome for the second (i.e., the amount gained or lost).  
\begin{enumerate}[label={\alph{*})}]
    \item Determine the joint distribution $F_{X,Y}$.
    \item Determine the marginal distributions $F_X$ and $F_Y$.
    \item Are $X$ and $Y$ independent?
    \item Find the distribution of $X$ given $Y = 100$ and given $Y = -100$.
\end{enumerate}

\end{enumerate}

\end{document}
