% PRÉAMBULE -- Devoirs ---------------------------------------------------

\documentclass[11pt]{amsart}

\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[french]{babel}

\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{amsfonts}
\usepackage{amscd}
\usepackage{amssymb}
\usepackage{amstext}
\usepackage{hyperref}
\usepackage{epsfig}
\usepackage{subfiles}
\usepackage{tikz}

\usepackage{enumerate}
\usepackage{multicol}
\usepackage{bbm}

\usepackage[margin=1in]{geometry}

\usepackage{csquotes}
\usepackage{epigraph}

% Définition des environnements.
\newtheorem{theoreme}{Théorème}
\newtheorem*{theoreme*}{Théorème}
\newtheorem{proposition}{Proposition}
\newtheorem*{proposition*}{Proposition}
\newtheorem{lemme}{Lemme}
\newtheorem*{lemme*}{Lemme}
\newtheorem{corollaire}{Corollaire}
\newtheorem*{corollaire*}{Corollaire}
\newtheorem{conjecture}{Conjecture}
\newtheorem*{conjecture*}{Conjecture}

\theoremstyle{definition}
\newtheorem{definition}{Définition}
\newtheorem*{definition*}{Définition}
\newtheorem*{notation}{Notation}
\newtheorem{exercice}{Exercice}
\newtheorem*{exercice*}{Exercice}
\newtheorem{question}{Question}
\newtheorem*{question*}{Question}
\newtheorem{probleme}{Problème}
\newtheorem*{probleme*}{Problème}
\newtheorem*{solution}{Solution}

\theoremstyle{remark}
\newtheorem{exemple}{Exemple}
\newtheorem*{exemple*}{Exemple}
\newtheorem*{remarque}{Remarque}

% Notations utiles
\newcommand{\tq} {\text{t.q.}}

% Notations -- ensembles
\newcommand{\N} {\mathbb{N}}
\newcommand{\Z} {\mathbb{Z}}
\newcommand{\Q} {\mathbb{Q}}
\newcommand{\R} {\mathbb{R}}
\newcommand{\C} {\mathbb{C}}
\newcommand{\K} {\mathbb{K}}

\newcommand{\abs}[1]{\left\vert #1 \right\vert}
\newcommand{\card}[1]{\abs{#1}}

% Notation -- proba
\newcommand{\PR} {\mathbb P}
\newcommand{\ES} {\mathbb E}
\newcommand{\Var} {\mathrm{Var}}
\newcommand{\Cov} {\mathrm{Cov}}

% Notation -- délimiteurs :
\newcommand{\ac}[1]{\left\{ #1 \right\}} %accolades
\newcommand{\pr}[1]{\left( #1 \right)} %parenthèses
\newcommand{\ct}[1]{\left[ #1 \right]} %crochets
\newcommand{\norm}[1]{\left\Vert #1 \right\Vert} %norme
\newcommand{\floor}[1]{\left\lfloor #1 \right\rfloor} %partie entière
\newcommand{\ceil}[1]{\left\lceil #1 \right\rceil} %partie entière + 1
\newcommand{\cond}{\ \middle\vert \ }

% Notation -- événements :
\newcommand{\tevent}[1]{\ac{\text{#1}}}

% Différence symétrique.
\newcommand \dsym {\mathop{}\!\mathbin\bigtriangleup\mathop{}\!}

% FIN PRÉAMBULE ------------------------------------------------------

\title[MAT1720 -- H22 -- Devoir 4]{MAT1720 -- Introduction aux probabilités -- Hiver 2022\\ Devoir 4}

\begin{document}

	\maketitle
	\vspace{-5mm}
	\setlength\parindent{0pt}
	\rule{\textwidth}{.5pt}
	\begin{center}
		\em
		\begin{tabular}{rl}
			\textbf{Échéance : } & lundi le 21 mars 2022, à 13h 30 (au début du cours) \\
			\textbf{Remise en mains propres : } & Vous devez remettre un document typographié et imprimé. \\
								& Aucun devoir en retard ne sera accepté. \\
								& Aucun travail manuscrit ne sera accepté. \\
								& Aucune remise électronique ne sera acceptée. \\
			\textbf{Pondération : } & $3 \frac13 \%$.
		\end{tabular}
	\end{center}
	\rule{\textwidth}{.5pt}
	\setlength\parindent{18pt}
	
	
	\section*{Le processus de Poisson}
	
	Le compteur Geiger est un instrument de mesure de la radiation ionisante. Son principe de fonctionnement, imaginé au tournant du XXe siècle par le scientifique allemand Hans Geiger, repose sur le simple principe qu'un gaz inerte (comme l'hélium ou le néon) ne peut conduire l'électricité que lorsque celui-ci est ionisé -- c'est-à-dire lorsque des électrons sont arrachés aux noyaux atomiques et libres de circuler.
	
	Au centre du compteur Geiger, on trouve un tube de Geiger-Müller. C'est un tube de verre scellé, rempli d'un gaz inerte. Aux extrémités du tube, une anode et une cathode appliquent une forte différence de potentiel électrique. Lorsqu'un rayonnement ionisant arrache des électrons à leurs noyaux atomiques, une décharge électrique survient, et on peut mesurer un courant (figure \ref{fig:geiger}).
	
	\begin{figure}[h]
		\centering
		\includegraphics{geiger}
		\caption{L'atome d'hélium percuté par la particule ionisante se sépare en ions chargés qui circulent, ce qui crée un courant électrique mesurable par l'ampèremètre.}
		\label{fig:geiger}
	\end{figure}
	
	Si au lieu de mesurer un courant avec l'ampèremètre, on branchait plutôt un haut-parleur en série avec le tube de Geiger-Müller, on entendrait un \og clic \fg{} à chaque fois qu'une décharge électrique parcourrait le circuit -- soit à chaque fois qu'une particule de rayonnement ionisant percuterait le tube de Geiger-Müller.
	
	L'emploi de compteurs Geiger est encore très répandu pour mesurer la présence de radiation ambiente dangereuse pour la santé. Le principe est simple : on place un compteur Geiger dans une pièce, et on compte le nombre de \og clics \fg{} qu'on entend en un certain intervalle de temps. Plus les \og clics \fg{} sont fréquents, et plus il y a de radiation ionisante à l'endroit où le détecteur est placé.
	
	On s'intéresse à $N(t)$, le nombre de \og clics \fg{} en fonction du temps $t$ écoulé de puis la mise sous tension du compteur Geiger (donc $t \geq 0$). $N(t)$ est un \textit{processus stochastique} -- c'est une \og fonction aléatoire \fg{}. Pour tout $t \geq 0$, $N(t)$ est une variable aléatoire. Notre but sera d'étudier la distribution jointe de la famille de variables aléatoires $(N (t))_{t \geq 0}$.
	
	Pour ce faire, nous allons étudier deux suites de variables aléatoires reliées à $N(t)$. D'abord, nous noterons $C_n$ le temps où le $n$ième \og clic \fg{} survient. Nous noterons également $T_1 = C_1$ et $T_n = C_n - C_{n-1}$ pour tout $n > 1$. Les variables aléatoires $T_n$ sont appelées les \textit{temps d'interarrivée} des \og clics \fg{}, puisqu'ils mesurent les temps entre l'arrivée de deux \og clics \fg{} consécutifs. La figure \ref{fig:geiger_process} illustre une réalisation de ce processus stochastique, basée sur une demi-seconde d'enregistrement d'un compteur Geiger.
	
	\begin{figure}[h]
		\centering
		\includegraphics{geiger_process}
		\caption{Un enregistrement des clics émis par un compteur Geiger. En rouge, la fonction $N(t)$ associée. Celle-ci augmente de $1$ à chaque \og clic \fg{}. Le temps entre le $(n-1)$ième et le $n$ième clic est $T_n = C_n - C_{n-1}$, le $n$ième temps d'interarrivée.}
		\label{fig:geiger_process}
	\end{figure}
	
	\begin{question*}[\em 10 points \em]
		Comme les désintégrations de particules sont des phénomènes quantiques imprévisibles, on peut présumer que les variables aléatoires $T_n$ ont les propriétés suivantes :
		\begin{itemize}
			\item Les $T_n$ sont indépendantes et identiquement distribuées.
			\item Les $T_n$ ont la propriété d'absence de mémoire.
			\item On a que $\ES \ct{T_n} = \lambda^{-1}$ pour un certain $\lambda > 0$ et pour tous $n$.
		\end{itemize}
		
		\begin{enumerate}[(a)]
			\item \textit{(1 point)}
				Donner la distribution marginale des $T_n$ (c'est la même pour tout $n$).
				
			\item \textit{(2 points)}
				Montrer que $C_n$ suit une loi $\Gamma (\lambda, n)$ pour tout $n \in \N$.
			
			\item \textit{(2 points)}
				Montrer que les événements $\{ N(t) \leq n \}$ et $\{ C_{n+1} > t \}$ sont égaux.
				
			\item \textit{(3 points)}
				Calculer $\PR \ac{C_{n+1} \geq t}$. \\
				\textit{Indice :} Souvenez-vous que $(u + t)^{n} = \sum_{k=0}^{n} {n \choose k} t^{n-k} u^k.$
				
			\item \textit{(2 point)}
				En vous servant de votre réponse à la partie précédente, conclure que pour tous $t \geq 0$, $N(t)$ suit une loi marginale de Poisson de paramètre $\lambda t$.
		\end{enumerate}				
	\end{question*}
	
	\begin{remarque}
		Le processus $N(t)$ est un \textit{processus de Poisson}. Il s'agit d'un exemple d'une chaîne de Markov à temps continu -- un sujet qui est abordé dans le cours MAT2717 -- Processus stochastiques. En particulier, on a aussi la propriété que $N(t + h) - N(t) \perp N(t)$ pour tous $t, h \geq 0$. Donc, $N(b)-N(a)$ suit une distribution de Poisson de paramètre $\lambda(b-a)$ pour tout $0 \leq a < b$ -- c'est le nombre de clics entendus entre les temps $a$ et $b$ (incluant $b$ mais pas $a$).
	\end{remarque}
	
\end{document}