\documentclass[oneside,11pt]{amsart}

%%% PACKAGES %%%
\usepackage[svgnames]{xcolor}
\usepackage{amsmath,amssymb,amsthm,booktabs,csquotes,microtype,graphicx,tikz,fancyhdr}
\usepackage{fontspec}
\usepackage{mathspec}
\usepackage{diagbox}
\usepackage[sf]{titlesec}
\usepackage[width=94ex]{geometry}
\usepackage[inline]{enumitem}
\usepackage[draft=false,hidelinks]{hyperref}
\usepackage[round]{natbib}

\newcommand*{\uniform}{\mathop{\mathrm{U}\kern0pt}\nolimits}
\newcommand*{\exponential}{\mathop{\mathrm{Exp}\kern0pt}\nolimits}
\newcommand*{\poisson}{\mathop{\mathrm{Poi}\kern0pt}\nolimits}

\graphicspath{{figures/}}

\makeatletter
\renewcommand\@pnumwidth{4ex}
\makeatother

\setmainfont[Ligatures = TeX]{Noto Serif}
\setmathsfont(Digits,Latin,Greek)[Ligatures = TeX]{Noto Serif}
\setmathrm[Ligatures = TeX]{Noto Serif}
\setmathsf[Ligatures = TeX]{Noto Sans}
\setsansfont[Ligatures = TeX]{Technika}

\newfontfamily{\logofont}[ItalicFont=*,BoldFont=*,BoldItalicFont=*]{LogoCVUT-Regular}

\usetikzlibrary{arrows,positioning,cd} 

%%% MACROS %%%
\newcommand*{\A}{\mathcal{A}} 
\newcommand*{\B}{\mathcal{B}} 
\newcommand*{\X}{\mathcal{X}} 
\newcommand*{\nn}{\mathbb{N}} 
\newcommand*{\zz}{\mathbb{Z}} 
\newcommand*{\qq}{\mathbb{Q}} 
\newcommand*{\rr}{\mathbb{R}} 
\newcommand*{\RR}{\rr} 
\newcommand*{\CC}{\cc} 
\newcommand*{\NN}{\nn} 
\newcommand*{\cc}{\mathbb{C}} 
\renewcommand*{\complement}{\mathbf{c}}
\renewcommand*{\emptyset}{\varnothing}
\newcommand{\defeq}{\stackrel{\text{def}}{=}}

\newcommand*{\stL}{\stackrel{L}{\to}}
\newcommand*{\stP}{\stackrel{P}{\to}}
\newcommand*{\stas}{\stackrel{a.s.}{\to}}
\newcommand*{\stUL}{\stackrel{UL}{\to}}
\newcommand*{\wh}{\widehat}
%
\newcommand*{\isom}{\cong} 
\newcommand*{\from}{\colon} 
\newcommand*{\id}{\mathrm{id}}  
\newcommand*{\ve}{\varepsilon}
\newcommand{\qmq}[1]{\quad\mbox{#1}\quad}
\newcommand{\bm}[1]{\ensuremath{\mathbf{#1}}}
\newcommand{\ceil}{\operatorname{ceil}}

\newcommand{\ol}{\overline}

%%% OPERATORS %%%
\newcommand*{\argmax}{\operatorname{argmax}}
\newcommand*{\E}{\mathop{\mathsf{E}{\kern0pt}}}
\newcommand*{\Var}{\mathop{\mathsf{Var}{\kern0pt}}}
\newcommand*{\Cov}{\mathop{\mathsf{Cov}{\kern0pt}}}
\newcommand*{\sd}{\mathop{\sigma{\kern0pt}}}
\renewcommand*{\P}{\mathop{\mathsf{P}\kern0pt}\nolimits}

\newcommand*{\duniform}{\mathop{\mathrm{DU}\kern0pt}\nolimits}

%%% THEOREMS %%%

\frenchspacing

\begin{document}

\begin{center}
    \sffamily 

    \vspace{1em}

    Probability and Mathematical statistics -- Winter 2024/2025\par
    \bigskip

    \begin{LARGE}
        Homework 3
    \end{LARGE}

    \vspace{2em}

    
    Deadline: November 22, 2024
    \vspace{1em}
\end{center}

The goal of this homework is to illustrate the relationshion between exponential and gamma distributions using Geiger counters as a case study.
	
A Geiger counter is an instrument for measuring ionizing radiation. Its operating principle, devised at the turn of the 20th century by German scientist Hans Geiger, is based on the fact that an inert gas (such as helium or neon) can only conduct electricity when it is ionized, i.e.\ when electrons are torn from atomic nuclei and allowed to circulate freely.
	
At the center of the Geiger counter is a Geiger-Müller tube. This is a sealed glass tube filled with an inert gas. At the ends of the tube, an anode and a cathode apply a strong electrical potential difference. When ionizing radiations tear electrons from their atomic nuclei, an electrical discharge occurs, and a current can be measured using an ammeter.

\begin{figure}[ht]
    \includegraphics{geiger_diagram}
\end{figure}
	
In a Geiger counter, a loudspeaker is connected in series with a Geiger--Müller tube. As a result, a clicking sound will be emitted every time an electrical discharge passes through the circuit, or in other words, every time a particle of ionizing radiation strikes the Geiger-Müller tube.
	
Geiger counters can be used to measure the presence of dangerous ambient radiation: a Geiger counter is placed in a room, and the number of clicks heard in a given time interval is counted. The more frequent the clicks, the more ionizing radiation is present.
	
Let us study the function $N(t)$ which counts the number of clicks as a function of the time $t$ elapsed since the Geiger counter was switched on (so $t \geq 0$). For all $t \geq 0$, we view $N(t)$ as a random variable. For an illustration, take the following audio recording of a Geiger counter over 1 second:
\begin{figure}[ht]
    \centering
    \includegraphics[scale=.7]{geiger_img}
    \label{fig:geiger_process}
\end{figure}

Let $C_n$ be the time where the $n$th click is heard and $T_n=C_n-C_{n-1}$, with the edge case $T_1 = C_1$. We say that $T_n$ is an \emph{interarrival time}. Both $T_n$ and $C_n$ are continuous random variables. Moreover we suppose that the decay of individual particles are random events which occur independently. In our model, this means that the variables $T_n$ are assumed to be independent, identically distributed, and  memoryless. Recall that the memoryless property means that
\begin{equation*}
    \P[T_n>t+s\mid T_n>t] = \P[T_n>s].
\end{equation*}
	
\begin{enumerate}[label={\textbf{\arabic{*}.}},itemsep=1em,series=questions]
    \item Let $X$ be a memoryless continuous random variable which takes only positive values.
    \begin{enumerate}[label={\alph{*})}]
        \item Let $G_X(t) = 1-F_X(t)$. Prove that $G_X(s+t)=G_X(s)G_X(t)$.
        \item Prove that $G_X(\alpha t) = G_X(t)^\alpha$ for all $\alpha\in\mathbb{R}$. 
        \item Conclude that $X\sim\exponential(\lambda)$ where $\lambda = -1/\ln(G_X(1))$.
    \end{enumerate}
    [Hint: for b), prove the result step-by-step: first for integers, then for rational, and finally, using a limit argument, for all real numbers.]
\end{enumerate}

\bigskip

Therefore
\begin{equation*}
    T_n \sim \exponential(\lambda) = \Gamma(1,\lambda).
\end{equation*}

Gamma distributions have the property that if $X_i \sim \Gamma(k_i,\lambda)$ are independent random variables for $i=1,\dots,n$, then $\sum_{i=1}^nX_i \sim \Gamma(\sum_{i=1}^n{k_i},\lambda)$. Therefore,
\begin{equation*}
    C_n = \sum_{i=1}^nT_n \sim \Gamma(n,\lambda).
\end{equation*}

\bigskip

\begin{enumerate}[label={\textbf{\arabic{*}.}},itemsep=1em,wide,resume=questions]
    \item Prove that 
        \begin{equation*}
            \P[C_{n+1}>t] = \sum_{k=0}^ne^{-t/\lambda }\frac{ t^k}{\lambda^kk!}.
        \end{equation*}
        [Hint: use the binomial identity, $(a+b)^n = \sum_{i=0}^n\binom{i}{n}a^ib^{n-i}$.]

    \item Show that $\{ N(t) \leq n \}=\{ C_{n+1} > t \}$ and deduce that $N(t)\sim\poisson(t/\lambda)$. 

    \item The next figure shows an audio recording of a Geiger counter over 1.5~s:
        \begin{center}
            \includegraphics[scale=.7]{geiger_img_2}
        \end{center}
    The estimated clicking times for this recording are given in the table below.
    \bigskip
    \begin{center}\small
        \begin{tabular}{*{13}{c}}
            \toprule
            $n$ & 1 & 2 & 3 & 4 & 5 & 6 & 7 & 8 & 9 \\\midrule
            $C_n$ & 0.242 & 0.48 & 0.642 & 0.766 & 1.017 & 1.048 & 1.078 & 1.258 & 1.383 \\
            $T_n$ & 0.242 & 0.238 & 0.162 & 0.124 & 0.252 & 0.031 & 0.03 & 0.181 & 0.125 \\
            \bottomrule
        \end{tabular}
    \end{center}
    \bigskip
    Assuming that $N(1.5)\sim\poisson(9)$, estimate the probability that the variables $T_n$ stay between the values observed in the above recording. 
\end{enumerate}
	
\end{document}
