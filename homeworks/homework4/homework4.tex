\documentclass[oneside,11pt]{amsart}

%%% PACKAGES %%%
\usepackage[svgnames]{xcolor}
\usepackage{amsmath,amssymb,amsthm,booktabs,csquotes,microtype,graphicx,tikz,fancyhdr}
\usepackage{fontspec}
\usepackage{mathspec}
\usepackage{diagbox}
\usepackage[sf]{titlesec}
\usepackage[width=94ex]{geometry}
\usepackage[inline]{enumitem}
\usepackage[draft=false,hidelinks]{hyperref}
\usepackage[round]{natbib}

\newlist{exo}{enumerate}{10}
\setlist[exo]{label={\textbf{\arabic{*}.}},itemsep=1em,labelsep=*,leftmargin=\parindent}

\newcommand*{\uniform}{\mathop{\mathrm{U}\kern0pt}\nolimits}
\newcommand*{\normal}{\mathop{\mathrm{N}\kern0pt}\nolimits}
\newcommand*{\exponential}{\mathop{\mathrm{Exp}\kern0pt}\nolimits}
\newcommand*{\poisson}{\mathop{\mathrm{Poi}\kern0pt}\nolimits}

\graphicspath{{figures/}}

\makeatletter
\renewcommand\@pnumwidth{4ex}
\makeatother

\setmainfont[Ligatures = TeX]{Noto Serif}
\setmathsfont(Digits,Latin,Greek)[Ligatures = TeX]{Noto Serif}
\setmathrm[Ligatures = TeX]{Noto Serif}
\setmathsf[Ligatures = TeX]{Noto Sans}
\setsansfont[Ligatures = TeX]{Technika}

\newfontfamily{\logofont}[ItalicFont=*,BoldFont=*,BoldItalicFont=*]{LogoCVUT-Regular}

\usetikzlibrary{arrows,positioning,cd} 

%%% MACROS %%%
\newcommand*{\A}{\mathcal{A}} 
\newcommand*{\B}{\mathcal{B}} 
\newcommand*{\X}{\mathcal{X}} 
\newcommand*{\nn}{\mathbb{N}} 
\newcommand*{\zz}{\mathbb{Z}} 
\newcommand*{\qq}{\mathbb{Q}} 
\newcommand*{\rr}{\mathbb{R}} 
\newcommand*{\RR}{\rr} 
\newcommand*{\CC}{\cc} 
\newcommand*{\NN}{\nn} 
\newcommand*{\cc}{\mathbb{C}} 
\renewcommand*{\complement}{\mathbf{c}}
\renewcommand*{\emptyset}{\varnothing}
\newcommand{\defeq}{\stackrel{\text{def}}{=}}

\newcommand*{\stL}{\stackrel{L}{\to}}
\newcommand*{\stP}{\stackrel{P}{\to}}
\newcommand*{\stas}{\stackrel{a.s.}{\to}}
\newcommand*{\stUL}{\stackrel{UL}{\to}}
\newcommand*{\wh}{\widehat}
%
\newcommand*{\isom}{\cong} 
\newcommand*{\from}{\colon} 
\newcommand*{\id}{\mathrm{id}}  
\newcommand*{\ve}{\varepsilon}
\newcommand{\qmq}[1]{\quad\mbox{#1}\quad}
\newcommand{\bm}[1]{\ensuremath{\mathbf{#1}}}
\newcommand{\ceil}{\operatorname{ceil}}

\newcommand{\ol}{\overline}

%%% OPERATORS %%%
\newcommand*{\argmax}{\operatorname{argmax}}
\newcommand*{\E}{\mathop{\mathsf{E}{\kern0pt}}}
\newcommand*{\Var}{\mathop{\mathsf{Var}{\kern0pt}}}
\newcommand*{\Cov}{\mathop{\mathsf{Cov}{\kern0pt}}}
\newcommand*{\sd}{\mathop{\sigma{\kern0pt}}}
\renewcommand*{\P}{\mathop{\mathsf{P}\kern0pt}\nolimits}

\newcommand*{\duniform}{\mathop{\mathrm{DU}\kern0pt}\nolimits}

%%% THEOREMS %%%

\frenchspacing

\begin{document}

\begin{center}
    \sffamily 

    \vspace{1em}

    Probability and Mathematical statistics -- Winter 2024/2025\par
    \bigskip

    \begin{LARGE}
        Homework 4
    \end{LARGE}

    \vspace{2em}

    
    Deadline: December 4, 2024
    \vspace{1em}
\end{center}

The goal of this homework is to explore the properties of three distributions that we saw only briefly during class: chi-squared, $t$-distributions, and $F$-distributions.

\bigskip
Recall that we defined the chi-squared distribution with $n$ degrees of freedom by $\chi^2(n) = \Gamma(n/2,2)$, or more explicitly $X\sim\chi^2(n)$ if it has a density function:
\begin{equation*}
    f_X(x) = 
    \begin{cases}
        \frac1{2^{n/2}\Gamma(n/2)}  x^{n/2-1}e^{-x/2} & \text{if }x>0\\
        0 & \text{otherwise.}
    \end{cases}
\end{equation*}
\bigskip

\begin{exo}[series=questions]
    \item Let $Z_1,\dots,Z_n$ be independent random variables which follow a standard normal distribution. 
        \begin{enumerate}[label={\alph*)}]
            \item Show that $Z_i^2\sim\chi^2(1)$ by computing the distribution directly.
            \item Show that the MGF of a $\chi^2(n)$ satisfies $M(t) = (1-2t)^{-n/2}$ for $t<1/2$. 
            \item Deduce that $\sum_{i=1}^nZ_i^2\sim\chi^2(n)$. [Hint: the $Z_i^2$ are independent.]
            \item Determine the expectation, variance, and skewness of $\sum_{i=1}^nZ_i^2$.
        \end{enumerate}
\end{exo}

\bigskip
Let $X\sim \normal(0,1)$ and $Y\sim\chi^2(n)$ be two independent random variables. Recall that, by definition, the random variable $T = X/(Y/n)^{1/2}$ follows a $t$-distribution with $n$ degrees of freedom.
\bigskip

\begin{exo}[resume=questions]
    \item Determine the density function of $T$. Calculate its expectation when $n\geq 2$ and its variance when $n\geq 3$. [Hint: the $t$-distribution does not have an MGF, so you will need Theorem 6.45 from the notes.]
\end{exo}

\bigskip
Let $X\sim\chi^2(m)$ and $Y\sim\chi^2(n)$ be two independent random variables. By definition, the random variable $F = (X/m)/(Y/n)$ follows an $F$-distribution with $m$ and $n$ degrees of freedom.
\bigskip
\begin{exo}[resume=questions]
    \item Determine the density function of $F$. Calculate its expectation when $m=1$, $n\geq 3$. [Hint: for the first part you will again need Theorem 6.45. For the second part you can reuse the previous exercises.]
\end{exo}
	
\end{document}
