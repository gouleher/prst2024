\documentclass[oneside,11pt]{amsart}

%%% PACKAGES %%%
\usepackage[svgnames]{xcolor}
\usepackage{amsmath,amssymb,amsthm,booktabs,csquotes,microtype,graphicx,tikz,fancyhdr}
\usepackage{fontspec}
\usepackage{mathspec}
\usepackage{diagbox}
\usepackage[sf]{titlesec}
\usepackage[width=94ex]{geometry}
\usepackage[inline]{enumitem}
\usepackage[draft=false,hidelinks]{hyperref}
\usepackage[round]{natbib}

\newlist{exo}{enumerate}{10}
\setlist[exo]{label={\textbf{\arabic{*}.}},itemsep=1em,labelsep=*,leftmargin=\parindent}
\newenvironment{solution}{\begingroup\color{NavyBlue}\begin{proof}[Solution]}{\end{proof}\endgroup}

\newcommand*{\uniform}{\mathop{\mathrm{U}\kern0pt}\nolimits}
\newcommand*{\normal}{\mathop{\mathrm{N}\kern0pt}\nolimits}
\newcommand*{\exponential}{\mathop{\mathrm{Exp}\kern0pt}\nolimits}
\newcommand*{\poisson}{\mathop{\mathrm{Poi}\kern0pt}\nolimits}

\graphicspath{{figures/}}

\makeatletter
\renewcommand\@pnumwidth{4ex}
\makeatother

\setmainfont[Ligatures = TeX]{Noto Serif}
\setmathsfont(Digits,Latin,Greek)[Ligatures = TeX]{Noto Serif}
\setmathrm[Ligatures = TeX]{Noto Serif}
\setmathsf[Ligatures = TeX]{Noto Sans}
\setsansfont[Ligatures = TeX]{Technika}

\newfontfamily{\logofont}[ItalicFont=*,BoldFont=*,BoldItalicFont=*]{LogoCVUT-Regular}

\usetikzlibrary{arrows,positioning,cd} 

%%% MACROS %%%
\newcommand*{\A}{\mathcal{A}} 
\newcommand*{\B}{\mathcal{B}} 
\newcommand*{\X}{\mathcal{X}} 
\newcommand*{\nn}{\mathbb{N}} 
\newcommand*{\zz}{\mathbb{Z}} 
\newcommand*{\qq}{\mathbb{Q}} 
\newcommand*{\rr}{\mathbb{R}} 
\newcommand*{\RR}{\rr} 
\newcommand*{\CC}{\cc} 
\newcommand*{\NN}{\nn} 
\newcommand*{\cc}{\mathbb{C}} 
\renewcommand*{\complement}{\mathbf{c}}
\renewcommand*{\emptyset}{\varnothing}
\newcommand{\defeq}{\stackrel{\text{def}}{=}}

\newcommand*{\stL}{\stackrel{L}{\to}}
\newcommand*{\stP}{\stackrel{P}{\to}}
\newcommand*{\stas}{\stackrel{a.s.}{\to}}
\newcommand*{\stUL}{\stackrel{UL}{\to}}
\newcommand*{\wh}{\widehat}
%
\newcommand*{\isom}{\cong} 
\newcommand*{\from}{\colon} 
\newcommand*{\id}{\mathrm{id}}  
\newcommand*{\ve}{\varepsilon}
\newcommand{\qmq}[1]{\quad\mbox{#1}\quad}
\newcommand{\bm}[1]{\ensuremath{\mathbf{#1}}}
\newcommand{\ceil}{\operatorname{ceil}}

\newcommand{\ol}{\overline}

%%% OPERATORS %%%
\newcommand*{\argmax}{\operatorname{argmax}}
\newcommand*{\E}{\mathop{\mathsf{E}{\kern0pt}}}
\newcommand*{\Var}{\mathop{\mathsf{Var}{\kern0pt}}}
\newcommand*{\Cov}{\mathop{\mathsf{Cov}{\kern0pt}}}
\newcommand*{\sd}{\mathop{\sigma{\kern0pt}}}
\renewcommand*{\P}{\mathop{\mathsf{P}\kern0pt}\nolimits}

\newcommand*{\duniform}{\mathop{\mathrm{DU}\kern0pt}\nolimits}

%%% THEOREMS %%%

\frenchspacing
\raggedbottom

\begin{document}

\begin{center}
    \sffamily 

    \vspace{1em}

    Probability and Mathematical statistics -- Winter 2024/2025\par
    \bigskip

    \begin{LARGE}
        Homework 4
    \end{LARGE}

    \vspace{2em}

    
    Deadline: December 4, 2024
    \vspace{1em}
\end{center}

The goal of this homework is to explore the properties of three distributions that we saw only briefly during class: chi-squared, $t$-distributions, and $F$-distributions.

\bigskip
Recall that we defined the chi-squared distribution with $n$ degrees of freedom by $\chi^2(n) = \Gamma(n/2,2)$, or more explicitly $X\sim\chi^2(n)$ if it has a density function:
\begin{equation*}
    f_X(x) = 
    \begin{cases}
        \frac1{2^{n/2}\Gamma(n/2)}  x^{n/2-1}e^{-x/2} & \text{if }x>0\\
        0 & \text{otherwise.}
    \end{cases}
\end{equation*}
\bigskip

\begin{exo}[series=questions]
    \item Let $Z_1,\dots,Z_n$ be independent random variables which follow a standard normal distribution. 
        \begin{enumerate}[label={\alph*)}]
            \item Show that $Z_i^2\sim\chi^2(1)$ by computing the distribution directly. \label{prob1-a}
            \item Show that the MGF of a $\chi^2(n)$ satisfies $M(t) = (1-2t)^{-n/2}$ for $t<1/2$. \label{prob1-b}
            \item Deduce that $\sum_{i=1}^nZ_i^2\sim\chi^2(n)$. [Hint: the $Z_i^2$ are independent.] \label{prob1-c}
            \item Determine the expectation, variance, and skewness of $\sum_{i=1}^nZ_i^2$. \label{prob1-d}
        \end{enumerate}
\end{exo}

\begin{solution}
        \ref{prob1-a} Since $Z_i\sim\normal(0,1)$,
        \begin{align*}
            F_{Z_i^2}(x) = \P[Z_i^2\leq x] = \P[-\sqrt{x}\leq Z_i\leq \sqrt{x}] = \frac2{\sqrt{2\pi}}\int_0^{\sqrt{x}}e^{-y^2/2}\, dy = \frac1{\sqrt{2\pi}}\int_0^{x}t^{-1/2}e^{-t/2}\, dt
        \end{align*}
        where the last equality uses the change of variable $t=x^2$. This shows that $Z_i^2$ has the density function of a $\chi^2(1)$.

        \ref{prob1-b} Let $X\sim\chi^2(n)$. By definition of the MGF, assuming that $1-2t> 0$,
        \begin{align*}
            M_X(t) &= \frac1{2^{n/2}\Gamma(n/2)}\int_0^\infty x^{n/2-1}e^{-x(1-2t)/2}\, dx \\
                   &= \frac1{2^{n/2}\Gamma(n/2)}\int_0^\infty \left(\frac{2}{1-2t}u\right)^{n/2-1}e^{-u}\frac{2}{1-2t}\, du  & u = \frac{1-2t}2x\\
                   &= \frac1{\Gamma(n/2)}(1-2t)^{-n/2}\int_0^\infty u^{n/2-1}e^{-u}\,du\\
                   &= (1-2t)^{-n/2}
        \end{align*}
        Note that the condition $1-2t> 0$ was required because of the change of variable $u=\frac{1-2t}2x$.

        \ref{prob1-c} Let $X = \sum_{i=1}^nZ_i^2$. Since $Z_i^2\sim\chi^2(1)$ and they are independent
        \begin{equation*}
            M_X(t) = \prod_{i=1}^nM_{Z_i^2}(t) = \prod_{i=1}^n(1-2t)^{-1/2} = (1-2t)^{-1/2}
        \end{equation*}
        for $t<1/2$. Since $X$ has the same MGF as a $\chi^2(n)$ on an open interval around 0, we conclude that $X\sim\chi^2(n)$.

        \ref{prob1-d} Let again $X = \sum_{i=1}^nZ_i^2$. We can use the MGF to calculate the first three moments. First we calculate the derivatives of the MGF:
        \begin{align*}
            &\frac{d}{dt}(1-2t)^{-n/2} = n(1-2t)^{-n/2-1},\\
            &\frac{d^2}{dt^2}n(n+2)(1-2t)^{-n/2-2},\\
            &\frac{d^3}{dt^3}(1-2t)^{-n/2} = n(n+2)(n+4)(1-2t)^{-n/2-3}.
        \end{align*}
        The first three non-central moments are obtained by evaluating these there expressions at $t=0$,
        \begin{equation*}
            \mu'_1(X) = n,\quad \mu'_2(X) = n(n+2),\quad \mu'_3(X) = n(n+2)(n+4).
        \end{equation*}
        We can easily calculate the expectation and variance:
        \begin{equation*}
            \E[X] = \mu_1'(X) = n,\quad \Var(X) = \mu_2'(X) - \mu_1'(X)^2 = 2n.
        \end{equation*}
        The skewness is given by
        \begin{align*}
            \frac{\mu_3(X)}{\Var(X)^{3/2}} &= \frac1{\sqrt{8n^3}}(\mu_3'(X)-3\mu_2'(X)\mu_1'(X) + 2\mu'_1(X)^3) \\
                                           &= \frac1{\sqrt{8n^3}}(n(n+2)(n+4)-3n^2(n+2) + 2n^3) \\
                                           &= \frac{8n}{\sqrt{8n^3}} = \sqrt{\frac8{n}}.
        \end{align*}
\end{solution}

\clearpage
Let $X\sim \normal(0,1)$ and $Y\sim\chi^2(n)$ be two independent random variables. Recall that, by definition, the random variable $T = X/(Y/n)^{1/2}$ follows a $t$-distribution with $n$ degrees of freedom.
\bigskip

\begin{exo}[resume=questions]
    \item Determine the density function of $T$. Calculate its expectation when $n\geq 2$ and its variance when $n\geq 3$. [Hint: the $t$-distribution does not have an MGF, so you will need Theorem 6.45 from the notes.]
\end{exo}

\begin{solution}
    Set $h(x,y) = (x/\sqrt{y/n},y)$. The inverse function is $h^{-1}(x,y) = (x\sqrt{y/n}, y)$. The Jacobian determinant is
    \begin{equation*}
        J = \left|\begin{matrix}\sqrt{y/n} & 0 \\ x/(2\sqrt{y/n}) & 1\end{matrix}\right| = \sqrt{y/n}.
    \end{equation*}
    By definition of $T$
    \begin{equation*}
        f_T(x) = \int_{-\infty}^{\infty} f_{h(X,Y)}(x,y)\, dy
    \end{equation*}
    By theorem 6.45
    \begin{align*}
        f_{h(X,Y)}(x,y) &= f_{X,Y}(h^{-1}(x,y))|J| \\
                        &= f_{X,Y}(x\sqrt{y/n}, y)\sqrt{y/n} \\
                        &= f_{X}(x\sqrt{y/n})f_Y(y)\sqrt{y/n} \\
                        &= \left(\frac{1}{\sqrt{2\pi}}e^{-\frac{x^2y}{2n}}\right)\left(\frac{1}{2^{n/2}\Gamma(\frac{n}2)}y^{n/2-1}e^{-y/2}\right)\sqrt{y/n}\\
                        &= \frac{1}{n^{1/2}\pi^{1/2}2^{\frac{n+1}2}\Gamma(\frac{n}2)}\,y^{\frac{n-1}2}e^{-y(\frac{x^2/n+1}2)},
    \end{align*}
    for $y>0$ and $x\in\rr$ (otherwise the density is 0). Therefore
    \begin{align*}
        f_T(x) &= \frac{1}{n^{1/2}\pi^{1/2}2^{\frac{n+1}2}\Gamma(\frac{n}2)}\int_{0}^{\infty}y^{\frac{n-1}2}e^{-y(\frac{x^2/n+1}2)}\, dy \\
               &= \frac{1}{n^{1/2}\pi^{1/2}2^{\frac{n+1}2}\Gamma(\frac{n}2)}\int_{0}^{\infty}\left(\frac{2t}{x^2/n+1}\right)^{\frac{n-1}2}e^{-t} \frac2{x^2/n+1} \, dy & t=y(\tfrac{x^2/n+1}2) \\
               &= \frac{1}{n^{1/2}\pi^{1/2}\Gamma(\frac{n}2)}(x^2/n+1)^{\frac{n+1}2}\int_{0}^{\infty}t^{\frac{n+1}2 - 1}e^{-t} \, dy  \\
               &= \frac{\Gamma(\frac{n+1}2)}{n^{1/2}\pi^{1/2}\Gamma(\frac{n}2)}(x^2/n+1)^{\frac{n+1}2}.
    \end{align*}

    To calculate the expectation, notice that $X$ and $(Y/n)^{-1/2}$ are independent, therefore
    \begin{equation*}
        \E[T] = \E[X]\E[(Y/n)^{-1/2}] = 0\cdot\sqrt{n}\cdot\E[Y^{-1/2}] = 0,
    \end{equation*}
    provided that $\E[Y^{-1/2}]$ exists. By the law of the unconscious statistician,
    \begin{equation*}
        \E[Y^{-1/2}] 
        = \frac1{2^{n/2}\Gamma(\frac{n}2)}\int_0^\infty y^{-\frac12}y^{n/2-1}e^{-y/2}\, dy 
        = \frac{2^{(n-1)/2}}{2^{n/2}\Gamma(\frac{n}2)}\int_0^\infty t^{(n-3)/2}e^{-t}\, dt 
        = \frac{\Gamma(\frac{n-1}2)}{\sqrt2\Gamma(\frac{n}2)} 
    \end{equation*}
    Therefore the expectation of $T$ is 0.

    We can use a similar strategy for the variance.
    \begin{equation*}
        \Var(T) = \E[T^2] = n\E[X^2]\E[Y^{-1}] = n \E[Y^{-1}],
    \end{equation*}
    where in the first equality we used the fact that $\E[T]=0$. Then we calculate $\E[Y^{-1}]$ using the law of the unconscious statistician.
    \begin{equation*}
        \E[Y^{-1/2}] 
        = \frac1{2^{n/2}\Gamma(\frac{n}2)}\int_0^\infty y^{n/2-2}e^{-y/2}\, dy 
        = \frac{2^{n/2-1}}{2^{n/2}\Gamma(\frac{n}2)}\int_0^\infty t^{n/2-2}e^{-t}\, dt 
        = \frac{\Gamma(\frac{n}2-1)}{2\Gamma(\frac{n}2)} = \frac1{n-2}.
    \end{equation*}
    Therefore $\Var(T) = \frac{n}{n-2}$.
\end{solution}

\clearpage
Let $X\sim\chi^2(m)$ and $Y\sim\chi^2(n)$ be two independent random variables. By definition, the random variable $F = (X/m)/(Y/n)$ follows an $F$-distribution with $m$ and $n$ degrees of freedom.
\bigskip
\begin{exo}[resume=questions]
    \item Determine the density function of $F$. Calculate its expectation when $m=1$, $n\geq 3$. [Hint: for the first part you will again need Theorem 6.45. For the second part you can reuse the previous exercises.]
\end{exo}

\begin{solution}
    The first part of the solution is similar to the previous problem. Set $h(x,y) = ((x/m)/(y/n),y)$. The inverse function is $h^{-1}(x,y) = (mxy/n, y)$. The Jacobian determinant is
    \begin{equation*}
        J = \left|\begin{matrix}my/n & 0 \\ mx/n & 1\end{matrix}\right| = my/n.
    \end{equation*}
    By theorem 6.45, for $x,y>0$,
    \begin{align*}
        f_{h(X,Y)}(x,y) &= f_{X,Y}(h^{-1}(x,y))|J| \\
                        &= f_{X,Y}(mxy/n, y)my/n \\
                        &= f_{X}(mxy/n)f_Y(y)my/n \\
                        &= \left(\frac{1}{2^{m/2}\Gamma(\frac{m}2)}(mxy/n)^{m/2-1}e^{-\frac{mxy}{2n}}\right)\left(\frac{1}{2^{n/2}\Gamma(\frac{n}2)}y^{n/2-1}e^{-y/2}\right)my/n\\
                        &= \frac{m^{m/2}}{n^{m/2}2^{\frac{m+n}2}\Gamma(\frac{n}2)\Gamma(\frac{m}2)}\,x^{m/2-1}y^{\frac{m+n}2-1}e^{-y(\frac{mx+n}{2n})},
    \end{align*}
    for $x,y>0$ (otherwise the density is 0). Then we need to calculate the marginal density:
    \begin{align*}
        f_F(x) &= \frac{m^{\frac{m}2}}{n^{\frac{m}2}2^{\frac{m+n}2}\Gamma(\frac{n}2)\Gamma(\frac{m}2)}\,x^{\frac{m}2-1}\int_0^\infty y^{\frac{m+n}2-1}e^{-y(\frac{mx+n}{2n})}\,dy \\
               &= \frac{m^{\frac{m}2}}{n^{\frac{m}2}2^{\frac{m+n}2}\Gamma(\frac{n}2)\Gamma(\frac{m}2)}\,x^{\frac{m}2-1}\int_0^\infty \left(\tfrac{2n}{mx+n}\right)^{\frac{m+n}2}t^{\frac{m+n}2-1}e^{-t}\,dt & t=y(\tfrac{mx+n}{2n})\\
               &= \frac{m^{\frac{m}2}}{n^{\frac{n}2}\Gamma(\frac{n}2)\Gamma(\frac{m}2)}\cdot \frac{x^{\frac{m}2-1}}{(mx+n)^{\frac{m+n}2}}\int_0^\infty t^{\frac{m+n}2-1}e^{-t}\,dt\\
               &= \frac{m^{\frac{m}2}}{n^{\frac{n}2}}\cdot\frac{\Gamma(\frac{m+n}2)}{\Gamma(\frac{n}2)\Gamma(\frac{m}2)}\cdot\frac{x^{\frac{m}2-1}}{(mx+n)^{\frac{m+n}2}},
    \end{align*}
    for $x>0$ (and 0 otherwise).
    
    To calculate the expectation of $F$ when $m=1$ and $n\geq 3$, we can reuse the answer from the second problem. In this case $F = X/(Y/n)$ where $X\sim\chi^2(1)$ and $Y\sim\chi^2(n)$. By the first problem $X = Z^2$ where $Z\sim\normal(0,1)$. Therefore $F = T^2$ where $T = Z/\sqrt{Y/n} \sim t(n)$. So $\E[F] = \E[T^2] = \frac{n}{n-2}$.
\end{solution}
	
\end{document}
