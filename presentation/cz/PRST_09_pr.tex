
\section{Absolutně spojité náhodné veličiny}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{PRST 9 (přednáška), 23.10.2023  }

{\large\bf 6. Absolutně spojité náhodné veličiny}\\
\medskip

\begin{define}
Řekneme, že náhodná veličina $X$ má \alert{absolutně spojité rozdělení
(ASR)}, jestliže existuje nezáporná reálná funkce $f_X(x)$ taková,
že pro každé $x\in\RR$ platí
$$
F_X(x) = \int_{-\infty}^x f_X(t)\, dt\, .
$$
Funkci $f_X(x)$ nazýváme \alert{(pravděpodobnostní) hustota} náhodné
veličiny $X$.
\end{define}

\begin{ex}
Mějme náhodnou veličinu $X$ zadanou pomocí své distribuční funkce. Má $X$  ASR?\\[2mm]
{\scriptsize
$
F_X(x)=
  \begin{cases}
    0 & \text{pro } x\leq 0 , \\
    \frac{x}{2} & \text{pro } 0<x\leq 2,\\
    1 &\text{pro } x\geq 2.
  \end{cases}
$
}\\

\vskip 5mm

\
\end{ex}

\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\begin{veta}
Nechť má náhodná veličina $X$ absolutně spojité rozdělení. Potom
ve všech bodech, kde existuje derivace distribuční funkce $F_X$
platí
$$
f_X(x) = F_X^\prime (x) = \frac{dF_X}{dx}(x)\, .
$$
\Dk\\

\vskip 0.5cm

\
\end{veta}

\begin{veta}
Nechť má náhodná veličina $X$ absolutně spojité rozdělení. Potom
pro každou množinu $A\in \B(\RR)$ platí
$$
\P[X\in A] = \int_A f_X(x)\, dx\, .
$$
\Dk\\

\vskip 1.2cm

\
\end{veta}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\begin{rem}
 Analogie s diskrétními veličinami, pro
které platí
\alert{
$
\P[X\in A] = \sum_{x_j\in A} \P[X=x_j]\, .
$}
\end{rem}

\begin{rem}
 Má-li náhodná veličina $X$ ASR, platí
$$
\P[X=a] = 0, \qquad \forall a \in\RR\, ,
$$
{\color{gray}( $\int_a^a f_X(x)\, dx =0$ nebo
$\P[X=a] = F_X(a)-\lim_{x\to a_-} F_X(x) = 0$, protože $F_X$ je
spojitá)}\\[2mm]

 To tedy znamená, že
$$
\P[X<a] = \P[X\leq a]
\quad
\mbox{a}
\quad
\P[a<X<b]= \P[a\leq X<b]=\P[a<X\leq b] = \P[a\leq X \leq b]\, .
$$
\end{rem}
\medskip

{\bf Základní vlastnosti hustoty pravděpodobnosti:}
$$
\mbox{a)}\quad \alert{f_X(x) \geq 0} \ \forall x\in\RR \qquad
  \mbox{b)} \quad \alert{\int_{-\infty}^\infty f_X(x)\, dx = 1}\, .
$$


\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\vspace*{-3mm}
\begin{ex}
Náhodná veličina $X$ má hustotu
$
f_X(x) = \displaystyle\frac{a}{1+x^2}\, , \  x\in\RR\, .
$
Určete konstantu $a$, $F_X(x)$ a $\P[-1<X<1]$.
\vskip 4.5cm

\
\end{ex}


\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\begin{define}
Říkáme, že náhodné veličiny $X_1,X_2,\ldots,X_n$ mají \alert{sdružené
absolutně spojité rozdělení (SASR)}, jestliže existuje nezáporná
funkce $f_{X_1,\ldots,X_n}(x_1,\ldots,x_n)$ taková, že
$$
F_{X_1,\ldots,X_n}(x_1,\ldots,x_n)=\int_{-\infty}^{x_1}
\int_{-\infty}^{x_2}\ldots \int_{-\infty}^{x_n}
f_{X_1,\ldots,X_n}(t_1,\ldots,t_n)\, dt_1\ldots dt_n
$$
pro všechny $n$-tice $(x_1,\ldots,x_n)\in\RR^n$. Funkci
$f_{X_1,\ldots,X_n}$ nazýváme \alert{sdružená hustota pravděpodobnosti}
náhodných veličin $X_1,X_2,\ldots,X_n$.
\end{define}
\begin{rem}
Podobně jako u jedné náhodné veličiny platí (v bodech kde derivace existuje)
\begin{itemize}
  \item[a)] $$f_{X_1,\ldots,X_n}(x_1,\ldots,x_n)=\frac{\partial^n
  F_{X_1,\ldots,X_n}(x_1,\ldots,x_n)}{\partial x_1\ldots \partial
  x_n}\, ,$$
  \item[b)] $\P[a_1<X_1\leq b_1,a_2<X_2\leq b_2,\ldots , a_n<X_n\leq
  b_n]= \int_{a_1}^{b_1}
\int_{a_2}^{b_2}\ldots \int_{a_n}^{b_n}
f_{X_1,\ldots,X_n}(x_1,\ldots,x_n)\, dx_1\ldots dx_n\, .$
 \end{itemize}
\end{rem}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\begin{veta}
Mají-li náhodné veličiny $X_1,X_2,X_3$ SASR, potom také $X_1,X_3$
mají SASR a platí
$$
f_{X_1,X_3}(x_1,x_3) = \int_{-\infty}^\infty
f_{X_1,X_2,X_3}(x_1,x_2,x_3)\, dx_2\, .
$$
\end{veta}
\begin{rem}
 Opět analogické k diskrétnímu případu, kde
\alert{$
\P[X_1=x_1]=\sum_{x_2}\P[X_1=x_1,X_2=x_2]\, .
$
}
\end{rem}
\begin{proof}
\vskip 3cm

\end{proof}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\begin{ex}
Náhodné veličiny $X,Y$ jsou zadány pomocí sdružené hustoty
pravděpodobnosti
$$
f_{X,Y}(x,y) =
  \begin{cases}
    e^{-(x+y)} & \text{pro } \min\{x,y\}\geq 0, \\
   0 & \text{jinak}.
  \end{cases}
$$
Určete marginální hustotu $f_Y(y)$ a $\P[X>2Y]$.
\end{ex}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\begin{veta}
Nechť náhodné veličiny $X_1,\ldots,X_n$ mají SASR. Potom
$$X_1,\ldots,X_n \mbox{ jsou nezávislé}\quad\Longleftrightarrow\quad
f_{X_1,\ldots,X_n}(x_1,\ldots, x_n) = \prod_{j=1}^n
f_{X_j}(x_j)\quad \forall (x_1,\ldots,x_n)\in\RR^n\, .
$$
\end{veta}
\begin{proof}
\vskip 4cm

\end{proof}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\begin{define}
Nechť $X,Y$ jsou náhodné veličiny. Potom \alert{podmíněnou distribuční
funkci} náhodné veličiny $X$ při dané hodnotě $Y=y$ definujeme jako
$$
F_{X|Y}(x|y) = \lim_{\varepsilon\to 0_+} \P[X\leq x \,|\, y-\varepsilon<Y\leq
y+\varepsilon]
$$
za předpokladu, že limita existuje. Pokud navíc existuje funkce
$f_{X|Y}(x|y)\geq 0$ taková, že
$$
F_{X|Y}(x|y) = \int_{-\infty}^x f_{X|Y}(t|y) \,dt\qquad \forall
x\in\RR\, ,
$$
nazýváme ji \alert{podmíněnou hustotou} náhodné veličiny $X$ za podmínky
$Y=y$.
\end{define}
\begin{veta}
Nechť náhodné veličiny $X,Y$ mají SASR. Potom v každém bodě
$(x,y)\in\RR^2$, kde je \\[2mm]
 {\color{rblue} a)} $f_{X,Y}(x,y)$ spojitá,\qquad
   {\color{rblue} b)} $f_Y(y)$ spojitá a $f_Y(y)>0$, \qquad
existuje podmíněná hustota náhodné\\[2mm] veličiny $X$ za podmínky $Y=y$
a platí
\alert{
$
f_{X|Y}(x|y) = \displaystyle\frac{f_{X,Y}(x,y)}{f_Y(y)}\, .
$
}
\end{veta}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{6.1 Funkce náhodných veličin}
\vspace*{-3mm}
V řadě problémů nás zajímá rozdělení pravděpodobnosti
$Y=h(X)$, pokud známe rozdělení $X$.
\vspace*{-2mm}
\begin{ex}
$X$ je diskrétní náhodná veličina zadaná tabulkou\\[2mm]
\begin{tabular}{c|ccccc}
    $x_i$ & 0 & $\frac{\pi}{4}$ & $\frac{\pi}{2}$ & $\frac{3\pi}{4}$ & $\pi$ \\ \hline
  $p_i$ & 0.1 & 0.3 & 0.2 & 0.1 & 0.3 \\
\end{tabular}\\
\bigskip
\bigskip

Určete rozdělení náhodné veličiny $Y=\sin X$.
\end{ex}
\vspace*{-3mm}
\begin{rem}
\begin{itemize}
\item transformace může rozdělení náh. veličiny hodně změnit,
dokonce ze spojité se může stát diskrétní (ne naopak)
\item pro diskrétní n. v. se dá nové rozdělení získat přímým
výpočtem $\quad\Rightarrow\quad$ \alert{omezíme se na spojité n. v.}
\item ukážeme dvě metody:\\[1mm]
\begin{itemize}
  \item[a)] výpočet distribuční funkce transformované náhodné veličiny\\[2mm]
  \item[b)] vzorec pro přímý výpočet hustoty transformované náhodné
  veličiny.
\end{itemize}
\end{itemize}
\end{rem}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}


{\color{rblue} Ad a)}
\vskip 1cm

\begin{ex}
Náhodná veličina $X$ je dána hustotou pravděpodobnosti
$
f_X(x)=
  \begin{cases}
    \frac{1}{5} & \text{pro } x\in\langle -2,3\rangle, \\
    0 & \text{jinak}.
  \end{cases}
$
\\
Najděte hustotu pravděpodobnosti náhodné veličiny $Y=X^2$.
\end{ex}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\begin{veta}
Nechť $X$ je spojitá náhodná veličina, \alert{$h:\RR\mapsto\RR$ je ryze
monotónní funkce} na množině $X(\Omega)$ a \alert{$h^{-1}$ je
diferencovatelná}. Potom náhodná veličina $Y=h(X)$ má hustotu
$$
f_Y(y) = f_X\left(h^{-1}(y)\right)\cdot
\left|\frac{dh^{-1}(y)}{dy}\right|\, .
$$
\end{veta}
\begin{proof}
\vskip 4cm
\end{proof}
\end{frame}







